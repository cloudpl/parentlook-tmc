package com.akshara.parentlook.db.dao;

import java.awt.AlphaComposite;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.poi.hpsf.Thumbnail;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.DataException;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;

import com.akshara.parentlook.db.bean.AttendenceBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.CmsBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.bean.SchoolNotifications;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.service.ApprovePickup;
import com.generic.mss.utils.HibernateUtils;
import com.mysql.jdbc.log.Log;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.sun.org.apache.regexp.internal.recompile;

import net.coobird.thumbnailator.Thumbnailator;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.makers.FixedSizeThumbnailMaker;

public class SmsBeanDao {
	private static final Logger LOG = Logger.getLogger(SmsBeanDao.class);

	public static int saveSchoolsDetails(SchoolsDetailsBean schoolsDetailsBean, File profilePhoto) {

		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			if (profilePhoto != null) {

				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(profilePhoto));
				byte[] imagebytes = getBytesFromFile(profilePhoto);
				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(imagebytes);

				String strType = b.encodeToString(encodingImgAsBytes);

				byte[] valueDecoded = Base64.decodeBase64(strType);
				String decodedImgStr = new String(valueDecoded);

				schoolsDetailsBean.setPhoto(imageBlob);
				schoolsDetailsBean.setImageStr(decodedImgStr);

			}
			schoolsDetailsBean.setStatus(true);

			i = (Integer) session.save(schoolsDetailsBean);
			 session.flush();
             session.clear();
			if (i > 0) {
			}
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		return i;
	}

	public static int saveBusesDetails(BusesBean busesBean) {
		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			busesBean.setDropStatus("0");
			busesBean.setPickup("0");
			busesBean.setActivities("0");
			busesBean.setRecordStatus("ACTIVE");
			i = (Integer) session.save(busesBean);
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		return i;
	}

	public static int saveStudentClassDetails(ClassBean classBean) {

		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			classBean.setRecordStatus("ACTIVE");

			i = (Integer) session.save(classBean);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			session.close();
		}

		return i;
	}

	public static int saveWayPoint(WayPointsBean wayPointsBean) {
		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			i = (Integer) session.save(wayPointsBean);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
		
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	public static List<SchoolsDetailsBean> getSchoolDetailsList() {
		List<SchoolsDetailsBean> list = new ArrayList<SchoolsDetailsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from SchoolsDetailsBean where Status='1'").list();
			 session.flush();
             session.clear();
             transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static byte[] getBytesFromFile(File file) throws IOException {

		InputStream is = new FileInputStream(file);

		long length = file.length();

		if (length > Integer.MAX_VALUE) {

		}

		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}

	public static List<ClassBean> getClasesDetailsList(int b) {
		List<ClassBean> list = new ArrayList<ClassBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from ClassBean where SchoolId=" + b).list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
					 
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<BusesBean> getBusDetailsList(int a) {
		List<BusesBean> list = new ArrayList<BusesBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from BusesBean where SchoolId=" + a + " and RecordStatus='ACTIVE'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static int saveDriverDetails(DriversBean driversBean, File photoFile, File idattchment,
			File licenceattachment, int schoolid) {
		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			if (photoFile != null && idattchment != null && licenceattachment != null) {
				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photoFile));

				Blob idBlob = Hibernate.createBlob(getBytesFromFile(idattchment));
				Blob licenceBlob = Hibernate.createBlob(getBytesFromFile(licenceattachment));
				driversBean.setIdTypeAttachment(idBlob);
				driversBean.setLicenceAttachment(licenceBlob);
				driversBean.setPhoto(imageBlob);
				driversBean.setSchoolId(schoolid);
				driversBean.setRecordStatus("ACTIVE");
				i = (Integer) session.save(driversBean);
				 session.flush();
	             session.clear();
				
			}
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	public static List<DriversBean> getDriverDetailsList(int a) {
		List<DriversBean> list = new ArrayList<DriversBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from DriversBean where SchoolId=" + a).list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static ParentsBean saveParentsDetails(ParentsBean parentsBean, File photoFile) {

		Session session = null;
		ParentsBean parentsbean = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			if (photoFile != null) {

				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photoFile));

				FileInputStream fis = new FileInputStream(photoFile);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
					for (int readNum; (readNum = fis.read(buf)) != -1;) {
						bos.write(buf, 0, readNum);
					}
				} catch (IOException ex) {

				}
				byte[] bytes = bos.toByteArray();
				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(bytes);

				String strType = b.encodeToString(encodingImgAsBytes);

				byte[] valueDecoded = Base64.decodeBase64(strType);
				String decodedImgStr = new String(valueDecoded);
				parentsBean.setImageStr(decodedImgStr);
				parentsBean.setPhoto(imageBlob);
				parentsBean.setRecordStatus("ACTIVE");
				parentsBean.setProfilePicture(decodedImgStr);
				parentsbean = (ParentsBean) session.save(parentsBean);
				 session.flush();
	             session.clear();
			
			}
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return parentsbean;
	}

	public static List<ParentsBean> getParentsList(int a) {
		List<ParentsBean> list = new ArrayList<ParentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from ParentsBean where SchoolId=" + a + " and RecordStatus='ACTIVE'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static int saveStudentDetails(StudentsBean studentsBean, File photoFile, int c, int firstName) {
		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			studentsBean.setTrackedStatus("3");

			if (photoFile != null) {

				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photoFile));
				int scaledWidth = 100;
				int scaledHeight = 100;
				boolean preserveAlpha = true;
				BufferedImage bufferedImage2 = ImageIO.read(photoFile);
				BufferedImage outputBuffer = createResizedCopy(bufferedImage2, scaledWidth, scaledHeight,
						preserveAlpha);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(outputBuffer, "jpg", baos);
				byte[] imageInByte = baos.toByteArray();
				byte[] imagebytes = getBytesFromFile(photoFile);
				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(imageInByte);

				String strType = b.encodeToString(encodingImgAsBytes);

				byte[] valueDecoded = Base64.decodeBase64(strType);
				String decodedImgStr = new String(valueDecoded);
				studentsBean.setPhoto(imageBlob);
				studentsBean.setParentId(firstName);
				studentsBean.setDropPoint("");
				studentsBean.setRecordStatus("ACTIVE");
				studentsBean.setUId(c);
				studentsBean.setThumbnailPhoto(imageInByte);
				studentsBean.setImageStr(decodedImgStr);
				studentsBean.setProfilePicture(decodedImgStr);
			} else {
				studentsBean.setDropPoint("");
				studentsBean.setParentId(firstName);
				studentsBean.setRecordStatus("ACTIVE");
				studentsBean.setUId(c);
			}
			i = (Integer) session.save(studentsBean);
			if (i > 0) {
			}
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	private static BufferedImage createResizedCopy(BufferedImage bufferedimg, int scaledWidth, int scaledHeight,
			boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) {
			g.setComposite(AlphaComposite.Src);
		}
		g.drawImage(bufferedimg, 0, 0, scaledWidth, scaledHeight, null);
		g.dispose();
		return scaledBI;
	}

	public static List<StudentsBean> getStudentDetailsList(int a) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where SchoolId=" + a + "and RecordStatus='ACTIVE'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<StudentsBean> getStudentDetailsListByPickupPointId(int a) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where PickupPointId=" + a).list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<RoutesBean> getRoutesList(int a) {

		List<RoutesBean> routes = new ArrayList<RoutesBean>();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			routes = session.createQuery("from RoutesBean where SchoolId=" + a + " and RecordStatus='ACTIVE'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return routes;
	}

	public static int saveRoute(int schoolId, String startPoint, String endPoint, String routeName) {
		int i = 0;
		Session session = null;
		RoutesBean routesBean = new RoutesBean();
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			routesBean.setEndPoint(endPoint);
			routesBean.setSchoolId(schoolId);
			routesBean.setRouteName(routeName);
			routesBean.setStartPoint(startPoint);
			routesBean.setRecordStatus("ACTIVE");
			i = (Integer) session.save(routesBean);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	public static SchoolsDetailsBean getSchoolDetails(int i) {
		SchoolsDetailsBean a = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			a = (SchoolsDetailsBean) session.createQuery("from SchoolsDetailsBean where SchoolId=" + i).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return a;
	}

	public static List<StudentsBean> getProgerssStudentsList(String direction, int schoolId) {
		List<StudentsBean> beans = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = null;

			if (direction.equals("1")) {

				query = "from StudentsBean  where SchoolId=" + schoolId + " and TrackedStatus='2' and DropPointId=0";
			} else {
				query = "from StudentsBean where SchoolId=" + schoolId + " and TrackedStatus='2' and PickupPointId=0";

			}

			beans = (List<StudentsBean>) session.createQuery(query).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return beans;
	}

	public static SchoolsDetailsBean getSchoolName(int schoolId) {
		SchoolsDetailsBean bean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			bean = (SchoolsDetailsBean) session.createQuery("from SchoolsDetailsBean where SchoolId=" + schoolId)
					.uniqueResult();
			 session.flush();
             session.clear();
			
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return bean;
	}

	public static List<RoutesBean> getRootsForSchool(int schoolId) {
		List<RoutesBean> beans = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();

			Transaction transaction = session.beginTransaction();

			String query = "from RoutesBean where SchoolId=" + schoolId + " and RecordStatus='ACTIVE'";
			beans = (List<RoutesBean>) session.createQuery(query).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return beans;
	}

	public static List<BusesBean> getBussesForRoot(int rootId) {
		List<BusesBean> beans = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from BusesBean where RouteId=" + rootId + " and RecordStatus='ACTIVE'";
			beans = (List<BusesBean>) session.createQuery(query).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return beans;
	}

	public static List<PickupPoints> getPickupPointsForBus(int busId, String direction) {
		List<PickupPoints> beans = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from PickupPoints where BusId=" + busId + " and Direction='" + direction + "'";
			beans = (List<PickupPoints>) session.createQuery(query).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return beans;
	}

	public static PickupPoints getPickupPointDetails(int pickupPointId) {

		PickupPoints pickupPoints = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from PickupPoints where pickupPointId=" + pickupPointId;
			pickupPoints = (PickupPoints) session.createQuery(query).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return pickupPoints;
	}

	public static StudentsBean updatePickupPointForStudent(int studentId, PickupPoints points) {

		StudentsBean studentsBean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from StudentsBean where StudentId=" + studentId;
			studentsBean = (StudentsBean) session.createQuery(query).uniqueResult();

			studentsBean.setBusId(points.getBusId());
			studentsBean.setPickupAlert("1");
			studentsBean.setDropAlert("1");
			studentsBean.setDistance("5,5");
			studentsBean.setPickupPoint(points.getPickupPoint());
			studentsBean.setPickupPointId(points.getPickupPointId());

			if (studentsBean.getPickupPointId() != 0 && studentsBean.getDropPointId() != 0) {
				studentsBean.setTrackedStatus("1");
			}
			int si = 0;
			try {
				si = (int) session.createSQLQuery("select Max(notificationId) from Notifications").uniqueResult();
			} catch (Exception e) {
				LOG.error(e);
			}
			int ss = si + 1;

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String d = format.format(date);

			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = new Date();
			String d1 = format1.format(date1);

			session.createSQLQuery("insert into Notifications values('" + ss + "','"
					+ studentsBean.getStudentFirstName() + " Pickpoint Approved By Admin" + "','"
					+ studentsBean.getStudentId() + "','Pickup','NO','" + d + "','StopPoints','" + d1 + "')")
					.executeUpdate();
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e);
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return studentsBean;
	}

	public static WayPointsBean getWayPoint(int studentId) {
		WayPointsBean wayPointsBean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();

			Transaction transaction = session.beginTransaction();

			String query = "from WayPointsBean where StudentId=" + studentId;
			wayPointsBean = (WayPointsBean) session.createQuery(query).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return wayPointsBean;
	}

	public static int addPickupPoint(int schoolId, String pickupPoint, int routeId, int busId, String pLatLang,
			String direction) {
		PickupPoints pickupPoints = new PickupPoints();
		int i = 0;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			pickupPoints.setRouteId(routeId);
			pickupPoints.setPLatLang(pLatLang);
			pickupPoints.setSchoolId(schoolId);
			pickupPoints.setBusId(busId);
			pickupPoints.setPickupPoint(pickupPoint);
			pickupPoints.setDirection(direction);
			i = (Integer) session.save(pickupPoints);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	public static List<StudentsBean> getStudentsForBus(int i) {
		List<StudentsBean> studentsBeans = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			studentsBeans = session.createQuery("from StudentsBean where BusId=" + i).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return studentsBeans;
	}

	public static List<RoutesBean> getRouteIdList(int i) {
		List<RoutesBean> rootList = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			rootList = session.createQuery("from RoutesBean where SchoolId=" + i).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return rootList;

	}

	public static List<BusesBean> getBusRootList(int routeId) {
		List<BusesBean> list = new ArrayList<BusesBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from BusesBean where RouteId=" + routeId + " and RecordStatus='ACTIVE'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;

	}

	public static ParentsBean editParent(int id1) {
		Session session = null;
		ParentsBean parentBean = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			parentBean = (ParentsBean) session.createQuery("from ParentsBean where ParentId=" + id1).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return parentBean;
	}

	public static void updateParentDetails(ParentsBean parentsBean, int a, File photoFile) {
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			ParentsBean bean1 = (ParentsBean) session.createQuery("from ParentsBean where ParentId=" + a)
					.uniqueResult();
			if (photoFile != null) {

				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photoFile));
				FileInputStream fis = new FileInputStream(photoFile);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
					for (int readNum; (readNum = fis.read(buf)) != -1;) {
						bos.write(buf, 0, readNum);
					}
				} catch (IOException ex) {

				}
				byte[] bytes = bos.toByteArray();
				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(bytes);

				String strType = b.encodeToString(encodingImgAsBytes);

				byte[] valueDecoded = Base64.decodeBase64(strType);
				String decodedImgStr = new String(valueDecoded);
				bean1.setFirstName(parentsBean.getFirstName());
				bean1.setLastName(parentsBean.getLastName());
				bean1.setImageStr(decodedImgStr);
				bean1.setAddress(parentsBean.getAddress());
				bean1.setPhoto(imageBlob);
				bean1.setEmail(parentsBean.getEmail());
				bean1.setMobileNumber(parentsBean.getMobileNumber());
				if (bean1.getImageStr() != null) {
					bean1.setProfilePicture(decodedImgStr);
				} else {
					bean1.setImageStr(bean1.getProfilePicture());
				}

				session.saveOrUpdate(bean1);
			} else {
				bean1.setFirstName(parentsBean.getFirstName());
				bean1.setLastName(parentsBean.getLastName());
				bean1.setImageStr(bean1.getProfilePicture());
				bean1.setAddress(parentsBean.getAddress());
				bean1.setEmail(parentsBean.getEmail());
				bean1.setMobileNumber(parentsBean.getMobileNumber());

				session.saveOrUpdate(bean1);

			}
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

	}

	public static ParentsBean viewparent(int id3) {
		ParentsBean bean1 = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean1 = (ParentsBean) session.get(ParentsBean.class, id3);
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return bean1;
	}

	public static void deleteParent(int id2) {
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			ParentsBean bean = (ParentsBean) session.get(ParentsBean.class, id2);
			Query query = session
					.createQuery("UPDATE ParentsBean SET RecordStatus = 'INACTIVE' WHERE  ParentId=" + id2);

			query.executeUpdate();

			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

	}

	public static SchoolsDetailsBean editSchool(int idd) {
		SchoolsDetailsBean bean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			bean = (SchoolsDetailsBean) session.createQuery("from SchoolsDetailsBean where SchoolId=" + idd)
					.uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return bean;

	}

	public static SchoolsDetailsBean viewSchoolById(int id2) {

		SchoolsDetailsBean bean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			bean = (SchoolsDetailsBean) session.get(SchoolsDetailsBean.class, id2);

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return bean;

	}

	public static void updateSchool(SchoolsDetailsBean schoolsDetailsBean, int idd) {
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			SchoolsDetailsBean e = (SchoolsDetailsBean) session.get(SchoolsDetailsBean.class,idd);

			e.setSchoolName(schoolsDetailsBean.getSchoolName());
			e.setContactNumber(schoolsDetailsBean.getContactNumber());
			e.setAddress(schoolsDetailsBean.getAddress());
			e.setEmail(schoolsDetailsBean.getEmail());
            e.setMapLocation(schoolsDetailsBean.getMapLocation());
            e.setMapLocationLatLang(schoolsDetailsBean.getMapLocationLatLang());
			session.saveOrUpdate(e);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
	}

	public static void deleteSchool(int id1) {

		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			SchoolsDetailsBean e = (SchoolsDetailsBean) session.get(SchoolsDetailsBean.class, id1);
			Query query = session.createQuery("UPDATE SchoolsDetailsBean SET Status = 0 WHERE  SchoolId=" + id1);

			query.executeUpdate();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
	}

	public static boolean deleteclass(int classid) {
		ClassBean classbean = new ClassBean();
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			classbean = (ClassBean) session.get(ClassBean.class, classid);
			classbean.setRecordStatus("INACTIVE");
			session.saveOrUpdate(classbean);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public static ClassBean getClassesDetailsbyId(int classid) {
		ClassBean classbean = new ClassBean();
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			classbean = (ClassBean) session.createQuery("from ClassBean where ClassId=" + classid).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return classbean;
	}

	public static boolean updateClassdetails(ClassBean classbean) {
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			ClassBean classbean1 = (ClassBean) session
					.createQuery("from ClassBean where ClassId=" + classbean.getClassId()).uniqueResult();
			classbean1.setSchoolId(classbean.getSchoolId());
			classbean1.setStudentClass(classbean.getStudentClass());
			classbean1.setSection(classbean.getSection());
			classbean1.setDescription(classbean.getDescription());
			classbean1.setRecordStatus("ACTIVE");
			session.saveOrUpdate(classbean1);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return true;

	}

	public static StudentsBean viewstudent(int id1) {

		StudentsBean studentbean = new StudentsBean();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			studentbean = (StudentsBean) session.get(StudentsBean.class, id1);

			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return studentbean;
	}

	public static StudentsBean editStudent(int id2) {

		StudentsBean studentbean = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			studentbean = (StudentsBean) session.createQuery("from StudentsBean where StudentId=" + id2).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return studentbean;
	}

	public static void updateStudent(StudentsBean studentbean, File photoFile) throws IOException {

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			StudentsBean e = (StudentsBean) session.get(StudentsBean.class, studentbean.getStudentId());
			if (photoFile != null) {

				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photoFile));
				int scaledWidth = 100;
				int scaledHeight = 100;
				boolean preserveAlpha = true;
				BufferedImage bufferedImage2 = ImageIO.read(photoFile);
				BufferedImage outputBuffer = createResizedCopy(bufferedImage2, scaledWidth, scaledHeight,
						preserveAlpha);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(outputBuffer, "jpg", baos);
				byte[] imageInByte = baos.toByteArray();

				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(imageInByte);

				String strType = b.encodeToString(encodingImgAsBytes);

				byte[] valueDecoded = Base64.decodeBase64(strType);
				String decodedImgStr = new String(valueDecoded);
				e.setImageStr(decodedImgStr);
				e.setPhoto(imageBlob);
				e.setThumbnailPhoto(imageInByte);
				if (e.getImageStr() != null) {
					e.setProfilePicture(decodedImgStr);
				} else {
					e.setImageStr(e.getProfilePicture());
				}

			}

			e.setStudentFirstName(studentbean.getStudentFirstName());
			e.setStudentLastName(studentbean.getStudentLastName());
			e.setRollNo(studentbean.getRollNo());
			e.setParentId(studentbean.getParentId());
			e.setImageStr(e.getProfilePicture());
			e.setClassId(studentbean.getClassId());

			session.saveOrUpdate(e);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

	}

	public static void delete(int id3) {

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			StudentsBean e = (StudentsBean) session.get(StudentsBean.class, id3);
			session.delete(e);
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
	}

	public static ParentsBean getParentDetailsFromSchoolId(int schoolId) {

		ParentsBean parentsbean = new ParentsBean();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			parentsbean = (ParentsBean) session.createQuery("from ParentsBean where SchoolId=" + schoolId)
					.uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return parentsbean;
	}

	public static GPSTrackingBean getgpsdetails(int busId) {
		Session session = null;
		GPSTrackingBean gpstrackingbeans = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			String query = "from GpsTrackingBean where BusId=" + busId;
			gpstrackingbeans = (GPSTrackingBean) session.createQuery("from GPSTrackingBean where BusId=" + busId)
					.uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return gpstrackingbeans;
	}

	public static void updateGpsDemo(int i, String string, String string2) {

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			GPSTrackingBean e = (GPSTrackingBean) session.get(GPSTrackingBean.class, i);
			e.setGeoLat(string);
			e.setGeoLang(string2);
			session.saveOrUpdate(e);
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
	}

	public static List<HistoryLogBean> getBusHistory(int busId) {
		List<HistoryLogBean> historyLogBeans = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			historyLogBeans = session.createQuery("from HistoryLogBean where BusId=" + busId).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return historyLogBeans;
	}

	public static int getBusStatusPic(int busId, String string) {

		Session session = null;
		int i = 0;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat timeFormat = new SimpleDateFormat("hh-mm-ss a");

			String date = dateFormat.format(new Date());
			HistoryLogBean pickup = null;
			HistoryLogBean pickend = null;
			try {
				pickup = (HistoryLogBean) session
						.createQuery("from HistoryLogBean where BusId=" + busId
								+ " and Type='Start Point' and Direction='" + string + "' and Date='" + date + "'")
						.uniqueResult();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				pickend = (HistoryLogBean) session
						.createQuery("from HistoryLogBean where BusId=" + busId
								+ " and Type='School Point' and Direction='" + string + "' and Date='" + date + "'")
						.uniqueResult();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (pickup != null) {
				i = 1;
			}
			if (pickend != null) {
				i = 2;
			}
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	public static int getBusStatusDrop(int busId, String string) {
		Session session = null;
		int i = 0;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat timeFormat = new SimpleDateFormat("hh-mm-ss a");

			String date = dateFormat.format(new Date());
			HistoryLogBean pickup = (HistoryLogBean) session.createQuery("from HistoryLogBean where BusId=" + busId
					+ " and Type='School Point' and Direction='" + string + "' and Date='" + date + "'").uniqueResult();

			HistoryLogBean pickend = (HistoryLogBean) session.createQuery("from HistoryLogBean where BusId=" + busId
					+ " and Type='Start Point' and Direction='" + string + "' and Date='" + date + "'").uniqueResult();
			if (pickup != null) {
				i = 1;
			}
			if (pickend != null) {
				i = 2;
			}
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	public static StudentsBean updateDropPointForStudent(int studentId, PickupPoints points) {
		StudentsBean studentsBean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from StudentsBean where StudentId=" + studentId;
			studentsBean = (StudentsBean) session.createQuery(query).uniqueResult();

			studentsBean.setBusId(points.getBusId());
			studentsBean.setPickupAlert("1");
			studentsBean.setDropAlert("1");
			studentsBean.setDistance("5,5");
			studentsBean.setDropPoint(points.getPickupPoint());
			studentsBean.setDropPointId(points.getPickupPointId());

			if (studentsBean.getPickupPointId() != 0 && studentsBean.getDropPointId() != 0) {
				studentsBean.setTrackedStatus("1");
			}
			int si = 0;
			try {
				si = (int) session.createSQLQuery("select Max(notificationId) from Notifications").uniqueResult();
			} catch (Exception e) {
				LOG.error(e);
			}
			int ss = si + 1;
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String dd = format.format(date);
			SimpleDateFormat format11 = new SimpleDateFormat("yyyy-MM-dd");
			Date date11 = new Date();
			String dd1 = format11.format(date11);
			session.createSQLQuery("insert into Notifications values('" + ss + "','"
					+ studentsBean.getStudentFirstName() + " DropPoint Approved By Admin" + "','"
					+ studentsBean.getStudentId() + "','Drop','NO','" + dd + "','StopPoints','" + dd1 + "')")
					.executeUpdate();

			session.saveOrUpdate(studentsBean);

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return studentsBean;
	}

	public static BusesBean getBusDetails(int busId) {
		BusesBean busesBean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from BusesBean where BusId=" + busId;
			busesBean = (BusesBean) session.createQuery(query).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return busesBean;
	}

	@SuppressWarnings("unchecked")
	public static List<SchoolAdminsBean> getAdminList() {
		List<SchoolAdminsBean> list = new ArrayList<SchoolAdminsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from SchoolAdminsBean").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static SchoolsDetailsBean editSchoolProfile(int a) {
		SchoolsDetailsBean bean = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean = (SchoolsDetailsBean) session.createQuery("from SchoolsDetailsBean where SchoolId=" + a)
					.uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return bean;
	}

	public static int updateSchoolProfile(SchoolsDetailsBean schoolBean, File photo, int a) {
		int i = 0;
		Session session = null;

		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			SchoolsDetailsBean bean1 = (SchoolsDetailsBean) session
					.createQuery("from SchoolsDetailsBean where SchoolId=" + a).uniqueResult();
			bean1.setSchoolName(schoolBean.getSchoolName());

			if (photo != null) {

				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photo));
				byte[] imagebytes = getBytesFromFile(photo);

				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(imagebytes);

				String strType = b.encodeToString(encodingImgAsBytes);

				byte[] valueDecoded = Base64.decodeBase64(strType);
				String decodedImgStr = new String(valueDecoded);
				bean1.setImageStr(decodedImgStr);

				bean1.setPhoto(imageBlob);

			}
			session.saveOrUpdate((bean1));
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;
	}

	public static List<StudentsBean> getStudentDetailsListByTrackedStatus(int a, String status, String recordstatus) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where TrackedStatus='" + status + "' and SchoolId=" + a
					+ " and RecordStatus='" + recordstatus + "' order by StudentFirstName").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static BusesBean getBusId(int busId) {

		BusesBean bean = new BusesBean();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean = (BusesBean) session.createQuery("from BusesBean where  BusId=" + busId).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return bean;
	}

	public static RoutesBean getRoute(int routeid) {
		RoutesBean bean = new RoutesBean();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean = (RoutesBean) session.createQuery("from RoutesBean where  RouteId=" + routeid).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{session.close();}
			
		

		return bean;
	}

	public static ParentsBean getParentDetails(int parentId) {
		ParentsBean bean = new ParentsBean();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean = (ParentsBean) session.createQuery("from ParentsBean where  ParentId=" + parentId).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return bean;
	}

	public static AttendenceBean getstudentattendence(int studentId, String format, String direction) {
		AttendenceBean atendencebean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from AttendenceBean where StudentId=" + studentId + " and LogDate='" + format
					+ "' and Direction='" + direction + "'";
			atendencebean = (AttendenceBean) session.createQuery(query).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return atendencebean;

	}

	public static RoutesBean getRouteDetails(int routeId) {

		RoutesBean routesBean = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from RoutesBean where RouteId=" + routeId;
			routesBean = (RoutesBean) session.createQuery(query).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return routesBean;
	}

	public static DriversBean getDriverDetailsbyID(int driverid) {
		DriversBean driversBean = new DriversBean();

		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			driversBean = (DriversBean) session
					.createQuery("from DriversBean where DriverId=" + driverid + " and RecordStatus='ACTIVE'")
					.uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return driversBean;
	}

	public static DriversBean editDriverDetailsByID(int a) {
		DriversBean driversBean = new DriversBean();

		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			driversBean = (DriversBean) session.createQuery("from DriversBean where schoolId=" + a).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return driversBean;
	}

	public static boolean updateDriverDetails(DriversBean driversbean, File photoFile, File idattchment,
			File licenceattachment, int a) {

		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			DriversBean driversbean1 = (DriversBean) session.get(DriversBean.class, driversbean.getDriverId());
			if (photoFile != null) {
				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photoFile));

				driversbean1.setPhoto(imageBlob);
			}

			if (idattchment != null) {
				Blob idBlob = Hibernate.createBlob(getBytesFromFile(idattchment));
				driversbean1.setIdTypeAttachment(idBlob);

			}

			if (licenceattachment != null) {
				Blob licenceBlob = Hibernate.createBlob(getBytesFromFile(licenceattachment));
				driversbean1.setLicenceAttachment(licenceBlob);
			}

			driversbean1.setDriverId(driversbean.getDriverId());
			driversbean1.setFirstName(driversbean.getFirstName());
			driversbean1.setLastName(driversbean.getLastName());

			driversbean1.setContactNumber(driversbean.getContactNumber());
			driversbean1.setAge(driversbean.getAge());
			driversbean1.setRelationTitle(driversbean.getRelationTitle());
			driversbean1.setRelativeName(driversbean.getRelativeName());
			driversbean1.setLicenceNumber(driversbean.getLicenceNumber());
			driversbean1.setIdType(driversbean.getIdType());
			driversbean1.setRecordStatus("ACTIVE");

			driversbean1.setCurrentAddress(driversbean.getCurrentAddress());
			driversbean1.setSchoolId(a);
			session.saveOrUpdate(driversbean1);

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public static boolean deleteDriver(int driverid) {

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			DriversBean driversBean = (DriversBean) session.get(DriversBean.class, driverid);
			driversBean.setRecordStatus("INACTIVE");
			session.saveOrUpdate(driversBean);

			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public static List gethistorydetails(int busId, String format) {

		List<HistoryLogBean> historyLogBeans = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			historyLogBeans = session
					.createQuery("from HistoryLogBean where BusId=" + busId + " and Date='" + format + "'").list();
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return historyLogBeans;
	}

	public static List gethistorydetailsbytype(int busId, String format, String type) {

		List<HistoryLogBean> historyLogBeans = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			historyLogBeans = session.createQuery("from HistoryLogBean where BusId=" + busId + " and Date='" + format
					+ "' and PickupPointId>0 and Direction='" + type + "'").list();
			 session.flush();
             session.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return historyLogBeans;
	}

	public static BusesBean getStudentId(int busid) {
		BusesBean list = new BusesBean();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			list = (BusesBean) session.createQuery("from BusesBean where BusId=" + busid).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;

	}

	public static RoutesBean getRouteId(int routeId) {
		RoutesBean list = new RoutesBean();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			list = (RoutesBean) session.createQuery("from RoutesBean where RouteId=" + routeId).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static ParentsBean getParentid(int parentId) {
		ParentsBean list = new ParentsBean();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			list = (ParentsBean) session.createQuery("from ParentsBean where ParentId =" + parentId).uniqueResult();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static void deleteStudent(int id3) {

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			StudentsBean e = (StudentsBean) session.get(StudentsBean.class, id3);
			e.setRecordStatus("INACTIVE");
			session.saveOrUpdate(e);
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
	}

	public static StudentsBean getStudentDetails(int studentid) {
		StudentsBean studentsBean = new StudentsBean();

		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			studentsBean = (StudentsBean) session.createQuery("from StudentsBean where StudentId=" + studentid)
					.uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return studentsBean;

	}

	public static ClassBean getclassbystudentid(int classid) {

		ClassBean classbean = new ClassBean();

		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			classbean = (ClassBean) session.createQuery("from ClassBean where ClassId=" + classid).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return classbean;
	}

	public static boolean updatebusdetails(BusesBean busbean1) {
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			BusesBean busbean = (BusesBean) session.createQuery("from BusesBean where BusId=" + busbean1.getBusId())
					.uniqueResult();

			busbean.setRouteId(busbean1.getRouteId());
			busbean.setRegDate(busbean1.getRegDate());
			busbean.setRegNumber(busbean1.getRegNumber());
			busbean.setOccupency(busbean1.getOccupency());
			busbean.setEngineNumber(busbean1.getEngineNumber());
			busbean.setChasisNumber(busbean1.getChasisNumber());
			busbean.setDriverId(busbean1.getDriverId());
			busbean.setRegValidity(busbean1.getRegValidity());
			busbean.setReturnStartTime(busbean1.getReturnStartTime());
			busbean.setTransportStartTime(busbean1.getTransportStartTime());
			busbean.setSchoolArraivalTime(busbean1.getSchoolArraivalTime());
			busbean.setReturnArrivalTime(busbean1.getReturnArrivalTime());
			busbean.setRecordStatus("ACTIVE");
			busbean.setInsurenceValidity(busbean1.getInsurenceValidity());
			busbean.setSchoolId(busbean1.getSchoolId());
			busbean.setBusName(busbean1.getBusName());
			busbean.setDescription(busbean1.getDescription());
			busbean.setStartPointLatLang(busbean1.getStartPointLatLang());
			busbean.setStartPointLocation(busbean1.getStartPointLocation());

			session.saveOrUpdate(busbean);

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return true;

	}

	public static boolean deletebus(int busid) {

		BusesBean bean = new BusesBean();
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean = (BusesBean) session.get(BusesBean.class, busid);
			Query query = session.createQuery("update  BusesBean set RecordStatus='INACTIVE'  WHERE  BusId=" + busid);
			query.executeUpdate();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public static List<StudentsBean> getStudentList(int busid) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where BusId=" + busid).list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static RoutesBean editRoute(int routeid) {
		RoutesBean routebean = new RoutesBean();

		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			routebean = (RoutesBean) session.createQuery("from RoutesBean where RouteId=" + routeid).uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return routebean;
	}

	public static boolean updateRoute(RoutesBean routebean) {
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			RoutesBean routesbean = (RoutesBean) session
					.createQuery("from RoutesBean where RouteId=" + routebean.getRouteId()).uniqueResult();
			routesbean.setRouteName(routebean.getRouteName());
			routesbean.setStartPoint(routebean.getStartPoint());
			routesbean.setEndPoint(routebean.getEndPoint());
			session.saveOrUpdate(routesbean);

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return true;

	}

	public static List<BusesBean> getBusList(int routeid) {
		List<BusesBean> list = new ArrayList<BusesBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from BusesBean where RouteId=" + routeid + " and RecordStatus='ACTIVE'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static void updateusedStatus(int parentid, String msg) {

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			ParentsBean parentsBean = (ParentsBean) session.get(ParentsBean.class, parentid);
			if (msg == "ACTIVE") {
				parentsBean.setUsedStatus("ACTIVE");
				session.saveOrUpdate(parentsBean);
			} else {
				if (msg == "INACTIVE") {
					parentsBean.setUsedStatus("INACTIVE");
					session.saveOrUpdate(parentsBean);

				}
			}
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
	}

	public static boolean deleteroute(int routeid) {
		RoutesBean bean = new RoutesBean();
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean = (RoutesBean) session.get(RoutesBean.class, routeid);
			Query query = session
					.createQuery("update  RoutesBean set RecordStatus='INACTIVE'  WHERE  RouteId=" + routeid);
			query.executeUpdate();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public static List<BusesBean> getbuseslistbydriverid(int driverid) {
		List<BusesBean> list = new ArrayList<BusesBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from BusesBean where DriverId=" + driverid + " and RecordStatus='ACTIVE'")
					.list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;

	}

	public static void driverusedStatus(int driverId, String msg) {
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			DriversBean driversbean = (DriversBean) session.get(DriversBean.class, driverId);
			if (msg == "ACTIVE") {
				driversbean.setUsedStatus("ACTIVE");
				session.saveOrUpdate(driversbean);
			} else {
				if (msg == "INACTIVE") {
					driversbean.setUsedStatus("INACTIVE");
					session.saveOrUpdate(driversbean);

				}
			}

			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

	}

	public static List<StudentsBean> getStudentbyparentid(int id2) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where ParentId=" + id2 + " and RecordStatus='ACTIVE'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;

	}

	public static List<StudentsBean> getStudentDetailsbystudentid(int id3) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where StudentId=" + id3 + "and TrackedStatus='1'").list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;

	}

	public static List<StudentsBean> getStudentbyclass(int classid) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where ClassId=" + classid + " and RecordStatus='ACTIVE'")
					.list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;

	}

	public static List<StudentsBean> getStudents(int schoolId, int classId, int rollno) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where ClassId=" + classId + " and SchoolId=" + schoolId
					+ " and RollNo=" + rollno).list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static int totalCount(SchoolNotifications bean, int a) {
		Session session = null;
		int i = 0;
		try {
			String query = null;
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			query = "select count(*) from SchoolNotifications where SchoolId=" + a + " and ReadStatus='FALSE'";
			i = ((Long) session.createQuery(query).uniqueResult()).intValue();

			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return i;
	}

	public static List<SchoolNotifications> notificationList(int a) {
		List<SchoolNotifications> list = null;
		Session session = null;
		String query;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			query = "from SchoolNotifications where SchoolId=" + a + " order by Id DESC";
			list = session.createQuery(query).list();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<SchoolNotifications> recentNotification(int a) {
		List<SchoolNotifications> list = null;
		Session session = null;
		String query;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			query = "from SchoolNotifications where SchoolId=" + a + " and ReadStatus='FALSE'";

			list = (List<SchoolNotifications>) session.createQuery(query).setMaxResults(5).list();
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;

	}

	public static SchoolNotifications setReadStatusForNotifications(int notifyId) {
		SchoolNotifications notifications = new SchoolNotifications();
		Session session = null;
		String query;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			query = "from SchoolNotifications where Id=" + notifyId;

			notifications = (SchoolNotifications) session.createQuery(query).uniqueResult();
			notifications.setReadStatus("TRUE");
			session.saveOrUpdate(notifications);
			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return notifications;
	}

	public static List getUserGcmId(StudentsBean studentsBean) {
		Session session = null;
		String query = "";
		List<String> gcmIds = new ArrayList<>();
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			gcmIds = session.createSQLQuery("select GcmId from DeviceInfo where UId=" + studentsBean.getUId()).list();
			 session.flush();
             session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return gcmIds;
	}

	public static ParentsBean checkPhoneNumber(int a, String phonenumber) {
		Session session = null;
		ParentsBean parentsbean = null;

		try {
			session = HibernateUtils.getSession();

			String qury = "from ParentsBean where MobileNumber='" + phonenumber + "' and SchoolId=" + a + " and RecordStatus='ACTIVE'";
			parentsbean = (ParentsBean) session.createQuery(qury).uniqueResult();

			if (parentsbean != null) {
				return parentsbean;
			}
			 session.flush();
             session.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return parentsbean;
	}

	public static DriversBean getDriver(int driverid) {
		DriversBean driversBean = new DriversBean();

		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			driversBean = (DriversBean) session.createQuery("from DriversBean where DriverId=" + driverid)
					.uniqueResult();

			 session.flush();
             session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return driversBean;
	}

	public static DriversBean checkDriverLicence(int a, String licence) {
		Session session = null;
		DriversBean driverbean = null;

		try {
			session = HibernateUtils.getSession();

			String qury = "from DriversBean where LicenceNumber='" + licence + "' and SchoolId=" + a;
			driverbean = (DriversBean) session.createQuery(qury).uniqueResult();

			if (driverbean != null) {
				return driverbean;
			}
			 session.flush();
             session.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return driverbean;
	}

	public static RoutesBean checkRouteName(int a, String routename) {
		Session session = null;
		RoutesBean routebean = null;

		try {
			session = HibernateUtils.getSession();

			String qury = "from RoutesBean where RouteName='" + routename + "' and SchoolId=" + a;
			routebean = (RoutesBean) session.createQuery(qury).uniqueResult();

			if (routebean != null) {
				return routebean;
			}
			 session.flush();
             session.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return routebean;
	}

	public static ClassBean checkStudentClass(int a, String classname, String section) {
		Session session = null;
		ClassBean classbean = null;

		try {
			session = HibernateUtils.getSession();

			String qury = "from ClassBean where StudentClass='" + classname + "' and Section='" + section
					+ "' and SchoolId=" + a;
			classbean = (ClassBean) session.createQuery(qury).uniqueResult();
 
			if (classbean != null) {
				return classbean;
			} session.flush();
            session.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return classbean;
	}

	public static BusesBean checkRegNumber(int a, String regnumber) {
		Session session = null;
		BusesBean busbean = null;

		try {
			session = HibernateUtils.getSession();

			String qury = "from BusesBean where RegNumber='" + regnumber + "' and SchoolId=" + a;
			busbean = (BusesBean) session.createQuery(qury).uniqueResult();

			if (busbean != null) {
				return busbean;
			} session.flush();
            session.clear();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return busbean;
	}

	public static StudentsBean studentRollNoCheck(int schoolId, int classId, int rollno) {
		Session session = null;
		StudentsBean studentbean = null;

		try {
			session = HibernateUtils.getSession();

			String qury = "from StudentsBean where ClassId=" + classId + " and SchoolId=" + schoolId + " and RollNo="
					+ rollno + " and RecordStatus='ACTIVE'";
			studentbean = (StudentsBean) session.createQuery(qury).uniqueResult();

			if (studentbean != null) {
				return studentbean;
			}session.flush();
            session.clear();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return studentbean;
	}

	public static List<PickupPoints> getPickUpPointsForBusId(int busId) {
		List<PickupPoints> beans = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from PickupPoints where BusId=" + busId;
			beans = (List<PickupPoints>) session.createQuery(query).list();
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return beans;
	}

	public static HistoryLogBean getHistoryDetailsOnBoard(int busId, int pickupPointId, String format, String string) {
		HistoryLogBean historyLogBeans = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			historyLogBeans = (HistoryLogBean) session
					.createQuery("from HistoryLogBean where BusId=" + busId + " and PickupPointId=" + pickupPointId
							+ " and Date='" + format + "' and Direction='" + string + "'")
					.uniqueResult();
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return historyLogBeans;
	}

	public static HistoryLogBean getHistoryDetailsDeBoard(int busId, int pickupPointId, String format, String string) {
		HistoryLogBean historyLogBeans = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			historyLogBeans = (HistoryLogBean) session
					.createQuery("from HistoryLogBean where BusId=" + busId + " and PickupPointId=" + pickupPointId
							+ " and Date='" + format + "' and Direction='" + string + "'")
					.uniqueResult();
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return historyLogBeans;
	}

	public static List<StudentsBean> getStudentListByPick(int pickupPointId, int a) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session
					.createQuery("from StudentsBean where PickupPointId=" + pickupPointId + " and SchoolId=" + a + "")
					.list();
			session.flush();
            session.clear();
            transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<StudentsBean> getStudentListByDrop(int pickupPointId, int a) {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session
					.createQuery("from StudentsBean where DropPointId=" + pickupPointId + " and SchoolId=" + a + "")
					.list();
			session.flush();
            session.clear();
            transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static int getUserId(String mobileNumber) {
		Session session = null;
		String query = "";
		int uid = 0;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			uid = (int) session.createSQLQuery("select UId from Users where MobileNumber=" + mobileNumber)
					.uniqueResult();
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return uid;
	}

	public static List getOtp(String mobileNumber) {

		Session session = null;
		String query = "";
		List<String> otp = new ArrayList<>();
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			otp = session.createSQLQuery("select otp from Parents where MobileNumber=" + mobileNumber).list();
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return otp;
	}

	public static BusesBean checkBusName(int a, String busName) {
		Session session = null;
		BusesBean busbean = null;

		try {
			session = HibernateUtils.getSession();

			String qury = "from BusesBean where BusName='" + busName + "' and SchoolId=" + a;
			busbean = (BusesBean) session.createQuery(qury).uniqueResult();

			if (busbean != null) {
				return busbean;
			}session.flush();
            session.clear();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return busbean;
	}

	public static List<GPSTrackingBean> getGpsDetails(int busId) {
		List<GPSTrackingBean> beans = null;
		Session session = null;
		try {

			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String query = "from GPSTrackingBean where BusId=" + busId;
			beans = (List<GPSTrackingBean>) session.createQuery(query).list();
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return beans;
	}

	public static GPSTrackingBean updateGpsPwd(int sno, String password) {
		Session session = null;
		GPSTrackingBean bean = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			bean = (GPSTrackingBean) session.load(GPSTrackingBean.class, sno);

			bean.setPassword(password);
			session.update(bean);
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return bean;
}

	public static List<StudentsBean> getStudentListBasedOnTrackedStatus(int busId) {
		List<StudentsBean> stubean = new ArrayList<StudentsBean>();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			stubean =session.createQuery("from StudentsBean where BusId=" + busId + "and TrackedStatus='1' and RecordStatus='ACTIVE'").list();
			session.flush();
            session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return stubean;
	}

	public static List<HistoryLogBean> getTripStatus(int busId) {
		Session session = null;
		List<HistoryLogBean> hisbean = new ArrayList<HistoryLogBean>();
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			String date = dateFormat.format(new Date());
			System.out.println(date);
			try {
				hisbean = (List<HistoryLogBean>) session
						.createQuery("from HistoryLogBean where BusId=" + busId+ "and Date='" + date + "' order by Id ASC")
						.list();
				session.flush();
	            session.clear();
				transaction.commit();
			} catch (Exception e) {
				e.printStackTrace();
			}
			 			

		 
		finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
			
		return hisbean;
	}

	public static List<BusesBean> getBuses() {
		List<BusesBean> list = new ArrayList<BusesBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from BusesBean where RecordStatus='ACTIVE'").list();
			session.flush();
            session.clear();
            transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<ParentsBean> getParents() {
		List<ParentsBean> list = new ArrayList<ParentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from ParentsBean where RecordStatus='ACTIVE'").list();
			session.flush();
            session.clear();
            transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<StudentsBean> getStudents() {
		List<StudentsBean> list = new ArrayList<StudentsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from StudentsBean where RecordStatus='ACTIVE'").list();
			session.flush();
            session.clear();
            transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static List<SchoolsDetailsBean> getRecentlyAddedSchools() {
		List<SchoolsDetailsBean> list = new ArrayList<SchoolsDetailsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from SchoolsDetailsBean ORDER BY SchoolId DESC").setMaxResults(5).list();
			session.flush();
            session.clear();
            transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static HistoryLogBean getHistoryDeatils(int busId, String format, String dir, String type) {
		HistoryLogBean historyLogBeans = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
historyLogBeans = (HistoryLogBean) session.createQuery("from HistoryLogBean where BusId=" + busId
+ " and Type='"+type+"' and Direction='" + dir + "' and Date='" + format + "'").uniqueResult();
session.flush();
session.clear();
			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
					
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return historyLogBeans;
	}

	public static CmsBean getInfo(int id) {
		CmsBean cmsbean = null;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
System.out.println(id);
cmsbean = (CmsBean) session.get(CmsBean.class, id);
			System.out.println(cmsbean.getName());
			 
			 
			transaction.commit();

		} catch (org.hibernate.exception.GenericJDBCException e) {
			LOG.error(e);

		 
		 
	}

		return cmsbean;


	 
}

	public static void updateContent(String content, int id) {
		CmsBean cmsbean;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			cmsbean = (CmsBean) session.get(CmsBean.class, id);
			 
			cmsbean.setContentClob(Hibernate.createClob(content));
			System.out.println(cmsbean.getContent()+"update");
			session.saveOrUpdate(cmsbean);
			session.close();
			transaction.commit();
	}catch (Exception e) {
		// TODO: handle exception
	}
		}
}