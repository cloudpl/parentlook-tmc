package com.akshara.parentlook.db.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.generic.mss.utils.HibernateUtils;

public class AdminDAO {

	public static boolean find(String AdminName) {
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			Query query = session.createQuery(
					"from SchoolAdminsBean where AdminName = :AdminName and AdminPassword = :AdminPassword");

			List<SchoolAdminsBean> list = query.list();

			if (list.size() > 0) {
				return true;

			}
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}

		}
		return false;
	}

	public static SchoolAdminsBean getAdminList(String adminName) {
		SchoolAdminsBean bean = new SchoolAdminsBean();
		Session session = null;
		try {
			session = HibernateUtils.getSession();

			Transaction transaction = session.beginTransaction();
			bean = (SchoolAdminsBean) session.createQuery("from SchoolAdminsBean where AdminName='" + adminName + "'")
					.uniqueResult();
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return bean;
	}

	public static int saveAdmin(SchoolAdminsBean adminBean) {

		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			Date date = new Date();
			adminBean.setCreatedDate(date);

			session.save(adminBean);
			session.flush();session.clear();transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;

	}

	public static int saveAdminDetails(SchoolAdminsBean adminBean, File photoAdmin, int a) {

		int i = 0;
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			Date date = new Date();

			if (photoAdmin != null) {

				Blob imageBlob = Hibernate.createBlob(getBytesFromFile(photoAdmin));

				adminBean.setImage(imageBlob);

			}
			adminBean.setSchoolId(a);
			adminBean.setAdminRole("SCHOOLADMIN");
			adminBean.setAdminStatus("ACTIVE");

			adminBean.setCreatedDate(date);
			i = (Integer) session.save(adminBean);
			session.flush();session.clear();transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return i;

	}

	private static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();

		if (length > Integer.MAX_VALUE) {

		}

		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}

	public static List<SchoolAdminsBean> getAdminDetailList(int a) {
		List<SchoolAdminsBean> list = new ArrayList<SchoolAdminsBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from SchoolAdminsBean where SchoolId=" + a).list();
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return list;
	}

	public static SchoolAdminsBean checkAdmin(String adminName, int a) {
		Session session = null;
		boolean b = false;
		SchoolAdminsBean adminBean = new SchoolAdminsBean();
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			String qury = "from SchoolAdminsBean where AdminName='" + adminName + "' and SchoolId=" + a;
			adminBean = (SchoolAdminsBean) session.createQuery(qury).uniqueResult();
			if (adminBean != null) {
				return adminBean;
			}
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return adminBean;
	}

	public static SchoolAdminsBean updatePwd(String newPassword, int a) {
		Session session = null;
		SchoolAdminsBean bean = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			bean = (SchoolAdminsBean) session.load(SchoolAdminsBean.class, a);

			bean.setAdminPassword(newPassword);
			bean.setToken("null");
			session.update(bean);

			session.flush();session.clear();transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return bean;
	}

	public static SchoolAdminsBean getAdminDetail(int a) {

		Session session = null;
		SchoolAdminsBean adminBean = new SchoolAdminsBean();

		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			adminBean = (SchoolAdminsBean) session.createQuery("from SchoolAdminsBean where AdminId=" + a)
					.uniqueResult();
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return adminBean;
	}

	public static SchoolAdminsBean getLastLogged(SchoolAdminsBean schoolAdminBean, int a) {
		Session session = null;
		int i = 0;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			String Query = "update SchoolAdmins set LastLoggedIn=Now() where AdminId=" + a;

			i = (int) session.createSQLQuery(Query).executeUpdate();
			if (i > 0) {
			}
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return schoolAdminBean;
	}

	public static SchoolAdminsBean checkPassword(String newPassword, int a) {
		Session session = null;

		SchoolAdminsBean adminBean = new SchoolAdminsBean();
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			String qury = "from SchoolAdminsBean where AdminPassword='" + newPassword + "' and SchoolId=" + a;
			adminBean = (SchoolAdminsBean) session.createQuery(qury).uniqueResult();
			if (adminBean != null) {
				return adminBean;
			}
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return adminBean;
	}

	public static SchoolAdminsBean saveAdminData(SchoolAdminsBean adminsBean) {

		Session session = null;

		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			session.save(adminsBean);

			session.flush();session.clear();transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return adminsBean;
	}

	public static SchoolAdminsBean updateAdmin(String token, SchoolAdminsBean adminbean, int a, Date d1) {
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();

			adminbean = (SchoolAdminsBean) session.load(SchoolAdminsBean.class, a);

			adminbean.setToken(token);

			adminbean.setTokenGeneratedDate(d1);

			session.saveOrUpdate(adminbean);

			session.flush();session.clear();transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return adminbean;
	}

	public static SchoolAdminsBean getAdminDetail(String token) {
		SchoolAdminsBean bean = new SchoolAdminsBean();
		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			bean = (SchoolAdminsBean) session.createQuery("from SchoolAdminsBean where Token='" + token + "'")
					.uniqueResult();
			session.flush();session.clear();transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}
		return bean;
	}

	public static List<SchoolAdminsBean> getAdmin(int a) {
		Session session = null;
		List<SchoolAdminsBean> adminBean = new ArrayList<SchoolAdminsBean>();

		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			adminBean = (List<SchoolAdminsBean>) session.createQuery("from SchoolAdminsBean where SchoolId=" + a)
					.list();
			session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return adminBean;
	}

}
