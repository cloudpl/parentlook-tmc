package com.akshara.parentlook.db.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.generic.mss.utils.HibernateUtils;

public class GpsDao {

	public static GPSTrackingBean getGpsList(int BusId) {
		GPSTrackingBean list = new GPSTrackingBean();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = (GPSTrackingBean) session.createQuery("from GPSTrackingBean where BusId=" + BusId).uniqueResult();
session.flush();session.clear();transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		return (GPSTrackingBean) list;

	}

}
