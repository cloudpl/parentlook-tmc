package com.akshara.parentlook.db.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.generic.mss.utils.HibernateUtils;

public class BusDetailsDao {

	public static List<BusesBean> getBusList(int b) {
		List<BusesBean> list = new ArrayList<BusesBean>();

		Session session = null;
		try {
			session = HibernateUtils.getSession();
			Transaction transaction = session.beginTransaction();
			list = session.createQuery("from BusesBean where SchoolId=" + b).list();
			session.flush();
			session.clear();
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		return list;

	}

}
