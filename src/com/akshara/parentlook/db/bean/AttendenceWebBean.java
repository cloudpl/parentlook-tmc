package com.akshara.parentlook.db.bean;

public class AttendenceWebBean {

	private int TotalStudents;

	private int onboard;

	private int onboardAbsent;

	private int deBoard;
	private int deBoardAbsent;
	String busName;
	String routename;

	public String getRoutename() {
		return routename;
	}

	public void setRoutename(String routename) {
		this.routename = routename;
	}

	 

	public int getTotalStudents() {
		return TotalStudents;
	}

	public void setTotalStudents(int totalStudents) {
		TotalStudents = totalStudents;
	}

	public int getOnboard() {
		return onboard;
	}

	public void setOnboard(int onboard) {
		this.onboard = onboard;
	}

	public int getOnboardAbsent() {
		return onboardAbsent;
	}

	public void setOnboardAbsent(int onboardAbsent) {
		this.onboardAbsent = onboardAbsent;
	}

	public int getDeBoard() {
		return deBoard;
	}

	public void setDeBoard(int deBoard) {
		this.deBoard = deBoard;
	}

	public int getDeBoardAbsent() {
		return deBoardAbsent;
	}

	public void setDeBoardAbsent(int deBoardAbsent) {
		this.deBoardAbsent = deBoardAbsent;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

}
