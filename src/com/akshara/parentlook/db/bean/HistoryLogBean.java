package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "History")
public class HistoryLogBean {

	@Id
	@Column(name = "Id", unique = true, nullable = false)
	private int Id;
	@Column(name = "BusId")
	private int BusId;
	@Column(name = "Type")
	private String Type;
	@Column(name = "Direction")
	private String Direction;
	@Column(name = "Time")
	private String Time;
	@Column(name = "Date")
	 private String Date;
	@Column(name = "PickupPointId")
	private int PickupPointId;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getBusId() {
		return BusId;
	}
	public void setBusId(int busId) {
		BusId = busId;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getDirection() {
		return Direction;
	}
	public void setDirection(String direction) {
		Direction = direction;
	}
	public String getTime() {
		return Time;
	}
	public void setTime(String time) {
		Time = time;
	}
	 
	public int getPickupPointId() {
		return PickupPointId;
	}
	public void setPickupPointId(int pickupPointId) {
		PickupPointId = pickupPointId;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}

	

}
