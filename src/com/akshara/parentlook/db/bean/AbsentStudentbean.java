package com.akshara.parentlook.db.bean;

public class AbsentStudentbean {

	private String name;
	private String studclass;
	private String rollno;
	private String routename;
	private int onboarded;
	private String busName;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStudclass() {
		return studclass;
	}

	public void setStudclass(String studclass) {
		this.studclass = studclass;
	}

	public String getRollno() {
		return rollno;
	}

	public void setRollno(String rollno) {
		this.rollno = rollno;
	}

	public String getRoutename() {
		return routename;
	}

	public void setRoutename(String routename) {
		this.routename = routename;
	}

	public int getOnboarded() {
		return onboarded;
	}

	public void setOnboarded(int onboarded) {
		this.onboarded = onboarded;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

}
