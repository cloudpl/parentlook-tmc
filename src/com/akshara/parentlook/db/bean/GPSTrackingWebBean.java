package com.akshara.parentlook.db.bean;

public class GPSTrackingWebBean {

	private int sno;

	private int BusId;

	private String DeviceId;

	private String geoLat;
	private int Surveillance;
	private String geoLang;

	private String Date;

	private String Timestamp;
    private String driverName;
    private String dirContactNum;
	private String Name;
	private String tripStatus;
	private String tracking;
	private String video;
	private String RouteName;
	private String status;
	private String busName;
	private String userName;
	private String password;
	private int loginstatus;
	private String schoolName;
	private String contacnum;
	private int tcount;
	private int vcount;
	private int schoolId;

	public int getSno() {
		return sno;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getDeviceId() {
		return DeviceId;
	}

	public void setDeviceId(String deviceId) {
		DeviceId = deviceId;
	}

	public String getGeoLat() {
		return geoLat;
	}

	public void setGeoLat(String geoLat) {
		this.geoLat = geoLat;
	}

	public String getGeoLang() {
		return geoLang;
	}

	public void setGeoLang(String geoLang) {
		this.geoLang = geoLang;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

	public String getUserName() {
		return userName;
	}

	public int getSurveillance() {
		return Surveillance;
	}

	public void setSurveillance(int surveillance) {
		Surveillance = surveillance;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getLoginstatus() {
		return loginstatus;
	}

	public void setLoginstatus(int loginstatus) {
		this.loginstatus = loginstatus;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getContacnum() {
		return contacnum;
	}

	public void setContacnum(String contacnum) {
		this.contacnum = contacnum;
	}

	public int getTcount() {
		return tcount;
	}

	public void setTcount(int tcount) {
		this.tcount = tcount;
	}

	public int getVcount() {
		return vcount;
	}

	public void setVcount(int vcount) {
		this.vcount = vcount;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public String getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(String tripStatus) {
		this.tripStatus = tripStatus;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getTracking() {
		return tracking;
	}

	public void setTracking(String tracking) {
		this.tracking = tracking;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDirContactNum() {
		return dirContactNum;
	}

	public void setDirContactNum(String dirContactNum) {
		this.dirContactNum = dirContactNum;
	}

}
