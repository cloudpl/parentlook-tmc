package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Generated;

@Entity
@Table(name = "Routes")
public class RoutesBean {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "RouteId")
	private int RouteId;

	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "StartPoint")
	private String StartPoint;

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

	@Column(name = "EndPoint")
	private String EndPoint;

	@Column(name = "RouteName")
	private String RouteName;

	@Column(name = "RecordStatus")
	private String RecordStatus;

	@Column(name = "UsedStatus")
	private String UsedStatus;

	public String getUsedStatus() {
		return UsedStatus;
	}

	public void setUsedStatus(String usedStatus) {
		UsedStatus = usedStatus;
	}

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public int getRouteId() {
		return RouteId;
	}

	public void setRouteId(int routeId) {
		RouteId = routeId;
	}

	public String getStartPoint() {
		return StartPoint;
	}

	public void setStartPoint(String startPoint) {
		StartPoint = startPoint;
	}

	public String getEndPoint() {
		return EndPoint;
	}

	public void setEndPoint(String endPoint) {
		EndPoint = endPoint;
	}

}
