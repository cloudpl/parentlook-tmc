package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PickupPoints")
public class PickupPoints {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PickupPointId", unique = true, nullable = false)
	private int PickupPointId;

	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "PickupPoint")
	private String PickupPoint;
	@Column(name = "PLatLang")
	private String PLatLang;

	@Column(name = "RouteId")
	private int RouteId;

	@Column(name = "BusId")
	private int BusId;

	@Column(name = "Direction")
	private String Direction;

	public int getPickupPointId() {
		return PickupPointId;
	}

	public void setPickupPointId(int pickupPointId) {
		PickupPointId = pickupPointId;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public String getPickupPoint() {
		return PickupPoint;
	}

	public void setPickupPoint(String pickupPoint) {
		PickupPoint = pickupPoint;
	}

	public int getRouteId() {
		return RouteId;
	}

	public void setRouteId(int routeId) {
		RouteId = routeId;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getPLatLang() {
		return PLatLang;
	}

	public void setPLatLang(String pLatLang) {
		PLatLang = pLatLang;
	}

	public String getDirection() {
		return Direction;
	}

	public void setDirection(String direction) {
		Direction = direction;
	}

}
