package com.akshara.parentlook.db.bean;

import javax.persistence.Column;

public class BusDetailBean {

	private int busId;

	private int busDrop;
	private int busPick;
	private String routeName;
	private int Surveillance;
	private String status;
	private String TripStatus;
	private String driverName;

	private String ChasisNumber;

	private int DriverId;
	private String otp;
	private String EngineNumber;

	private String InsurenceValidity;

	private int Occupency;

	private String RegDate;

	private String RegNumber;

	private String RegValidity;

	private String ReturnArrivalTime;

	private String ReturnStartTime;

	private int RouteId;

	private String SchoolArraivalTime;

	private String TransportStartTime;

	private String SchoolId;

	private String Activities;
	private String Pickup;
	private String DropStatus;

	private String StartPointLocation;
	private String StartPointLatLang;
	private String RecordStatus;

	private String usedStatus;

	private String BusName;

	private String Description;

	public int getBusId() {
		return busId;
	}

	public void setBusId(int busId) {
		this.busId = busId;
	}

	public int getBusDrop() {
		return busDrop;
	}

	public void setBusDrop(int busDrop) {
		this.busDrop = busDrop;
	}

	public int getBusPick() {
		return busPick;
	}

	public void setBusPick(int busPick) {
		this.busPick = busPick;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getChasisNumber() {
		return ChasisNumber;
	}

	public void setChasisNumber(String chasisNumber) {
		ChasisNumber = chasisNumber;
	}

	public int getDriverId() {
		return DriverId;
	}

	public void setDriverId(int driverId) {
		DriverId = driverId;
	}

	public String getEngineNumber() {
		return EngineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		EngineNumber = engineNumber;
	}

	public String getInsurenceValidity() {
		return InsurenceValidity;
	}

	public void setInsurenceValidity(String insurenceValidity) {
		InsurenceValidity = insurenceValidity;
	}

	public int getOccupency() {
		return Occupency;
	}

	public void setOccupency(int occupency) {
		Occupency = occupency;
	}

	public String getRegDate() {
		return RegDate;
	}

	public void setRegDate(String regDate) {
		RegDate = regDate;
	}

	public String getRegNumber() {
		return RegNumber;
	}

	public void setRegNumber(String regNumber) {
		RegNumber = regNumber;
	}

	public String getRegValidity() {
		return RegValidity;
	}

	public void setRegValidity(String regValidity) {
		RegValidity = regValidity;
	}

	public String getReturnArrivalTime() {
		return ReturnArrivalTime;
	}

	public String getTripStatus() {
		return TripStatus;
	}

	public void setTripStatus(String tripStatus) {
		TripStatus = tripStatus;
	}

	public void setReturnArrivalTime(String returnArrivalTime) {
		ReturnArrivalTime = returnArrivalTime;
	}

	public String getReturnStartTime() {
		return ReturnStartTime;
	}

	public void setReturnStartTime(String returnStartTime) {
		ReturnStartTime = returnStartTime;
	}

	public int getRouteId() {
		return RouteId;
	}

	public void setRouteId(int routeId) {
		RouteId = routeId;
	}

	public String getSchoolArraivalTime() {
		return SchoolArraivalTime;
	}

	public void setSchoolArraivalTime(String schoolArraivalTime) {
		SchoolArraivalTime = schoolArraivalTime;
	}

	public String getTransportStartTime() {
		return TransportStartTime;
	}

	public void setTransportStartTime(String transportStartTime) {
		TransportStartTime = transportStartTime;
	}

	public String getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(String schoolId) {
		SchoolId = schoolId;
	}

	public String getActivities() {
		return Activities;
	}

	public void setActivities(String activities) {
		Activities = activities;
	}

	public String getPickup() {
		return Pickup;
	}

	public void setPickup(String pickup) {
		Pickup = pickup;
	}

	public String getDropStatus() {
		return DropStatus;
	}

	public void setDropStatus(String dropStatus) {
		DropStatus = dropStatus;
	}

	public String getStartPointLocation() {
		return StartPointLocation;
	}

	public void setStartPointLocation(String startPointLocation) {
		StartPointLocation = startPointLocation;
	}

	public String getStartPointLatLang() {
		return StartPointLatLang;
	}

	public void setStartPointLatLang(String startPointLatLang) {
		StartPointLatLang = startPointLatLang;
	}

	public String getBusName() {
		return BusName;
	}

	public void setBusName(String busName) {
		BusName = busName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public String getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(String usedStatus) {
		this.usedStatus = usedStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getSurveillance() {
		return Surveillance;
	}

	public void setSurveillance(int surveillance) {
		Surveillance = surveillance;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
