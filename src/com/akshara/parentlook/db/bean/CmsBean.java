package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Clob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CMS")
public class CmsBean {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private int Id;
	
	@Column(name="Name")
	private String Name;
	
	@Column(name="Content")
	private String Content;
	
	@Column(name="LastModified")
	private Date LastModified;
	private Clob contentClob;
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public Date getLastModified() {
		return LastModified;
	}

	public void setLastModified(Date lastModified) {
		LastModified = lastModified;
	}

	public Clob getContentClob() {
		return contentClob;
	}

	public void setContentClob(Clob contentClob) {
		this.contentClob = contentClob;
	}
	
	
	
}
