package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GPSTracking")
public class GPSTrackingBean {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "sno", unique = true, nullable = false)
	private int sno;

	@Column(name = "BusId", unique = true)
	private int BusId;

	@Column(name = "DeviceId")
	private String DeviceId;

	@Column(name = "geoLat")
	private String geoLat;

	@Column(name = "geoLang")
	private String geoLang;

	@Column(name = "Date")
	private String Date;

	@Column(name = "Timestamp")
	private String Timestamp;

	@Column(name = "Name")
	private String Name;

	@Column(name="UserName")
	private String UserName;
	
	@Column(name="Password")
	private String Password;
	
	@Column(name="LoginStatus")
	private int LoginStatus;
	
	@Column(name="Surveillance")
	private int Surveillance;
	
	@Column(name="Tracking")
	private int Tracking;
	
	public int getSno() {
		return sno;
	}

	public void setSno(int sno) {
		this.sno = sno;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getDeviceId() {
		return DeviceId;
	}

	public void setDeviceId(String deviceId) {
		DeviceId = deviceId;
	}

	public String getGeoLat() {
		return geoLat;
	}

	public void setGeoLat(String geoLat) {
		this.geoLat = geoLat;
	}

	public String getGeoLang() {
		return geoLang;
	}

	public void setGeoLang(String geoLang) {
		this.geoLang = geoLang;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public int getSurveillance() {
		return Surveillance;
	}

	public void setSurveillance(int surveillance) {
		Surveillance = surveillance;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public int getLoginStatus() {
		return LoginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		LoginStatus = loginStatus;
	}

	public int getTracking() {
		return Tracking;
	}

	public void setTracking(int tracking) {
		Tracking = tracking;
	}

}
