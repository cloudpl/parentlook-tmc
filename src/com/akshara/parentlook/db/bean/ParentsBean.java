package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.awt.Image;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Parents")
public class ParentsBean {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ParentId")
	private int ParentId;

	@Column(name = "LastName")
	private String LastName;

	@Column(name = "FirstName")
	private String FirstName;

	@Column(name = "Address")
	private String Address;

	@Column(name = "MobileNumber")
	private String MobileNumber;

	@Column(name = "loginStatus")
	private String loginStatus;

	@Column(name = "password")
	private String password;

	@Column(name = "otp")
	private String otp;

	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "UId")
	private int UId;
	@Column(name = "Photo")
	private Blob Photo;
	@Column(name = "RecordStatus")
	private String RecordStatus;
	private String usedStatus;

	@Column(name = "ProfilePicture")
	private String ProfilePicture;

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public int getParentId() {
		return ParentId;
	}

	public void setParentId(int parentId) {
		ParentId = parentId;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getAddress() {
		return Address;
	}

	public Blob getPhoto() {
		return Photo;
	}

	public void setPhoto(Blob photo) {
		Photo = photo;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	@Column(name = "Email")
	private String Email;

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public int getUId() {
		return UId;
	}

	public void setUId(int uId) {
		UId = uId;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	@Column(name = "ImageStr")
	private String ImageStr;

	public String getImageStr() {
		return ImageStr;
	}

	public void setImageStr(String imageStr) {
		ImageStr = imageStr;
	}

	public String getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(String usedStatus) {
		this.usedStatus = usedStatus;
	}

}
