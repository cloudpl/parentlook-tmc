package com.akshara.parentlook.db.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SchoolNotifications")
public class SchoolNotifications {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int Id;

	@Column(name = "Description")
	private String Description;

	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "StudentId")
	private int StudentId;

	@Column(name = "ReadStatus")
	private String ReadStatus;

	@Column(name = "Type")
	private String Type;

	@Column(name = "Category")
	private String Category;
	@Column(name = "Time")
	private String Time;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getReadStatus() {
		return ReadStatus;
	}

	public void setReadStatus(String readStatus) {
		ReadStatus = readStatus;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

}
