package com.akshara.parentlook.db.bean;

import java.sql.Blob;

import javax.persistence.Column;

public class DummyBean {
	private int BusId;
	private String RouteName;
	private int presntedStudents;
	private int obsentedStudents;
	private String BusName;
	private String StudentFirstName;
	private String StudentLastName;
	private Blob Photo;
	private String Name;
	private String RollNo;
	private int StudentId;
	private String Description;
	private String StartPoint;
	private String EndPoint;
	private String ImageStr;
	private String RecordStatus;
	private String usedStatus;
	private String ProfilePicture;

	public int getPresntedStudents() {
		return presntedStudents;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public void setPresntedStudents(int presntedStudents) {
		this.presntedStudents = presntedStudents;
	}

	public int getObsentedStudents() {
		return obsentedStudents;
	}

	public void setObsentedStudents(int obsentedStudents) {
		this.obsentedStudents = obsentedStudents;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

	public String getBusName() {
		return BusName;
	}

	public void setBusName(String busName) {
		BusName = busName;
	}

	public String getStudentFirstName() {
		return StudentFirstName;
	}

	public void setStudentFirstName(String studentFirstName) {
		StudentFirstName = studentFirstName;
	}

	public String getStudentLastName() {
		return StudentLastName;
	}

	public void setStudentLastName(String studentLastName) {
		StudentLastName = studentLastName;
	}

	public Blob getPhoto() {
		return Photo;
	}

	public void setPhoto(Blob photo) {
		Photo = photo;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getRollNo() {
		return RollNo;
	}

	public void setRollNo(String rollNo) {
		RollNo = rollNo;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getStartPoint() {
		return StartPoint;
	}

	public void setStartPoint(String startPoint) {
		StartPoint = startPoint;
	}

	public String getEndPoint() {
		return EndPoint;
	}

	public void setEndPoint(String endPoint) {
		EndPoint = endPoint;
	}

	public String getImageStr() {
		return ImageStr;
	}

	public void setImageStr(String imageStr) {
		ImageStr = imageStr;
	}

	public String getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(String usedStatus) {
		this.usedStatus = usedStatus;
	}

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

}
