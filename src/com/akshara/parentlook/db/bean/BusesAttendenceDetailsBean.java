package com.akshara.parentlook.db.bean;

public class BusesAttendenceDetailsBean {

	private int busid;

	private String routename;

	private int occupency;

	private int onboard;

	private int onboardAbsent;

	private int deBoard;
	private int deBoardAbsent;
	private String busName;

	public int getBusid() {
		return busid;
	}

	public void setBusid(int busid) {
		this.busid = busid;
	}

	public String getRoutename() {
		return routename;
	}

	public void setRoutename(String routename) {
		this.routename = routename;
	}

	public int getOccupency() {
		return occupency;
	}

	public void setOccupency(int occupency) {
		this.occupency = occupency;
	}

	public int getOnboard() {
		return onboard;
	}

	public void setOnboard(int onboard) {
		this.onboard = onboard;
	}

	public int getOnboardAbsent() {
		return onboardAbsent;
	}

	public void setOnboardAbsent(int onboardAbsent) {
		this.onboardAbsent = onboardAbsent;
	}

	public int getDeBoard() {
		return deBoard;
	}

	public void setDeBoard(int deBoard) {
		this.deBoard = deBoard;
	}

	public int getDeBoardAbsent() {
		return deBoardAbsent;
	}

	public void setDeBoardAbsent(int deBoardAbsent) {
		this.deBoardAbsent = deBoardAbsent;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

}
