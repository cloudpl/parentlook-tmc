package com.akshara.parentlook.db.bean;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Schools")
public class SchoolsDetailsBean {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "SchoolName")
	private String SchoolName;

	@Column(name = "ContactNumber")
	private String ContactNumber;

	@Column(name = "Address")
	private String Address;

	@Column(name = "Email")
	private String Email;
	@Column(name = "MapLocation")
	private String MapLocation;

	@Column(name = "Photo")
	private Blob Photo;

	public Blob getPhoto() {
		return Photo;
	}

	public void setPhoto(Blob photo) {
		Photo = photo;
	}

	@Column(name = "MapLocationLatLang")
	private String MapLocationLatLang;

	@Column(name = "Status", columnDefinition = "tinyint(1) default 1")
	private Boolean Status;

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public String getSchoolName() {
		return SchoolName;
	}

	public void setSchoolName(String schoolName) {
		SchoolName = schoolName;
	}

	public String getContactNumber() {
		return ContactNumber;
	}

	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getMapLocation() {
		return MapLocation;
	}

	public void setMapLocation(String mapLocation) {
		MapLocation = mapLocation;
	}

	public String getMapLocationLatLang() {
		return MapLocationLatLang;
	}

	public void setMapLocationLatLang(String mapLocationLatLang) {
		MapLocationLatLang = mapLocationLatLang;
	}

	public Boolean getStatus() {
		return Status;
	}

	public void setStatus(Boolean status) {
		Status = status;
	}

	private String imageStr;

	public String getImageStr() {
		return imageStr;
	}

	public void setImageStr(String imageStr) {
		this.imageStr = imageStr;
	}
private String usedStatus;

public String getUsedStatus() {
	return usedStatus;
}

public void setUsedStatus(String usedStatus) {
	this.usedStatus = usedStatus;
}
}
