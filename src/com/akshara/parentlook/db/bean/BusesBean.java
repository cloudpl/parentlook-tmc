package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Buses")
public class BusesBean {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "BusId", unique = true, nullable = false)
	private int BusId;

	@Column(name = "ChasisNumber")
	private String ChasisNumber;

	@Column(name = "DriverId")
	private int DriverId;

	@Column(name = "EngineNumber")
	private String EngineNumber;

	@Column(name = "InsurenceValidity")
	private String InsurenceValidity;

	@Column(name = "Occupency")
	private int Occupency;

	@Column(name="otp")
	private String otp;
	@Column(name = "RegDate")
	private String RegDate;

	@Column(name = "RegNumber")
	private String RegNumber;

	@Column(name = "RegValidity")
	private String RegValidity;

	@Column(name = "ReturnArrivalTime")
	private String ReturnArrivalTime;

	@Column(name = "ReturnStartTime")
	private String ReturnStartTime;

	@Column(name = "RouteId")
	private int RouteId;

	@Column(name = "SchoolArraivalTime")
	private String SchoolArraivalTime;

	@Column(name = "TransportStartTime")
	private String TransportStartTime;

	@Column(name = "SchoolId")
	private String SchoolId;

	@Column(name = "Activities")
	private String Activities;
	@Column(name = "Pickup")
	private String Pickup;
	@Column(name = "DropStatus")
	private String DropStatus;

	@Column(name = "StartPointLocation")
	private String StartPointLocation;
	@Column(name = "StartPointLatLang")
	private String StartPointLatLang;

	@Column(name = "BusName")
	private String BusName;

	@Column(name = "Description")
	private String Description;

	@Column(name = "RecordStatus")
	private String RecordStatus;

	private String usedStatus;

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getChasisNumber() {
		return ChasisNumber;
	}

	public void setChasisNumber(String chasisNumber) {
		ChasisNumber = chasisNumber;
	}

	public int getDriverId() {
		return DriverId;
	}

	public void setDriverId(int driverId) {
		DriverId = driverId;
	}

	public String getEngineNumber() {
		return EngineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		EngineNumber = engineNumber;
	}

	public String getInsurenceValidity() {
		return InsurenceValidity;
	}

	public void setInsurenceValidity(String insurenceValidity) {
		InsurenceValidity = insurenceValidity;
	}

	public int getOccupency() {
		return Occupency;
	}

	public void setOccupency(int occupency) {
		Occupency = occupency;
	}

	public String getRegDate() {
		return RegDate;
	}

	public void setRegDate(String regDate) {
		RegDate = regDate;
	}

	public String getRegNumber() {
		return RegNumber;
	}

	public void setRegNumber(String regNumber) {
		RegNumber = regNumber;
	}

	public String getRegValidity() {
		return RegValidity;
	}

	public void setRegValidity(String regValidity) {
		RegValidity = regValidity;
	}

	public String getReturnArrivalTime() {
		return ReturnArrivalTime;
	}

	public void setReturnArrivalTime(String returnArrivalTime) {
		ReturnArrivalTime = returnArrivalTime;
	}

	public String getReturnStartTime() {
		return ReturnStartTime;
	}

	public void setReturnStartTime(String returnStartTime) {
		ReturnStartTime = returnStartTime;
	}

	public int getRouteId() {
		return RouteId;
	}

	public void setRouteId(int routeId) {
		RouteId = routeId;
	}

	public String getSchoolArraivalTime() {
		return SchoolArraivalTime;
	}

	public void setSchoolArraivalTime(String schoolArraivalTime) {
		SchoolArraivalTime = schoolArraivalTime;
	}

	public String getTransportStartTime() {
		return TransportStartTime;
	}

	public void setTransportStartTime(String transportStartTime) {
		TransportStartTime = transportStartTime;
	}

	public String getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(String schoolId) {
		SchoolId = schoolId;
	}

	public String getActivities() {
		return Activities;
	}

	public void setActivities(String activities) {
		Activities = activities;
	}

	public String getPickup() {
		return Pickup;
	}

	public void setPickup(String pickup) {
		Pickup = pickup;
	}

	public String getDropStatus() {
		return DropStatus;
	}

	public void setDropStatus(String dropStatus) {
		DropStatus = dropStatus;
	}

	public String getStartPointLocation() {
		return StartPointLocation;
	}

	public void setStartPointLocation(String startPointLocation) {
		StartPointLocation = startPointLocation;
	}

	public String getStartPointLatLang() {
		return StartPointLatLang;
	}

	public void setStartPointLatLang(String startPointLatLang) {
		StartPointLatLang = startPointLatLang;
	}

	public String getBusName() {
		return BusName;
	}

	public void setBusName(String busName) {
		BusName = busName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(String usedStatus) {
		this.usedStatus = usedStatus;
	}

	private String Active, InActive, Arrived;

	public String getActive() {
		return Active;
	}

	public void setActive(String active) {
		Active = active;
	}

	public String getInActive() {
		return InActive;
	}

	public void setInActive(String inActive) {
		InActive = inActive;
	}

	public String getArrived() {
		return Arrived;
	}

	public void setArrived(String arrived) {
		Arrived = arrived;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

}
