package com.akshara.parentlook.db.bean;

import java.sql.Blob;
import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Generated;

@Entity
@Table(name = "Driver")
public class DriversBean {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "DriverId")
	private int DriverId;

	@Column(name = "FirstName")
	private String FirstName;

	@Column(name = "LastName")
	private String LastName;

	@Column(name = "ContactNumber")
	private String ContactNumber;

	@Column(name = "RelationTitle")
	private String RelationTitle;

	@Column(name = "RelativeName")
	private String RelativeName;

	@Column(name = "Age")
	private int Age;

	@Column(name = "DriverPic")
	private Blob Photo;

	@Column(name = "LicenceNumber")
	private String LicenceNumber;
	@Column(name = "LicenceAttachment")
	private Blob licenceAttachment;

	public Blob getLicenceAttachment() {
		return licenceAttachment;
	}

	public void setLicenceAttachment(Blob licenceAttachment) {
		this.licenceAttachment = licenceAttachment;
	}

	public Blob getIdTypeAttachment() {
		return idTypeAttachment;
	}

	public void setIdTypeAttachment(Blob idTypeAttachment) {
		this.idTypeAttachment = idTypeAttachment;
	}

	@Column(name = "IDType")
	private String IdType;
	@Column(name = "IdTypeAttachment")
	private Blob idTypeAttachment;

	@Column(name = "CurrentAddress")
	private String CurrentAddress;
	@Column(name = "SchoolId")
	private int SchoolId;
	@Column(name = "RecordStatus")
	private String RecordStatus;

	private String usedStatus;

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public int getDriverId() {
		return DriverId;
	}

	public void setDriverId(int driverId) {
		DriverId = driverId;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getContactNumber() {
		return ContactNumber;
	}

	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}

	public String getRelationTitle() {
		return RelationTitle;
	}

	public void setRelationTitle(String relationTitle) {
		RelationTitle = relationTitle;
	}

	public String getRelativeName() {
		return RelativeName;
	}

	public void setRelativeName(String relativeName) {
		RelativeName = relativeName;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public Blob getPhoto() {
		return Photo;
	}

	public void setPhoto(Blob photo) {
		Photo = photo;
	}

	public String getLicenceNumber() {
		return LicenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		LicenceNumber = licenceNumber;
	}

	public String getIdType() {
		return IdType;
	}

	public void setIdType(String idType) {
		IdType = idType;
	}

	public String getCurrentAddress() {
		return CurrentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		CurrentAddress = currentAddress;
	}

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public String getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(String usedStatus) {
		this.usedStatus = usedStatus;
	}

}
