package com.akshara.parentlook.db.bean;

public class StudentViewBean {
	private String StudentName, StudentClass, Location, DropPoint, PickupPoint;
	private int PickupPointId, DropPointId, BusId, StudentId;
	private String StartPoint, EndPoint, RouteName;
	private String FirstName, ContactNo, LastName;
	private String BusName;

	public int getBusId() {
		return BusId;
	}

	public String getStartPoint() {
		return StartPoint;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getBusName() {
		return BusName;
	}

	public void setBusName(String busName) {
		BusName = busName;
	}

	public String getContactNo() {
		return ContactNo;
	}

	public void setContactNo(String contactNo) {
		ContactNo = contactNo;
	}

	public void setStartPoint(String startPoint) {
		StartPoint = startPoint;
	}

	public String getEndPoint() {
		return EndPoint;
	}

	public void setEndPoint(String endPoint) {
		EndPoint = endPoint;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public int getPickupPointId() {
		return PickupPointId;
	}

	public void setPickupPointId(int pickupPointId) {
		PickupPointId = pickupPointId;
	}

	public int getDropPointId() {
		return DropPointId;
	}

	public void setDropPointId(int dropPointId) {
		DropPointId = dropPointId;
	}

	public String getDropPoint() {
		return DropPoint;
	}

	public void setDropPoint(String dropPoint) {
		DropPoint = dropPoint;
	}

	public String getPickupPoint() {
		return PickupPoint;
	}

	public void setPickupPoint(String pickupPoint) {
		PickupPoint = pickupPoint;
	}

	private int ClassId;

	public String getStudentName() {
		return StudentName;
	}

	public void setStudentName(String studentName) {
		StudentName = studentName;
	}

	public String getStudentClass() {
		return StudentClass;
	}

	public void setStudentClass(String studentClass) {
		StudentClass = studentClass;
	}

	public int getClassId() {
		return ClassId;
	}

	public void setClassId(int classId) {
		ClassId = classId;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

}
