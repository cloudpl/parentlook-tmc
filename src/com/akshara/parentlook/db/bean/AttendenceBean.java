package com.akshara.parentlook.db.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Attendance")
public class AttendenceBean {
	@Id
	@Column(name = "DeviceLogId")
	private int DevicLogId;
	@Column(name = "StudentId")
	private int StudentId;
	@Column(name = "LogTime")
	private String LogTime;
	@Column(name = "DownloadTime")
	private String DownLoadTime;
	@Column(name = "DeviceId")
	private String DeviceId;
	@Column(name = "Direction")
	private String Direction;
	@Column(name = "LogDate")
	private String LogDate;

	public int getDevicLogId() {
		return DevicLogId;
	}

	public void setDevicLogId(int devicLogId) {
		DevicLogId = devicLogId;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getLogTime() {
		return LogTime;
	}

	public void setLogTime(String logTime) {
		LogTime = logTime;
	}

	public String getDownLoadTime() {
		return DownLoadTime;
	}

	public void setDownLoadTime(String downLoadTime) {
		DownLoadTime = downLoadTime;
	}

	public String getDirection() {
		return Direction;
	}

	public void setDirection(String direction) {
		Direction = direction;
	}

	public String getLogDate() {
		return LogDate;
	}

	public void setLogDate(String logDate) {
		LogDate = logDate;
	}

	public String getDeviceId() {
		return DeviceId;
	}

	public void setDeviceId(String deviceId) {
		DeviceId = deviceId;
	}

}
