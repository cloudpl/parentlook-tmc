package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Generated;

@Entity
@Table(name = "WayPoints")
public class WayPointsBean {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "WayPointId")
	private int WayPointId;

	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "StudentId")
	private int StudentId;

	@Column(name = "WayPoint")
	private String WayPoint;
	@Column(name = "LatLang")
	private String LatLang;

	public int getWayPointId() {
		return WayPointId;
	}

	public void setWayPointId(int wayPointId) {
		WayPointId = wayPointId;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getWayPoint() {
		return WayPoint;
	}

	public void setWayPoint(String wayPoint) {
		WayPoint = wayPoint;
	}

	public String getLatLang() {
		return LatLang;
	}

	public void setLatLang(String latLang) {
		LatLang = latLang;
	}

}
