package com.akshara.parentlook.db.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SchoolAdmins")

public class SchoolAdminsBean {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "AdminId", unique = true, nullable = false)
	private int AdminId;

	@Column(name = "Token")
	private String Token;

	@Column(name = "TokenGeneratedDate")
	private Date TokenGeneratedDate;
	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "AdminName")
	private String AdminName;

	@Column(name = "AdminPassword")
	private String AdminPassword;

	@Column(name = "Email")
	private String Email;

	@Column(name = "Image")
	private Blob Image;

	@Column(name = "PhoneNumbe")
	private String PhoneNumbe;

	@Column(name = "AdminRole")
	private String AdminRole;

	@Column(name = "AdminStatus")
	private String AdminStatus;

	@Column(name = "CreatedDate")
	private Date CreatedDate;

	@Column(name = "LastLoggedIn")
	private Date LastLoggedIn;

	public int getAdminId() {
		return AdminId;
	}

	public void setAdminId(int adminId) {
		AdminId = adminId;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public String getAdminPassword() {
		return AdminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		AdminPassword = adminPassword;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public Blob getImage() {
		return Image;
	}

	public void setImage(Blob image) {
		Image = image;
	}

	public String getPhoneNumbe() {
		return PhoneNumbe;
	}

	public void setPhoneNumbe(String phoneNumbe) {
		PhoneNumbe = phoneNumbe;
	}

	public String getAdminRole() {
		return AdminRole;
	}

	public void setAdminRole(String adminRole) {
		AdminRole = adminRole;
	}

	public String getAdminStatus() {
		return AdminStatus;
	}

	public void setAdminStatus(String adminStatus) {
		AdminStatus = adminStatus;
	}

	public Date getCreatedDate() {
		return CreatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		CreatedDate = createdDate;
	}

	public Date getLastLoggedIn() {
		return LastLoggedIn;
	}

	public void setLastLoggedIn(Date lastLoggedIn) {
		LastLoggedIn = lastLoggedIn;
	}

	public String getAdminName() {
		return AdminName;
	}

	public void setAdminName(String adminName) {
		AdminName = adminName;
	}

	private String imageStr;

	public String getImageStr() {
		return imageStr;
	}

	public void setImageStr(String imageStr) {
		this.imageStr = imageStr;
	}

	public Date getTokenGeneratedDate() {
		return TokenGeneratedDate;
	}

	public void setTokenGeneratedDate(Date tokenGeneratedDate) {
		TokenGeneratedDate = tokenGeneratedDate;
	}

	public String getToken() {
		return Token;
	}

	public void setToken(String token) {
		Token = token;
	}

}
