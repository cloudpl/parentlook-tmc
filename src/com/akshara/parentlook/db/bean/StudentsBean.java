package com.akshara.parentlook.db.bean;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "Students")
public class StudentsBean {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "StudentId")
	private int StudentId;

	@Column(name = "StudentFirstName")
	private String StudentFirstName;

	@Column(name = "StudentLastName")
	private String StudentLastName;
	@Column(name = "Photo")
	@Lob
	private Blob Photo;

	@Column(name = "SchoolId")
	private int SchoolId;

	@Column(name = "ThumbnailPhoto")
	private byte[] ThumbnailPhoto;

	@Column(name = "ClassId")
	private int ClassId;

	@Column(name = "ParentId")
	private int ParentId;

	@Column(name = "RollNo")
	private String RollNo;

	@Column(name = "TrackedStatus")
	private String TrackedStatus;

	@Column(name = "PickupPointId")
	private int PickupPointId;

	@Column(name = "PickupPoint")
	private String PickupPoint;

	@Column(name = "ImageStr")
	private String ImageStr;

	@Column(name = "Distance")
	private String Distance;

	@Column(name = "BusId")
	private int BusId;

	@Column(name = "UId")
	private int UId;

	@Column(name = "PickupAlert")
	private String pickupAlert;
	@Column(name = "DropAlert")
	private String DropAlert;
	@Column(name = "DropPoint")
	private String DropPoint;
	@Column(name = "DropPointId")
	private int DropPointId;

	@Column(name = "RecordStatus")
	private String RecordStatus;

	@Column(name = "ProfilePicture")
	private String ProfilePicture;

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getStudentFirstName() {
		return StudentFirstName;
	}

	public void setStudentFirstName(String studentFirstName) {
		StudentFirstName = studentFirstName;
	}

	public String getStudentLastName() {
		return StudentLastName;
	}

	public void setStudentLastName(String studentLastName) {
		StudentLastName = studentLastName;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public int getClassId() {
		return ClassId;
	}

	public void setClassId(int classId) {
		ClassId = classId;
	}

	public String getTrackedStatus() {
		return TrackedStatus;
	}

	public void setTrackedStatus(String trackedStatus) {
		TrackedStatus = trackedStatus;
	}

	public int getPickupPointId() {
		return PickupPointId;
	}

	public void setPickupPointId(int pickupPointId) {
		PickupPointId = pickupPointId;
	}

	public String getPickupPoint() {
		return PickupPoint;
	}

	public void setPickupPoint(String pickupPoint) {
		PickupPoint = pickupPoint;
	}

	public Blob getPhoto() {
		return Photo;
	}

	public void setPhoto(Blob photo) {
		Photo = photo;
	}

	public String getImageStr() {
		return ImageStr;
	}

	public void setImageStr(String imageStr) {
		ImageStr = imageStr;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getDistance() {
		return Distance;
	}

	public void setDistance(String distance) {
		Distance = distance;
	}

	public int getUId() {
		return UId;
	}

	public void setUId(int uId) {
		UId = uId;
	}

	public String getPickupAlert() {
		return pickupAlert;
	}

	public void setPickupAlert(String pickupAlert) {
		this.pickupAlert = pickupAlert;
	}

	public String getDropAlert() {
		return DropAlert;
	}

	public void setDropAlert(String dropAlert) {
		DropAlert = dropAlert;
	}

	public String getDropPoint() {
		return DropPoint;
	}

	public void setDropPoint(String dropPoint) {
		DropPoint = dropPoint;
	}

	public int getDropPointId() {
		return DropPointId;
	}

	public void setDropPointId(int dropPointId) {
		DropPointId = dropPointId;
	}

	public String getRollNo() {
		return RollNo;
	}

	public void setRollNo(String rollNo) {
		RollNo = rollNo;
	}

	public int getParentId() {
		return ParentId;
	}

	public void setParentId(int parentId) {
		ParentId = parentId;
	}

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public byte[] getThumbnailPhoto() {
		return ThumbnailPhoto;
	}

	public void setThumbnailPhoto(byte[] thumbnailPhoto) {
		ThumbnailPhoto = thumbnailPhoto;
	}

}
