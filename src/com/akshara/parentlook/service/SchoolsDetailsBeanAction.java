package com.akshara.parentlook.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class SchoolsDetailsBeanAction extends ActionSupport
		implements ModelDriven<com.akshara.parentlook.db.bean.SchoolsDetailsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(SchoolsDetailsBeanAction.class);
	private int schActive;
	private int schInActive;
	private int schAdminActive;
	private int schAdminInActive;
	private File PhotoFile;

	public File getPhotoFile() {
		return PhotoFile;
	}

	public void setPhotoFile(File photoFile) {
		PhotoFile = photoFile;
	}

	private List<SchoolsDetailsBean> schoolsList = new ArrayList<SchoolsDetailsBean>();
	SchoolsDetailsBean schoolsDetailsBean;
	private List<SchoolAdminsBean> adminList = new ArrayList<SchoolAdminsBean>();

	public List<SchoolAdminsBean> getAdminList() {
		return adminList;
	}

	public void setAdminList(List<SchoolAdminsBean> adminList) {
		this.adminList = adminList;
	}

	private Map<String, Object> userSession;
	private int stucount;

	public SchoolsDetailsBean getModel() {
		schoolsDetailsBean = new SchoolsDetailsBean();
		return schoolsDetailsBean;
	}

	@Override
	public String execute() throws Exception {

		try {
			int a = SmsBeanDao.saveSchoolsDetails(schoolsDetailsBean, PhotoFile);

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String schoolsList() throws Exception {
		try {
			schoolsList = SmsBeanDao.getSchoolDetailsList();
			
			
			
		
			
			
			
			
			
			
			
			adminList = SmsBeanDao.getAdminList();
			if (schoolsList.size() > 0) {

				userSession.put("SCHOOLSLIST", schoolsList);
				schActive = 0;
				schInActive = 0;

				Iterator itr = schoolsList.iterator();
				while (itr.hasNext()) {
					SchoolsDetailsBean bean = (SchoolsDetailsBean) itr.next();
					List<BusesBean> listBusesBean = new ArrayList<BusesBean>();
					listBusesBean=SmsBeanDao.getBusDetailsList(bean.getSchoolId());
					List<ParentsBean> parentslist=new ArrayList<ParentsBean>();
					parentslist=SmsBeanDao.getParentsList(bean.getSchoolId());
					List<StudentsBean> studentlist=new ArrayList<StudentsBean>();
					studentlist=SmsBeanDao.getStudentDetailsList(bean.getSchoolId());
					if(listBusesBean.size()>0 && parentslist.size()>0 && studentlist.size()>0){
						bean.setUsedStatus("ACTIVE");
					}else{
						bean.setUsedStatus("INACTIVE");
					}
					
					
					
					
					
					if (bean.getStatus()) {
						schActive++;

					} else {
						schInActive++;
					}

				}
			}
			if (adminList.size() > 0) {
				schAdminActive = 0;
				schAdminInActive = 0;
				Iterator itr1 = adminList.iterator();
				while (itr1.hasNext()) {
					SchoolAdminsBean bean1 = (SchoolAdminsBean) itr1.next();
					if (bean1.getAdminStatus().equals("ACTIVE")) {
						schAdminActive++;
					} else {
						schAdminInActive++;
					}

				}
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;

	}

	public String add() {
		return "addschool";

	}

	public String getSchool() {
		schoolsList = SmsBeanDao.getSchoolDetailsList();
		return "school";
	}

	public int getSchActive() {
		return schActive;
	}

	public void setSchActive(int schActive) {
		this.schActive = schActive;
	}

	public int getSchInActive() {
		return schInActive;
	}

	public void setSchInActive(int schInActive) {
		this.schInActive = schInActive;
	}

	public int getSchAdminActive() {
		return schAdminActive;
	}

	public void setSchAdminActive(int schAdminActive) {
		this.schAdminActive = schAdminActive;
	}

	public int getSchAdminInActive() {
		return schAdminInActive;
	}

	public void setSchAdminInActive(int schAdminInActive) {
		this.schAdminInActive = schAdminInActive;
	}

	public String addSchool() throws Exception {
		return SUCCESS;

	}

	public List<SchoolsDetailsBean> getSchoolsList() {
		return schoolsList;
	}

	public void setSchoolsList(List<SchoolsDetailsBean> schoolsList) {
		this.schoolsList = schoolsList;
	}

	@Override
	public void setSession(Map<String, Object> userSession) {
		this.userSession = userSession;

	}

	public int getStucount() {
		return stucount;
	}

	public void setStucount(int stucount) {
		this.stucount = stucount;
	}

}
