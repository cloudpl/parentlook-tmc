package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportBusesExcel extends ActionSupport implements SessionAware {
	private InputStream inputStream;
	private Map<String, Object> usersession;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Bus");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("BusId");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("BusName");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("Route");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("Driver");
		sheet.autoSizeColumn(3);
		rowhead.createCell(4).setCellValue("RegNo");
		sheet.autoSizeColumn(4);
		rowhead.createCell(5).setCellValue("StartTime");
		sheet.autoSizeColumn(5);
		rowhead.createCell(6).setCellValue("ReturnTime");
		sheet.autoSizeColumn(6);
		rowhead.createCell(7).setCellValue("StartingPoint");
		sheet.autoSizeColumn(7);

		int a = (Integer) usersession.get("schoolId");
		List<BusesBean> buslist = new ArrayList<BusesBean>();
		buslist = SmsBeanDao.getBusDetailsList(a);
		int x = 1;
		Iterator<BusesBean> iterator = buslist.iterator();
		while (iterator.hasNext()) {

			BusesBean bean = (BusesBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			int busid = bean.getBusId();
			String busname = bean.getBusName();
			String regno = bean.getRegNumber();
			String starttime = bean.getReturnStartTime();
			String rettime = bean.getReturnArrivalTime();
			String startingpoint = bean.getStartPointLocation();
			row.createCell(0).setCellValue(busid);
			row.createCell(1).setCellValue(busname);
			row.createCell(4).setCellValue(regno);
			row.createCell(5).setCellValue(starttime);

			row.createCell(6).setCellValue(rettime);
			row.createCell(7).setCellValue(startingpoint);

			int routeid = bean.getRouteId();
			RoutesBean routesBean = new RoutesBean();
			routesBean = SmsBeanDao.getRouteDetails(routeid);
			if (routesBean != null) {
				String routename = routesBean.getRouteName();
				row.createCell(2).setCellValue(routename);
			}
			int driverid = bean.getDriverId();
			DriversBean driversBean = new DriversBean();
			driversBean = SmsBeanDao.getDriverDetailsbyID(driverid);
			if (driversBean != null) {
				String drivername = driversBean.getFirstName() + driversBean.getLastName();

				row.createCell(3).setCellValue(drivername);

			}

			x++;
		}
		autoSizeColumn(workbook);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return null;
	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
