package com.akshara.parentlook.service;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ViewClassDetailsAction extends ActionSupport {
	private static final Logger LOG = Logger.getLogger(ViewClassDetailsAction.class);
	private int classid;
	ClassBean classbean;

	public int getClassid() {
		return classid;
	}

	public void setClassid(int classid) {
		this.classid = classid;
	}

	public ClassBean getClassbean() {
		return classbean;
	}

	public void setClassbean(ClassBean classbean) {
		this.classbean = classbean;
	}

	@Override
	public String execute() throws Exception {
		try {
			classbean = SmsBeanDao.getClassesDetailsbyId(classid);
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

}
