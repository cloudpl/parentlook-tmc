package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class GetSchoolDetails extends ActionSupport implements SessionAware, ModelDriven<SchoolsDetailsBean> {

	private static final Logger LOG = Logger.getLogger(GetSchoolDetails.class);
	SchoolsDetailsBean schoolDetailsBean;

	@Override
	public SchoolsDetailsBean getModel() {
		schoolDetailsBean = new SchoolsDetailsBean();
		return schoolDetailsBean;
	}

	private Map<String, Object> usersession;

	@Override
	public String execute() throws Exception {
		try {
			SchoolsDetailsBean one = SmsBeanDao.getSchoolDetails(schoolDetailsBean.getSchoolId());
			BeanUtils.copyProperties(schoolDetailsBean, one);
			usersession.put("schoolId", schoolDetailsBean.getSchoolId());
			usersession.put("schoolName", schoolDetailsBean.getSchoolName());
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

}
