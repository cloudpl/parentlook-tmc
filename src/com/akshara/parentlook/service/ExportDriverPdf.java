package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportDriverPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("Driver Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("                   ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() throws SQLException, BadElementException, MalformedURLException, IOException {
		PdfPTable table = new PdfPTable(10);
		int a = (Integer) usersession.get("schoolId");
		List<DriversBean> driverList = new ArrayList<DriversBean>();
		driverList = SmsBeanDao.getDriverDetailsList(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

		table.addCell(new Phrase("DriverId", boldFont));
		table.addCell(new Phrase("DriverName", boldFont));
		table.addCell(new Phrase(" ", boldFont));
		table.addCell(new Phrase("MobileNumber", boldFont));
		table.addCell(new Phrase("Reference", boldFont));
		table.addCell(new Phrase("LicenceNo", boldFont));
		table.addCell(new Phrase(" ", boldFont));
		table.addCell(new Phrase("IdType", boldFont));
		table.addCell(new Phrase(" ", boldFont));
		table.addCell(new Phrase("CurrentAddress", boldFont));

		Iterator<DriversBean> iterator = driverList.iterator();
		while (iterator.hasNext()) {
			DriversBean bean = (DriversBean) iterator.next();
			if (bean != null) {
				if (bean.getRecordStatus().equals("ACTIVE")) {

					Integer driverId = bean.getDriverId();
					String id = driverId.toString();
					table.addCell(id);
					String name = bean.getFirstName() + bean.getLastName();
					table.addCell(name);
					Blob imageBlob = bean.getPhoto();
					if (imageBlob != null) {
						Image image = null;
						byte[] imageBytes3 = imageBlob.getBytes(1, (int) imageBlob.length());
						image = Image.getInstance(imageBytes3);
						table.addCell(image);
					} else {
						String image = "   ";
						table.addCell(image);
					}
					String mobileNumber = bean.getContactNumber();
					table.addCell(mobileNumber);

					String ref = bean.getRelationTitle() + bean.getRelativeName();
					table.addCell(ref);
					String licence = bean.getLicenceNumber();
					table.addCell(licence);
					Blob licBlob = bean.getLicenceAttachment();
					if (licBlob != null) {
						Image licenceimage = null;
						byte[] imageBytes1 = licBlob.getBytes(1, (int) licBlob.length());
						licenceimage = Image.getInstance(imageBytes1);
						table.addCell(licenceimage);
					} else {
						String licenceimg = "    ";
						table.addCell(licenceimg);

					}

					String idtype = bean.getIdType();
					table.addCell(idtype);
					Blob idtypeBlob = bean.getIdTypeAttachment();
					if (idtypeBlob != null) {
						Image idtypeimage = null;
						byte[] imageBytes2 = idtypeBlob.getBytes(1, (int) idtypeBlob.length());
						idtypeimage = Image.getInstance(imageBytes2);

						table.addCell(idtypeimage);
					} else {
						String idtypeimg = "    ";
						table.addCell(idtypeimg);

					}
					String current = bean.getCurrentAddress();
					table.addCell(current);

				}
			}
		}
		return table;

	}

}
