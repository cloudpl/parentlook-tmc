package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class RootsDetailActionbySchoolId extends ActionSupport implements SessionAware {
	private static final Logger LOG = Logger.getLogger(RootsDetailActionbySchoolId.class);
	private Map<String, Object> usersession;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	private String RouteName;
	List<BusesBean> buseslist = new ArrayList<>();
	List<RoutesBean> routelist = new ArrayList<>();

	@Override
	public String execute() throws Exception {
		try {
			int schoolid = (Integer) usersession.get("schoolId");
			routelist = SmsBeanDao.getRoutesList(schoolid);

			buseslist = SmsBeanDao.getBusDetailsList(schoolid);
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public List<RoutesBean> getRoutelist() {
		return routelist;
	}

	public void setRoutelist(List<RoutesBean> routelist) {
		this.routelist = routelist;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public List<BusesBean> getBuseslist() {
		return buseslist;
	}

	public void setBuseslist(List<BusesBean> buseslist) {
		this.buseslist = buseslist;
	}

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

}
