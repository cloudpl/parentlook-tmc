package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.GPSTrackingWebBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ManageDevice extends ActionSupport implements SessionAware{
	private Map<String,Object> usersession;

	private List<GPSTrackingWebBean> gpsTrackingWebBeans=new ArrayList<GPSTrackingWebBean>();

	@Override
	public String execute() throws Exception {
		int a = (int) usersession.get("schoolId");
		List<BusesBean> busbean=new ArrayList<BusesBean>();
		busbean=SmsBeanDao.getBusDetailsList(a);
		Iterator itr=busbean.iterator();
		while (itr.hasNext()) {
		BusesBean busbean1=(BusesBean) itr.next();
	 GPSTrackingBean gpsTrackingBean=null;
 GPSTrackingWebBean gpswebbean=new GPSTrackingWebBean();
		gpsTrackingBean=SmsBeanDao.getgpsdetails(busbean1.getBusId());
		if(gpsTrackingBean!=null){
		BusesBean busBean1=SmsBeanDao.getBusDetails(gpsTrackingBean.getBusId());
		gpswebbean.setBusId(gpsTrackingBean.getBusId());
	gpswebbean.setBusName(busBean1.getBusName());
	gpswebbean.setUserName(gpsTrackingBean.getUserName());
	gpswebbean.setPassword(gpsTrackingBean.getPassword());
	gpswebbean.setLoginstatus(gpsTrackingBean.getLoginStatus());
	gpsTrackingWebBeans.add(gpswebbean);
		}
		
		}
		return "success";
	}

	@Override
	public void setSession(Map<String, Object> usersession) {

		this.usersession=usersession;
	}

	public List<GPSTrackingWebBean> getGpsTrackingWebBeans() {
		return gpsTrackingWebBeans;
	}

	public void setGpsTrackingWebBeans(List<GPSTrackingWebBean> gpsTrackingWebBeans) {
		this.gpsTrackingWebBeans = gpsTrackingWebBeans;
	}

	 
}
