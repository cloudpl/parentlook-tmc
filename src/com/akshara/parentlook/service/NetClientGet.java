package com.akshara.parentlook.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

public class NetClientGet {

	private static final Logger LOG = Logger.getLogger(NetClientGet.class);

	public static void main(String[] args) {

		String output = null;
		try {

			URL url = new URL(
					"https://maps.googleapis.com/maps/api/directions/json?origin=KPHB%20Phase%2015%20%20Kukatpally&destination=Janapriya%20High%20School,%20Miyapur,%20Hyderabad,%20Telangana%20500049,%20India&waypoints=optimize:true|%20KPHB%20Phase%2015%20%20Kukatpally|%20Madhuban,%20Nizampet%20Rd,%20Nizampet,%20Hyderabad,%20Telangana%20500090,%20India|17,%20Nizampet%20Rd,%20Jai%20Bharat%20Nagar,%20Nagarjuna%20Homes,%20Kukatpally,%20Hyderabad,%20Telangana,%20India|9/5/97,%20Nizampet%20Rd,%20Jai%20Bharat%20Nagar,%20Hyder%20Nagar,%20Brindavan%20Colony,%20Nizampet,%20Hyderabad,%20Telangana%20500090,%20India|3-82,%20Nizampet%20Rd,%20Nizampet,%20Hyderabad,%20Telangana%20500090,%20India|&key=AIzaSyBmDYBawBkUUGIqUoHs72EdlK3UugfhGM4");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			while ((output = br.readLine()) != null) {
			}

			conn.disconnect();

		} catch (MalformedURLException e) {

			LOG.error(e);

		} catch (IOException e) {

			LOG.error(e);

		}

	}

}