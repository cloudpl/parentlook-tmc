package com.akshara.parentlook.service;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.service.SendAndroidNotification;

public class ApproveDrop extends ActionSupport {

	private static final Logger LOG = Logger.getLogger(ApprovePickup.class);

	private int StudentId;

	private int PickupPointId;

	@Override
	public String execute() throws Exception {

		try {
			PickupPoints points = SmsBeanDao
					.getPickupPointDetails(PickupPointId);
			StudentsBean studentsBean = SmsBeanDao.updateDropPointForStudent(
					StudentId, points);
			
			List ls=SmsBeanDao.getUserGcmId(studentsBean);
			 Iterator iterator=ls.iterator();
			 while(iterator.hasNext()){
				 String gcmId=(String) iterator.next();
				 System.out.println(gcmId);
			
			 SendAndroidNotification.sendNotification(studentsBean.getStudentFirstName()+" Droppoint Approved ", gcmId);
			 }
			
			

		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public int getPickupPointId() {
		return PickupPointId;
	}

	public void setPickupPointId(int pickupPointId) {
		PickupPointId = pickupPointId;
	}

}
