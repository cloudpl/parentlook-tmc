package com.akshara.parentlook.service;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class SaveRouteDetails extends ActionSupport {

	private static final Logger LOG = Logger.getLogger(SaveRouteDetails.class);

	private String StartPoint;
	private String EndPoint;
	private String RouteName;
	private int SchoolId;

	@Override
	public String execute() throws Exception {

		try {

			int i = SmsBeanDao.saveRoute(SchoolId, StartPoint, EndPoint, RouteName);

		} catch (Exception e) {
			LOG.error(e);
		}

		return super.execute();
	}

	public String getStartPoint() {
		return StartPoint;
	}

	public void setStartPoint(String startPoint) {
		StartPoint = startPoint;
	}

	public String getEndPoint() {
		return EndPoint;
	}

	public String getRouteName() {
		return RouteName;
	}

	public void setRouteName(String routeName) {
		RouteName = routeName;
	}

	public void setEndPoint(String endPoint) {
		EndPoint = endPoint;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

}
