package com.akshara.parentlook.service;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ViewDriverDetailsAction extends ActionSupport implements ModelDriven<DriversBean>, SessionAware {
	private static final Logger LOG = Logger.getLogger(ViewClassDetailsAction.class);
	private int driverid;

	DriversBean driversbean;
	private String imageStr;
	private String idStr;
	private String licenceStr;
	private Map<String, Object> usersession;

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public DriversBean getModel() {
		driversbean = new DriversBean();
		return driversbean;
	}

	@Override
	public String execute() throws Exception {
		try {
			driversbean = SmsBeanDao.getDriverDetailsbyID(driverid);

			if (driversbean.getPhoto() != null && driversbean.getIdTypeAttachment() != null
					&& driversbean.getLicenceAttachment() != null) {
				byte[] itemImage = driversbean.getPhoto().getBytes(1, (int) driversbean.getPhoto().length());
				byte[] idImage = driversbean.getIdTypeAttachment().getBytes(1,
						(int) driversbean.getIdTypeAttachment().length());
				byte[] licenceImage = driversbean.getLicenceAttachment().getBytes(1,
						(int) driversbean.getLicenceAttachment().length());

				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(itemImage);
				byte[] encodingIDImgAsBytes = b.encode(idImage);
				byte[] LicenceImageAsBytes = b.encode(licenceImage);
				String imagetype = b.encodeToString(encodingImgAsBytes);
				String licencetype = b.encodeToString(LicenceImageAsBytes);
				String idtype = b.encodeToString(encodingIDImgAsBytes);
				byte[] valueDecoded = Base64.decodeBase64(imagetype);
				byte[] idimageDecoded = Base64.decodeBase64(idtype);
				byte[] licecnceimageDecoded = Base64.decodeBase64(licencetype);
				String decodedImgStr = new String(valueDecoded);
				String decodedIdStr = new String(idimageDecoded);
				String decodedLicenceStr = new String(licecnceimageDecoded);

				imageStr = decodedImgStr;
				idStr = decodedIdStr;
				licenceStr = decodedLicenceStr;

			}

		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	public String getImageStr() {
		return imageStr;
	}

	public void setImageStr(String imageStr) {
		this.imageStr = imageStr;
	}

	public String getIdStr() {
		return idStr;
	}

	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}

	public String getLicenceStr() {
		return licenceStr;
	}

	public void setLicenceStr(String licenceStr) {
		this.licenceStr = licenceStr;
	}

	public int getDriverid() {
		return driverid;
	}

	public void setDriverid(int driverid) {
		this.driverid = driverid;
	}

	public DriversBean getDriversbean() {
		return driversbean;
	}

	public void setDriversbean(DriversBean driversbean) {
		this.driversbean = driversbean;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

}
