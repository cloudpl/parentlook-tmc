package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportClassExcel extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 1L;
	private Map<String, Object> usersession;
	private InputStream inputStream;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("class");

		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("ClassId");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("Class");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("Section");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("Description");
		sheet.autoSizeColumn(3);

		int a = (Integer) usersession.get("schoolId");
		List<ClassBean> classlist = new ArrayList<ClassBean>();
		classlist = SmsBeanDao.getClasesDetailsList(a);
		int x = 1;
		Iterator<ClassBean> iterator = classlist.iterator();
		while (iterator.hasNext()) {

			ClassBean bean = (ClassBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			int classid = bean.getClassId();
			String clas = bean.getStudentClass();
			String section = bean.getSection();
			String des = bean.getDescription();

			row.createCell(0).setCellValue(classid);
			row.createCell(1).setCellValue(clas);
			row.createCell(2).setCellValue(section);
			row.createCell(3).setCellValue(des);

			x++;
		}
		autoSizeColumn(workbook);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));

		return null;

	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
