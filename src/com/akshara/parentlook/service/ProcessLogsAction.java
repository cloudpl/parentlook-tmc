package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ProcessLogsAction extends ActionSupport implements SessionAware {

	Map<String, Object> usersesiion;

	@Override
	public void setSession(Map<String, Object> usersesiion) {
		this.usersesiion = usersesiion;
	}

	private Map<String, List> routesandbusesmap = new HashMap<>();
	private int route;

	@Override
	public String execute() throws Exception {
		int i = (int) usersesiion.get("schoolId");
		List<RoutesBean> rootlist = SmsBeanDao.getRoutesList(i);
		Iterator iterator = rootlist.iterator();
		while (iterator.hasNext()) {
			RoutesBean object = (RoutesBean) iterator.next();
			List<BusesBean> buseslist = SmsBeanDao.getBusRootList(object.getRouteId());

			routesandbusesmap.put(object.getStartPoint() + "-" + object.getEndPoint(), buseslist);
			route = object.getRouteId();
		}

		return SUCCESS;
	}

	public Map<String, List> getRoutesandbusesmap() {
		return routesandbusesmap;
	}

	public void setRoutesandbusesmap(Map<String, List> routesandbusesmap) {
		this.routesandbusesmap = routesandbusesmap;
	}

	public int getRoute() {
		return route;
	}

	public void setRoute(int route) {
		this.route = route;
	}

}
