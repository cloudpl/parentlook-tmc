package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class RouteNameCheck extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private String routename;
	private String msg;
	private RoutesBean routebean;

	@Override
	public String execute() throws Exception {
		int a = (int) usersession.get("schoolId");
		try {

			routebean = SmsBeanDao.checkRouteName(a, routename);
			if (routebean != null) {

				msg = "Exists";
			} else {
				msg = "NOT Exists";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public String getRoutename() {
		return routename;
	}

	public void setRoutename(String routename) {
		this.routename = routename;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public RoutesBean getRoutebean() {
		return routebean;
	}

	public void setRoutebean(RoutesBean routebean) {
		this.routebean = routebean;
	}

}
