package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportStopPointApprovedExcel extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private InputStream inputStream;
	private final String RecordStatus = "ACTIVE";

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("StopPontApproved");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("StudentName");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("Class");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("Location");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("Pickup Point");
		sheet.autoSizeColumn(3);
		rowhead.createCell(4).setCellValue("Drop-off Ponit");
		sheet.autoSizeColumn(4);
		rowhead.createCell(5).setCellValue("RouteName");
		sheet.autoSizeColumn(5);
		rowhead.createCell(6).setCellValue("BusName");
		sheet.autoSizeColumn(6);

		String status = "1";

		int a = (int) usersession.get("schoolId");

		List<StudentsBean> bean = SmsBeanDao.getStudentDetailsListByTrackedStatus(a, status, RecordStatus);
		int x = 1;
		Iterator iterator = bean.iterator();
		while (iterator.hasNext()) {
			StudentsBean studentsBean = (StudentsBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			if (studentsBean != null) {
				String name = studentsBean.getStudentFirstName() + studentsBean.getStudentLastName();
				String pick = studentsBean.getPickupPoint();
				String drop = studentsBean.getDropPoint();
				row.createCell(0).setCellValue(name);
				row.createCell(3).setCellValue(pick);
				row.createCell(4).setCellValue(drop);

				int classid = studentsBean.getClassId();
				ClassBean bean2 = new ClassBean();

				bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
				if (bean2 != null) {
					String classname = bean2.getStudentClass();
					row.createCell(1).setCellValue(classname);
				}
				WayPointsBean pointsBean = SmsBeanDao.getWayPoint(studentsBean.getStudentId());
				if (pointsBean != null) {
					String addr = pointsBean.getWayPoint();
					row.createCell(2).setCellValue(addr);
				}
				BusesBean busBean = SmsBeanDao.getBusId(studentsBean.getBusId());
				if (busBean != null) {
					String busname = busBean.getBusName();
					row.createCell(6).setCellValue(busname);
				}
				if (busBean != null) {
					int routeid = busBean.getRouteId();
					RoutesBean routbean = SmsBeanDao.getRoute(routeid);
					if (routbean != null) {
						String route = routbean.getRouteName();

						row.createCell(5).setCellValue(route);

					}
				}
			}

			x++;
		}
		autoSizeColumn(workbook);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return null;
	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
