package com.akshara.parentlook.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.GPSTrackingWebBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class SchoolTrackingVideo extends ActionSupport implements SessionAware{

	private int schoolId;
	private String schoolname;
	List<GPSTrackingWebBean> gpsList = new ArrayList<GPSTrackingWebBean>();
    private Map<String,Object> usersession;
	public String execute() throws Exception {
		SchoolsDetailsBean schoolbean=SmsBeanDao.getSchoolName(schoolId);
		schoolname=schoolbean.getSchoolName();
		System.out.println(schoolbean.getSchoolName()+"(((((((((((((((((((((((((((");
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		dateFormat.format(date);
		System.out.println(dateFormat.format(date)+"00000000000000000000000");
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = new Date();
		List<BusesBean> buseslist = new ArrayList<BusesBean>();
		buseslist = SmsBeanDao.getBusDetailsList(schoolId);
		
		if (buseslist.size() > 0) {
			Iterator iterator2 = buseslist.iterator();
			while (iterator2.hasNext()) {
				System.out.println(gpsList.size()+"$$$$$$$$$$$$$$$$$$$$");
				BusesBean busBean = (BusesBean) iterator2.next();
				System.out.println("*******BusID*******" + busBean.getBusId());
				GPSTrackingBean gpsTrackingBean = new GPSTrackingBean();
				GPSTrackingWebBean gpswebBean = new GPSTrackingWebBean();
				String tbegin = busBean.getReturnStartTime();
				String treturn = busBean.getTransportStartTime();
				String str1 = tbegin.replaceAll("\\s", "");
				String str2 = treturn.replaceAll("\\s", "");
				Date d1 = dateFormat.parse(str1);
				Date d2 = dateFormat.parse(str2);
				gpsTrackingBean = SmsBeanDao.getgpsdetails(busBean.getBusId());
				if (gpsTrackingBean!=null) {
					System.out.println(gpsList.size()+"gpssssssssssslisttttt");
					if (dateFormat.parse(dateFormat.format(date)).after(dateFormat.parse(dateFormat.format(d1)))) {

						String dir = "Drop";
						String type = "Start Point";
						

						HistoryLogBean historyBean = SmsBeanDao.getHistoryDeatils(gpsTrackingBean.getBusId(),
								dateFormat1.format(date1), dir, type);
						if (historyBean != null) {
							
							break;
						} else {
							System.out.println(gpsTrackingBean.getBusId() + "busiddd");
							BusesBean busbean1 = SmsBeanDao.getBusDetails(gpsTrackingBean.getBusId());
							System.out.println("8888888gpsbusname88888888888888888" + busbean1.getBusName());
							DriversBean driverbean=SmsBeanDao.getDriverDetailsbyID(busbean1.getDriverId());
							gpswebBean.setDriverName(driverbean.getFirstName()+driverbean.getLastName());
							gpswebBean.setDirContactNum(driverbean.getContactNumber());
							gpswebBean.setBusName(busbean1.getBusName());
							if (gpsTrackingBean.getTracking() == 0) {
								 
                                gpswebBean.setTracking("off");
								gpswebBean.setTripStatus("Drop");
								if (gpsTrackingBean.getSurveillance() == 0) {
									gpswebBean.setVideo("off");
								}else{
									gpswebBean.setVideo("on");
								}
								
								gpsList.add(gpswebBean);
							}
							
							
							
						}

					} else if (dateFormat.parse(dateFormat.format(date))
							.after(dateFormat.parse(dateFormat.format(d2)))) {
						System.out.println("&&&&&&&&&&&&&&&&&&Pickup&&&&&&&&&&&&&&&&&&&&&&&&&&&");
						String dir1 = "Pickup";
						String type1 = "School Point";
						
						HistoryLogBean historyBean = SmsBeanDao.getHistoryDeatils(gpsTrackingBean.getBusId(),
								dateFormat1.format(date1), dir1, type1);
						if (historyBean != null) {
							break;
						} else {
							BusesBean busbean1 = SmsBeanDao.getBusDetails(gpsTrackingBean.getBusId());
							System.out.println("8888888gpsbusname88888888888888888" + busbean1.getBusName());
							DriversBean driverbean=SmsBeanDao.getDriverDetailsbyID(busbean1.getDriverId());
							gpswebBean.setDriverName(driverbean.getFirstName()+driverbean.getLastName());
							gpswebBean.setDirContactNum(driverbean.getContactNumber());
							gpswebBean.setBusName(busbean1.getBusName());
							System.out.println(gpsTrackingBean.getBusId() + "busiddd");

							if (gpsTrackingBean.getTracking() == 0) {
								gpswebBean.setTracking("off");
								gpswebBean.setTripStatus("Pickup");
								if (gpsTrackingBean.getSurveillance() == 0) {
									gpswebBean.setVideo("off");
								}else{
									gpswebBean.setVideo("on");
								}
								gpsList.add(gpswebBean);
							}
							
						}

					}
				}else{
					return SUCCESS;
				}

				

			}
		}
		return SUCCESS;

	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public List<GPSTrackingWebBean> getGpsList() {
		return gpsList;
	}

	public void setGpsList(List<GPSTrackingWebBean> gpsList) {
		this.gpsList = gpsList;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		// TODO Auto-generated method stub
		this.usersession=usersession;
	}

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}
}
