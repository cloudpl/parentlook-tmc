package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ViewRouteDetailsAction extends ActionSupport {

	private int routeid;
	RoutesBean routebean;

	@Override
	public String execute() throws Exception {
		routebean = SmsBeanDao.getRoute(routeid);
		return SUCCESS;
	}

	public String editRoute() {
		routebean = SmsBeanDao.editRoute(routeid);
		return "edit";

	}

	public String deleteRoute() {
		List<BusesBean> listbusbean = new ArrayList<BusesBean>();
		listbusbean = SmsBeanDao.getBusList(routeid);
		if (listbusbean.size() > 0) {
			return "list";
		} else {
			try {
				boolean b = SmsBeanDao.deleteroute(routeid);
				if (b == true) {
					return "deleteroute";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return SUCCESS;

	}

	public int getRouteid() {
		return routeid;
	}

	public void setRouteid(int routeid) {
		this.routeid = routeid;
	}

	public RoutesBean getRoutebean() {
		return routebean;
	}

	public void setRoutebean(RoutesBean routebean) {
		this.routebean = routebean;
	}

}
