package com.akshara.parentlook.service;

import java.io.File;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class UpdateDriverDetailsAction extends ActionSupport implements SessionAware, ModelDriven<DriversBean> {
	Map<String, Object> usersession;
	DriversBean driversbean;
	private File PhotoFile;
	private File idattachmentfile;
	private File licenceattachmentfile;

	@Override
	public DriversBean getModel() {
		driversbean = new DriversBean();
		return driversbean;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	@Override
	public String execute() throws Exception {
		boolean b = false;
		try {
			int a = (Integer) usersession.get("schoolId");

			b = SmsBeanDao.updateDriverDetails(driversbean, PhotoFile, idattachmentfile, licenceattachmentfile, a);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (b == true) {
			return SUCCESS;
		}
		return INPUT;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public DriversBean getDriversbean() {
		return driversbean;
	}

	public void setDriversbean(DriversBean driversbean) {
		this.driversbean = driversbean;
	}

	public File getPhotoFile() {
		return PhotoFile;
	}

	public void setPhotoFile(File photoFile) {
		PhotoFile = photoFile;
	}

	public File getIdattachmentfile() {
		return idattachmentfile;
	}

	public void setIdattachmentfile(File idattachmentfile) {
		this.idattachmentfile = idattachmentfile;
	}

	public File getLicenceattachmentfile() {
		return licenceattachmentfile;
	}

	public void setLicenceattachmentfile(File licenceattachmentfile) {
		this.licenceattachmentfile = licenceattachmentfile;
	}

}
