package com.akshara.parentlook.service;

import java.io.File;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class EditParentAction extends ActionSupport implements ModelDriven<ParentsBean>, SessionAware {
	private static final Logger LOG = Logger.getLogger(EditParentAction.class);
	private ParentsBean parentsBean;
	private File PhotoFile1;
	Map<String, Object> usersession;

	public ParentsBean getParentsBean() {
		return parentsBean;
	}

	public void setParentsBean(ParentsBean parentsBean) {
		this.parentsBean = parentsBean;
	}

	public int getId1() {
		return id1;
	}

	public void setId1(int id1) {
		this.id1 = id1;
	}

	private int id1;

	@Override
	public ParentsBean getModel() {
		parentsBean = new ParentsBean();
		return parentsBean;
	}

	@Override
	public String execute() throws Exception {
		try {
			parentsBean = SmsBeanDao.editParent(id1);

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String updateParent() {
		try {

			int a = parentsBean.getParentId();

			SmsBeanDao.updateParentDetails(parentsBean, a, PhotoFile1);

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public File getPhotoFile1() {
		return PhotoFile1;
	}

	public void setPhotoFile1(File photoFile1) {
		PhotoFile1 = photoFile1;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

}
