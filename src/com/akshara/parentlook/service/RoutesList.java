package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class RoutesList extends ActionSupport implements SessionAware {

	private static final Logger LOG = Logger.getLogger(RoutesList.class);

	private int schoolId;
	private String schoolName;
	private Map<String, Object> usersession;

	List<RoutesBean> routesList = new ArrayList<RoutesBean>();
	List<RoutesBean> routesList1 = new ArrayList<RoutesBean>();

	private List<SchoolsDetailsBean> schoolIdList = new ArrayList<SchoolsDetailsBean>();

	@Override
	public String execute() throws Exception {
		try {
			schoolId = (Integer) usersession.get("schoolId");
			routesList1 = SmsBeanDao.getRoutesList(schoolId);

			Iterator iterator = routesList1.iterator();
			while (iterator.hasNext()) {
				List<BusesBean> busbean = new ArrayList<BusesBean>();
				RoutesBean routebean = (RoutesBean) iterator.next();
				busbean = SmsBeanDao.getBusList(routebean.getRouteId());
				RoutesBean routebeanFin = new RoutesBean();
				BeanUtils.copyProperties(routebeanFin, routebean);
				if (busbean.size() > 0) {
					routebeanFin.setUsedStatus("ACTIVE");

				} else {
					routebeanFin.setUsedStatus("INACTIVE");
				}
				routesList.add(routebeanFin);
			}

		} catch (Exception e) {
			LOG.error(e);
		}

		return super.execute();
	}

	public String addRoute() {

		schoolId = (Integer) usersession.get("schoolId");
		schoolName = (String) usersession.get("schoolName");
		schoolIdList = SmsBeanDao.getSchoolDetailsList();
		return SUCCESS;
	}

	public List<RoutesBean> getRoutesList() {
		return routesList;
	}

	public void setRoutesList(List<RoutesBean> routesList) {
		this.routesList = routesList;
	}

	public List<SchoolsDetailsBean> getSchoolIdList() {
		return schoolIdList;
	}

	public void setSchoolIdList(List<SchoolsDetailsBean> schoolIdList) {
		this.schoolIdList = schoolIdList;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

}
