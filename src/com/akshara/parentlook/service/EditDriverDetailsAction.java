package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class EditDriverDetailsAction extends ActionSupport implements SessionAware, ModelDriven<DriversBean> {
	private int driverid;
	List<String> idtypes = new ArrayList<String>();
	List<String> relation = new ArrayList<String>();

	public List<String> getIdtypes() {
		return idtypes;
	}

	public void setIdtypes(List<String> idtypes) {
		this.idtypes = idtypes;
	}

	DriversBean driversbean;
	Map<String, Object> usersession;
	private String imageStr;
	private String idStr;
	private String licenceStr;

	public String getImageStr() {
		return imageStr;
	}

	public void setImageStr(String imageStr) {
		this.imageStr = imageStr;
	}

	public String getIdStr() {
		return idStr;
	}

	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}

	public String getLicenceStr() {
		return licenceStr;
	}

	public void setLicenceStr(String licenceStr) {
		this.licenceStr = licenceStr;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {
		try {
			System.out.println("driverid=" + driverid);
			driversbean = SmsBeanDao.getDriverDetailsbyID(driverid);
			idtypes.add("aadharcard");
			idtypes.add("pancard");
			idtypes.add("voterid");
			idtypes.add("passport");

			relation.add("S/o");
			relation.add("D/o");
			relation.add("W/o");
			relation.add("C/o");

			if (driversbean.getPhoto() != null) {
				byte[] itemImage = driversbean.getPhoto().getBytes(1, (int) driversbean.getPhoto().length());

				Base64 b = new Base64();
				byte[] encodingImgAsBytes = b.encode(itemImage);

				String imagetype = b.encodeToString(encodingImgAsBytes);

				byte[] valueDecoded = Base64.decodeBase64(imagetype);

				String decodedImgStr = new String(valueDecoded);

				imageStr = decodedImgStr;

			}
			if (driversbean.getIdTypeAttachment() != null) {
				Base64 b = new Base64();
				byte[] idImage = driversbean.getIdTypeAttachment().getBytes(1,
						(int) driversbean.getIdTypeAttachment().length());
				byte[] encodingIDImgAsBytes = b.encode(idImage);

				String idtype = b.encodeToString(encodingIDImgAsBytes);
				byte[] idimageDecoded = Base64.decodeBase64(idtype);
				String decodedIdStr = new String(idimageDecoded);
				idStr = decodedIdStr;

			}
			if (driversbean.getLicenceAttachment() != null) {
				Base64 b = new Base64();
				byte[] licenceImage = driversbean.getLicenceAttachment().getBytes(1,
						(int) driversbean.getLicenceAttachment().length());
				byte[] LicenceImageAsBytes = b.encode(licenceImage);

				String licencetype = b.encodeToString(LicenceImageAsBytes);
				byte[] licecnceimageDecoded = Base64.decodeBase64(licencetype);
				String decodedLicenceStr = new String(licecnceimageDecoded);
				licenceStr = decodedLicenceStr;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public DriversBean getDriversbean() {
		return driversbean;
	}

	public void setDriversbean(DriversBean driversbean) {
		this.driversbean = driversbean;
	}

	public int getDriverid() {
		return driverid;
	}

	public void setDriverid(int driverid) {
		this.driverid = driverid;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public List<String> getRelation() {
		return relation;
	}

	public void setRelation(List<String> relation) {
		this.relation = relation;
	}

	@Override
	public DriversBean getModel() {
		driversbean = new DriversBean();
		return driversbean;
	}

}
