package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportLocationUnsubmittedExcel extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private final String RecordStatus = "ACTIVE";
	private InputStream inputStream;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("LocationUnsubmitted");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("StudentName");
		rowhead.createCell(1).setCellValue("ParentName");
		rowhead.createCell(2).setCellValue("Class");
		rowhead.createCell(3).setCellValue("MobileNo");

		String status = "3";

		int a = (int) usersession.get("schoolId");

		List<StudentsBean> bean = SmsBeanDao.getStudentDetailsListByTrackedStatus(a, status, RecordStatus);
		int x = 1;
		Iterator iterator = bean.iterator();
		while (iterator.hasNext()) {
			StudentsBean studentsBean = (StudentsBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			if (studentsBean != null) {
				String name = studentsBean.getStudentFirstName() + studentsBean.getStudentLastName();

				row.createCell(0).setCellValue(name);

				int classid = studentsBean.getClassId();
				ClassBean bean2 = new ClassBean();

				bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
				if (bean2 != null) {
					String classname = bean2.getStudentClass();
					row.createCell(2).setCellValue(classname);
				}
				ParentsBean parentBean = SmsBeanDao.getParentDetails(studentsBean.getParentId());
				if (parentBean != null) {

					String pname = parentBean.getFirstName() + parentBean.getLastName();
					String moblie = parentBean.getMobileNumber();

					row.createCell(1).setCellValue(pname);

					row.createCell(3).setCellValue(moblie);

				}
			}

			x++;
		}
		autoSizeColumn(workbook);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));

		return null;
	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
