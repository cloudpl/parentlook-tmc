package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.opensymphony.xwork2.ActionSupport;

public class AdminPasswordChecking extends ActionSupport implements
		SessionAware {

	private static final Logger LOG = Logger
			.getLogger(AdminPasswordChecking.class);

	private Map<String, Object> usersession;
	private SchoolAdminsBean adminBean;
	private String msg;
	private String NewPassword;

	public SchoolAdminsBean getAdminBean() {
		return adminBean;
	}

	public void setAdminBean(SchoolAdminsBean adminBean) {
		this.adminBean = adminBean;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getNewPassword() {
		return NewPassword;
	}

	public void setNewPassword(String newPassword) {
		NewPassword = newPassword;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		int a = (int) usersession.get("schoolId");
		try {
			adminBean = AdminDAO.checkPassword(NewPassword, a);
			if (adminBean != null) {

				msg = "Exists";
			} else {
				msg = "NOT Exists";
			}
		} catch (Exception e) {

			LOG.error(e);

		}

		return SUCCESS;
	}
}
