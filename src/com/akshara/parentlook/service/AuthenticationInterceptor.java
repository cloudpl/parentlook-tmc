package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

public class AuthenticationInterceptor implements Interceptor {

	private static final Logger LOG = Logger.getLogger(AuthenticationInterceptor.class);
	Map<String, Object> session;

	@Override
	public void destroy() {

	}

	@Override
	public void init() {

	}

	@Override
	public String intercept(ActionInvocation arg0) throws Exception {

		LOG.info("intercept(ActionInvocation)");
		try {
			session = ActionContext.getContext().getSession();
			Object object = session.get("adminId");
			if (object == null) {
				session.clear();
				
				return "invalied";
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return arg0.invoke();
	}

}
