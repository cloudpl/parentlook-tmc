package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ViewBusDetailsAction extends ActionSupport implements SessionAware {

	private int busid;
	BusesBean busbean;
	private Map<String, Object> usersession;
	private List<RoutesBean> routesList = new ArrayList<RoutesBean>();
	private List<DriversBean> driversBeans = new ArrayList<>();

	@Override
	public String execute() throws Exception {
		int a = (Integer) usersession.get("schoolId");
		setRoutesList(SmsBeanDao.getRoutesList(a));
		busbean = SmsBeanDao.getBusDetails(busid);
		int ab = Integer.parseInt(busbean.getSchoolId());
		busbean.getDriverId();
		setDriversBeans(SmsBeanDao.getDriverDetailsList(ab));
		return SUCCESS;
	}

	public String getbusdetailsbyId() {
		busbean = SmsBeanDao.getBusDetails(busid);

		return "editbus";
	}

	public String deleteBus() {
		List<StudentsBean> liststudentbean = new ArrayList<StudentsBean>();
		liststudentbean = SmsBeanDao.getStudentList(busid);
		if (liststudentbean.size() > 0) {
			return "list";
		} else {
			try {
				boolean b = SmsBeanDao.deletebus(busid);
				if (b == true) {
					return "delete";
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return SUCCESS;

	}

	public int getBusid() {
		return busid;
	}

	public void setBusid(int busid) {
		this.busid = busid;
	}

	public BusesBean getBusbean() {
		return busbean;
	}

	public void setBusbean(BusesBean busbean) {
		this.busbean = busbean;
	}

	public List<DriversBean> getDriversBeans() {
		return driversBeans;
	}

	public void setDriversBeans(List<DriversBean> driversBeans) {
		this.driversBeans = driversBeans;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {

		this.usersession = usersession;

	}

	public List<RoutesBean> getRoutesList() {
		return routesList;
	}

	public void setRoutesList(List<RoutesBean> routesList) {
		this.routesList = routesList;
	}

}
