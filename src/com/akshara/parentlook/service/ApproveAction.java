package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentViewBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ApproveAction extends ActionSupport implements SessionAware  {

 Map<String,Object> usersession;
 
 
 List<StudentViewBean> viewbean=new ArrayList<StudentViewBean>();
 
 List<StudentViewBean> stopviewbean=new ArrayList<StudentViewBean>();
 List<StudentViewBean> locviewbean=new ArrayList<StudentViewBean>();
 private final String RecordStatus="ACTIVE";
 
public List<StudentViewBean> getLocviewbean() {
	return locviewbean;
}
public void setLocviewbean(List<StudentViewBean> locviewbean) {
	this.locviewbean = locviewbean;
}
public List<StudentViewBean> getStopviewbean() {
	return stopviewbean;
}
public void setStopviewbean(List<StudentViewBean> stopviewbean) {
	this.stopviewbean = stopviewbean;
}
public List<StudentViewBean> getViewbean() {
	return viewbean;
}
public void setViewbean(List<StudentViewBean> viewbean) {
	this.viewbean = viewbean;
}
@Override
	public String execute() throws Exception {
	 String status="2";
	 
	 
	 int a=(int) usersession.get("schoolId");
	
	  
	  List<StudentsBean> bean=SmsBeanDao.getStudentDetailsListByTrackedStatus(a,status,RecordStatus);
	  Iterator iterator=bean.iterator();
	  while(iterator.hasNext()){
		  StudentsBean studentsBean=(StudentsBean) iterator.next();
		  StudentViewBean bean2=new StudentViewBean();
		  bean2.setStudentId(studentsBean.getStudentId());
		  bean2.setStudentName(studentsBean.getStudentFirstName()+" "+studentsBean.getStudentLastName());
		  bean2.setPickupPointId(studentsBean.getPickupPointId());
		  bean2.setPickupPoint(studentsBean.getPickupPoint());
		  bean2.setDropPoint(studentsBean.getDropPoint());
		  bean2.setDropPointId(studentsBean.getDropPointId());
		 
		  WayPointsBean  pointsBean=SmsBeanDao.getWayPoint(studentsBean.getStudentId());
		  
		  if(pointsBean!=null){
		  bean2.setLocation(pointsBean.getWayPoint());
	  }
		  ClassBean classBean=SmsBeanDao.getClassesDetailsbyId(studentsBean.getClassId());
		  if(classBean!=null){
		  bean2.setStudentClass(classBean.getStudentClass());
		  }
		  viewbean.add(bean2);
		  
	  }
	  
	  
	 
		return SUCCESS;
	}

	public String stopPoint()
	{
		String status="1";
		 
		 
		 int a=(int) usersession.get("schoolId");

		  List<StudentsBean> bean=SmsBeanDao.getStudentDetailsListByTrackedStatus(a,status,RecordStatus);
		
		  Iterator iterator=bean.iterator();
		  while(iterator.hasNext()){
			  StudentsBean studentsBean=(StudentsBean) iterator.next();
			  StudentViewBean bean2=new StudentViewBean();
			  if(studentsBean!=null){
				  
				  bean2.setStudentName(studentsBean.getStudentFirstName()+"  "+studentsBean.getStudentLastName());
				  bean2.setPickupPointId(studentsBean.getPickupPointId());
				  bean2.setPickupPoint(studentsBean.getPickupPoint());
				  bean2.setDropPoint(studentsBean.getDropPoint());
				  bean2.setDropPointId(studentsBean.getDropPointId());
				  WayPointsBean  pointsBean=SmsBeanDao.getWayPoint(studentsBean.getStudentId());
				  if(pointsBean!=null){
					  bean2.setLocation(pointsBean.getWayPoint());
				  }
				 
				  ClassBean classBean=SmsBeanDao.getClassesDetailsbyId(studentsBean.getClassId());
				  if(classBean!=null){
					  bean2.setStudentClass(classBean.getStudentClass());
				  }
				 
				  BusesBean busBean=SmsBeanDao.getBusId(studentsBean.getBusId());
				  if(busBean!=null){
					  bean2.setBusId(busBean.getBusId()); 
					  bean2.setBusName(busBean.getBusName());
					  int routeid=busBean.getRouteId();
					  RoutesBean routbean=SmsBeanDao.getRoute(routeid);
					  if(routbean!=null){
					  bean2.setRouteName(routbean.getRouteName());
					  }
				  }
				  
				 
				  stopviewbean.add(bean2);
			  }
			  
			  
		  }
		
		
		return "stoppoint";
		
	}
	public String location()
	{
		 String status="3";
	
		 
		 int a=(int) usersession.get("schoolId");
		  
		  List<StudentsBean> bean=SmsBeanDao.getStudentDetailsListByTrackedStatus(a,status,RecordStatus);
		  Iterator iterator=bean.iterator();
		  while(iterator.hasNext()){
			  StudentsBean studentsBean=(StudentsBean) iterator.next();
			  StudentViewBean bean2=new StudentViewBean();
			  bean2.setStudentName(studentsBean.getStudentFirstName()+" "+studentsBean.getStudentLastName());
			 ClassBean classBean=SmsBeanDao.getClassesDetailsbyId(studentsBean.getClassId());
			 if(classBean!=null){
				 
				 bean2.setStudentClass(classBean.getStudentClass());
			 }
			 
			  ParentsBean parentBean=SmsBeanDao.getParentDetails(studentsBean.getParentId());
			  if(parentBean!=null){
				  
				  bean2.setFirstName(parentBean.getFirstName());
					 bean2.setLastName(parentBean.getLastName());
					  bean2.setContactNo(parentBean.getMobileNumber());
			  }
			 
			
			  locviewbean.add(bean2);
		  }
		return "location";
		
	}
	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession=usersession;
	}
	 
	

}