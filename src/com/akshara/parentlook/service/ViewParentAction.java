package com.akshara.parentlook.service;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ViewParentAction extends ActionSupport {
	private static final Logger LOG = Logger.getLogger(ViewParentAction.class);
	private int id3;
	private ParentsBean parentBean;

	@Override
	public String execute() throws Exception {

		try {
			parentBean = SmsBeanDao.viewparent(id3);
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public int getId3() {
		return id3;
	}

	public void setId3(int id3) {
		this.id3 = id3;
	}

	public ParentsBean getParentBean() {
		return parentBean;
	}

	public void setParentBean(ParentsBean parentBean) {
		this.parentBean = parentBean;
	}

}
