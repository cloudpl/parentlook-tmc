package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class GetBussesForRootAction extends ActionSupport {

	private static final Logger LOG = Logger.getLogger(GetBussesForRootAction.class);

	private int rootId;
	private List<BusesBean> bussesBeans = new ArrayList<BusesBean>();

	@Override
	public String execute() throws Exception {
		try {
			bussesBeans = SmsBeanDao.getBussesForRoot(rootId);

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public int getRootId() {
		return rootId;
	}

	public void setRootId(int rootId) {
		this.rootId = rootId;
	}

	public List<BusesBean> getBussesBeans() {
		return bussesBeans;
	}

	public void setBussesBeans(List<BusesBean> bussesBeans) {
		this.bussesBeans = bussesBeans;
	}

}
