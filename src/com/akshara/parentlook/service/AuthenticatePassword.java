package com.akshara.parentlook.service;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class AuthenticatePassword extends ActionSupport implements SessionAware {

	private String Name;
	private String token;
	private String confirmpassword;
	private String newpassword;
	Map<String, Object> usersession;
	SchoolAdminsBean schooladminbean;
	private String errMsg;
	private String msg;
	private String errmsg;

	@Override
	public String execute() throws Exception {

		return SUCCESS;
	}

	public String userNameCheck() {

		try {

			SchoolAdminsBean adminbean = new SchoolAdminsBean();

			adminbean = (SchoolAdminsBean) AdminDAO.getAdminList(Name);

			if (adminbean != null) {

				usersession.put("adminId", adminbean.getAdminId());
				usersession.put("oldpassword", adminbean.getAdminPassword());
				if (Name.equals(adminbean.getAdminName())) {

					String token = generateToken(Name);
					System.out.println(token);
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					Date date = new Date();
					Date d1 = null;
					d1 = dateFormat.parse(dateFormat.format(date));

					System.out.println(d1 + "tokengen");

					schooladminbean = AdminDAO.updateAdmin(token, adminbean, adminbean.getAdminId(), d1);

					String emailLink = "localhost:8080/ParentLook/AuthenticateForgotRequest?" + token;
					return "forgot";

				} else {
					errmsg = "Username doesn't Exists";
					return INPUT;
				}
			} else {
				errmsg = "Username doesn't Exists";
				return INPUT;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;

	}

	public boolean tokenCheck() {
		try {

			System.out.println(token + "devika");
			SchoolAdminsBean schoolbean = new SchoolAdminsBean();
			schoolbean = AdminDAO.getAdminDetail(token);
			if (schoolbean != null) {
				if (token.equals(schoolbean.getToken())) {
					Date d1 = schoolbean.getTokenGeneratedDate();
					Date d = new Date(System.currentTimeMillis());
					System.out.print(d + "current");
					long diff = d.getTime() - d1.getTime();
					long diffHours = diff / (60 * 60 * 1000);
					usersession.put("diff", diffHours);
				 

					Integer i = (int) (long) diffHours;
					System.out.println(i + "diff");

					if (24 > i) {

					} else {
						errMsg = "Token Expired";
						String token = generateToken(schoolbean.getAdminName());

						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

						Date date = new Date();
						Date d2 = null;
						d2 = dateFormat.parse(dateFormat.format(date));

						System.out.println(d1 + "tokengen");

						schooladminbean = AdminDAO.updateAdmin(token, schoolbean, schoolbean.getAdminId(), d2);

						String emailLink = "localhost:8080/ParentLook/AuthenticateForgotRequest?" + token;
						return false;
					}
					return true;
				}
			} else {
				msg = "Entered OTP Wrong";
				return false;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}

	public String updatePassword() {
		int a = (Integer) usersession.get("adminId");

		boolean b = tokenCheck();
		if (b == true) {
			AdminDAO.updatePwd(newpassword, a);
			return "change";
		} else {

			return INPUT;
		}

	}

	protected static SecureRandom random = new SecureRandom();

	private String generateToken(String name2) {

		String CharSet = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890";
		String Token = "";
		int chars = 8;
		for (int a = 1; a <= chars; a++) {
			Token += CharSet.charAt(new Random().nextInt(CharSet.length()));
		}
		return Token;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
