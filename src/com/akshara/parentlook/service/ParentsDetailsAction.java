package com.akshara.parentlook.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ParentsDetailsAction extends ActionSupport implements ModelDriven<ParentsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(ParentsDetailsAction.class);

	List<ParentsBean> parentsList = new ArrayList<ParentsBean>();
	List<ParentsBean> parentsImageList = new ArrayList<ParentsBean>();
	ParentsBean parentsBean;
	private Map<String, Object> usersession;
	private File PhotoFile;
	private String errmessage;
	private String opt1;

	public ParentsBean getModel() {
		parentsBean = new ParentsBean();
		return parentsBean;
	}

	@Override
	public String execute() throws Exception {
		try {
			int sd = (Integer) usersession.get("schoolId");

			parentsBean.setSchoolId(sd);
			String mobilenumber = parentsBean.getMobileNumber();
			if (mobilenumber != null) {
				int uid = SmsBeanDao.getUserId(mobilenumber);
				List otp = SmsBeanDao.getOtp(parentsBean.getMobileNumber());
				if (otp.size() > 0) {
					String otp1 = (String) otp.get(0);
					parentsBean.setOtp(otp1);
				}
				if (uid > 0) {

					parentsBean.setUId(uid);
					parentsBean.setLoginStatus("true");
				} else {
					parentsBean.setUId(0);
					parentsBean.setLoginStatus("reg");
				}
			}
			parentsBean = SmsBeanDao.saveParentsDetails(parentsBean, PhotoFile);
			usersession.put("parentsId", parentsBean.getParentId());
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String addParent() throws Exception {
		return SUCCESS;
	}

	public String getParents() throws Exception {
		int a = (Integer) usersession.get("schoolId");

		try {
			parentsList = SmsBeanDao.getParentsList(a);

			Iterator iterator = parentsList.iterator();

			while (iterator.hasNext()) {
				ParentsBean drim = (ParentsBean) iterator.next();
				List<StudentsBean> studentlist = SmsBeanDao.getStudentbyparentid(drim.getParentId());

				if (studentlist.size() > 0) {
					SmsBeanDao.updateusedStatus(drim.getParentId(), "ACTIVE");
					drim.setUsedStatus("ACTIVE");
				} else {
					SmsBeanDao.updateusedStatus(drim.getParentId(), "INACTIVE");
					drim.setUsedStatus("INACTIVE");

				}

				parentsImageList.add(drim);

			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public List<ParentsBean> getParentsList() {
		return parentsList;
	}

	public void setParentsList(List<ParentsBean> parentsList) {
		this.parentsList = parentsList;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public String getErrmessage() {
		return errmessage;
	}

	public void setErrmessage(String errmessage) {
		this.errmessage = errmessage;
	}

	public File getPhotoFile() {
		return PhotoFile;
	}

	public void setPhotoFile(File photoFile) {
		PhotoFile = photoFile;
	}

	public List<ParentsBean> getParentsImageList() {
		return parentsImageList;
	}

	public String getOpt1() {
		return opt1;
	}

	public void setOpt1(String opt1) {
		this.opt1 = opt1;
	}

	public void setParentsImageList(List<ParentsBean> parentsImageList) {
		this.parentsImageList = parentsImageList;
	}

}
