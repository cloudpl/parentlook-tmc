package com.akshara.parentlook.service;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class CheckStudentclass extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private String classname;
	private String section;
	private String msg;
	private ClassBean classbean;

	@Override
	public String execute() throws Exception {
		int a = (int) usersession.get("schoolId");
		try {
			classbean = SmsBeanDao.checkStudentClass(a, classname, section);

			if (classbean != null  ) {
 
				msg = "Exists";
			} else {
				System.out.println("elseeeeeeee");
				msg = "NOT Exists";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public ClassBean getClassbean() {
		return classbean;
	}

	public void setClassbean(ClassBean classbean) {
		this.classbean = classbean;
	}

	 

}
