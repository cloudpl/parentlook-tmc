package com.akshara.parentlook.service;

import com.akshara.parentlook.db.bean.SchoolNotifications;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ReadNotifications extends ActionSupport {

	private int notifyId;
	private SchoolNotifications notification = new SchoolNotifications();
	private String StudentName;

	@Override
	public String execute() throws Exception {

		notification = SmsBeanDao.setReadStatusForNotifications(notifyId);
		StudentsBean bean = SmsBeanDao.getStudentDetails(notification.getStudentId());
		if (bean != null) {
			StudentName = bean.getStudentFirstName() + " " + bean.getStudentLastName();

		}

		return super.execute();
	}

	public int getNotifyId() {
		return notifyId;
	}

	public void setNotifyId(int notifyId) {
		this.notifyId = notifyId;
	}

	public SchoolNotifications getNotification() {
		return notification;
	}

	public void setNotification(SchoolNotifications notification) {
		this.notification = notification;
	}

	public String getStudentName() {
		return StudentName;
	}

	public void setStudentName(String studentName) {
		StudentName = studentName;
	}

}
