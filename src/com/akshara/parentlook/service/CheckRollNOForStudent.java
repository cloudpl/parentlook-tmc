package com.akshara.parentlook.service;

import java.util.List;

import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class CheckRollNOForStudent extends ActionSupport {

	private int classId;
	private int schoolId;
	private int rollno;
	private StudentsBean studentbean;

	public StudentsBean getStudentbean() {
		return studentbean;
	}

	public void setStudentbean(StudentsBean studentbean) {
		this.studentbean = studentbean;
	}

	private String actMsg;

	@Override
	public String execute() throws Exception {

		try {
			studentbean = SmsBeanDao.studentRollNoCheck(schoolId, classId, rollno);
			if (studentbean != null) {

				actMsg = "Exists";
			} else {
				actMsg = "NOT Exists";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		return SUCCESS;

	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public int getRollno() {
		return rollno;
	}

	public void setRollno(int rollno) {
		this.rollno = rollno;
	}

	public String getActMsg() {
		return actMsg;
	}

	public void setActMsg(String actMsg) {
		this.actMsg = actMsg;
	}

}
