package com.akshara.parentlook.service;

import java.io.File;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class SchoolProfileAction extends ActionSupport implements ModelDriven<SchoolsDetailsBean>, SessionAware {
	private SchoolsDetailsBean schoolBean;
	private Map<String, Object> usersession;
	private File Photo2;
	private String tab;

	public File getPhoto2() {
		return Photo2;
	}

	public void setPhoto2(File photo2) {
		Photo2 = photo2;
	}

	@Override
	public SchoolsDetailsBean getModel() {
		schoolBean = new SchoolsDetailsBean();
		return schoolBean;
	}

	@Override
	public String execute() throws Exception {
		int a = (int) usersession.get("schoolId");
		schoolBean = SmsBeanDao.editSchoolProfile(a);
		String schoolName = schoolBean.getSchoolName();
		usersession.put("schoolName", schoolName);
		if (schoolBean.getPhoto() != null) {
			byte[] itemImage = schoolBean.getPhoto().getBytes(1, (int) schoolBean.getPhoto().length());

			Base64 b = new Base64();
			byte[] encodingImgAsBytes = b.encode(itemImage);

			String strType = b.encodeToString(encodingImgAsBytes);

			byte[] valueDecoded = Base64.decodeBase64(strType);
			String decodedImgStr = new String(valueDecoded);

			schoolBean.setImageStr(decodedImgStr);

			tab = "2";

			usersession.put("image", decodedImgStr);
		}

		return SUCCESS;
	}

	public String updateProfile() {
		int a = (int) usersession.get("schoolId");

		try {

			int i = SmsBeanDao.updateSchoolProfile(schoolBean, Photo2, a);

		} catch (Exception e) {
			e.printStackTrace();
			return "input";
		}

		return "update";

	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public SchoolsDetailsBean getSchoolBean() {
		return schoolBean;
	}

	public void setSchoolBean(SchoolsDetailsBean schoolBean) {
		this.schoolBean = schoolBean;
	}

	public String getTab() {
		return tab;
	}

	public void setTab(String tab) {
		this.tab = tab;
	}

}
