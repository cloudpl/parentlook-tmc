package com.akshara.parentlook.service;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class UpdateClassDetailsAction extends ActionSupport implements ModelDriven<ClassBean> {
	private static final Logger LOG = Logger.getLogger(UpdateClassDetailsAction.class);
	ClassBean classbean = null;

	@Override
	public String execute() throws Exception {
		try {
			boolean b = SmsBeanDao.updateClassdetails(classbean);
		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	@Override
	public ClassBean getModel() {
		classbean = new ClassBean();
		return classbean;
	}

}
