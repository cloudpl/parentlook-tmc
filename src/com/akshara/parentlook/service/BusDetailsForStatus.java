package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusDetailBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class BusDetailsForStatus extends ActionSupport implements SessionAware {

	private static final Logger LOG = Logger.getLogger(GetBusesGpsForRoute.class);
	private int routeId;
	private Map<String, Object> usersession;
	private int busInactivePick;
	private int activePick;
	private int busArrviedPick;

	private int busInactiveDrop;
	private int activeDrop;
	private int busArrviedDrop;
	private String busStatus;

	List<GPSTrackingBean> busesdetail = new ArrayList<>();
	List<BusesBean> busesbean = new ArrayList<>();
	List<BusDetailBean> busDetailBeans = new ArrayList<>();
	SchoolsDetailsBean schoolbean = new SchoolsDetailsBean();

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	@Override
	public String execute() throws Exception {

		try {
			int a = (Integer) usersession.get("schoolId");

			if (routeId > 0) {
				busesbean = SmsBeanDao.getBusRootList(routeId);
			} else {

				busesbean = SmsBeanDao.getBusDetailsList(a);
			}

			schoolbean = SmsBeanDao.getSchoolDetails(a);

			String[] sta = busStatus.split("_");

			Iterator it = busesbean.iterator();
			while (it.hasNext()) {
				BusesBean busbean1 = (BusesBean) it.next();

				GPSTrackingBean gpsTrackingBean = SmsBeanDao.getgpsdetails(busbean1.getBusId());

				if (gpsTrackingBean != null) {

					int buspickSta = SmsBeanDao.getBusStatusPic(busbean1.getBusId(), "Pickup");
					int busdroSta = SmsBeanDao.getBusStatusDrop(busbean1.getBusId(), "Drop");

					String busst = sta[0];
					String dropStatus = sta[1];
					if (dropStatus.equals("pickup")) {

						if (busst.equals("inactive")) {
							if (buspickSta == 0) {
								RoutesBean routebean = SmsBeanDao.getRoute(busbean1.getRouteId());
								BusDetailBean bean = new BusDetailBean();
								if (routebean != null) {
									bean.setRouteName(routebean.getRouteName());
								}
								bean.setBusDrop(busdroSta);
								bean.setBusPick(buspickSta);
								bean.setBusName(busbean1.getBusName());
								bean.setBusId(busbean1.getBusId());

								busDetailBeans.add(bean);

							}
						} else if (busst.equals("active")) {
							if (buspickSta == 1) {
								RoutesBean routebean = SmsBeanDao.getRoute(busbean1.getRouteId());
								BusDetailBean bean = new BusDetailBean();
								if (routebean != null) {
									bean.setRouteName(routebean.getRouteName());
								}
								bean.setBusDrop(busdroSta);
								bean.setBusPick(buspickSta);
								bean.setBusId(busbean1.getBusId());
								bean.setBusName(busbean1.getBusName());
								busDetailBeans.add(bean);

							}
						} else {

							if (buspickSta == 2) {
								RoutesBean routebean = SmsBeanDao.getRoute(busbean1.getRouteId());
								BusDetailBean bean = new BusDetailBean();

								bean.setBusDrop(busdroSta);
								bean.setBusPick(buspickSta);
								bean.setBusId(busbean1.getBusId());
								bean.setBusName(busbean1.getBusName());
								if (routebean != null) {
									bean.setRouteName(routebean.getRouteName());
								}
								busDetailBeans.add(bean);

							}

						}

					} else if (dropStatus.equals("drop")) {

						if (busst.equals("inactive")) {
							if (busdroSta == 0) {
								RoutesBean routebean = SmsBeanDao.getRoute(busbean1.getRouteId());
								BusDetailBean bean = new BusDetailBean();

								bean.setBusDrop(busdroSta);
								bean.setBusPick(buspickSta);
								bean.setBusName(busbean1.getBusName());
								bean.setBusId(busbean1.getBusId());
								if (routebean != null) {
									bean.setRouteName(routebean.getRouteName());
								}
								busDetailBeans.add(bean);

							}

						} else if (busst.equals("active")) {
							if (busdroSta == 1) {
								RoutesBean routebean = SmsBeanDao.getRoute(busbean1.getRouteId());
								BusDetailBean bean = new BusDetailBean();

								bean.setBusDrop(busdroSta);
								bean.setBusPick(buspickSta);
								bean.setBusId(busbean1.getBusId());
								bean.setBusName(busbean1.getBusName());
								if (routebean != null) {
									bean.setRouteName(routebean.getRouteName());
								}
								busDetailBeans.add(bean);

							}
						} else {

							if (busdroSta == 2) {
								RoutesBean routebean = SmsBeanDao.getRoute(busbean1.getRouteId());
								BusDetailBean bean = new BusDetailBean();

								bean.setBusDrop(busdroSta);
								bean.setBusPick(buspickSta);
								bean.setBusId(busbean1.getBusId());
								bean.setBusName(busbean1.getBusName());
								if (routebean != null) {
									bean.setRouteName(routebean.getRouteName());
								}
								busDetailBeans.add(bean);

							}
						}

					}

				}
			}

		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	public String getBusStatus() {
		return busStatus;
	}

	public void setBusStatus(String busStatus) {
		this.busStatus = busStatus;
	}

	public List<BusDetailBean> getBusDetailBeans() {
		return busDetailBeans;
	}

	public void setBusDetailBeans(List<BusDetailBean> busDetailBeans) {
		this.busDetailBeans = busDetailBeans;
	}

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

}
