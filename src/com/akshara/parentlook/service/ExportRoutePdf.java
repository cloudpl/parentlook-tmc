package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportRoutePdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("Route Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("                   ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(4);
		int a = (Integer) usersession.get("schoolId");
		List<RoutesBean> routesList = new ArrayList<RoutesBean>();
		routesList = SmsBeanDao.getRootsForSchool(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

		table.addCell(new Phrase("RouteId", boldFont));
		table.addCell(new Phrase("RouteName", boldFont));
		table.addCell(new Phrase("StartPoint", boldFont));
		table.addCell(new Phrase("EndPoint", boldFont));

		Iterator<RoutesBean> iterator = routesList.iterator();
		while (iterator.hasNext()) {
			RoutesBean bean = (RoutesBean) iterator.next();
			if (bean != null) {
				if (bean.getRecordStatus().equals("ACTIVE")) {

					Integer routeId = bean.getRouteId();
					String id = routeId.toString();
					String routename = bean.getRouteName();
					String start = bean.getStartPoint();
					String s1 = start.substring(0, Math.min(start.length(), 15));
					String end = bean.getEndPoint();
					String s = end.substring(0, Math.min(end.length(), 15));

					table.addCell(id);
					table.addCell(routename);
					table.addCell(s1);
					table.addCell(s);

				}
			}
		}

		return table;
	}

}
