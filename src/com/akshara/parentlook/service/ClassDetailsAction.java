package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.akshara.parentlook.web.bean.ClassWebBean;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ClassDetailsAction extends ActionSupport implements ModelDriven<ClassBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(ClassDetailsAction.class);

	ClassBean classBean;
	private List<ClassBean> classesList1 = new ArrayList<ClassBean>();
	private List<ClassWebBean> classeslist = new ArrayList<>();
	private List<SchoolsDetailsBean> schoolIdList = new ArrayList<SchoolsDetailsBean>();
	private Map<String, Object> usersession;

	private String schName;

	public ClassBean getModel() {
		classBean = new ClassBean();
		return classBean;
	}

	@Override
	public String execute() throws Exception {
		try {
			int a = SmsBeanDao.saveStudentClassDetails(classBean);
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String classesList() throws Exception {
		int b = (Integer) usersession.get("schoolId");
		try {
			classesList1 = SmsBeanDao.getClasesDetailsList(b);
			Iterator it = classesList1.iterator();

			while (it.hasNext()) {
				ClassWebBean classwebbean = new ClassWebBean();

				classBean = (ClassBean) it.next();
				BeanUtils.copyProperties(classwebbean, classBean);

				List<StudentsBean> studentlist = new ArrayList<>();

				studentlist = SmsBeanDao.getStudentbyclass(classwebbean.getClassId());
				if (studentlist.size() > 0) {
					classwebbean.setUsedStatus("ACTIVE");

				} else {
					classwebbean.setUsedStatus("INACTIVE");
				}

				classeslist.add(classwebbean);
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;

	}

	public String addClass() throws Exception {
		try {
			int b = (Integer) usersession.get("schoolId");
			SchoolsDetailsBean bean = SmsBeanDao.getSchoolDetails(b);
			schName = bean.getSchoolName();
			schoolIdList = SmsBeanDao.getSchoolDetailsList();
		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;

	}

	public List<SchoolsDetailsBean> getSchoolIdList() {
		return schoolIdList;
	}

	public void setSchoolIdList(List<SchoolsDetailsBean> schoolIdList) {
		this.schoolIdList = schoolIdList;
	}

	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public String getSchName() {
		return schName;
	}

	public void setSchName(String schName) {
		this.schName = schName;
	}

	public List<ClassWebBean> getClasseslist() {
		return classeslist;
	}

	public void setClasseslist(List<ClassWebBean> classeslist) {
		this.classeslist = classeslist;
	}

}
