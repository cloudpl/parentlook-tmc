package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class GetPickupPointsAction extends ActionSupport {

	private static final Logger LOG = Logger.getLogger(GetPickupPointsAction.class);

	private int busId;
	private String direction;
	private List<PickupPoints> pickupPoints = new ArrayList<PickupPoints>();
	private List<PickupPoints> dropPoints = new ArrayList<PickupPoints>();

	@Override
	public String execute() throws Exception {

		try {

			pickupPoints = SmsBeanDao.getPickupPointsForBus(busId, direction);
			if (pickupPoints.size() > 0) {

			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public int getBusId() {
		return busId;
	}

	public void setBusId(int busId) {
		this.busId = busId;
	}

	public List<PickupPoints> getPickupPoints() {
		return pickupPoints;
	}

	public void setPickupPoints(List<PickupPoints> pickupPoints) {
		this.pickupPoints = pickupPoints;
	}

	public List<PickupPoints> getDropPoints() {
		return dropPoints;
	}

	public void setDropPoints(List<PickupPoints> dropPoints) {
		this.dropPoints = dropPoints;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

}
