package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.Font;
import com.opensymphony.xwork2.ActionSupport;

public class ExportParentExcel extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private InputStream inputStream;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Parents");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("ParentId");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("ParentName");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("Address");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("MobileNumber");
		sheet.autoSizeColumn(3);
		rowhead.createCell(4).setCellValue("Email");
		sheet.autoSizeColumn(4);
		int a = (Integer) usersession.get("schoolId");
		List<ParentsBean> parentlist = new ArrayList<ParentsBean>();
		parentlist = SmsBeanDao.getParentsList(a);
		int x = 1;
		Iterator<ParentsBean> iterator = parentlist.iterator();
		while (iterator.hasNext()) {

			ParentsBean bean = (ParentsBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			int parentid = bean.getParentId();
			String parentName = bean.getFirstName() + bean.getLastName();
			String addr = bean.getAddress();
			String mobile = bean.getMobileNumber();
			String email = bean.getEmail();

			row.createCell(0).setCellValue(parentid);
			row.createCell(1).setCellValue(parentName);
			row.createCell(2).setCellValue(addr);
			row.createCell(3).setCellValue(mobile);
			row.createCell(4).setCellValue(email);

			x++;
		}
		autoSizeColumn(workbook);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));

		return null;
	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		System.out.println("fffffff");
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
