package com.akshara.parentlook.service;

import java.util.Map;

import com.akshara.parentlook.db.bean.CmsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.generic.mss.utils.ClobUtil;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class Settings extends ActionSupport {

	private CmsBean cmsbean;

	private String content;
	int id = 1;
	@Override
	public String execute() throws Exception {
		// System.out.println(cmsbean);
		return SUCCESS;
	}

	public String edit() {
		
		cmsbean = SmsBeanDao.getInfo(id);
		System.out.println(cmsbean.getId() + cmsbean.getName());

		/*
		 * content = ClobUtil .getClobAsHtml(caseStudy.getCaseContentClob());
		 */
		content = ClobUtil.getClobAsHtml(cmsbean.getContentClob());
	
		return "edit";

	}
	public String update(){
		System.out.println(content+id);
		SmsBeanDao.updateContent(content,id);
		return "update";
		
	}

	public CmsBean getCmsbean() {
		return cmsbean;
	}

	public void setCmsbean(CmsBean cmsbean) {
		this.cmsbean = cmsbean;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	 
}
