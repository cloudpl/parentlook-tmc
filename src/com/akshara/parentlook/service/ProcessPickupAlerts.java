package com.akshara.parentlook.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ProcessPickupAlerts extends ActionSupport {

	private static final Logger LOG = Logger.getLogger(ProcessPickupAlerts.class);
	private int BusId;

	private String lat;

	private List<StudentsBean> studentsBeans = new ArrayList<StudentsBean>();

	private List<PickupPoints> pickupPoints = new ArrayList<PickupPoints>();
	private List ids = new ArrayList();
	private Set pickupPoints2 = new LinkedHashSet();
	private Set pickupPointsIds = new LinkedHashSet();

	@Override
	public String execute() throws Exception {

		try {
			studentsBeans = SmsBeanDao.getStudentsForBus(1);

			Iterator iterator = studentsBeans.iterator();

			while (iterator.hasNext()) {

				StudentsBean studentsBean = (StudentsBean) iterator.next();

				pickupPoints2.add(studentsBean.getPickupPoint());

				pickupPointsIds.add(studentsBean.getPickupPointId());
			}

			Iterator itr = pickupPointsIds.iterator();

			while (itr.hasNext()) {

				int op = (Integer) itr.next();

				ids.add(op);
			}

			String url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + lat
					+ "&destination=Janapriya High School, Miyapur, Hyderabad, Telangana 500049, India&waypoints=optimize:true";

			String s = "|";

			int k = 1;
			Iterator iterator2 = pickupPoints2.iterator();
			while (iterator2.hasNext()) {

				String sa = (String) iterator2.next();
				if (k < pickupPoints2.size()) {

					s = s + sa + "|";
				} else {
					s = s + sa;

				}

			}

			String finUrl = url + s + "&key=AIzaSyBmDYBawBkUUGIqUoHs72EdlK3UugfhGM4";

			String js = "";
			String output = null;
			try {

				finUrl = finUrl.replaceAll("\\s+", "%20");
				URL urls = new URL(finUrl);
				HttpURLConnection conn = (HttpURLConnection) urls.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/json");

				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				while ((output = br.readLine()) != null) {
					js = js + output;
				}
				conn.disconnect();

			} catch (MalformedURLException e) {

				LOG.error(e);

			} catch (IOException e) {

				LOG.error(e);

			}

			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(js);

			JSONArray routes = (JSONArray) jsonObject.get("routes");

			JSONObject obj = (JSONObject) routes.get(0);

			JSONArray legs = (JSONArray) obj.get("legs");

			JSONArray waypoint_order = (JSONArray) obj.get("waypoint_order");

			List times = new ArrayList();

			Iterator iterator3 = legs.iterator();
			int k1 = 0;
			int sum = 0;
			while (iterator3.hasNext()) {

				if (k1 < legs.size() - 1) {
					JSONObject leg1 = (JSONObject) iterator3.next();

					JSONObject duration = (JSONObject) leg1.get("duration");

					String time = (String) duration.get("text");

					int sum1 = Integer.parseInt(time.substring(0, time.length() - 4).trim());
					if (k1 == 0) {
						sum = sum + sum1;
						times.add(sum1);
					} else {
						sum = sum + sum1;
						times.add(sum);
					}

				}
				k1++;
			}
			List<StudentsBean> studentsBeansToRemove = new ArrayList<StudentsBean>();
			Iterator iterator4 = waypoint_order.iterator();
			int pos = 0;
			while (iterator4.hasNext()) {

				Long i1 = (Long) iterator4.next();

				int i = i1.intValue();

				Iterator iterator5 = studentsBeans.iterator();

				while (iterator5.hasNext()) {

					StudentsBean bean = (StudentsBean) iterator5.next();

					if (bean.getPickupPointId() == (Integer) ids.get(i)) {

						String[] dur1 = bean.getDistance().split(",");
						String[] dur = bean.getDistance().split(",");
						if ((Integer) times.get(pos) <= Integer.parseInt(dur[0])) {

							studentsBeansToRemove.add(bean);

						}

					}

				}

				pos++;

			}

			studentsBeans.removeAll(studentsBeansToRemove);

		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

}
