package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.opensymphony.xwork2.ActionSupport;

public class AdminNameChecking extends ActionSupport implements SessionAware {

	private static final Logger LOG = Logger.getLogger(AdminNameChecking.class);

	private Map<String, Object> usersession;
	private SchoolAdminsBean adminBean;

	public SchoolAdminsBean getAdminBean() {
		return adminBean;
	}

	public void setAdminBean(SchoolAdminsBean adminBean) {
		this.adminBean = adminBean;
	}

	private String msg;
	private String AdminName;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getAdminName() {
		return AdminName;
	}

	public void setAdminName(String adminName) {
		AdminName = adminName;
	}

	@Override
	public String execute() throws Exception {

		int a = (int) usersession.get("schoolId");
		try {

			adminBean = AdminDAO.checkAdmin(AdminName, a);
			if (adminBean != null) {

				msg = "Exists";
			} else {
				msg = "NOT Exists";
			}

		} catch (Exception e) {

			LOG.error(e);

		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}
}
