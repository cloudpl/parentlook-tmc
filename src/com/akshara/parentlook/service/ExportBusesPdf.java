package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportBusesPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);

		document.open();
		Paragraph paragraph1 = new Paragraph("Buses Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("                   ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(8);
		int a = (Integer) usersession.get("schoolId");
		List<BusesBean> busList = new ArrayList<BusesBean>();
		busList = SmsBeanDao.getBusDetailsList(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(new Phrase("BusId", boldFont));
		table.addCell(new Phrase("BusName", boldFont));
		table.addCell(new Phrase("RouteName", boldFont));
		table.addCell(new Phrase("DriverName", boldFont));
		table.addCell(new Phrase("RegNo", boldFont));
		table.addCell(new Phrase("StartTime", boldFont));
		table.addCell(new Phrase("ReturnTime", boldFont));
		table.addCell(new Phrase("StartingPoint", boldFont));

		Iterator<BusesBean> iterator = busList.iterator();
		while (iterator.hasNext()) {
			BusesBean bean = (BusesBean) iterator.next();
			if (bean != null) {
				if (bean.getRecordStatus().equals("ACTIVE")) {

					Integer busId = bean.getBusId();
					String id = busId.toString();
					table.addCell(id);
					String name = bean.getBusName();
					table.addCell(name);
					int routeid = bean.getRouteId();
					RoutesBean routesBean = new RoutesBean();
					routesBean = SmsBeanDao.getRouteDetails(routeid);
					if (routesBean != null) {

						String routename = routesBean.getRouteName();
						table.addCell(routename);
					} else {
						String root = "   ";
						table.addCell(root);
					}
					int driverid = bean.getDriverId();
					DriversBean driversBean = new DriversBean();
					driversBean = SmsBeanDao.getDriverDetailsbyID(driverid);
					if (driversBean != null) {
						String drivername = driversBean.getFirstName() + driversBean.getLastName();
						table.addCell(drivername);
					} else {
						String dri = "    ";
						table.addCell(dri);
					}
					String regno = bean.getRegNumber();
					String start = bean.getSchoolArraivalTime();
					String returntime = bean.getReturnStartTime();
					String startpoint = bean.getStartPointLocation();

					String s = startpoint.substring(0, Math.min(startpoint.length(), 15));

					table.addCell(regno);
					table.addCell(start);
					table.addCell(returntime);
					table.addCell(s);

				}
			}
		}

		return table;
	}

}
