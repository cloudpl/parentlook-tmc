package com.akshara.parentlook.service;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class DeleteClassDetailsAction extends ActionSupport {
	private static final Logger LOG = Logger.getLogger(DeleteClassDetailsAction.class);
	private int classid;

	@Override
	public String execute() throws Exception {
		try {
			boolean b = SmsBeanDao.deleteclass(classid);
			if (b == true) {
				return SUCCESS;
			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public int getClassid() {
		return classid;
	}

	public void setClassid(int classid) {
		this.classid = classid;
	}

}
