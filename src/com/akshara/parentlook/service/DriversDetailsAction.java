package com.akshara.parentlook.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.activation.MimeType;
import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.core.MediaType;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.akshara.parentlook.web.bean.DriversWebBean;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class DriversDetailsAction extends ActionSupport implements ModelDriven<DriversBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(DriversDetailsAction.class);

	DriversBean driversBean;
	List<DriversBean> driverImageList = new ArrayList<DriversBean>();
	private Map<String, Object> usersession;

	List<DriversBean> driversList1 = new ArrayList<DriversBean>();
	private File PhotoFile;
	private File idattachmentfile;
	private File licenceattachmentfile;
	List<String> idtypes = new ArrayList<String>();
	private String fileName;
	private List<DriversWebBean> driversList = new ArrayList<DriversWebBean>();

	public DriversBean getModel() {
		driversBean = new DriversBean();
		return driversBean;
	}

	@Override
	public String execute() throws Exception {

		try {
			int a = (Integer) usersession.get("schoolId");
			int a1 = SmsBeanDao.saveDriverDetails(driversBean, PhotoFile, idattachmentfile, licenceattachmentfile, a);
			idtypes.add("aadharcard");
			idtypes.add("pancard");
			idtypes.add("voterid");
			idtypes.add("passport");

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String getDriver() throws Exception {

		try {
			int a = (Integer) usersession.get("schoolId");

			driversList1 = SmsBeanDao.getDriverDetailsList(a);

			Iterator iterator = driversList1.iterator();

			while (iterator.hasNext()) {

				DriversWebBean webBean = new DriversWebBean();
				DriversBean drim = (DriversBean) iterator.next();

				BeanUtils.copyProperties(webBean, drim);

				if (drim.getPhoto() != null && drim.getIdTypeAttachment() != null
						&& drim.getLicenceAttachment() != null) {
					byte[] itemImage = drim.getPhoto().getBytes(1, (int) drim.getPhoto().length());
					byte[] idImage = drim.getIdTypeAttachment().getBytes(1, (int) drim.getIdTypeAttachment().length());
					byte[] licenceImage = drim.getLicenceAttachment().getBytes(1,
							(int) drim.getLicenceAttachment().length());

					Base64 b = new Base64();
					byte[] encodingImgAsBytes = b.encode(itemImage);
					byte[] encodingIDImgAsBytes = b.encode(idImage);
					byte[] LicenceImageAsBytes = b.encode(licenceImage);
					String imagetype = b.encodeToString(encodingImgAsBytes);
					String licencetype = b.encodeToString(LicenceImageAsBytes);
					String idtype = b.encodeToString(encodingIDImgAsBytes);
					byte[] valueDecoded = Base64.decodeBase64(imagetype);
					byte[] idimageDecoded = Base64.decodeBase64(idtype);
					byte[] licecnceimageDecoded = Base64.decodeBase64(licencetype);
					String decodedImgStr = new String(valueDecoded);
					String decodedIdStr = new String(idimageDecoded);
					String decodedLicenceStr = new String(licecnceimageDecoded);

					webBean.setImageStr(decodedImgStr);
					webBean.setIdStr(decodedIdStr);
					webBean.setLicenceStr(decodedLicenceStr);

				}
				List<BusesBean> buseslist = buseslist = SmsBeanDao.getbuseslistbydriverid(drim.getDriverId());

				if (buseslist.size() > 0) {
					SmsBeanDao.driverusedStatus(drim.getDriverId(), "ACTIVE");
					webBean.setUsedStatus("ACTIVE");
				} else {
					SmsBeanDao.driverusedStatus(drim.getDriverId(), "INACTIVE");
					webBean.setUsedStatus("INACTIVE");

				}

				driversList.add(webBean);
			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public File getIdattachmentfile() {
		return idattachmentfile;
	}

	public void setIdattachmentfile(File idattachmentfile) {
		this.idattachmentfile = idattachmentfile;
	}

	public File getLicenceattachmentfile() {
		return licenceattachmentfile;
	}

	public void setLicenceattachmentfile(File licenceattachmentfile) {
		this.licenceattachmentfile = licenceattachmentfile;
	}

	public String addDriver() throws Exception {

		return SUCCESS;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public File getPhotoFile() {
		return PhotoFile;
	}

	public void setPhotoFile(File photoFile) {
		PhotoFile = photoFile;
	}

	public List<DriversBean> getDriverImageList() {
		return driverImageList;
	}

	public void setDriverImageList(List<DriversBean> driverImageList) {
		this.driverImageList = driverImageList;
	}

	public List<String> getIdtypes() {
		return idtypes;
	}

	public void setIdtypes(List<String> idtypes) {
		this.idtypes = idtypes;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public List<DriversWebBean> getDriversList() {
		return driversList;
	}

	public void setDriversList(List<DriversWebBean> driversList) {
		this.driversList = driversList;
	}

}
