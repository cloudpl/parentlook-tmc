package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class DriverLicenceCheck extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private String licence;
	private String msg;
	private DriversBean driverbean;

	@Override
	public String execute() throws Exception {
		int a = (int) usersession.get("schoolId");
		try {

			driverbean = SmsBeanDao.checkDriverLicence(a, licence);
			if (driverbean != null) {

				msg = "Exists";
			} else {
				msg = "NOT Exists";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public DriversBean getDriverbean() {
		return driverbean;
	}

	public void setDriverbean(DriversBean driverbean) {
		this.driverbean = driverbean;
	}

}
