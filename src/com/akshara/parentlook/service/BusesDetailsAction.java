package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusDetailBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class BusesDetailsAction extends ActionSupport implements ModelDriven<BusesBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(BusesDetailsAction.class);

	private List<BusesBean> busesList = new ArrayList<BusesBean>();

	private List<DriversBean> driversBeans = new ArrayList<>();
	private List<BusesBean> busesList1 = new ArrayList<BusesBean>();
	private List<RoutesBean> routesList = new ArrayList<RoutesBean>();
	private List<BusDetailBean> busDetailBeans = new ArrayList<>();
	BusesBean busesBean;
	private Map<String, Object> usersession;

	public BusesBean getModel() {

		busesBean = new BusesBean();
		return busesBean;
	}

	@Override
	public String execute() throws Exception {

		try {
			int a = SmsBeanDao.saveBusesDetails(busesBean);
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String busesList() throws Exception {
		int a = (Integer) usersession.get("schoolId");
		try {

			busesList1 = SmsBeanDao.getBusDetailsList(a);
			Iterator ite = busesList1.iterator();

			while (ite.hasNext()) {

				BusesBean bean = (BusesBean) ite.next();
				List<StudentsBean> beans = SmsBeanDao.getStudentList(bean.getBusId());
				BusDetailBean bean2 = new BusDetailBean();
				BeanUtils.copyProperties(bean2, bean);

				if (beans.size() > 0) {
					bean2.setUsedStatus("ACTIVE");
				} else {
					bean2.setUsedStatus("INACTIVE");
				}

				RoutesBean routesBean = SmsBeanDao.getRoute(bean.getRouteId());
				if (routesBean != null) {
					bean2.setRouteName(routesBean.getRouteName());
					;

				}

				DriversBean driversBean = SmsBeanDao.getDriverDetailsbyID(bean.getDriverId());
				if (driversBean != null) {
					bean2.setDriverName(driversBean.getFirstName() + " " + driversBean.getLastName());
				}

				busDetailBeans.add(bean2);
			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String addBus() throws Exception {

		try {
			int a = (Integer) usersession.get("schoolId");
			routesList = SmsBeanDao.getRoutesList(a);
			setDriversBeans(SmsBeanDao.getDriverDetailsList(a));
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public List<BusesBean> getBusesList() {
		return busesList;
	}

	public void setBusesList(List<BusesBean> busesList) {
		this.busesList = busesList;
	}

	public List<RoutesBean> getRoutesList() {
		return routesList;
	}

	public void setRoutesList(List<RoutesBean> routesList) {
		this.routesList = routesList;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {

		this.usersession = usersession;

	}

	public List<DriversBean> getDriversBeans() {
		return driversBeans;
	}

	public void setDriversBeans(List<DriversBean> driversBeans) {
		this.driversBeans = driversBeans;
	}

	public List<BusDetailBean> getBusDetailBeans() {
		return busDetailBeans;
	}

	public void setBusDetailBeans(List<BusDetailBean> busDetailBeans) {
		this.busDetailBeans = busDetailBeans;
	}

}
