package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportStopPointApprovedPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;
	private final String RecordStatus = "ACTIVE";

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("StopPointApproved Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("             ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(7);

		String status = "1";

		int a = (int) usersession.get("schoolId");

		List<StudentsBean> bean = SmsBeanDao.getStudentDetailsListByTrackedStatus(a, status, RecordStatus);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(new Phrase("StudentName", boldFont));
		table.addCell(new Phrase("Class", boldFont));
		table.addCell(new Phrase("Location", boldFont));
		table.addCell(new Phrase("PickUp Point", boldFont));
		table.addCell(new Phrase("Drop-off Point", boldFont));
		table.addCell(new Phrase("Route Name", boldFont));
		table.addCell(new Phrase("BusName", boldFont));

		Iterator iterator = bean.iterator();
		while (iterator.hasNext()) {
			StudentsBean studentsBean = (StudentsBean) iterator.next();
			if (studentsBean != null) {

				String name = studentsBean.getStudentFirstName() + "  " + studentsBean.getStudentLastName();
				table.addCell(name);

				int classid = studentsBean.getClassId();
				ClassBean bean2 = new ClassBean();

				bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
				if (bean2 != null) {
					String classname = bean2.getStudentClass();
					table.addCell(classname);
				} else {
					String clas = "   ";
					table.addCell(clas);
				}
				WayPointsBean pointsBean = SmsBeanDao.getWayPoint(studentsBean.getStudentId());
				if (pointsBean != null) {
					String addr = pointsBean.getWayPoint();
					String s = addr.substring(0, Math.min(addr.length(), 10));
					table.addCell(s);
				} else {
					String add = "   ";
					table.addCell(add);
				}
				String pick = studentsBean.getPickupPoint();
				String drop = studentsBean.getDropPoint();
				if (pick != null) {

					String pic = pick.substring(0, Math.min(pick.length(), 10));
					table.addCell(pic);
				} else {
					String pi = "       ";
					table.addCell(pi);
				}
				if (drop != null) {
					String dop = drop.substring(0, Math.min(drop.length(), 10));

					table.addCell(dop);
				} else {
					String drop1 = "  ";
					table.addCell(drop1);
				}
				BusesBean busBean = SmsBeanDao.getBusId(studentsBean.getBusId());
				if (busBean != null) {
					int routeid = busBean.getRouteId();
					RoutesBean routbean = SmsBeanDao.getRoute(routeid);
					if (routbean != null) {
						String route = routbean.getRouteName();

						table.addCell(route);
					} else {
						String root = "  ";
						table.addCell(root);
					}
				} else {
					String root = "  ";
					table.addCell(root);
				}
				if (busBean != null) {

					String busname = busBean.getBusName();
					table.addCell(busname);
				} else {
					String bus = "   ";
					table.addCell(bus);
				}

			}

		}

		return table;
	}
}
