package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;

import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class BusHistoryTrack extends ActionSupport {

	private int busId;
	private List<HistoryLogBean> historyList = new ArrayList<>();

	@Override
	public String execute() throws Exception {
		historyList = SmsBeanDao.getBusHistory(busId);

		return SUCCESS;
	}

	public int getBusId() {
		return busId;
	}

	public void setBusId(int busId) {
		this.busId = busId;
	}

	public List<HistoryLogBean> getHistoryList() {
		return historyList;
	}

	public void setHistoryList(List<HistoryLogBean> historyList) {
		this.historyList = historyList;
	}

}
