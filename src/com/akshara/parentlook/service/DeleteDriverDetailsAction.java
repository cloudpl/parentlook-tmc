package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.pdf.hyphenation.TernaryTree.Iterator;
import com.opensymphony.xwork2.ActionSupport;

public class DeleteDriverDetailsAction extends ActionSupport {
	List<BusesBean> buseslist = new ArrayList<>();

	public int getDriverid() {

		return driverid;
	}

	public void setDriverid(int driverid) {
		this.driverid = driverid;
	}

	int driverid;

	@Override
	public String execute() throws Exception {
		try {

			boolean b = SmsBeanDao.deleteDriver(driverid);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

}
