package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusDetailBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.GPSTrackingWebBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.mchange.v2.codegen.bean.BeangenUtils;
import com.opensymphony.xwork2.ActionSupport;

public class GetBusesGpsForRoute extends ActionSupport implements SessionAware {
	private static final Logger LOG = Logger.getLogger(GetBusesGpsForRoute.class);
	private int routeId;
	private String Latitude;
	private String schoolname;

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	private String Longitude;

	private Map<String, Object> usersession;

	List<GPSTrackingWebBean> busesdetail = new ArrayList<>();
	List<BusesBean> busesbean = new ArrayList<>();
	List<BusDetailBean> busDetailBeans = new ArrayList<>();
	SchoolsDetailsBean schoolbean = new SchoolsDetailsBean();
	private int busInactivePick;
	private int activePick;
	private int busArrviedPick;

	private int busInactiveDrop;
	private int activeDrop;
	private int busArrviedDrop;

	@Override
	public String execute() throws Exception {

		try {
			int a = (Integer) usersession.get("schoolId");

			if (routeId > 0) {
				busesbean = SmsBeanDao.getBusRootList(routeId);
			} else {

				busesbean = SmsBeanDao.getBusDetailsList(a);

			}

			schoolbean = SmsBeanDao.getSchoolDetails(a);

			Iterator it = busesbean.iterator();
			while (it.hasNext()) {
				BusesBean busbean1 = (BusesBean) it.next();
				GPSTrackingBean gpsTrackingBean1 = null;
				GPSTrackingWebBean gpsTrackingBean = new GPSTrackingWebBean();

				try {

					gpsTrackingBean1 = SmsBeanDao.getgpsdetails(busbean1.getBusId());
					if (gpsTrackingBean1 != null) {
						gpsTrackingBean.setBusId(gpsTrackingBean1.getBusId());
						gpsTrackingBean.setDeviceId(gpsTrackingBean1.getDeviceId());
						gpsTrackingBean.setGeoLat(gpsTrackingBean1.getGeoLat());
						gpsTrackingBean.setGeoLang(gpsTrackingBean1.getGeoLang());
						gpsTrackingBean.setDate(gpsTrackingBean1.getDate());
						gpsTrackingBean.setTimestamp(gpsTrackingBean1.getTimestamp());
						gpsTrackingBean.setName(gpsTrackingBean1.getName());
						gpsTrackingBean.setSurveillance(gpsTrackingBean1.getSurveillance());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				BusDetailBean bean = new BusDetailBean();
				if (gpsTrackingBean.getBusId() > 0) {
					int buspickSta = SmsBeanDao.getBusStatusPic(busbean1.getBusId(), "Pickup");
					int busdroSta = SmsBeanDao.getBusStatusDrop(busbean1.getBusId(), "Drop");
					List<HistoryLogBean> hisbean = new ArrayList<HistoryLogBean>();
					hisbean = SmsBeanDao.getTripStatus(gpsTrackingBean1.getBusId());
					if (hisbean.size() > 0) {
						Iterator itr = hisbean.iterator();
						while (itr.hasNext()) {
							HistoryLogBean historybean = (HistoryLogBean) itr.next();
							BusesBean busbean = SmsBeanDao.getBusDetails(historybean.getBusId());

							if (historybean.getDirection().equals("Drop")
									&& historybean.getType().equals("Start Point")) {
								bean.setTripStatus("Trip Completed");
								System.out.println("trp completed" + historybean.getId());
								bean.setSurveillance(0);

							} else if (historybean.getDirection().equals("Drop")
									&& historybean.getType().equals("School Point")) {
								if (gpsTrackingBean1.getSurveillance() == 0) {
									bean.setSurveillance(0);
								} else {
									bean.setSurveillance(1);
								}
								System.out.println("Drop-off begin" + historybean.getId());
								bean.setTripStatus("Drop-off begin");
							} else if (historybean.getDirection().equals("Pickup")
									&& historybean.getType().equals("School Point")) {
								bean.setTripStatus("Pickup completed");
								System.out.println("Pickup completed" + historybean.getId());
								bean.setSurveillance(0);
							} else if (historybean.getDirection().equals("Pickup")
									&& historybean.getType().equals("Start Point")) {
								if (gpsTrackingBean1.getSurveillance() == 0) {
									bean.setSurveillance(0);
								} else {
									bean.setSurveillance(1);
								}
								bean.setTripStatus("Pickup Begin");
								System.out.println("Pickup begin" + historybean.getId());
							}
						}
					} else {
						bean.setTripStatus("Trip not begin");
						bean.setSurveillance(0);
						System.out.println(bean.getSurveillance() + "survalianceeeeeeeee");
					}
					if (buspickSta == 0) {
						gpsTrackingBean.setStatus("0");
						bean.setStatus("0");
						busInactivePick++;
					} else if (buspickSta == 1) {
						activePick++;
						gpsTrackingBean.setStatus("1");
						bean.setStatus("1");
					} else {
						gpsTrackingBean.setStatus("2");
						bean.setStatus("2");
						if (busdroSta == 0) {
							gpsTrackingBean.setStatus("2");
							bean.setStatus("2");
						} else if (busdroSta == 1) {
							gpsTrackingBean.setStatus("3");
							bean.setStatus("3");
						} else {

							gpsTrackingBean.setStatus("4");
							bean.setStatus("4");
						}
						busArrviedPick++;

					}

					if (busdroSta == 0) {
						busInactiveDrop++;
					} else if (busdroSta == 1) {
						activeDrop++;
					} else {
						busArrviedDrop++;

					}
					RoutesBean routebean = SmsBeanDao.getRoute(busbean1.getRouteId());

					if (routebean != null) {
						bean.setRouteName(routebean.getRouteName());
						gpsTrackingBean.setRouteName(routebean.getRouteName());
					}
					bean.setBusDrop(busdroSta);
					bean.setBusPick(buspickSta);
					bean.setBusId(busbean1.getBusId());
					 bean.setSurveillance(gpsTrackingBean.getSurveillance());
					bean.setBusName(busbean1.getBusName());
					busDetailBeans.add(bean);

					gpsTrackingBean.setBusName(busbean1.getBusName());
					busesdetail.add(gpsTrackingBean);

				}

			}
			String str = schoolbean.getMapLocationLatLang();
			str = str.substring(1, str.length() - 1);
			String str1[] = str.split(",");
			Latitude = str1[0];
			Longitude = str1[1];
			schoolname = schoolbean.getSchoolName();
		} catch (Exception e) {
			LOG.error(e);
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<BusDetailBean> getBusDetailBeans() {
		return busDetailBeans;
	}

	public void setBusDetailBeans(List<BusDetailBean> busDetailBeans) {
		this.busDetailBeans = busDetailBeans;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public List<GPSTrackingWebBean> getBusesdetail() {
		return busesdetail;
	}

	public void setBusesdetail(List<GPSTrackingWebBean> busesdetail) {
		this.busesdetail = busesdetail;
	}

	public SchoolsDetailsBean getSchoolbean() {
		return schoolbean;
	}

	public void setSchoolbean(SchoolsDetailsBean schoolbean) {
		this.schoolbean = schoolbean;
	}

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public int getBusInactivePick() {
		return busInactivePick;
	}

	public void setBusInactivePick(int busInactivePick) {
		this.busInactivePick = busInactivePick;
	}

	public int getActivePick() {
		return activePick;
	}

	public void setActivePick(int activePick) {
		this.activePick = activePick;
	}

	public int getBusArrviedPick() {
		return busArrviedPick;
	}

	public void setBusArrviedPick(int busArrviedPick) {
		this.busArrviedPick = busArrviedPick;
	}

	public int getBusInactiveDrop() {
		return busInactiveDrop;
	}

	public void setBusInactiveDrop(int busInactiveDrop) {
		this.busInactiveDrop = busInactiveDrop;
	}

	public int getActiveDrop() {
		return activeDrop;
	}

	public void setActiveDrop(int activeDrop) {
		this.activeDrop = activeDrop;
	}

	public int getBusArrviedDrop() {
		return busArrviedDrop;
	}

	public void setBusArrviedDrop(int busArrviedDrop) {
		this.busArrviedDrop = busArrviedDrop;
	}

	public List<BusesBean> getBusesbean() {
		return busesbean;
	}

	public void setBusesbean(List<BusesBean> busesbean) {
		this.busesbean = busesbean;
	}

}
