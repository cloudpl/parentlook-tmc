package com.akshara.parentlook.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.AbsentStudentbean;
import com.akshara.parentlook.db.bean.AttendenceBean;
import com.akshara.parentlook.db.bean.AttendenceWebBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class BusesListBySchooolId extends ActionSupport implements SessionAware{

	private Map<String,Object> usersession;
	 private String todaydate;
	List<BusesBean> buseslist=new ArrayList<>();
	List<AttendenceWebBean> busesattendencelist=new ArrayList<AttendenceWebBean>();
	 List onboardAbsentlist=new ArrayList<>();
	 List deboardAbsentlist=new ArrayList<>();
	 List<AbsentStudentbean> absentstudentlist=new ArrayList<>();
	 List<AbsentStudentbean>deboardedlist=new ArrayList<>();
	@Override
	public void setSession(Map<String, Object> usersession) {
		// TODO Auto-generated method stub
		this.usersession=usersession;
	}
	private int totalstudents;
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		int a = (Integer) usersession.get("schoolId");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String format = formatter.format(new Date());
		setTodaydate(format);
		System.out.println(format);
		try{
			buseslist = SmsBeanDao.getBusDetailsList(a);
			System.out.println(buseslist.size()+"bussize");
			if(buseslist.size()>0){
				Iterator iterator=buseslist.iterator();
				while(iterator.hasNext()){
					BusesBean busbean=(BusesBean) iterator.next();
					int onboardcount=0;
					int onboardabsentcount=0;
					int de_boardcount=0;
					int de_boardabsentcount=0;
					//int ocuupency=busbean.getOccupency();
					//System.out.println(ocuupency+"occupency");
					 List<StudentsBean> stubean=SmsBeanDao.getStudentListBasedOnTrackedStatus(busbean.getBusId());
					totalstudents=stubean.size();
					RoutesBean routesBean=SmsBeanDao.getRouteDetails(busbean.getRouteId());
					 List<PickupPoints> pickuplist=SmsBeanDao.getPickUpPointsForBusId(busbean.getBusId());
					 if(pickuplist.size()>0){
						 Iterator iterator2=pickuplist.iterator();
						 while(iterator2.hasNext()){
							 PickupPoints pickupbean=(PickupPoints) iterator2.next();
							 HistoryLogBean historyBeanOnBoard=SmsBeanDao.getHistoryDetailsOnBoard(busbean.getBusId(),pickupbean.getPickupPointId(),format,"Pickup");
							// System.out.println(historyBeanOnBoard.size()+"historyonboard");
							 HistoryLogBean historyBeanDeBoard=SmsBeanDao.getHistoryDetailsDeBoard(busbean.getBusId(),pickupbean.getPickupPointId(),format,"Drop");
						 //System.out.println("historydeboard"+historyBeanDeBoard.getPickupPointId());
							 if(historyBeanOnBoard!=null){
									 List<StudentsBean> studentlist=SmsBeanDao.getStudentListByPick(historyBeanOnBoard.getPickupPointId(),a);
									 System.out.println(historyBeanOnBoard.getPickupPointId()+"historyyyyyyyyy");
									 Iterator iterator5=studentlist.iterator();
									 while(iterator5.hasNext()){
										 StudentsBean studentbean=(StudentsBean) iterator5.next();
										 AttendenceBean attendenceOnBoard=SmsBeanDao.getstudentattendence(studentbean.getStudentId(),format,"0");
										 if(attendenceOnBoard!=null){
											 onboardcount+=1;
										 }else{
											 onboardabsentcount+=1;
											 onboardAbsentlist.add(studentbean.getStudentId());
										 }
									 }
								 
								
							 } 
							 if(historyBeanDeBoard!=null){
								 
									 List<StudentsBean> studentlist=SmsBeanDao.getStudentListByDrop(historyBeanDeBoard.getPickupPointId(),a);
									 System.out.println("deboard"+historyBeanDeBoard.getPickupPointId());
									 Iterator iterator6=studentlist.iterator();
									 while(iterator6.hasNext()){
										 StudentsBean studnetbean1=(StudentsBean) iterator6.next();
										 AttendenceBean attendenceDeBoard=SmsBeanDao.getstudentattendence(studnetbean1.getStudentId(), format, "1");
										// System.out.println(attendenceDeBoard.getStudentId());
										 if(attendenceDeBoard!=null){
											 System.out.println("deboardattdenace"+attendenceDeBoard.getStudentId());
											 de_boardcount+=1;
										 }else{
											 de_boardabsentcount+=1;
											 deboardAbsentlist.add(studnetbean1.getStudentId());
										 }
									 }
								 
							 }
						 }
					 }
					 
					 AttendenceWebBean attendenceWebBean=new AttendenceWebBean();
					 attendenceWebBean.setBusName(busbean.getBusName());
					 if(routesBean!=null){
								System.out.println(routesBean.getRouteName()+"routename");
					 attendenceWebBean.setRoutename(routesBean.getRouteName());}
					// attendenceWebBean.setOccupency(ocuupency);
					
					 attendenceWebBean.setTotalStudents(totalstudents);
					
					 attendenceWebBean.setOnboard(onboardcount);
					 attendenceWebBean.setOnboardAbsent(onboardabsentcount);
					 attendenceWebBean.setDeBoard(de_boardcount);
					 attendenceWebBean.setDeBoardAbsent(de_boardabsentcount);
					 busesattendencelist.add(attendenceWebBean);
				}
				
			}
             /*OnBoardAttendence*/		
			Iterator iterator=onboardAbsentlist.iterator();
			while(iterator.hasNext()){
				AbsentStudentbean absentStudentbean=new AbsentStudentbean();
				int studentid=(Integer)iterator.next();
				StudentsBean st=SmsBeanDao.getStudentDetails(studentid);
				
				  ClassBean classbean=SmsBeanDao.getclassbystudentid(st.getClassId());
				
				BusesBean busesbean1=SmsBeanDao.getBusDetails(st.getBusId());
				if(busesbean1!=null)
				{
				  RoutesBean routesbean1=SmsBeanDao.getRouteDetails(busesbean1.getRouteId());
			
				
				  
				 absentStudentbean.setBusName(busesbean1.getBusName());
				  absentStudentbean.setName(st.getStudentFirstName()+"-"+st.getStudentLastName());
					absentStudentbean.setRollno(st.getRollNo());
					if(routesbean1!=null)
					{
				absentStudentbean.setRoutename(routesbean1.getRouteName());
					
					}
					if(classbean!=null){
					absentStudentbean.setStudclass(classbean.getStudentClass());
					}
				  
					absentstudentlist.add(absentStudentbean);
				}
			
			}
				/*DeBoardAttendence*/
			Iterator Itr=deboardAbsentlist.iterator();
			while(Itr.hasNext())
			{
				AbsentStudentbean absentStudentbean1=new AbsentStudentbean();
				int studentid=(Integer)Itr.next();
				System.out.println("deboardAbesentstudentid="+studentid);
				StudentsBean st=SmsBeanDao.getStudentDetails(studentid);
				
			  ClassBean classbean=SmsBeanDao.getclassbystudentid(st.getClassId());
			
			BusesBean busesbean1=SmsBeanDao.getBusDetails(st.getBusId());
			if(busesbean1!=null)
			{
			  RoutesBean routesbean1=SmsBeanDao.getRouteDetails(busesbean1.getRouteId());
		
		
			  
			  absentStudentbean1.setBusName(busesbean1.getBusName());
			  absentStudentbean1.setName(st.getStudentFirstName()+"-"+st.getStudentLastName());
				absentStudentbean1.setRollno(st.getRollNo());
				if(routesbean1!=null)
				{
			absentStudentbean1.setRoutename(routesbean1.getRouteName());
			}
				if(classbean!=null){
				absentStudentbean1.setStudclass(classbean.getStudentClass());
				}
			  
				deboardedlist.add(absentStudentbean1);
			}
			}	
			}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		usersession.put("todaydate",todaydate);
		
		return SUCCESS;
	}

	public List<BusesBean> getBuseslist() {
		return buseslist;
	}

	public void setBuseslist(List<BusesBean> buseslist) {
		this.buseslist = buseslist;
	}

	 

	public List<AttendenceWebBean> getBusesattendencelist() {
		return busesattendencelist;
	}

	public void setBusesattendencelist(List<AttendenceWebBean> busesattendencelist) {
		this.busesattendencelist = busesattendencelist;
	}

	public String getTodaydate() {
		return todaydate;
	}

	public void setTodaydate(String todaydate) {
		this.todaydate = todaydate;
	}

	public List getOnboardAbsentlist() {
		return onboardAbsentlist;
	}

	public List<AbsentStudentbean> getDeboardedlist() {
		return deboardedlist;
	}

	public void setDeboardedlist(List<AbsentStudentbean> deboardedlist) {
		this.deboardedlist = deboardedlist;
	}

	public void setOnboardAbsentlist(List onboardAbsentlist) {
		this.onboardAbsentlist = onboardAbsentlist;
	}

	public List getDeboardAbsentlist() {
		return deboardAbsentlist;
	}

	public void setDeboardAbsentlist(List deboardAbsentlist) {
		this.deboardAbsentlist = deboardAbsentlist;
	}

	public List<AbsentStudentbean> getAbsentstudentlist() {
		return absentstudentlist;
	}

	public void setAbsentstudentlist(List<AbsentStudentbean> absentstudentlist) {
		this.absentstudentlist = absentstudentlist;
	}

	public int getTotalstudents() {
		return totalstudents;
	}

	public void setTotalstudents(int totalstudents) {
		this.totalstudents = totalstudents;
	}

}
