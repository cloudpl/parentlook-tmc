package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportRouteExcel extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private InputStream inputStream;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Route");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("RouteId");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("RouteName");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("StartPoint");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("EndPoint");
		sheet.autoSizeColumn(3);

		int a = (Integer) usersession.get("schoolId");
		List<RoutesBean> routelist = new ArrayList<RoutesBean>();
		routelist = SmsBeanDao.getRootsForSchool(a);
		int x = 1;
		Iterator<RoutesBean> iterator = routelist.iterator();
		while (iterator.hasNext()) {

			RoutesBean bean = (RoutesBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			int routeid = bean.getRouteId();
			String rotname = bean.getRouteName();
			String startpoint = bean.getStartPoint();
			String endpoint = bean.getEndPoint();

			row.createCell(0).setCellValue(routeid);
			row.createCell(1).setCellValue(rotname);
			row.createCell(2).setCellValue(startpoint);
			row.createCell(3).setCellValue(endpoint);

			x++;
		}
		autoSizeColumn(workbook);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));

		return null;
	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
