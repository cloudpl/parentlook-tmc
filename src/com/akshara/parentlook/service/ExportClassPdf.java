package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportClassPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("Classes Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("                   ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(4);
		int a = (Integer) usersession.get("schoolId");
		List<ClassBean> classesList = new ArrayList<ClassBean>();
		classesList = SmsBeanDao.getClasesDetailsList(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

		table.addCell(new Phrase("ClassId", boldFont));
		table.addCell(new Phrase("ClassName", boldFont));
		table.addCell(new Phrase("Section", boldFont));
		table.addCell(new Phrase("Description", boldFont));

		Iterator<ClassBean> iterator = classesList.iterator();
		while (iterator.hasNext()) {
			ClassBean bean = (ClassBean) iterator.next();
			if (bean != null) {
				if (bean.getRecordStatus().equals("ACTIVE")) {

					Integer classId = bean.getClassId();
					String id = classId.toString();

					String name = bean.getStudentClass();
					String section = bean.getSection();
					String description = bean.getDescription();
					String s = description.substring(0, Math.min(description.length(), 10));

					table.addCell(id);
					table.addCell(name);
					table.addCell(section);
					table.addCell(s);

				}
			}
		}

		return table;
	}

}
