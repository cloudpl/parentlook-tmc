package com.akshara.parentlook.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.DummyBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class StudentsDetailsAction extends ActionSupport implements ModelDriven<StudentsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(StudentsDetailsAction.class);

	List<StudentsBean> studentsList = new ArrayList<StudentsBean>();
	List<StudentsBean> studentImageList = new ArrayList<StudentsBean>();
	private List<SchoolsDetailsBean> schoolIdList = new ArrayList<SchoolsDetailsBean>();
	private List<ParentsBean> parentsList = new ArrayList<ParentsBean>();

	private List<ClassBean> classeslist = new ArrayList<ClassBean>();
	List<BusesBean> list = new ArrayList<BusesBean>();
	List<DummyBean> dummyList = new ArrayList<DummyBean>();
	StudentsBean studentsBean;
	private int FirstName;

	public int getFirstName() {
		return FirstName;
	}

	public void setFirstName(int firstName) {
		FirstName = firstName;
	}

	private File PhotoFile;
	private Map<String, Object> usersession;
	int a = 0;
	private String actionmesage;

	public String getActionmesage() {
		return actionmesage;
	}

	public void setActionmesage(String actionmesage) {
		this.actionmesage = actionmesage;
	}

	private int schoolId;
	private int ParentId;

	public int getParentId() {
		return ParentId;
	}

	public void setParentId(int parentId) {
		ParentId = parentId;
	}

	public StudentsBean getModel() {

		studentsBean = new StudentsBean();
		return studentsBean;
	}

	@Override
	public String execute() throws Exception {

		try {
			ParentsBean bean1 = SmsBeanDao.getParentDetails(FirstName);
			int b = bean1.getUId();
			int a = SmsBeanDao.saveStudentDetails(studentsBean, PhotoFile, b, FirstName);

		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	public String getStudents() throws Exception {
		a = (Integer) usersession.get("schoolId");

		try {
			studentsList = SmsBeanDao.getStudentDetailsList(a);

			Iterator iterator = studentsList.iterator();

			while (iterator.hasNext()) {
				StudentsBean drim = (StudentsBean) iterator.next();
				drim.setImageStr(drim.getImageStr());
				drim.setProfilePicture(drim.getProfilePicture());
				studentImageList.add(drim);

			}
			Iterator<StudentsBean> iterator2 = studentsList.iterator();
			while (iterator2.hasNext()) {
				StudentsBean bean = (StudentsBean) iterator2.next();
				BusesBean busesBean = SmsBeanDao.getStudentId(bean.getBusId());
				RoutesBean routesBean = new RoutesBean();
				if (busesBean != null) {

					routesBean = SmsBeanDao.getRouteId(busesBean.getRouteId());
				}

				ClassBean classBean = SmsBeanDao.getClassesDetailsbyId(bean.getClassId());
				ParentsBean parentsBean = SmsBeanDao.getParentid(bean.getParentId());
				if (bean.getPhoto() != null) {

					bean.setImageStr(bean.getImageStr());
					bean.setProfilePicture(bean.getProfilePicture());
				}

				DummyBean bean2 = new DummyBean();
				if (busesBean != null) {

					bean2.setBusName(busesBean.getBusName());
				}

				bean2.setStudentId(bean.getStudentId());
				bean2.setStudentFirstName(bean.getStudentFirstName());
				bean2.setStudentLastName(bean.getStudentLastName());
				bean2.setPhoto(bean.getPhoto());
				bean2.setProfilePicture(bean.getProfilePicture());
				bean2.setRollNo(bean.getRollNo());
				if (parentsBean != null) {

					bean2.setName(parentsBean.getFirstName() + "   " + parentsBean.getLastName());
				}

				if (routesBean != null) {

					bean2.setRouteName(routesBean.getRouteName());
				}

				bean2.setPhoto(bean.getPhoto());
				bean2.setImageStr(bean.getImageStr());
				bean2.setProfilePicture(bean.getProfilePicture());
				bean2.setRecordStatus(bean.getRecordStatus());
				if (classBean != null) {
					bean2.setDescription(classBean.getStudentClass() + " - " + classBean.getSection());
				}
				List<StudentsBean> studentlist1 = new ArrayList<>();
				studentlist1 = SmsBeanDao.getStudentDetailsbystudentid(bean2.getStudentId());
				if (studentlist1.size() > 0) {
					bean2.setUsedStatus("ACTIVE");
				} else {
					{
						bean2.setUsedStatus("INACTIVE");
					}
				}
				dummyList.add(bean2);
			}

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String addStudent() throws Exception {
		try {
			schoolId = (Integer) usersession.get("schoolId");
			schoolIdList = SmsBeanDao.getSchoolDetailsList();
			parentsList = SmsBeanDao.getParentsList(schoolId);

			classeslist = SmsBeanDao.getClasesDetailsList(schoolId);
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public List<StudentsBean> getStudentsList() {
		return studentsList;
	}

	public void setStudentsList(List<StudentsBean> studentsList) {
		this.studentsList = studentsList;
	}

	public List<StudentsBean> getStudentImageList() {
		return studentImageList;
	}

	public void setStudentImageList(List<StudentsBean> studentImageList) {
		this.studentImageList = studentImageList;
	}

	public File getPhotoFile() {
		return PhotoFile;
	}

	public void setPhotoFile(File photoFile) {
		PhotoFile = photoFile;
	}

	public List<SchoolsDetailsBean> getSchoolIdList() {
		return schoolIdList;
	}

	public void setSchoolIdList(List<SchoolsDetailsBean> schoolIdList) {
		this.schoolIdList = schoolIdList;
	}

	public List<ParentsBean> getParentsList() {
		return parentsList;
	}

	public void setParentsList(List<ParentsBean> parentsList) {
		this.parentsList = parentsList;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public List<ClassBean> getClasseslist() {
		return classeslist;
	}

	public void setClasseslist(List<ClassBean> classeslist) {
		this.classeslist = classeslist;
	}

	public List<BusesBean> getList() {
		return list;
	}

	public void setList(List<BusesBean> list) {
		this.list = list;
	}

	public StudentsBean getStudentsBean() {
		return studentsBean;
	}

	public void setStudentsBean(StudentsBean studentsBean) {
		this.studentsBean = studentsBean;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public static Logger getLog() {
		return LOG;
	}

	public List<DummyBean> getDummyList() {
		return dummyList;
	}

	public void setDummyList(List<DummyBean> dummyList) {
		this.dummyList = dummyList;
	}

}
