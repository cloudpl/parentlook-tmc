package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class SchoolAction extends ActionSupport implements ModelDriven<SchoolsDetailsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(SchoolAction.class);
	private SchoolsDetailsBean schoolsDetailsBean;
	//private SchoolsDetailsBean bean;
	private int idd;
	private int id1;
	private int id2;
	private int schoolid;
	private Map<String, Object> usersession;
private  List<GPSTrackingBean> gpslist=new ArrayList<GPSTrackingBean>();
	@Override
	public String execute() throws Exception {

		try {
			List<BusesBean> busbean=new ArrayList<BusesBean>();
			 busbean=SmsBeanDao.getBusDetailsList(idd);
			 Iterator itr=busbean.iterator();
			 while(itr.hasNext()){
				 BusesBean bus=(BusesBean) itr.next();
				
				 gpslist=SmsBeanDao.getGpsDetails(bus.getBusId());
				 
			 }
			schoolsDetailsBean = SmsBeanDao.editSchool(idd);

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public SchoolsDetailsBean getSchoolsDetailsBean() {
		return schoolsDetailsBean;
	}

	public void setSchoolsDetailsBean(SchoolsDetailsBean schoolsDetailsBean) {
		this.schoolsDetailsBean = schoolsDetailsBean;
	}

	public int getIdd() {
		return idd;
	}

	public void setIdd(int idd) {
		this.idd = idd;
	}

	 

	public int getId1() {
		return id1;
	}

	public void setId1(int id1) {
		this.id1 = id1;
	}

	public int getId2() {
		return id2;
	}

	public void setId2(int id2) {
		this.id2 = id2;
	}

	 

	public String updateSchool() {
		SmsBeanDao.updateSchool(schoolsDetailsBean,schoolid);
		
		return "update";
	}

	@Override
	public SchoolsDetailsBean getModel() {
		schoolsDetailsBean = new SchoolsDetailsBean();
		return schoolsDetailsBean;
	}

	public String deleteSchool() {
		
			SmsBeanDao.deleteSchool(id1);
	
		return "delete";
	}

	 

	public List<GPSTrackingBean> getGpslist() {
		return gpslist;
	}

	public void setGpslist(List<GPSTrackingBean> gpslist) {
		this.gpslist = gpslist;
	}

	public int getSchoolid() {
		return schoolid;
	}

	public void setSchoolid(int schoolid) {
		this.schoolid = schoolid;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}
}
