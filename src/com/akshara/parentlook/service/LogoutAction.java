package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.sun.mail.iap.Response;

public class LogoutAction extends ActionSupport implements SessionAware {

	private static final Logger LOG = Logger.getLogger(LogoutAction.class);
	Map<String, Object> usersession;

	@Override
	public String execute() throws Exception {
		try {
			usersession = ActionContext.getContext().getSession();
			usersession.remove("adminId");

			usersession.remove("schoolId");
			usersession.clear();

		} catch (Exception e) {

			LOG.error(e);
		}
		return SUCCESS;
	}

	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

}
