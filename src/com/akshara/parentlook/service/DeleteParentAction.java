package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class DeleteParentAction extends ActionSupport {
	private static final Logger LOG = Logger.getLogger(DeleteParentAction.class);
	private int id2;

	List<StudentsBean> studentlist = new ArrayList<>();

	@Override
	public String execute() throws Exception {

		try {

			SmsBeanDao.deleteParent(id2);

		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	public int getId2() {
		return id2;
	}

	public void setId2(int id2) {
		this.id2 = id2;
	}

}
