package com.akshara.parentlook.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.AbsentStudentbean;
import com.akshara.parentlook.db.bean.AttendenceBean;
import com.akshara.parentlook.db.bean.AttendenceWebBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class AttendenceListByDate extends ActionSupport implements SessionAware{

	private String changedDate;
	private int totalstudents;
	private Map<String,Object> usersession;
	List<BusesBean> buseslist=new ArrayList<>();
	List<AttendenceWebBean> busesattendencelist=new ArrayList<AttendenceWebBean>();
	 List onboardAbsentlist=new ArrayList<>();
	 List deboardAbsentlist=new ArrayList<>();
	 List<AbsentStudentbean> absentstudentlist=new ArrayList<>();
	 List<AbsentStudentbean>deboardedlist=new ArrayList<>();
	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession=usersession;
	}

	@Override
	public String execute() throws Exception {
		int a = (Integer) usersession.get("schoolId");
		 System.out.println(changedDate+"changedate");
		try{
			buseslist = SmsBeanDao.getBusDetailsList(a);
			if(buseslist.size()>0){ 
				Iterator iterator=buseslist.iterator();
				while(iterator.hasNext()){
					BusesBean busbean=(BusesBean) iterator.next();
					int onboardcount=0;
					int onboardabsentcount=0;
					int de_boardcount=0;
					int de_boardabsentcount=0;
				//	int ocuupency=busbean.getOccupency();
				
					 List<StudentsBean> stubean=SmsBeanDao.getStudentListBasedOnTrackedStatus(busbean.getBusId());
					
					 totalstudents=stubean.size();
					 System.out.println(totalstudents+"sizeeeerajani");
					RoutesBean routesBean=SmsBeanDao.getRouteDetails(busbean.getRouteId());
					 List<PickupPoints> pickuplist=SmsBeanDao.getPickUpPointsForBusId(busbean.getBusId());
					 if(pickuplist.size()>0){
						 Iterator iterator2=pickuplist.iterator();
						 while(iterator2.hasNext()){
							 PickupPoints pickupbean=(PickupPoints) iterator2.next();
							 HistoryLogBean historyBeanOnBoard=SmsBeanDao.getHistoryDetailsOnBoard(busbean.getBusId(),pickupbean.getPickupPointId(),changedDate,"Pickup");
						 
							 HistoryLogBean historyBeanDeBoard=SmsBeanDao.getHistoryDetailsDeBoard(busbean.getBusId(),pickupbean.getPickupPointId(),changedDate,"Drop");
							
							 if(historyBeanOnBoard!=null){
									 List<StudentsBean> studentlist=SmsBeanDao.getStudentListByPick(historyBeanOnBoard.getPickupPointId(),a);
									 Iterator iterator5=studentlist.iterator();
									 while(iterator5.hasNext()){
										 StudentsBean studentbean=(StudentsBean) iterator5.next();
										 AttendenceBean attendenceOnBoard=SmsBeanDao.getstudentattendence(studentbean.getStudentId(),changedDate,"0");
										 if(attendenceOnBoard!=null){
											 onboardcount+=1;
										 }else{
											 onboardabsentcount+=1;
											 onboardAbsentlist.add(studentbean.getStudentId());
										
										 }
									 }
								 
								
							 } 
							 if(historyBeanDeBoard!=null){
									 List<StudentsBean> studentlist=SmsBeanDao.getStudentListByDrop(historyBeanDeBoard.getPickupPointId(),a);
									 Iterator iterator6=studentlist.iterator();
									 while(iterator6.hasNext()){
										 StudentsBean studnetbean1=(StudentsBean) iterator6.next();
										 AttendenceBean attendenceDeBoard=SmsBeanDao.getstudentattendence(studnetbean1.getStudentId(), changedDate, "1");
										 if(attendenceDeBoard!=null){
											 de_boardcount+=1;
										 }else{
											 de_boardabsentcount+=1;
											 deboardAbsentlist.add(studnetbean1.getStudentId());
										 }
									 }
								 
							 }
						 }
					 }
					 
					 AttendenceWebBean attendenceWebBean=new AttendenceWebBean();
					 attendenceWebBean.setBusName(busbean.getBusName());
					 if(routesBean!=null){
					 attendenceWebBean.setRoutename(routesBean.getRouteName());}
					 attendenceWebBean.setTotalStudents(totalstudents);
					 attendenceWebBean.setOnboard(onboardcount);
					 attendenceWebBean.setOnboardAbsent(onboardabsentcount);
					 attendenceWebBean.setDeBoard(de_boardcount);
					 attendenceWebBean.setDeBoardAbsent(de_boardabsentcount);
					 busesattendencelist.add(attendenceWebBean);
					 
				}
				
			}
			if(onboardAbsentlist.size()>0){
			Iterator iterator=onboardAbsentlist.iterator();
			while(iterator.hasNext()){
				AbsentStudentbean absentStudentbean=new AbsentStudentbean();
				int studentid=(Integer)iterator.next();
				StudentsBean st=SmsBeanDao.getStudentDetails(studentid);
				
				  ClassBean classbean=SmsBeanDao.getclassbystudentid(st.getClassId());
				
				BusesBean busesbean1=SmsBeanDao.getBusDetails(st.getBusId());
				if(busesbean1!=null)
				{
				  RoutesBean routesbean1=SmsBeanDao.getRouteDetails(busesbean1.getRouteId());
			
				
				  
				 absentStudentbean.setBusName(busesbean1.getBusName());
				  absentStudentbean.setName(st.getStudentFirstName()+"-"+st.getStudentLastName());
					absentStudentbean.setRollno(st.getRollNo());
					if(routesbean1!=null)
					{
				absentStudentbean.setRoutename(routesbean1.getRouteName());
					
					}
					if(classbean!=null){
					absentStudentbean.setStudclass(classbean.getStudentClass());
					}
				  
					absentstudentlist.add(absentStudentbean);
				}
			}
			}
			Iterator Itr=deboardAbsentlist.iterator();
			while(Itr.hasNext())
			{
				AbsentStudentbean absentStudentbean1=new AbsentStudentbean();
				int studentid1=(Integer)Itr.next();
				StudentsBean st=SmsBeanDao.getStudentDetails(studentid1);
				
			  ClassBean classbean=SmsBeanDao.getclassbystudentid(st.getClassId());
			
			BusesBean busesbean1=SmsBeanDao.getBusDetails(st.getBusId());
			if(busesbean1!=null)
			{
			  RoutesBean routesbean1=SmsBeanDao.getRouteDetails(busesbean1.getRouteId());
		
		
			  
			  absentStudentbean1.setBusName(busesbean1.getBusName());
			  absentStudentbean1.setName(st.getStudentFirstName()+"-"+st.getStudentLastName());
				absentStudentbean1.setRollno(st.getRollNo());
				if(routesbean1!=null)
				{
			absentStudentbean1.setRoutename(routesbean1.getRouteName());
			}
				if(classbean!=null){
				absentStudentbean1.setStudclass(classbean.getStudentClass());
				}
			  
				deboardedlist.add(absentStudentbean1);
			}
			}	
		
		}catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public String getChangedDate() {
		return changedDate;
	}

	public void setChangedDate(String changedDate) {
		this.changedDate = changedDate;
	}

	public List<BusesBean> getBuseslist() {
		return buseslist;
	}

	public void setBuseslist(List<BusesBean> buseslist) {
		this.buseslist = buseslist;
	}

	public List<AttendenceWebBean> getBusesattendencelist() {
		return busesattendencelist;
	}

	public void setBusesattendencelist(List<AttendenceWebBean> busesattendencelist) {
		this.busesattendencelist = busesattendencelist;
	}

	public List getOnboardAbsentlist() {
		return onboardAbsentlist;
	}

	public void setOnboardAbsentlist(List onboardAbsentlist) {
		this.onboardAbsentlist = onboardAbsentlist;
	}

	public List getDeboardAbsentlist() {
		return deboardAbsentlist;
	}

	public void setDeboardAbsentlist(List deboardAbsentlist) {
		this.deboardAbsentlist = deboardAbsentlist;
	}

	public List<AbsentStudentbean> getAbsentstudentlist() {
		return absentstudentlist;
	}

	public void setAbsentstudentlist(List<AbsentStudentbean> absentstudentlist) {
		this.absentstudentlist = absentstudentlist;
	}

	public List<AbsentStudentbean> getDeboardedlist() {
		return deboardedlist;
	}

	public void setDeboardedlist(List<AbsentStudentbean> deboardedlist) {
		this.deboardedlist = deboardedlist;
	}

	public int getTotalstudents() {
		return totalstudents;
	}

	public void setTotalstudents(int totalstudents) {
		this.totalstudents = totalstudents;
	}
}
