package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.BusDetailsDao;
import com.akshara.parentlook.db.dao.GpsDao;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class BusesDetailsSchoolAction extends ActionSupport implements SessionAware {

	private static final Logger LOG = Logger.getLogger(BusesDetailsSchoolAction.class);

	private BusesBean busesBean;
	List<GPSTrackingBean> list = new ArrayList<GPSTrackingBean>();
	List<BusesBean> listBusesBean = new ArrayList<BusesBean>();
	Map<Integer, String> map = new LinkedHashMap<Integer, String>();
	List<BusesBean> busList = new ArrayList<BusesBean>();

	public List<BusesBean> getBusList() {
		return busList;
	}

	public void setBusList(List<BusesBean> busList) {
		this.busList = busList;
	}

	List<RoutesBean> rootList = new ArrayList<RoutesBean>();

	public List<RoutesBean> getRootList() {
		return rootList;
	}

	public void setRootList(List<RoutesBean> rootList) {
		this.rootList = rootList;
	}

	private Map<String, Object> usersession;
	private int BusId;
	private String lat, lon;

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public List<GPSTrackingBean> getList() {
		return list;
	}

	public void setList(List<GPSTrackingBean> list) {
		this.list = list;
	}

	public Map<Integer, String> getMap() {
		return map;
	}

	public void setMap(Map<Integer, String> map) {
		this.map = map;
	}

	private int SchoolId;

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public BusesBean getBusesBean() {
		return busesBean;
	}

	public void setBusesBean(BusesBean busesBean) {
		this.busesBean = busesBean;
	}

	public List<BusesBean> getListBusesBean() {
		return listBusesBean;
	}

	public void setListBusesBean(List<BusesBean> listBusesBean) {
		this.listBusesBean = listBusesBean;

	}

	@Override
	public String execute() {
		try {
			int i = (Integer) usersession.get("schoolId");
			SchoolsDetailsBean bean1 = SmsBeanDao.getSchoolDetails(i);
			String schollat = bean1.getMapLocationLatLang();
			String str1 = schollat.substring(1, schollat.length() - 1);
			String[] st = str1.split(",");
			lat = st[0];
			lon = st[1];
			rootList = SmsBeanDao.getRouteIdList(i);
			listBusesBean = BusDetailsDao.getBusList(i);
			Iterator<BusesBean> iterator = listBusesBean.iterator();
			Iterator<RoutesBean> rootItr = rootList.iterator();
			while (iterator.hasNext()) {
				BusesBean bean = (BusesBean) iterator.next();
				GPSTrackingBean gpsBean = GpsDao.getGpsList(bean.getBusId());
				list.add(gpsBean);

			}
			while (rootItr.hasNext()) {
				RoutesBean rootBean = (RoutesBean) rootItr.next();
				List<BusesBean> busesBean = SmsBeanDao.getBusRootList(rootBean.getRouteId());

				busList.addAll(busesBean);
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;

	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

}
