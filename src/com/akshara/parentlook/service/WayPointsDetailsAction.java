package com.akshara.parentlook.service;

import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class WayPointsDetailsAction extends ActionSupport implements ModelDriven<WayPointsBean> {
	private static final Logger LOG = Logger.getLogger(WayPointsDetailsAction.class);

	WayPointsBean wayPointsBean;

	public WayPointsBean getModel() {
		wayPointsBean = new WayPointsBean();
		return wayPointsBean;
	}

	@Override
	public String execute() throws Exception {
		try {
			int a = SmsBeanDao.saveWayPoint(wayPointsBean);
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}
}
