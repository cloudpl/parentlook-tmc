package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentViewBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportPendingApprovalsPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;
private final String RecordStatus="ACTIVE";
	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("pendingApprovals Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("             ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(5);

		String status = "2";

		int a = (int) usersession.get("schoolId");

		List<StudentsBean> bean = SmsBeanDao.getStudentDetailsListByTrackedStatus(a, status,RecordStatus);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(new Phrase("Name", boldFont));
		table.addCell(new Phrase("Class", boldFont));
		table.addCell(new Phrase("Address", boldFont));
		table.addCell(new Phrase("PickupLocation", boldFont));
		table.addCell(new Phrase("DropLocation", boldFont));

		Iterator iterator = bean.iterator();
		while (iterator.hasNext()) {
			StudentsBean studentsBean = (StudentsBean) iterator.next();
			if (studentsBean != null) {

				String name = studentsBean.getStudentFirstName() +"  "+ studentsBean.getStudentLastName();

				String pick = studentsBean.getPickupPoint();
				String drop = studentsBean.getDropPoint();

				if (pick == null && drop == null) {
					String stuname = studentsBean.getStudentFirstName() +" "+ studentsBean.getStudentLastName();
					table.addCell(stuname);

					String pik = "Set PickUp Point";
					String dop = "Set Drop Point";
					int classid = studentsBean.getClassId();
					ClassBean bean2 = new ClassBean();

					bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
					if (bean2 != null) {
						String classname = bean2.getStudentClass();
						table.addCell(classname);
					}
					WayPointsBean pointsBean = SmsBeanDao.getWayPoint(studentsBean.getStudentId());
					if (pointsBean != null) {
						String addr = pointsBean.getWayPoint();

						
						String s = addr.substring(0, Math.min(addr.length(), 10));
						table.addCell(s);

					} else {
						String add = "   ";
						
						table.addCell(add);
					}

					table.addCell(pik);
					table.addCell(dop);

				}
				else if (pick == null && drop != null) {

					String stuname = studentsBean.getStudentFirstName() + studentsBean.getStudentLastName();
					table.addCell(stuname);
					
					String pik = "Set PickUp Point";
					String dop = studentsBean.getDropPoint();
					int classid = studentsBean.getClassId();
					ClassBean bean2 = new ClassBean();

					bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
					if (bean2 != null) {
						String classname = bean2.getStudentClass();
						table.addCell(classname);
					}
					WayPointsBean pointsBean = SmsBeanDao.getWayPoint(studentsBean.getStudentId());
					if (pointsBean != null) {
						String addr = pointsBean.getWayPoint();

						String s = addr.substring(0, Math.min(addr.length(), 10));
						table.addCell(s);

					} else {
						String add = "  ";
						table.addCell(add);
					}
					table.addCell(pik);
					table.addCell(dop);

				}
				else {
					String stuname = studentsBean.getStudentFirstName() + studentsBean.getStudentLastName();
					table.addCell(stuname);
					
					 System.out.println(stuname+"pick not null and drop null");
					 
					String pik = studentsBean.getPickupPoint();
					String dop = "Set Drop Point";
					int classid = studentsBean.getClassId();
					ClassBean bean2 = new ClassBean();

					bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
					if (bean2 != null) {
						String classname = bean2.getStudentClass();
						table.addCell(classname);
					}
					WayPointsBean pointsBean = SmsBeanDao.getWayPoint(studentsBean.getStudentId());
					if (pointsBean != null) {
						String addr = pointsBean.getWayPoint();
						String s = addr.substring(0, Math.min(addr.length(), 10));
						table.addCell(s);

					} else {
						String add = "  ";
						table.addCell(add);
					}

					table.addCell(pik);
					table.addCell(dop);

				}
			}
		}
		return table;
	}
}
