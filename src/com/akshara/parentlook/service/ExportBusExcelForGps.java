package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.GPSTrackingWebBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportBusExcelForGps extends ActionSupport implements SessionAware{

		private InputStream inputStream;
		private Map<String, Object> usersession;

		@Override
		public void setSession(Map<String, Object> usersession) {
			this.usersession = usersession;
		}

		public InputStream exportToExcel() throws Exception {
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("GpsBusDetails");
			XSSFRow rowhead = sheet.createRow(0);

			rowhead.createCell(0).setCellValue("BusId");
			sheet.autoSizeColumn(0);
			rowhead.createCell(1).setCellValue("BusName");
			sheet.autoSizeColumn(1);
			rowhead.createCell(2).setCellValue("UserName");
			sheet.autoSizeColumn(2);
			rowhead.createCell(3).setCellValue("Password");
			sheet.autoSizeColumn(3);
			rowhead.createCell(4).setCellValue("LoginStatus");
			sheet.autoSizeColumn(4);
			 
			int a = (Integer) usersession.get("schoolId");
			List<BusesBean> buslist = new ArrayList<BusesBean>();
			buslist = SmsBeanDao.getBusDetailsList(a);
			int x = 1;
			Iterator<BusesBean> iterator = buslist.iterator();
			while (iterator.hasNext()) {

				BusesBean busbean1=(BusesBean) iterator.next();
				 GPSTrackingBean gpsTrackingBean=new GPSTrackingBean();
					gpsTrackingBean=SmsBeanDao.getgpsdetails(busbean1.getBusId());
					if(gpsTrackingBean!=null){
					BusesBean busBean1=SmsBeanDao.getBusDetails(gpsTrackingBean.getBusId());

				XSSFRow row = sheet.createRow(x);

				int busid = gpsTrackingBean.getBusId();
				String busname = busBean1.getBusName();
				String uname = gpsTrackingBean.getUserName();
				String pwd = gpsTrackingBean.getPassword();
				int login=gpsTrackingBean.getLoginStatus();
				
				row.createCell(0).setCellValue(busid);
				row.createCell(1).setCellValue(busname);
				row.createCell(2).setCellValue(uname);
				row.createCell(3).setCellValue(pwd);

				row.createCell(4).setCellValue(login);

			 				x++;
			}}
			autoSizeColumn(workbook);
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			workbook.write(arrayOutputStream);
			this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
			return null;
		}

		private void autoSizeColumn(XSSFWorkbook workbook) {
			int noOfSheets = workbook.getNumberOfSheets();
			for (int i = 0; i < noOfSheets; i++) {
				XSSFSheet sheet = workbook.getSheetAt(i);
				if (sheet.getPhysicalNumberOfRows() > 0) {
					Row row = sheet.getRow(0);
					Iterator<Cell> iterator = row.cellIterator();
					while (iterator.hasNext()) {
						Cell cell = iterator.next();
						int columIndex = cell.getColumnIndex();
						sheet.autoSizeColumn(columIndex);
					}
				}
			}
		}

		@Override
		public String execute() throws Exception {
			exportToExcel();
			return SUCCESS;
		}

		public InputStream getInputStream() throws Exception {
			return this.inputStream;
		}

		public void setInputStream(java.io.InputStream inputStream) {
			this.inputStream = inputStream;
		}

}
