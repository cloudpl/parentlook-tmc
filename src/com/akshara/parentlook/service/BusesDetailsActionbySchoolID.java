package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class BusesDetailsActionbySchoolID extends ActionSupport implements SessionAware {
	private static final Logger LOG = Logger.getLogger(BusesDetailsActionbySchoolID.class);
	GPSTrackingBean gpstrackingBean = null;

	public GPSTrackingBean getGpstrackingBean() {
		return gpstrackingBean;
	}

	public void setGpstrackingBean(GPSTrackingBean gpstrackingBean) {
		this.gpstrackingBean = gpstrackingBean;
	}

	public List<GPSTrackingBean> getBusesdetail() {
		return busesdetail;
	}

	public void setBusesdetail(List<GPSTrackingBean> busesdetail) {
		this.busesdetail = busesdetail;
	}

	List<GPSTrackingBean> busesdetail = new ArrayList<>();
	List<BusesBean> busesbean = new ArrayList<>();
	SchoolsDetailsBean schoolbean = new SchoolsDetailsBean();
	private Map<String, Object> usersession;

	private String Latitude;
	private String schoolname;

	public String getSchoolname() {
		return schoolname;
	}

	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	private String Longitude;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {
		int a = (Integer) usersession.get("schoolId");
		schoolbean = SmsBeanDao.getSchoolDetails(a);
		busesbean = SmsBeanDao.getBusDetailsList(a);
		usersession.put("schoolName", schoolbean.getSchoolName());
		if (schoolbean.getPhoto() != null) {
			byte[] itemImage = schoolbean.getPhoto().getBytes(1,
					(int) schoolbean.getPhoto().length());

			Base64 b = new Base64();
			byte[] encodingImgAsBytes = b.encode(itemImage);

			String strType = b.encodeToString(encodingImgAsBytes);

			byte[] valueDecoded = Base64.decodeBase64(strType);
			String decodedImgStr = new String(valueDecoded);

			schoolbean.setImageStr(decodedImgStr);

		

		usersession.put("image", decodedImgStr);
		}
		Iterator it = busesbean.iterator();
		while (it.hasNext()) {
			BusesBean busbean1 = (BusesBean) it.next();
			GPSTrackingBean gpsTrackingBean = SmsBeanDao.getgpsdetails(busbean1.getBusId());
			if (gpsTrackingBean != null)
				busesdetail.add(gpsTrackingBean);
		}

		try{
		String str = schoolbean.getMapLocationLatLang();
		str = str.substring(1, str.length() - 1);
		String str1[] = str.split(",");
		Latitude = str1[0];
		Longitude = str1[1];
		schoolname = schoolbean.getSchoolName();
		}catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public SchoolsDetailsBean getSchoolbean() {
		return schoolbean;
	}

	public void setSchoolbean(SchoolsDetailsBean schoolbean) {
		this.schoolbean = schoolbean;
	}

	public List<BusesBean> getBusesbean() {
		return busesbean;
	}

	public void setBusesbean(List<BusesBean> busesbean) {
		this.busesbean = busesbean;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

}
