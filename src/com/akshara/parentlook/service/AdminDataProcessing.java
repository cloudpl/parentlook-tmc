package com.akshara.parentlook.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;

import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class AdminDataProcessing extends ActionSupport implements
		ModelDriven<SchoolAdminsBean> {

	private static final Logger LOG = Logger
			.getLogger(AdminDataProcessing.class);
	private String filePath;
	private File AdminFile;
	private SchoolAdminsBean adminBean1;

	public SchoolAdminsBean getAdminBean1() {
		return adminBean1;
	}

	public void setAdminBean1(SchoolAdminsBean adminBean1) {
		this.adminBean1 = adminBean1;
	}

	public String execute() throws Exception {
		return SUCCESS;
	}

	public String xslToDb() throws IOException, ParseException {
		try {
			FileInputStream fileInputStream = new FileInputStream(AdminFile);

			POIFSFileSystem fs = new POIFSFileSystem(fileInputStream);
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			Row row;
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {

				int SchoolId = (int) sheet.getRow(i).getCell(1)
						.getNumericCellValue();

				String AdminName = sheet.getRow(i).getCell(2)
						.getStringCellValue();
				String AdminPassword = sheet.getRow(i).getCell(3)
						.getStringCellValue();
				String Email = sheet.getRow(i).getCell(4).getStringCellValue();
				String PhoneNumber = sheet.getRow(i).getCell(6)
						.getStringCellValue();
				String AdminRole = sheet.getRow(i).getCell(7)
						.getStringCellValue();
				String AdminStatus = sheet.getRow(i).getCell(8)
						.getStringCellValue();
				String CreatedDate = sheet.getRow(i).getCell(9)
						.getStringCellValue();
				Date LastLoggedIn = sheet.getRow(i).getCell(10)
						.getDateCellValue();
				String imageStr = sheet.getRow(i).getCell(11)
						.getStringCellValue();
				adminBean1.setSchoolId(SchoolId);
				adminBean1.setAdminName(AdminName);
				adminBean1.setAdminPassword(AdminPassword);
				adminBean1.setEmail(Email);
				adminBean1.setPhoneNumbe(PhoneNumber);
				adminBean1.setAdminRole(AdminRole);
				adminBean1.setAdminStatus(AdminStatus);
				AdminDAO.saveAdminData(adminBean1);
				 
			}

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;

	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public File getAdminFile() {
		return AdminFile;
	}

	public void setAdminFile(File adminFile) {
		AdminFile = adminFile;
	}

	@Override
	public SchoolAdminsBean getModel() {
		adminBean1 = new SchoolAdminsBean();
		return adminBean1;
	}

}
