package com.akshara.parentlook.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolNotifications;
import com.akshara.parentlook.db.bean.SchoolNotificationsWeb;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class SchoolNotificationAction extends ActionSupport implements ModelDriven<SchoolNotifications>, SessionAware {
	private SchoolNotifications bean;
	List<SchoolNotificationsWeb> notificationsWebs = new ArrayList<SchoolNotificationsWeb>();
	List<SchoolNotifications> list = new ArrayList<SchoolNotifications>();
	private int count;
	private Map<String, Object> usersession;

	@Override
	public SchoolNotifications getModel() {
		bean = new SchoolNotifications();
		return bean;
	}

	@Override
	public String execute() throws Exception {
		int a = (Integer) usersession.get("schoolId");
		count = SmsBeanDao.totalCount(bean, a);
		list = SmsBeanDao.notificationList(a);
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {

			SchoolNotifications notifications = (SchoolNotifications) iterator.next();

			SchoolNotificationsWeb notificationsWeb = new SchoolNotificationsWeb();
			notificationsWeb.setId(notifications.getId());
			if (notifications.getDescription().length() > 100) {
				notificationsWeb.setDescription(notifications.getDescription().substring(0, 100) + "....");
			} else {
				notificationsWeb.setDescription(notifications.getDescription());

			}

			notificationsWeb.setReadStatus(notifications.getReadStatus());
			notificationsWeb.setSchoolId(notifications.getSchoolId());
			StudentsBean bean = SmsBeanDao.getStudentDetails(notifications.getStudentId());
			if (bean != null) {
				notificationsWeb.setStudentName(bean.getStudentFirstName() + " " + bean.getStudentLastName());

			}
			notificationsWeb.setCategory(notifications.getCategory());
			notificationsWeb.setTime("Time to caluclate");
			notificationsWeb.setType(notifications.getType());
			notificationsWeb.setStudentId(notifications.getStudentId());

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();

			String dateEnd = format.format(date);
			System.out.println(dateEnd);
			Date d1 = null;
			Date d2 = null;

			d1 = format.parse(notifications.getTime());
			d2 = format.parse(dateEnd);

			long diff = d2.getTime() - d1.getTime();

			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;
			long diffDays = diff / (24 * 60 * 60 * 1000);

			String datediff = "  " + diffDays + "days, " + diffHours + "hours, " + diffMinutes + "minutes, "
					+ diffSeconds + "seconds. ";
			String fTime = diffSeconds + " Seconds ago";
			if (diffDays > 0) {
				fTime = diffDays + " Days ago";
			} else if (diffHours > 0) {
				fTime = diffHours + " Hours ago";
			} else if (diffMinutes > 0) {
				fTime = diffDays + " Mints ago";
			}

			notificationsWeb.setTime(fTime);

			notificationsWebs.add(notificationsWeb);
		}

		return "success";
	}

	public int getCount() {
		return count;
	}

	public SchoolNotifications getBean() {
		return bean;
	}

	public void setBean(SchoolNotifications bean) {
		this.bean = bean;
	}

	public List<SchoolNotifications> getList() {
		return list;
	}

	public void setList(List<SchoolNotifications> list) {
		this.list = list;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<SchoolNotificationsWeb> getNotificationsWebs() {
		return notificationsWebs;
	}

	public void setNotificationsWebs(List<SchoolNotificationsWeb> notificationsWebs) {
		this.notificationsWebs = notificationsWebs;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

}
