package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class AdminChangePassword extends ActionSupport implements ModelDriven<SchoolAdminsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(AdminChangePassword.class);
	private SchoolsDetailsBean schoolBean;
	private String NewPassword;
	private String ConfirmPassword;
	private SchoolAdminsBean adminBean;
	private Map<String, Object> usersession;
	private String OldPasswordVal;
	private String msg;

	public String getOldPasswordVal() {
		return OldPasswordVal;
	}

	public void setOldPasswordVal(String oldPasswordVal) {
		OldPasswordVal = oldPasswordVal;
	}

	public String getNewPassword() {
		return NewPassword;
	}

	public void setNewPassword(String newPassword) {
		NewPassword = newPassword;
	}

	public String getConfirmPassword() {
		return ConfirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		ConfirmPassword = confirmPassword;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String execute() throws Exception {

		int a = (int) usersession.get("schoolId");
		schoolBean = SmsBeanDao.editSchoolProfile(a);
		String schoolName = schoolBean.getSchoolName();
		usersession.put("schoolName", schoolName);
		if (schoolBean.getPhoto() != null) {
			byte[] itemImage = schoolBean.getPhoto().getBytes(1, (int) schoolBean.getPhoto().length());

			Base64 b = new Base64();
			byte[] encodingImgAsBytes = b.encode(itemImage);

			String strType = b.encodeToString(encodingImgAsBytes);

			byte[] valueDecoded = Base64.decodeBase64(strType);
			String decodedImgStr = new String(valueDecoded);

			schoolBean.setImageStr(decodedImgStr);

			usersession.put("image", decodedImgStr);
		}

		return SUCCESS;
	}

	public String changePwd() throws Exception {
		try {
			int a = (Integer) usersession.get("adminId");
			adminBean = (SchoolAdminsBean) AdminDAO.getAdminDetail(a);
			if (OldPasswordVal.equals(adminBean.getAdminPassword())) {

			} else {
				msg = "oldpassword doesn't match";
				return "error";
			}

			if (NewPassword.equals(adminBean.getAdminPassword())) {
				return INPUT;
			}

			else if (NewPassword.equals(ConfirmPassword)) {
				adminBean = AdminDAO.updatePwd(NewPassword, a);
			} else {
				return INPUT;
			}

		} catch (Exception e) {
			LOG.error(e);
		}

		return SUCCESS;
	}

	@Override
	public SchoolAdminsBean getModel() {
		adminBean = new SchoolAdminsBean();

		return adminBean;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public SchoolAdminsBean getAdminBean() {
		return adminBean;
	}

	public void setAdminBean(SchoolAdminsBean adminBean) {
		this.adminBean = adminBean;
	}

	public SchoolsDetailsBean getSchoolBean() {
		return schoolBean;
	}

	public void setSchoolBean(SchoolsDetailsBean schoolBean) {
		this.schoolBean = schoolBean;
	}

}
