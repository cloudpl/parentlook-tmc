package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ParentPhoneCheck extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private String phonenumber;
	private String msg;
	private ParentsBean parentbean;

	@Override
	public String execute() throws Exception {
		int a = (int) usersession.get("schoolId");
		try {
			parentbean = SmsBeanDao.checkPhoneNumber(a, phonenumber);

			if (parentbean != null) {

				msg = "Exists";
			} else {
				msg = "NOT Exists";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public ParentsBean getParentbean() {
		return parentbean;
	}

	public void setParentbean(ParentsBean parentbean) {
		this.parentbean = parentbean;
	}

}
