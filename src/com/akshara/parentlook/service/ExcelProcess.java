package com.akshara.parentlook.service;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.opensymphony.xwork2.ActionSupport;

public class ExcelProcess extends ActionSupport {

	private static final Logger LOG = Logger.getLogger(ExcelProcess.class);
	private int busId;

	@Override
	public String execute() throws Exception {

		String filedir = "/usr/local/tomcat7/webapps/Geologs/Bus" + busId;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = dateFormat.format(new Date());
		String filepath = filedir + "/geolog.xls";
		LOG.info("ExcelProcess" + filepath);

		try {

			File file1 = new File(filepath);
			HSSFWorkbook workbook = null;
			if (file1.exists()) {
				FileInputStream fileStream = new FileInputStream(file1);

				workbook = new HSSFWorkbook(fileStream);

				HSSFSheet sheet = workbook.getSheetAt(0);

				int num = sheet.getPhysicalNumberOfRows();
				for (int i = 0; i < num; i++) {
					Row row = sheet.getRow(i);
					Cell latcell = row.getCell(0);
					Cell langcell = row.getCell(1);
					Cell busidcell = row.getCell(2);
					Cell picdropcell = row.getCell(3);

					String lat = latcell.getStringCellValue();
					String lang = langcell.getStringCellValue();
					int busId = (int) busidcell.getNumericCellValue();
					int picDrop = (int) picdropcell.getNumericCellValue();
					LOG.info("latitude for Bus:" + lat);
					LOG.info("Longitude For Bus:" + lang);
					LOG.info("Bus Id:" + busId);
					LOG.info("Picup and Drop Status:" + picDrop);

				}
			}

		} catch (Exception e) {
			LOG.error(e);
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public int getBusId() {
		return busId;
	}

	public void setBusId(int busId) {
		this.busId = busId;
	}
}
