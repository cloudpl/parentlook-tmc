package com.akshara.parentlook.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.GPSTrackingBean;
import com.akshara.parentlook.db.bean.GPSTrackingWebBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class AdminLogin extends ActionSupport implements ModelDriven<SchoolAdminsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(AdminLogin.class);
	private Map<String, Object> usersession;
	private SchoolAdminsBean schoolAdminBean;
	private String errMsg;
	List<SchoolAdminsBean> adminList = new ArrayList<SchoolAdminsBean>();
	List<SchoolsDetailsBean> schoolslist = new ArrayList<SchoolsDetailsBean>();
	List<GPSTrackingWebBean> gpsweblist = new ArrayList<GPSTrackingWebBean>();

	public SchoolAdminsBean getSchoolAdminBean() {
		return schoolAdminBean;
	}

	public void setSchoolAdminBean(SchoolAdminsBean schoolAdminBean) {
		this.schoolAdminBean = schoolAdminBean;
	}

	@Override
	public String execute() throws Exception {
		try {
			SchoolAdminsBean adminBean = new SchoolAdminsBean();

			adminBean = (SchoolAdminsBean) AdminDAO.getAdminList(schoolAdminBean.getAdminName());
			if (adminBean != null) {
				if (schoolAdminBean.getAdminName().equals(adminBean.getAdminName())
						&& schoolAdminBean.getAdminPassword().equals(adminBean.getAdminPassword())) {
					if (adminBean.getAdminStatus().equals("ACTIVE")) {
						if (adminBean.getAdminRole().equals("SITEADMIN")) {

							usersession.put("adminId", adminBean.getAdminId());
							usersession.put("schoolId", adminBean.getSchoolId());
							usersession.put("adminName", adminBean.getAdminName());
							usersession.put("adminRole", adminBean.getAdminRole());
							schoolslist = SmsBeanDao.getRecentlyAddedSchools();
							int a = (Integer) usersession.get("adminId");
							AdminDAO.getLastLogged(schoolAdminBean, a);
							return "superadmin";

						} else {

							usersession.put("adminId", adminBean.getAdminId());
							usersession.put("schoolId", adminBean.getSchoolId());
							usersession.put("adminRole", adminBean.getAdminRole());
							usersession.put("adminName", adminBean.getAdminName());

							int a = (Integer) usersession.get("adminId");
							AdminDAO.getLastLogged(schoolAdminBean, a);

							return "schooladmin";
						}
					}
				} else {
					errMsg = "Invalid Login Credentials";
					return "invalid";
				}
			} else {

				errMsg = "Invalid Login Credentials";
				return "invalid";
			}

		} catch (Exception e) {
			LOG.error(e);

		}
		return SUCCESS;

	}

	public String login() throws Exception {

		return SUCCESS;
	}

	@Override
	public SchoolAdminsBean getModel() {
		schoolAdminBean = new SchoolAdminsBean();
		return schoolAdminBean;
	}

	public String loginDash() throws ParseException {

		List<SchoolsDetailsBean> schoolbean = new ArrayList<SchoolsDetailsBean>();
		schoolbean = SmsBeanDao.getSchoolDetailsList();
		List<BusesBean> busbean = new ArrayList<BusesBean>();
		busbean = SmsBeanDao.getBuses();
		List<ParentsBean> parentsbean = new ArrayList<ParentsBean>();
		parentsbean = SmsBeanDao.getParents();
		System.out.println(parentsbean.size() + "sizeee");
		List<StudentsBean> studentsbean = new ArrayList<StudentsBean>();
		studentsbean = SmsBeanDao.getStudents();
		usersession.put("schoolslist", schoolbean.size());
		usersession.put("buseslist", busbean.size());
		usersession.put("parentslist", parentsbean.size());
		usersession.put("studentslist", studentsbean.size());

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		dateFormat.format(date);
		DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = new Date();
		List<SchoolsDetailsBean> schoolslist = new ArrayList<SchoolsDetailsBean>();

		schoolslist = SmsBeanDao.getSchoolDetailsList();
		if (schoolslist.size() > 0) {
			Iterator iterator = schoolslist.iterator();
			while (iterator.hasNext()) {
				int vcount = 0;
				int tcount = 0;
				SchoolsDetailsBean schoolBean = (SchoolsDetailsBean) iterator.next();
				System.out.println("***************schoolname****************" + schoolBean.getSchoolName());
				GPSTrackingWebBean gpswebBean = new GPSTrackingWebBean();
				List<BusesBean> buseslist = new ArrayList<BusesBean>();
				buseslist = SmsBeanDao.getBusDetailsList(schoolBean.getSchoolId());
				if (buseslist.size() > 0) {
					Iterator iterator2 = buseslist.iterator();
					while (iterator2.hasNext()) {
						BusesBean busBean = (BusesBean) iterator2.next();
						System.out.println("*******BusID*******" + busBean.getBusId());
						GPSTrackingBean gpsTrackingBean = new GPSTrackingBean();

						String tbegin = busBean.getReturnStartTime();
						String treturn = busBean.getTransportStartTime();
						String str1 = tbegin.replaceAll("\\s", "");
						String str2 = treturn.replaceAll("\\s", "");
						Date d1 = dateFormat.parse(str1);
						Date d2 = dateFormat.parse(str2);
						gpsTrackingBean = SmsBeanDao.getgpsdetails(busBean.getBusId());
						if (gpsTrackingBean != null) {
							if (dateFormat.parse(dateFormat.format(date))
									.after(dateFormat.parse(dateFormat.format(d1)))) {

								String dir = "Drop";
								String type = "Start Point";
System.out.println("@@@@@@@@@@@@@Drop@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
								HistoryLogBean historyBean = SmsBeanDao.getHistoryDeatils(gpsTrackingBean.getBusId(),
										dateFormat1.format(date1), dir, type);
								if (historyBean != null) {
									break;
								} else {
									System.out.println(gpsTrackingBean.getBusId() + "busiddd");

									if (gpsTrackingBean.getTracking() == 0) {
										tcount++;
										System.out.println(tcount + "tracking count");
										gpswebBean.setTcount(tcount);
									}
									if (gpsTrackingBean.getSurveillance() == 0) {
										vcount++;
										System.out.println(vcount + "video count");
										gpswebBean.setVcount(vcount);
									}
								}

							} else if (dateFormat.parse(dateFormat.format(date))
									.after(dateFormat.parse(dateFormat.format(d2)))) {
								System.out.println("@@@@@@@@@@@@@Pickup@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
								String dir1 = "Pickup";
								String type1 = "School Point";
								HistoryLogBean historyBean = SmsBeanDao.getHistoryDeatils(gpsTrackingBean.getBusId(),
										dateFormat1.format(date1), dir1, type1);
								if (historyBean != null) {
									break;
								} else {
									System.out.println(gpsTrackingBean.getBusId() + "busiddd");

									if (gpsTrackingBean.getTracking() == 0) {
										tcount++;
										System.out.println(tcount + "tracking count");
										gpswebBean.setTcount(tcount);
									}
									if (gpsTrackingBean.getSurveillance() == 0) {
										vcount++;
										System.out.println(vcount + "video count");
										gpswebBean.setVcount(vcount);
									}
								}

							}

						}
					}
				}

				gpswebBean.setSchoolName(schoolBean.getSchoolName());
				gpswebBean.setSchoolId(schoolBean.getSchoolId());
				gpswebBean.setContacnum(schoolBean.getContactNumber());
				gpsweblist.add(gpswebBean);
			}
		}

		/*
		 * schoolslist=SmsBeanDao.getRecentlyAddedSchools(); int a = (Integer)
		 * usersession.get("adminId"); AdminDAO.getLastLogged(schoolAdminBean,
		 * a);
		 */
		return "log";
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public String getErrMsg() {
		return errMsg;
	}

	public List<SchoolsDetailsBean> getSchoolslist() {
		return schoolslist;
	}

	public void setSchoolslist(List<SchoolsDetailsBean> schoolslist) {
		this.schoolslist = schoolslist;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public List<GPSTrackingWebBean> getGpsweblist() {
		return gpsweblist;
	}

	public void setGpsweblist(List<GPSTrackingWebBean> gpsweblist) {
		this.gpsweblist = gpsweblist;
	}

}
