package com.akshara.parentlook.service;

public class StudentsWebBean {

	private int StudentId;
	private String StudentName;
	private int SchoolId;
	private int ClassId;
	private int ParentId;
	private String TrackedStatus;
	private String schoolName;
	private String wayPoint;
	private int BusId;
	private String latLang;

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public String getStudentName() {
		return StudentName;
	}

	public void setStudentName(String studentName) {
		StudentName = studentName;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public int getClassId() {
		return ClassId;
	}

	public void setClassId(int classId) {
		ClassId = classId;
	}

	public int getParentId() {
		return ParentId;
	}

	public void setParentId(int parentId) {
		ParentId = parentId;
	}

	public String getTrackedStatus() {
		return TrackedStatus;
	}

	public void setTrackedStatus(String trackedStatus) {
		TrackedStatus = trackedStatus;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getWayPoint() {
		return wayPoint;
	}

	public void setWayPoint(String wayPoint) {
		this.wayPoint = wayPoint;
	}

	public String getLatLang() {
		return latLang;
	}

	public void setLatLang(String latLang) {
		this.latLang = latLang;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

}
