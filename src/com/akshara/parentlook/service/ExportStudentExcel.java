package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportStudentExcel extends ActionSupport implements SessionAware {

	private Map<String, Object> usersession;
	private InputStream inputStream;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Students");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("StudentId");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("StudentName");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("Class");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("RollNo");
		sheet.autoSizeColumn(3);
		rowhead.createCell(4).setCellValue("Parent");
		sheet.autoSizeColumn(4);
		rowhead.createCell(5).setCellValue("Bus");
		sheet.autoSizeColumn(5);
		rowhead.createCell(6).setCellValue("Route");
		sheet.autoSizeColumn(6);

		int a = (Integer) usersession.get("schoolId");
		List<StudentsBean> studentlist = new ArrayList<StudentsBean>();
		studentlist = SmsBeanDao.getStudentDetailsList(a);
		int x = 1;
		Iterator<StudentsBean> iterator = studentlist.iterator();
		while (iterator.hasNext()) {

			StudentsBean bean = (StudentsBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			int studentid = bean.getStudentId();
			row.createCell(0).setCellValue(studentid);
			String studentName = bean.getStudentFirstName() + bean.getStudentLastName();
			row.createCell(1).setCellValue(studentName);
			int classid = bean.getClassId();
			ClassBean classBean = new ClassBean();
			classBean = SmsBeanDao.getClassesDetailsbyId(classid);
			if (classBean != null) {
				String className = classBean.getDescription();
				row.createCell(2).setCellValue(className);
			}
			String roll = bean.getRollNo();
			row.createCell(3).setCellValue(roll);
			int parentid = bean.getParentId();
			ParentsBean parentsBean = new ParentsBean();
			parentsBean = SmsBeanDao.getParentDetails(parentid);
			if (parentsBean != null) {
				String parentname = parentsBean.getFirstName() + parentsBean.getLastName();
				row.createCell(4).setCellValue(parentname);
			}
			int busid = bean.getBusId();
			BusesBean busbean = SmsBeanDao.getBusDetails(busid);
			if (busbean != null) {
				String bus = busbean.getBusName();
				row.createCell(5).setCellValue(bus);
			}
			if (busbean != null) {
				int routeid = busbean.getRouteId();

				RoutesBean bean3 = new RoutesBean();
				if (bean3 != null) {
					bean3 = SmsBeanDao.getRouteDetails(routeid);

					String routename = bean3.getRouteName();

					row.createCell(6).setCellValue(routename);

				}
			}

			x++;
		}
		autoSizeColumn(workbook);

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));

		return null;
	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
