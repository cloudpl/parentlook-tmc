package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportLocationUnsubmittedPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;
	private final String RecordStatus = "ACTIVE";

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("LocationUnsubmitted Detail List",
				new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("             ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(4);

		String status = "3";

		int a = (int) usersession.get("schoolId");

		List<StudentsBean> bean = SmsBeanDao.getStudentDetailsListByTrackedStatus(a, status, RecordStatus);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(new Phrase("StudentName", boldFont));
		table.addCell(new Phrase("ParentName", boldFont));
		table.addCell(new Phrase("Class", boldFont));
		table.addCell(new Phrase("MobileNo", boldFont));

		Iterator iterator = bean.iterator();
		while (iterator.hasNext()) {
			StudentsBean studentsBean = (StudentsBean) iterator.next();
			if (studentsBean != null) {

				String name = studentsBean.getStudentFirstName() + "  " + studentsBean.getStudentLastName();

				table.addCell(name);

				ParentsBean parentBean = SmsBeanDao.getParentDetails(studentsBean.getParentId());
				if (parentBean != null) {

					String pname = parentBean.getFirstName() + " " + parentBean.getLastName();
					table.addCell(pname);
				} else {
					String parent = "   ";
					table.addCell(parent);
				}
				int classid = studentsBean.getClassId();
				ClassBean bean2 = new ClassBean();

				bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
				if (bean2 != null) {
					String classname = bean2.getStudentClass();
					table.addCell(classname);
				} else {
					String cls = "  ";
					table.addCell(cls);
				}
				if (parentBean != null) {
					String moblie = parentBean.getMobileNumber();
					table.addCell(moblie);

				} else {
					String mob = "   ";
					table.addCell(mob);
				}

			}
		}

		return table;
	}

}
