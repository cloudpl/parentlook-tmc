package com.akshara.parentlook.service;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class CheckRegNumForBus extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private String regnumber;
	private String msg;
	private BusesBean busbean;
    private String busName;
    private String busmsg;
	@Override
	public String execute() throws Exception {
		int a = (int) usersession.get("schoolId");
		try {

			busbean = SmsBeanDao.checkRegNumber(a, regnumber);
			if (busbean != null) {

				msg = "Exists";
			} else {
				msg = "NOT Exists";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}

	public String ckeckBusName(){
		
		int a = (int) usersession.get("schoolId");
		try {

			busbean = SmsBeanDao.checkBusName(a, busName);
			if (busbean != null) {
				busmsg = "Exists";
			} else {
				busmsg = "NOT Exists";
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return SUCCESS;
	}
	
	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getRegnumber() {
		return regnumber;
	}

	public void setRegnumber(String regnumber) {
		this.regnumber = regnumber;
	}

	public BusesBean getBusbean() {
		return busbean;
	}

	public void setBusbean(BusesBean busbean) {
		this.busbean = busbean;
	}

	public String getBusName() {
		return busName;
	}

	public void setBusName(String busName) {
		this.busName = busName;
	}

	public String getBusmsg() {
		return busmsg;
	}

	public void setBusmsg(String busmsg) {
		this.busmsg = busmsg;
	}

}
