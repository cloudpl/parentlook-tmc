package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;
import com.akshara.parentlook.service.StudentsDetailsAction;

public class ExportStudentPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("Student Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("             ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() throws BadElementException, MalformedURLException, IOException, SQLException {
		PdfPTable table = new PdfPTable(8);
		PdfPCell cell = new PdfPCell();

		int a = (Integer) usersession.get("schoolId");
		List<StudentsBean> studentsList = new ArrayList<StudentsBean>();
		studentsList = SmsBeanDao.getStudentDetailsList(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(new Phrase("Id", boldFont));
		table.addCell(new Phrase("Name", boldFont));
		table.addCell(new Phrase(" ", boldFont));
		table.addCell(new Phrase("RollNo", boldFont));
		table.addCell(new Phrase("Parent", boldFont));
		table.addCell(new Phrase("Bus", boldFont));
		table.addCell(new Phrase("Class", boldFont));
		table.addCell(new Phrase("Route", boldFont));

		Iterator<StudentsBean> iterator = studentsList.iterator();
		while (iterator.hasNext()) {
			StudentsBean bean = (StudentsBean) iterator.next();
			if (bean != null) {
				if (bean.getRecordStatus().equals("ACTIVE")) {
					Integer studentId = bean.getStudentId();
					String id = studentId.toString();
					table.addCell(id);
					String name = bean.getStudentFirstName() + bean.getStudentLastName();
					table.addCell(name);

					Blob imageBlob = bean.getPhoto();
					if (imageBlob != null) {
						System.out.println(imageBlob + "imge");
						byte[] imageBytes = imageBlob.getBytes(1, (int) imageBlob.length());
						if (imageBytes != null) {
							Image image = Image.getInstance(imageBytes);
							table.addCell(image);
						}

					} else {
						String img = "   ";
						table.addCell(img);
					}
					String rollno = bean.getRollNo();
					if (rollno != null) {
						table.addCell(rollno);
					} else {
						String roll = "  ";
						table.addCell(roll);
					}
					int parentid = bean.getParentId();

					ParentsBean bean1 = new ParentsBean();
					bean1 = SmsBeanDao.getParentDetails(parentid);
					if (bean1 != null) {
						String parentname = bean1.getFirstName() + bean1.getLastName();
						table.addCell(parentname);
					} else {
						String parent = "   ";
						table.addCell(parent);
					}

					int busid = bean.getBusId();
					BusesBean busbean = SmsBeanDao.getBusDetails(busid);
					if (busbean != null) {
						String bus = busbean.getBusName();
						table.addCell(bus);
					} else {
						String bus = "   ";
						table.addCell(bus);
					}
					int classid = bean.getClassId();
					ClassBean bean2 = new ClassBean();

					bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
					if (bean2 != null) {
						String classname = bean2.getDescription();
						table.addCell(classname);
					} else {
						String clas = "  ";
						table.addCell(clas);
					}
					if (busbean != null) {

						int routeid = busbean.getRouteId();

						RoutesBean bean3 = new RoutesBean();
						bean3 = SmsBeanDao.getRouteDetails(routeid);
						if (bean3 != null) {

							String routename = bean3.getRouteName();

							table.addCell(routename);
						} else {

							String route = "  ";
							table.addCell(route);
						}

					} else {

						String route = "  ";
						table.addCell(route);
					}
				}

			}
		}

		return table;
	}

}
