package com.akshara.parentlook.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.DummyBean;
import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ViewStudent extends ActionSupport implements ModelDriven<StudentsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(ViewStudent.class);

	private static final long serialVersionUID = 1L;

	StudentsBean studentbean;
	ParentsBean parentsBean2 = new ParentsBean();
	ClassBean classBean = new ClassBean();
	private int id1;
	private int id2;
	private int id3;
	private int schoolId;
	Map<String, Object> usersession;
	DummyBean dummybean = new DummyBean();
	private ParentsBean parentsbean = new ParentsBean();
	List<StudentsBean> studentImageList = new ArrayList<StudentsBean>();
	private File PhotoFile;

	List<ParentsBean> parentsList = new ArrayList<ParentsBean>();
	List<ClassBean> classeslist = new ArrayList<ClassBean>();
	private String imgstr;
	private String ProfilePicture;

	@Override
	public StudentsBean getModel() {
		studentbean = new StudentsBean();
		return studentbean;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {
		try {
			schoolId = (Integer) usersession.get("schoolId");
			studentbean = SmsBeanDao.viewstudent(id1);

			parentsbean = SmsBeanDao.getParentDetails(studentbean.getParentId());
			classBean = SmsBeanDao.getclassbystudentid(studentbean.getClassId());
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public String edit() {

		try {
			schoolId = (Integer) usersession.get("schoolId");

			studentbean = SmsBeanDao.editStudent(id2);
			parentsBean2 = SmsBeanDao.editParent(studentbean.getParentId());
			classBean = SmsBeanDao.getClassesDetailsbyId(studentbean.getClassId());
			parentsList = SmsBeanDao.getParentsList(schoolId);
			classeslist = SmsBeanDao.getClasesDetailsList(schoolId);
			ProfilePicture = studentbean.getProfilePicture();

		} catch (Exception e) {
			LOG.error(e);
		}
		return "edit";

	}

	public String update() {
		try {
			SmsBeanDao.updateStudent(studentbean, PhotoFile);

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e);
		}
		return "update";
	}

	public String detete() {
		try {
			SmsBeanDao.deleteStudent(id3);
		} catch (Exception e) {
			LOG.error(e);
		}
		return "delete";
	}

	public int getId1() {
		return id1;
	}

	public void setId1(int id1) {
		this.id1 = id1;
	}

	public StudentsBean getStudentbean() {
		return studentbean;
	}

	public void setStudentbean(StudentsBean studentbean) {
		this.studentbean = studentbean;
	}

	public int getId2() {
		return id2;
	}

	public void setId2(int id2) {
		this.id2 = id2;
	}

	public int getId3() {
		return id3;
	}

	public void setId3(int id3) {
		this.id3 = id3;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public ParentsBean getParentsbean() {
		return parentsbean;
	}

	public void setParentsbean(ParentsBean parentsbean) {
		this.parentsbean = parentsbean;
	}

	public List<StudentsBean> getStudentImageList() {
		return studentImageList;
	}

	public void setStudentImageList(List<StudentsBean> studentImageList) {
		this.studentImageList = studentImageList;
	}

	public static Logger getLog() {
		return LOG;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public File getPhotoFile() {
		return PhotoFile;
	}

	public void setPhotoFile(File photoFile) {
		PhotoFile = photoFile;
	}

	public List<ParentsBean> getParentsList() {
		return parentsList;
	}

	public void setParentsList(List<ParentsBean> parentsList) {
		this.parentsList = parentsList;
	}

	public String getImgstr() {
		return imgstr;
	}

	public void setImgstr(String imgstr) {
		this.imgstr = imgstr;
	}

	public String getProfilePicture() {
		return ProfilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		ProfilePicture = profilePicture;
	}

	public List<ClassBean> getClasseslist() {
		return classeslist;
	}

	public void setClasseslist(List<ClassBean> classeslist) {
		this.classeslist = classeslist;
	}

	public ParentsBean getParentsBean2() {
		return parentsBean2;
	}

	public void setParentsBean2(ParentsBean parentsBean2) {
		this.parentsBean2 = parentsBean2;
	}

	public ClassBean getClassBean() {
		return classBean;
	}

	public void setClassBean(ClassBean classBean) {
		this.classBean = classBean;
	}

}
