package com.akshara.parentlook.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class PendingApprovels extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private final String RecordStatus = "ACTIVE";

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;

	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("StopPontApproved");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("StudentName");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("Class");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("Location");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("Pickup Point");
		sheet.autoSizeColumn(3);
		rowhead.createCell(4).setCellValue("Drop-off Ponit");
		sheet.autoSizeColumn(4);
		rowhead.createCell(5).setCellValue("RouteName");
		sheet.autoSizeColumn(5);
		rowhead.createCell(6).setCellValue("BusId");
		sheet.autoSizeColumn(6);

		String status = "1";

		int a = (int) usersession.get("schoolId");

		List<StudentsBean> bean = SmsBeanDao.getStudentDetailsListByTrackedStatus(a, status, RecordStatus);
		int x = 1;
		Iterator iterator = bean.iterator();
		while (iterator.hasNext()) {
			StudentsBean studentsBean = (StudentsBean) iterator.next();
			XSSFRow row = sheet.createRow(x);

			if (studentsBean != null) {
				String name = studentsBean.getStudentFirstName() + studentsBean.getStudentLastName();
				String pick = studentsBean.getPickupPoint();
				String drop = studentsBean.getDropPoint();

				int classid = studentsBean.getClassId();
				ClassBean bean2 = new ClassBean();

				bean2 = SmsBeanDao.getClassesDetailsbyId(classid);
				if (bean2 != null) {
					String classname = bean2.getStudentClass();

					WayPointsBean pointsBean = SmsBeanDao.getWayPoint(studentsBean.getStudentId());
					if (pointsBean != null) {
						String addr = pointsBean.getWayPoint();

						BusesBean busBean = SmsBeanDao.getBusId(studentsBean.getBusId());
						if (busBean != null) {
							Integer id = busBean.getBusId();
							String busid = id.toString();
							int routeid = busBean.getRouteId();
							RoutesBean routbean = SmsBeanDao.getRoute(routeid);
							if (routbean != null) {
								String route = routbean.getRouteName();
								row.createCell(0).setCellValue(name);
								row.createCell(1).setCellValue(classname);
								row.createCell(2).setCellValue(addr);
								row.createCell(3).setCellValue(pick);
								row.createCell(4).setCellValue(drop);
								row.createCell(5).setCellValue(route);
								row.createCell(6).setCellValue(busid);
							}
						}

					}
				}

			}

			x++;
		}

		String filename = "PendingApprovals";
		StringBuffer sb = new StringBuffer(filename);
		sb.append(".xls");
		File file = new File(sb.toString());
		try {
			OutputStream outputStream = new FileOutputStream(file);
			workbook.write(outputStream);
			outputStream.flush();
			outputStream.close();
			InputStream inputStream = new FileInputStream(file);
			return inputStream;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.exportToExcel();
	}

}
