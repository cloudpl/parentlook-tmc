package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolNotifications;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class SchoolNotificationLoad extends ActionSupport implements SessionAware {
	private SchoolNotifications bean;
	List<SchoolNotifications> list = new ArrayList<SchoolNotifications>();
	private int count;
	private Map<String, Object> usersession;

	@Override
	public String execute() throws Exception {
		int a = (Integer) usersession.get("schoolId");
		list = SmsBeanDao.recentNotification(a);
		count = SmsBeanDao.totalCount(bean, a);
		usersession.put("count", count);

		return SUCCESS;
	}

	public SchoolNotifications getBean() {
		return bean;
	}

	public void setBean(SchoolNotifications bean) {
		this.bean = bean;
	}

	public List<SchoolNotifications> getList() {
		return list;
	}

	public void setList(List<SchoolNotifications> list) {
		this.list = list;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public Map<String, Object> getUsersession() {
		return usersession;
	}

	public void setUsersession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

}
