package com.akshara.parentlook.service;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class UpdateBusDetailsAction extends ActionSupport implements ModelDriven<BusesBean> {

	BusesBean busbean;

	@Override
	public String execute() throws Exception {
		boolean b = SmsBeanDao.updatebusdetails(busbean);
		if (b == true) {

			return SUCCESS;
		}
		return INPUT;
	}

	@Override
	public BusesBean getModel() {
		busbean = new BusesBean();
		return busbean;
	}

}
