package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.AbsentStudentbean;
import com.akshara.parentlook.db.bean.AttendenceBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportAttendenceDeBoardExcel extends ActionSupport implements SessionAware {

	private String date;
	private InputStream inputStream;
	private Map<String, Object> usersession;
	List<BusesBean> buseslist = new ArrayList<>();
	List deboardAbsentlist = new ArrayList<>();

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("DeBoardAttendenceList");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("Student Name");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("Class");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("Roll No");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("Bus Name");
		sheet.autoSizeColumn(3);
		rowhead.createCell(4).setCellValue("Route Name");
		sheet.autoSizeColumn(4);

		int x = 1;

		try {

			int a = (Integer) usersession.get("schoolId");
			buseslist = SmsBeanDao.getBusDetailsList(a);
			if (buseslist.size() > 0) {
				Iterator iterator = buseslist.iterator();
				while (iterator.hasNext()) {
					BusesBean busbean = (BusesBean) iterator.next();

					RoutesBean routesBean = SmsBeanDao.getRouteDetails(busbean.getRouteId());
					List<PickupPoints> pickuplist = SmsBeanDao.getPickUpPointsForBusId(busbean.getBusId());
					if (pickuplist.size() > 0) {
						Iterator iterator2 = pickuplist.iterator();
						while (iterator2.hasNext()) {
							PickupPoints pickupbean = (PickupPoints) iterator2.next();
							HistoryLogBean historyBeanDeBoard = SmsBeanDao.getHistoryDetailsDeBoard(busbean.getBusId(),
									pickupbean.getPickupPointId(), date, "Drop");

							if (historyBeanDeBoard != null) {
								List<StudentsBean> studentlist = SmsBeanDao
										.getStudentListByDrop(historyBeanDeBoard.getPickupPointId(), a);
								Iterator iterator6 = studentlist.iterator();
								while (iterator6.hasNext()) {
									StudentsBean studnetbean1 = (StudentsBean) iterator6.next();
									AttendenceBean attendenceDeBoard = SmsBeanDao
											.getstudentattendence(studnetbean1.getStudentId(), date, "1");
									if (attendenceDeBoard != null) {
									} else {
										deboardAbsentlist.add(studnetbean1.getStudentId());

									}
								}
							}
						}
					}
				}
				Iterator Itr = deboardAbsentlist.iterator();
				while (Itr.hasNext()) {
					XSSFRow row = sheet.createRow(x);
					int studentid1 = (Integer) Itr.next();
					StudentsBean st = SmsBeanDao.getStudentDetails(studentid1);
					row.createCell(0).setCellValue(st.getStudentFirstName() + " " + st.getStudentLastName());
					row.createCell(2).setCellValue(st.getRollNo());
					ClassBean classbean = SmsBeanDao.getclassbystudentid(st.getClassId());

					BusesBean busesbean1 = SmsBeanDao.getBusDetails(st.getBusId());
					if (busesbean1 != null) {
						row.createCell(3).setCellValue(busesbean1.getBusName());
						RoutesBean routesbean1 = SmsBeanDao.getRouteDetails(busesbean1.getRouteId());
						if (routesbean1 != null) {
							String route = routesbean1.getRouteName();
							row.createCell(4).setCellValue(route);
						}
						if (classbean != null) {
							String classname = classbean.getStudentClass();
							row.createCell(1).setCellValue(classname);
						}

					}
					x++;
				}

			}
			autoSizeColumn1(workbook);
			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
			workbook.write(arrayOutputStream);
			this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private void autoSizeColumn1(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public List<BusesBean> getBuseslist() {
		return buseslist;
	}

	public void setBuseslist(List<BusesBean> buseslist) {
		this.buseslist = buseslist;
	}

	public List getDeboardAbsentlist() {
		return deboardAbsentlist;
	}

	public void setDeboardAbsentlist(List deboardAbsentlist) {
		this.deboardAbsentlist = deboardAbsentlist;
	}

}
