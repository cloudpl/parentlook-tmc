package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.AbsentStudentbean;
import com.akshara.parentlook.db.bean.AttendenceBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportAttendenceOnBoardPdf extends ActionSupport implements SessionAware {

	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;
	private String date;
	List onboardAbsentlist = new ArrayList<>();
	List<BusesBean> buseslist = new ArrayList<>();

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);

		document.open();
		Paragraph paragraph1 = new Paragraph("AttendenceOnBoard List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("                   ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(5);
		int a = (Integer) usersession.get("schoolId");
		List<BusesBean> busList = new ArrayList<BusesBean>();
		busList = SmsBeanDao.getBusDetailsList(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(new Phrase("Student Name", boldFont));
		table.addCell(new Phrase("Class", boldFont));
		table.addCell(new Phrase("Roll No", boldFont));
		table.addCell(new Phrase("BusName", boldFont));
		table.addCell(new Phrase("RouteName", boldFont));
		try {
			buseslist = SmsBeanDao.getBusDetailsList(a);
			if (buseslist.size() > 0) { // size()>0
				Iterator iterator = buseslist.iterator();
				while (iterator.hasNext()) {
					BusesBean busbean = (BusesBean) iterator.next();

					RoutesBean routesBean = SmsBeanDao.getRouteDetails(busbean.getRouteId());
					List<PickupPoints> pickuplist = SmsBeanDao.getPickUpPointsForBusId(busbean.getBusId());
					if (pickuplist.size() > 0) {
						Iterator iterator2 = pickuplist.iterator();
						while (iterator2.hasNext()) {
							PickupPoints pickupbean = (PickupPoints) iterator2.next();
							HistoryLogBean historyBeanOnBoard = SmsBeanDao.getHistoryDetailsOnBoard(busbean.getBusId(),
									pickupbean.getPickupPointId(), date, "Pickup");

							if (historyBeanOnBoard != null) {
								List<StudentsBean> studentlist = SmsBeanDao
										.getStudentListByPick(historyBeanOnBoard.getPickupPointId(), a);
								Iterator iterator5 = studentlist.iterator();
								while (iterator5.hasNext()) {
									StudentsBean studentbean = (StudentsBean) iterator5.next();
									AttendenceBean attendenceOnBoard = SmsBeanDao
											.getstudentattendence(studentbean.getStudentId(), date, "0");
									if (attendenceOnBoard != null) {
									} else {
										onboardAbsentlist.add(studentbean.getStudentId());

									}
								}
								if (onboardAbsentlist.size() > 0) {
									Iterator ite = onboardAbsentlist.iterator();
									while (ite.hasNext()) {
										int studentid = (Integer) ite.next();

										StudentsBean st = SmsBeanDao.getStudentDetails(studentid);
										String studentname = st.getStudentFirstName() + st.getStudentLastName();

										table.addCell(studentname);
										ClassBean classbean = SmsBeanDao.getclassbystudentid(st.getClassId());
										if (classbean != null) {
											String classname = classbean.getDescription();
											table.addCell(classname);
										} else {
											String classname = "  ";
											table.addCell(classname);
										}
										if (st.getRollNo() != null) {
											String rollno = st.getRollNo();
											table.addCell(rollno);
										} else {
											String rollno = "  ";
											table.addCell(rollno);
										}

										BusesBean busesbean1 = SmsBeanDao.getBusDetails(st.getBusId());
										if (busesbean1 != null) {
											String busname1 = busesbean1.getBusName();
											table.addCell(busname1);
										} else {
											String busname1 = "  ";
											table.addCell(busname1);
										}
										if (busesbean1 != null) {
											RoutesBean routesbean1 = SmsBeanDao
													.getRouteDetails(busesbean1.getRouteId());

											if (routesbean1 != null) {
												String route = routesbean1.getRouteName();
												table.addCell(route);

											} else {
												String route = " ";
												table.addCell(route);
											}

										} else {
											String routename = "  ";
											table.addCell(routename);
										}
									}
								}

							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return table;
	}

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List getOnboardAbsentlist() {
		return onboardAbsentlist;
	}

	public void setOnboardAbsentlist(List onboardAbsentlist) {
		this.onboardAbsentlist = onboardAbsentlist;
	}

	public List<BusesBean> getBuseslist() {
		return buseslist;
	}

	public void setBuseslist(List<BusesBean> buseslist) {
		this.buseslist = buseslist;
	}
}
