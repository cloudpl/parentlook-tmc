package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ExportDriverExcel extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private InputStream inputStream;

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	public InputStream exportToExcel() throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Driver");
		XSSFRow rowhead = sheet.createRow(0);

		rowhead.createCell(0).setCellValue("DriverId");
		sheet.autoSizeColumn(0);
		rowhead.createCell(1).setCellValue("DriverName");
		sheet.autoSizeColumn(1);
		rowhead.createCell(2).setCellValue("ContactNo");
		sheet.autoSizeColumn(2);
		rowhead.createCell(3).setCellValue("Reference");
		sheet.autoSizeColumn(3);
		rowhead.createCell(4).setCellValue("LicenceNo");
		sheet.autoSizeColumn(4);
		rowhead.createCell(5).setCellValue("IdType");
		sheet.autoSizeColumn(5);
		rowhead.createCell(6).setCellValue("CurrentAddress");
		sheet.autoSizeColumn(6);

		int a = (Integer) usersession.get("schoolId");
		List<DriversBean> driverlist = new ArrayList<DriversBean>();
		driverlist = SmsBeanDao.getDriverDetailsList(a);
		int x = 1;
		Iterator<DriversBean> iterator = driverlist.iterator();
		while (iterator.hasNext()) {

			DriversBean bean = (DriversBean) iterator.next();

			XSSFRow row = sheet.createRow(x);

			int driverid = bean.getDriverId();
			String name = bean.getFirstName() + bean.getLastName();
			String contact = bean.getContactNumber();
			String ref = bean.getRelationTitle() + bean.getRelativeName();
			String licence = bean.getLicenceNumber();
			String idtype = bean.getIdType();
			String current = bean.getCurrentAddress();

			row.createCell(0).setCellValue(driverid);
			row.createCell(1).setCellValue(name);
			row.createCell(2).setCellValue(contact);
			row.createCell(3).setCellValue(ref);
			row.createCell(4).setCellValue(licence);
			row.createCell(5).setCellValue(idtype);
			row.createCell(6).setCellValue(current);

			x++;
		}
		autoSizeColumn(workbook);
		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
		workbook.write(arrayOutputStream);
		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));

		return null;
	}

	private void autoSizeColumn(XSSFWorkbook workbook) {
		int noOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < noOfSheets; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(0);
				Iterator<Cell> iterator = row.cellIterator();
				while (iterator.hasNext()) {
					Cell cell = iterator.next();
					int columIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columIndex);
				}
			}
		}
	}

	@Override
	public String execute() throws Exception {
		exportToExcel();
		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {
		return this.inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}
}
