package com.akshara.parentlook.service;

import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class UpdateRoute extends ActionSupport implements ModelDriven<RoutesBean> {

	RoutesBean routebean;

	@Override
	public RoutesBean getModel() {
		routebean = new RoutesBean();
		return routebean;
	}

	@Override
	public String execute() throws Exception {
		try {
			boolean b = SmsBeanDao.updateRoute(routebean);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	public RoutesBean getRoutebean() {
		return routebean;
	}

	public void setRoutebean(RoutesBean routebean) {
		this.routebean = routebean;
	}

}
