package com.akshara.parentlook.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.DriversBean;
import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class AddAdminAction extends ActionSupport implements ModelDriven<SchoolAdminsBean>, SessionAware {

	private static final Logger LOG = Logger.getLogger(AddAdminAction.class);

	private SchoolAdminsBean adminBean;
	private Map<String, Object> usersession;
	List<SchoolAdminsBean> adminList = new ArrayList<SchoolAdminsBean>();
	List<SchoolAdminsBean> adminImgList = new ArrayList<SchoolAdminsBean>();

	public List<SchoolAdminsBean> getAdminImgList() {
		return adminImgList;
	}

	public void setAdminImgList(List<SchoolAdminsBean> adminImgList) {
		this.adminImgList = adminImgList;
	}

	public void setAdminList(List<SchoolAdminsBean> adminList) {
		this.adminList = adminList;
	}

	private File PhotoAdmin;

	@Override
	public SchoolAdminsBean getModel() {

		adminBean = new SchoolAdminsBean();
		return adminBean;
	}

	public SchoolAdminsBean getAdminBean() {
		return adminBean;
	}

	public void setAdminBean(SchoolAdminsBean adminBean) {
		this.adminBean = adminBean;
	}

	@Override
	public String execute() throws Exception {
		try {
			int i = (Integer) usersession.get("schoolId");
			int a = AdminDAO.saveAdminDetails(adminBean, PhotoAdmin, i);

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;

	}

	public String admin() {
		return SUCCESS;

	}

	public String getAdminList() {
		int a = (Integer) usersession.get("schoolId");
		try {
			adminList = AdminDAO.getAdminDetailList(a);
			Iterator iterator = adminList.iterator();

			while (iterator.hasNext()) {
				SchoolAdminsBean drim = (SchoolAdminsBean) iterator.next();
				if (drim.getImage() != null) {
					byte[] itemImage = drim.getImage().getBytes(1, (int) drim.getImage().length());

					Base64 b = new Base64();
					byte[] encodingImgAsBytes = b.encode(itemImage);

					String strType = b.encodeToString(encodingImgAsBytes);

					byte[] valueDecoded = Base64.decodeBase64(strType);
					String decodedImgStr = new String(valueDecoded);

					drim.setImageStr(decodedImgStr);

				}

				adminImgList.add(drim);
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;

	}

	public File getPhotoAdmin() {
		return PhotoAdmin;
	}

	public void setPhotoAdmin(File photoAdmin) {
		PhotoAdmin = photoAdmin;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {

		this.usersession = usersession;
	}

}
