package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.ClassBean;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentViewBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.bean.WayPointsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class ApproveStudentDropAction extends ActionSupport implements SessionAware {

	private static final Logger LOG = Logger
			.getLogger(ApproveStudentDropAction.class);

	private int StudentId;
	private int SchoolId;
	private String schoolName;
	private String StudentName;
	private String wayPoint;
	private String latLang;
	private int BusId;
	private int RouteId;
	private int busi;
	private int routi;
	private String busname;
	private String routename; 
	private String pickaddr;
	private List<RoutesBean> rootsBeans = new ArrayList<RoutesBean>();
	Map<String,Object> usersession;
	
	 List<StudentViewBean> viewbean=new ArrayList<StudentViewBean>();
	 private StudentViewBean viewBean2=new StudentViewBean();
	@Override
	public String execute() throws Exception {

		try {

			
			
			
			 int a=(int) usersession.get("schoolId");
			 
			 List<RoutesBean> rootsBeans1 = SmsBeanDao.getRootsForSchool(SchoolId);
				Iterator itr=rootsBeans1.iterator();
				while(itr.hasNext()){
					List<BusesBean> busbean=new ArrayList<BusesBean>();
					RoutesBean routebean = (RoutesBean) itr.next();
					busbean=SmsBeanDao.getBusList(routebean.getRouteId());
					
					if(busbean.size()>0){
						rootsBeans.add(routebean);
					}
					
				}
			 StudentsBean studentsBean=SmsBeanDao.getStudentDetails(StudentId);
			 
		  
			 if(studentsBean!=null){
			   busi=studentsBean.getBusId();
			   
			 }
			 BusesBean busb=null;
			 try{
					if(busi>0){
						busb=SmsBeanDao.getBusId(busi);
							busname=busb.getBusName();
							
					
						     routi=busb.getRouteId();
						      
						    	 
						    	 RoutesBean routen=SmsBeanDao.getRoute(routi);
							     if(routen!=null){
							      routename=routen.getRouteName();
						     }
							     pickaddr=studentsBean.getPickupPoint();
					}
					
					
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			      
			      
			 WayPointsBean  pointsBean=SmsBeanDao.getWayPoint(studentsBean.getStudentId());
			 viewBean2.setStudentName(studentsBean.getStudentFirstName()+""+studentsBean.getStudentLastName());
			 viewBean2.setLocation(pointsBean.getWayPoint());
			  ClassBean classBean=SmsBeanDao.getClassesDetailsbyId(studentsBean.getClassId());
			  viewBean2.setStudentClass(classBean.getStudentClass());
			WayPointsBean wayPointsBean = SmsBeanDao.getWayPoint(StudentId);
			 
			BusesBean bean=SmsBeanDao.getBusDetails(BusId);
			if(bean!=null){
				
				setRouteId(bean.getRouteId());
			}
			
			
			
			
			latLang = wayPointsBean.getLatLang();
			wayPoint = wayPointsBean.getWayPoint();

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public List<RoutesBean> getRootsBeans() {
		return rootsBeans;
	}

	public void setRootsBeans(List<RoutesBean> rootsBeans) {
		this.rootsBeans = rootsBeans;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getStudentName() {
		return StudentName;
	}

	public void setStudentName(String studentName) {
		StudentName = studentName;
	}

	public String getWayPoint() {
		return wayPoint;
	}

	public void setWayPoint(String wayPoint) {
		this.wayPoint = wayPoint;
	}

	public String getLatLang() {
		return latLang;
	}

	public void setLatLang(String latLang) {
		this.latLang = latLang;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public int getRouti() {
		return routi;
	}

	public void setRouti(int routi) {
		this.routi = routi;
	}

	 

	public String getBusname() {
		return busname;
	}

	public String getRoutename() {
		return routename;
	}

	public void setRoutename(String routename) {
		this.routename = routename;
	}

	public void setBusname(String busname) {
		this.busname = busname;
	}

	public int getRouteId() {
		return RouteId;
	}

	public void setRouteId(int routeId) {
		RouteId = routeId;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession=usersession;
	}

	public StudentViewBean getViewBean2() {
		return viewBean2;
	}

	public void setViewBean2(StudentViewBean viewBean2) {
		this.viewBean2 = viewBean2;
	}

	public String getPickaddr() {
		return pickaddr;
	}

	public void setPickaddr(String pickaddr) {
		this.pickaddr = pickaddr;
	}

	public int getBusi() {
		return busi;
	}

	public void setBusi(int busi) {
		this.busi = busi;
	}

}
