package com.akshara.parentlook.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.akshara.parentlook.db.bean.ParentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64.InputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import sun.misc.BASE64Decoder;

public class ExportParentPdf extends ActionSupport implements SessionAware {
	private Map<String, Object> usersession;
	private java.io.InputStream inputStream;

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);
		document.open();
		Paragraph paragraph1 = new Paragraph("Parent Detail List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("                   ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() throws SQLException, BadElementException, MalformedURLException, IOException {
		PdfPTable table = new PdfPTable(6);
		PdfPCell cell = new PdfPCell();
		int a = (Integer) usersession.get("schoolId");
		List<ParentsBean> parantsList = new ArrayList<ParentsBean>();
		parantsList = SmsBeanDao.getParentsList(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

		table.addCell(new Phrase("ParentId", boldFont));
		table.addCell(new Phrase("Name", boldFont));
		table.addCell(new Phrase(" ", boldFont));
		table.addCell(new Phrase("MobileNumber", boldFont));
		table.addCell(new Phrase("Email", boldFont));
		table.addCell(new Phrase("Address", boldFont));

		Iterator<ParentsBean> iterator = parantsList.iterator();
		while (iterator.hasNext()) {
			ParentsBean bean = (ParentsBean) iterator.next();

			if (bean != null) {

				if (bean.getRecordStatus().equals("ACTIVE")) {
					Integer parentId = bean.getParentId();
					String id = parentId.toString();

					Blob imageBlob = bean.getPhoto();

					if (imageBlob != null) {

						byte[] imageBytes = imageBlob.getBytes(1, (int) imageBlob.length());
						Image image = Image.getInstance(imageBytes);
						String name = bean.getFirstName() + bean.getLastName();
						String mobileNumber = bean.getMobileNumber();
						String email = bean.getEmail();
						String address = bean.getAddress();

						table.addCell(id);
						table.addCell(image);
						table.addCell(name);
						table.addCell(mobileNumber);
						table.addCell(email);
						table.addCell(address);
					} else {

						String name = bean.getFirstName() + bean.getLastName();
						String mobileNumber = bean.getMobileNumber();
						String email = bean.getEmail();
						String address = bean.getAddress();
						String image = "     ";

						table.addCell(id);
						table.addCell(image);
						table.addCell(name);
						table.addCell(mobileNumber);
						table.addCell(email);
						table.addCell(address);
					}

				}
			}
		}
		return table;
	}

}
