package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolAdminsBean;
import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.dao.AdminDAO;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class SchoolAdminsAction extends ActionSupport implements SessionAware{
	private Map<String, Object> usersession;
	private SchoolAdminsBean schooladmin;
	private SchoolsDetailsBean schooldetailbean;
	private List<SchoolAdminsBean> adminlist = new ArrayList<SchoolAdminsBean>();
	private List<SchoolAdminsBean> adminImgList = new ArrayList<SchoolAdminsBean>();
private int SchoolId;
	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {
		//int a = (int) usersession.get("schoolId");
		try {
			adminlist = AdminDAO.getAdmin(SchoolId);
			schooldetailbean=SmsBeanDao.getSchoolDetails(SchoolId);
			usersession.put("schoolId", SchoolId);
			usersession.put("schoolName",schooldetailbean.getSchoolName());
			Iterator iterator = adminlist.iterator();

			while (iterator.hasNext()) {
				SchoolAdminsBean drim = (SchoolAdminsBean) iterator.next();
				if (drim.getImage() != null) {
					byte[] itemImage = drim.getImage().getBytes(1, (int) drim.getImage().length());

					Base64 b = new Base64();
					byte[] encodingImgAsBytes = b.encode(itemImage);

					String strType = b.encodeToString(encodingImgAsBytes);

					byte[] valueDecoded = Base64.decodeBase64(strType);
					String decodedImgStr = new String(valueDecoded);

					drim.setImageStr(decodedImgStr);

				}

				adminImgList.add(drim);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public List<SchoolAdminsBean> getAdminlist() {
		return adminlist;
	}

	public void setAdminlist(List<SchoolAdminsBean> adminlist) {
		this.adminlist = adminlist;
	}

	public List<SchoolAdminsBean> getAdminImgList() {
		return adminImgList;
	}

	public void setAdminImgList(List<SchoolAdminsBean> adminImgList) {
		this.adminImgList = adminImgList;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public SchoolsDetailsBean getSchooldetailbean() {
		return schooldetailbean;
	}

	public void setSchooldetailbean(SchoolsDetailsBean schooldetailbean) {
		this.schooldetailbean = schooldetailbean;
	}

}
