package com.akshara.parentlook.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.SchoolsDetailsBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class GetProgressStudentsDropListAction extends ActionSupport implements SessionAware {

	private static final Logger LOG = Logger.getLogger(GetProgressStudentsDropListAction.class);
	private Map<String, Object> usersession;
	private List<StudentsBean> studentsBeans = new ArrayList<StudentsBean>();
	private List<StudentsWebBean> studentList = new ArrayList<StudentsWebBean>();

	@Override
	public String execute() throws Exception {

		try {
			int schoolId = (int) usersession.get("schoolId");
			studentsBeans = SmsBeanDao.getProgerssStudentsList("1", schoolId);

			Iterator iterator = studentsBeans.iterator();

			while (iterator.hasNext()) {
				StudentsBean bean = (StudentsBean) iterator.next();

				StudentsWebBean bean2 = new StudentsWebBean();
				BeanUtils.copyProperties(bean2, bean);

				SchoolsDetailsBean detailsBean = SmsBeanDao.getSchoolName(bean.getSchoolId());
				bean2.setSchoolName(detailsBean.getSchoolName());
				studentList.add(bean2);

			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public List<StudentsBean> getStudentsBeans() {
		return studentsBeans;
	}

	public void setStudentsBeans(List<StudentsBean> studentsBeans) {
		this.studentsBeans = studentsBeans;
	}

	public List<StudentsWebBean> getStudentList() {
		return studentList;
	}

	public void setStudentList(List<StudentsWebBean> studentList) {
		this.studentList = studentList;
	}

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

}
