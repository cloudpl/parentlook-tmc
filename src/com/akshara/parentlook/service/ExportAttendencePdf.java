package com.akshara.parentlook.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.akshara.parentlook.db.bean.AttendenceBean;
import com.akshara.parentlook.db.bean.BusesBean;
import com.akshara.parentlook.db.bean.HistoryLogBean;
import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.RoutesBean;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

public class ExportAttendencePdf extends ActionSupport implements SessionAware {

	private Map<String, Object> usersession;
	private String date;
	private java.io.InputStream inputStream;
	List<BusesBean> buseslist = new ArrayList<>();

	@Override
	public void setSession(Map<String, Object> usersession) {
		this.usersession = usersession;
	}

	@Override
	public String execute() throws Exception {

		ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

		Document document = new Document();
		PdfWriter.getInstance(document, arrayOutputStream);

		document.open();
		Paragraph paragraph1 = new Paragraph("Attendence List", new Font(Font.FontFamily.HELVETICA, 20));
		paragraph1.setAlignment(Element.ALIGN_CENTER);
		Paragraph paragraph2 = new Paragraph("                   ");
		PdfPTable table = createTable();

		document.add(paragraph1);
		document.add(paragraph2);

		document.add(table);
		document.close();

		this.setInputStream(new ByteArrayInputStream(arrayOutputStream.toByteArray()));
		return SUCCESS;
	}

	private PdfPTable createTable() {
		PdfPTable table = new PdfPTable(7);
		int a = (Integer) usersession.get("schoolId");
		List<BusesBean> busList = new ArrayList<BusesBean>();
		buseslist = SmsBeanDao.getBusDetailsList(a);

		Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.addCell(new Phrase("Bus", boldFont));
		table.addCell(new Phrase("Route", boldFont));
		table.addCell(new Phrase("Occupency", boldFont));
		table.addCell(new Phrase("On-Boarded", boldFont));
		table.addCell(new Phrase("Absentees", boldFont));
		table.addCell(new Phrase("De-Boarded", boldFont));
		table.addCell(new Phrase("Absentees", boldFont));

		try {
			if (buseslist.size() > 0) { // size()>0
				Iterator iterator = buseslist.iterator();
				while (iterator.hasNext()) {
					BusesBean busbean = (BusesBean) iterator.next();
					int onboardcount = 0;
					int onboardabsentcount = 0;
					int de_boardcount = 0;
					int de_boardabsentcount = 0;

					if (busbean.getBusName() != null) {

						String busname = busbean.getBusName();
						table.addCell(busname);
					} else {
						String name = "   ";
						table.addCell(name);
					}
					RoutesBean routesBean = SmsBeanDao.getRouteDetails(busbean.getRouteId());
					if (routesBean != null) {

						String routename = routesBean.getRouteName();
						table.addCell(routename);
					} else {
						String root = "   ";
						table.addCell(root);
					}
					Integer occupency = busbean.getOccupency();
					if (occupency > 0) {
						String occup = occupency.toString();
						table.addCell(occup);
					} else {
						String occu = "  ";
						table.addCell(occu);
					}
					List<PickupPoints> pickuplist = SmsBeanDao.getPickUpPointsForBusId(busbean.getBusId());
					if (pickuplist.size() > 0) {
						Iterator iterator2 = pickuplist.iterator();
						while (iterator2.hasNext()) {
							PickupPoints pickupbean = (PickupPoints) iterator2.next();
							HistoryLogBean historyBeanOnBoard = SmsBeanDao.getHistoryDetailsOnBoard(busbean.getBusId(),
									pickupbean.getPickupPointId(), date, "Pickup");

							HistoryLogBean historyBeanDeBoard = SmsBeanDao.getHistoryDetailsDeBoard(busbean.getBusId(),
									pickupbean.getPickupPointId(), date, "Drop");

							if (historyBeanOnBoard != null) {
								List<StudentsBean> studentlist = SmsBeanDao
										.getStudentListByPick(historyBeanOnBoard.getPickupPointId(), a);
								Iterator iterator5 = studentlist.iterator();
								while (iterator5.hasNext()) {
									StudentsBean studentbean = (StudentsBean) iterator5.next();
									AttendenceBean attendenceOnBoard = SmsBeanDao
											.getstudentattendence(studentbean.getStudentId(), date, "0");
									if (attendenceOnBoard != null) {
										onboardcount += 1;
									} else {
										onboardabsentcount += 1;

									}
								}

							}
							if (historyBeanDeBoard != null) {
								List<StudentsBean> studentlist = SmsBeanDao
										.getStudentListByDrop(historyBeanDeBoard.getPickupPointId(), a);
								Iterator iterator6 = studentlist.iterator();
								while (iterator6.hasNext()) {
									StudentsBean studnetbean1 = (StudentsBean) iterator6.next();
									AttendenceBean attendenceDeBoard = SmsBeanDao
											.getstudentattendence(studnetbean1.getStudentId(), date, "1");
									if (attendenceDeBoard != null) {
										de_boardcount += 1;
									} else {
										de_boardabsentcount += 1;
									}
								}

							}
						}
					}
					Integer onboard = onboardcount;
					String onb = onboard.toString();
					table.addCell(onb);
					Integer onboardabs = onboardabsentcount;
					String deb = onboardabs.toString();
					table.addCell(deb);
					Integer deboard = de_boardcount;
					String debp = deboard.toString();
					table.addCell(debp);
					Integer deboardab = de_boardabsentcount;
					String debab = deboardab.toString();
					table.addCell(debab);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return table;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public java.io.InputStream getInputStream() {
		return inputStream;
	}

	public List<BusesBean> getBuseslist() {
		return buseslist;
	}

	public void setBuseslist(List<BusesBean> buseslist) {
		this.buseslist = buseslist;
	}

	public void setInputStream(java.io.InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
