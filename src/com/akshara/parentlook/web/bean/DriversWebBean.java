package com.akshara.parentlook.web.bean;

import java.sql.Blob;

public class DriversWebBean {

	private int DriverId;
	private String FirstName;
	private String LastName;
	private String ContactNumber;
	private String RelationTitle;
	private String RelativeName;
	private int Age;
	private Blob Photo;
	private String LicenceNumber;
	private Blob licenceAttachment;
	private String imageStr;
	private String idStr;
	private String licenceStr;
	private String IdType;
	private Blob idTypeAttachment;
	private String CurrentAddress;
	private int SchoolId;
	private String RecordStatus;
	private String usedStatus;

	public int getDriverId() {
		return DriverId;
	}

	public void setDriverId(int driverId) {
		DriverId = driverId;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getContactNumber() {
		return ContactNumber;
	}

	public void setContactNumber(String contactNumber) {
		ContactNumber = contactNumber;
	}

	public String getRelationTitle() {
		return RelationTitle;
	}

	public void setRelationTitle(String relationTitle) {
		RelationTitle = relationTitle;
	}

	public String getRelativeName() {
		return RelativeName;
	}

	public void setRelativeName(String relativeName) {
		RelativeName = relativeName;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public Blob getPhoto() {
		return Photo;
	}

	public void setPhoto(Blob photo) {
		Photo = photo;
	}

	public String getLicenceNumber() {
		return LicenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		LicenceNumber = licenceNumber;
	}

	public Blob getLicenceAttachment() {
		return licenceAttachment;
	}

	public void setLicenceAttachment(Blob licenceAttachment) {
		this.licenceAttachment = licenceAttachment;
	}

	public String getImageStr() {
		return imageStr;
	}

	public void setImageStr(String imageStr) {
		this.imageStr = imageStr;
	}

	public String getIdStr() {
		return idStr;
	}

	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}

	public String getLicenceStr() {
		return licenceStr;
	}

	public void setLicenceStr(String licenceStr) {
		this.licenceStr = licenceStr;
	}

	public String getIdType() {
		return IdType;
	}

	public void setIdType(String idType) {
		IdType = idType;
	}

	public Blob getIdTypeAttachment() {
		return idTypeAttachment;
	}

	public void setIdTypeAttachment(Blob idTypeAttachment) {
		this.idTypeAttachment = idTypeAttachment;
	}

	public String getCurrentAddress() {
		return CurrentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		CurrentAddress = currentAddress;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public String getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		RecordStatus = recordStatus;
	}

	public String getUsedStatus() {
		return usedStatus;
	}

	public void setUsedStatus(String usedStatus) {
		this.usedStatus = usedStatus;
	}

}
