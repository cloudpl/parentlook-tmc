package com.generic.mss.utils;

import java.sql.Clob;

import org.apache.log4j.Logger;

public class ClobUtil {
	private static final Logger LOG = Logger.getLogger(ClobUtil.class);

	public static String getClobAsString(Clob clob) {

		String str = null;
		if (clob != null) {
			try {
				str = clob.getSubString(1, (int) clob.length());
				System.out.println("clobutil"+str);
				str = RichParserUtil.parseHTMLToText(str);
				System.out.println(str+"afetr");
			} catch (Exception e) {

			}
		}

		return str;
	}

	public static String getClobAsNormalString(Clob clob) {

		String str = null;
		if (clob != null) {
			try {

				str = clob.getSubString(1, (int) clob.length());

			} catch (Exception e) {
				LOG.error(e);
			}
		}

		return str;
	}

	public static String getClobAsHtml(Clob clob) {

		String str = null;
		if (clob != null) {
			try {
				str = clob.getSubString(1, (int) clob.length());

			} catch (Exception e) {
				LOG.error(e);
			}
		}

		return str;
	}

	public static String getClobAsTrimmedString(Clob clob) {

		String str = null;
		if (clob != null) {
			try {
				str = clob.getSubString(1, (int) clob.length());
				str = RichParserUtil.parseHTMLToText(str);
				if (str != null && str.length() >= 200) {
					str = str.substring(0, 200);
				}
			} catch (Exception e) {

			}
		}

		return str;
	}

	public static String getClobAsTrimmedString1(Clob clob) {

		String str = null;
		if (clob != null) {
			try {
				str = clob.getSubString(1, (int) clob.length());
				str = RichParserUtil.parseHTMLToText(str);

			} catch (Exception e) {

			}
		}

		return str;
	}

}
