package com.generic.mss.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class HibernateUtils {

	public HibernateUtils() {
	}

	private static final Log LOG = LogFactory.getLog(HibernateUtils.class);

	private static SessionFactory SESSION_FACTORY = null;

	public static synchronized SessionFactory buildSessionFactory() {

		if (SESSION_FACTORY == null) {
			try {

				Logger.getLogger("com.ssc.pacs.utility.SessionFactoryUtil").info("CREATING SESSION_FACTORY");
				SESSION_FACTORY = new AnnotationConfiguration().configure("hibernate.cfg.xml").buildSessionFactory();
			} catch (Exception e) {
				Logger.getLogger("com.ssc.pacs.utility..SessionFactoryUtil")
						.error("EXCEPTION IN CREATING SESSIONFACTORY ");
				Logger.getLogger("ccom.ssc.pacs.utility..SessionFactoryUtil").error(e);
			}

		}

		return SESSION_FACTORY;
	}

	public static Session getSession() {

		if (SESSION_FACTORY != null) {

			return SESSION_FACTORY.openSession();
		} else {

			SESSION_FACTORY = buildSessionFactory();
			return SESSION_FACTORY.openSession();
		}

	}

}
