package com.generic.mss.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.log4j.Logger;

public class RichParserUtil {
	private static final Logger LOG = Logger.getLogger(RichParserUtil.class);

	public static String parseToText(String richText) {
		String text = null;
		if (richText != null) {
			LOG.warn("Original text " + richText);
			RTFEditorKit rtfParser = new RTFEditorKit();
			Document document = rtfParser.createDefaultDocument();
			try {
				rtfParser.read(new ByteArrayInputStream(richText.getBytes()),
						document, 0);
				text = document.getText(0, document.getLength());
				LOG.warn("Text After Parsing  " + text);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (BadLocationException e) {
				e.printStackTrace();
			}

		}
		LOG.warn("Final text " + text);
		return text;
	}

	public static String parseHTMLToText(String richText) {

		Html2Text parser = new Html2Text();
		try {
System.out.println("rich");
			parser.parse(new StringReader(richText));

		} catch (IOException e) {
			LOG.error(e);
		}
		return parser.getText();
	}
}
