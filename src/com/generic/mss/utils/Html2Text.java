package com.generic.mss.utils;

import java.io.*;
import javax.swing.text.html.*;
import javax.swing.text.html.parser.*;

public class Html2Text extends HTMLEditorKit.ParserCallback {
	StringBuffer s;

	public Html2Text() {
	}

	public void parse(Reader in) throws IOException {
		s = new StringBuffer();
		ParserDelegator delegator = new ParserDelegator();
		// the third parameter is TRUE to ignore charset directive
		delegator.parse(in, this, Boolean.TRUE);
	}

	public void handleText(char[] text, int pos) {
		s.append(text);
	}

	public String getText() {
		return s.toString();
	}

	public static void main(String[] args) {
		try {
			// the HTML to convert
System.out.println("html");
			Reader reader = new StringReader(
					"<p>Ganesha ...</p><p>&nbsp;</p><p><strong>Hi..........</strong></p>");
			Html2Text parser = new Html2Text();
			parser.parse(reader);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}