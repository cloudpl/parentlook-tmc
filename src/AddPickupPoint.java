
import org.apache.log4j.Logger;

import com.akshara.parentlook.db.bean.PickupPoints;
import com.akshara.parentlook.db.bean.StudentsBean;
import com.akshara.parentlook.db.dao.SmsBeanDao;
import com.opensymphony.xwork2.ActionSupport;

public class AddPickupPoint extends ActionSupport {

	private static final Logger LOG = Logger.getLogger(AddPickupPoint.class);
	private int StudentId;

	private int SchoolId;

	private String PickupPoint;

	private int RouteId;

	private int BusId;
	private String PLatLang;
	private String direction;

	@Override
	public String execute() throws Exception {

		try {
			int pId = SmsBeanDao.addPickupPoint(SchoolId, PickupPoint, RouteId, BusId, PLatLang, direction);

			PickupPoints points = SmsBeanDao.getPickupPointDetails(pId);
			StudentsBean studentsBean = SmsBeanDao.updatePickupPointForStudent(StudentId, points);

		} catch (Exception e) {
			LOG.error(e);
		}
		return SUCCESS;
	}

	public int getStudentId() {
		return StudentId;
	}

	public void setStudentId(int studentId) {
		StudentId = studentId;
	}

	public int getSchoolId() {
		return SchoolId;
	}

	public void setSchoolId(int schoolId) {
		SchoolId = schoolId;
	}

	public String getPickupPoint() {
		return PickupPoint;
	}

	public void setPickupPoint(String pickupPoint) {
		PickupPoint = pickupPoint;
	}

	public int getRouteId() {
		return RouteId;
	}

	public void setRouteId(int routeId) {
		RouteId = routeId;
	}

	public int getBusId() {
		return BusId;
	}

	public void setBusId(int busId) {
		BusId = busId;
	}

	public String getPLatLang() {
		return PLatLang;
	}

	public void setPLatLang(String pLatLang) {
		PLatLang = pLatLang;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

}
