import java.util.Random;

import com.akshara.parentlook.db.dao.SmsBeanDao;

public class UpdateGps {

	public static void main(String[] args) throws InterruptedException {

		String[] busgps = { "17.456929,78.398779", "17.436736,78.443906", "17.438701, 78.443112",
				"17.440892, 78.442061", "17.443185, 78.440709", "17.445621, 78.439271", "17.448241, 78.437834",
				"17.451004, 78.436482", "17.453563, 78.434980", "17.459008, 78.432512", "17.461894, 78.431225",
				"17.467523, 78.428714", "17.472190, 78.425796", "17.475608, 78.423457", "17.480356, 78.417213",
				"17.483037, 78.413801", "17.482996, 78.413866" };

		Random r = new Random();
		int Low = 0;
		int High = 15;
		for (int j = 0; j <= 100; j++) {
			Thread.sleep(10000);
			for (int i = 1; i <= 3; i++) {

				int Result = r.nextInt(High - Low) + Low;

				String s = busgps[Result];
				String[] latlang = s.split(",");

				SmsBeanDao.updateGpsDemo(i, latlang[0], latlang[1]);

			}

		}

	}

}
