<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ParentLook | Log in</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
   
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/AdminLTE.min.css">
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

 
    <script type="text/javascript">
    $(document).ready(function(){
    	$("#sub").click(function(){
    		var newpass=$("#newpassword").val();
    		var confirm=$("#confirmpassword").val();
    		var oldpassword=$("#oldpassword").val();
    		
    		if(oldpassword==newpass){
    			$("#newpassword").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>oldpassword and newpassword are same</div>");
    			return false;
    		}else{
    			 $("#newpassword").parent().next(".validation").remove();
    		}
    		if(newpass!=confirm){
    			$("#confirmpassword").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>newpassword and comfirm password are doesn't match</div>");
    			return false;
    		}else{
    			 $("#confirmpassword").parent().next(".validation").remove();
    		}
    		
    	
    
    	});
    	});
    
	
    </script>
  
  </head>
  <body class="hold-transition login-page" style='background:orange url("img/login-bg-map.png") no-repeat scroll center center'>
  <div class="col-md-6 pull-right" style="background:white;height:100vh;">
    <div class="login-box">
      <div class="login-logo">
       <div align="center"><img src="img/logo.png"/></div>
        <a href="#"><b>Parent</b>Look</a>
      </div> 
     
      <div class="login-box-body">
       
        <s:form action="updatePassword" theme="simple">
  

        <s:hidden name="oldpassword" id="oldpassword" value="%{#session.oldpassword}"></s:hidden>
        <s:hidden name="otp" value="%{#session.diff}" id="to"></s:hidden>
          <div class="form-group has-feedback">
          <label>New Password</label>
            <input type="password" class="form-control" id="newpassword" name="newpassword" placeholder="New Password" onkeyup='check();' required>
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>
           <div class="form-group has-feedback">
          <label>Confirm Password</label>
            <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password"  onkeyup='check();' required>
             <span id='message'></span>
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>
           <div class="form-group has-feedback">
          <label>OTP</label>
            <input type="text" class="form-control" id="otp" name="token" placeholder="otp" required>
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
         <span class="text-red"><s:property value="errMsg"/></span>
            <span class="text-red"><s:property value="msg"/></span>
          </div>
          <div class="row no-margin">
            
            <div class="pull-right">
              <!-- <button type="submit" class="btn bg-blue flat">Submit</button> -->
         <input type="submit" class="btn bg-blue flat" id="sub" value="Submit">
        
            </div> 
             
          </div>
          <br/><br/>
        </s:form>


        

      </div> 
    </div> 
</div>
    <script src="jQuery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
     
    
  </body>
</html>
