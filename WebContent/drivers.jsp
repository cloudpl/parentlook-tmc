<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
 <link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
 	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
 	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
    	$(".active").removeClass();
  	  $("#logistics").addClass("active");
  	  $("#drivers").addClass("active");
  	 var driversList= "<s:property value='driversList.size'/>";
  	
  	 if(driversList>0){
 	   $("#excel").show();
 	 //  $("#pdf").show();
 	   
    }else{
 	   $("#excel").hide();
 	  // $("#pdf").hide();
    }
  $(".del").click(function(){
	  var x = confirm("Are you sure you want to delete ?");
	  
	  if(x==true){
		  
		  return true;
	  }else{
		  
		  return false;
	  }
  })
    });
    	
  
    
    
	function imgError1(image) {
		
		
		image.onerror = "";
		image.src = "img/default_profile.png";
		return true;
	}
	

</script>
 <style type="text/css">
 
	#fabutton {
  background: none;
  padding: 0px;
  border: none;
}
 </style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>
 		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
 		<div class="content-wrapper">
			<section class="content-header">
				<div class="text-right">
				<span><s:a action="addDriver" cssClass="bg-blue pad margin-r-5">+AddNewDriver</s:a></span>
				<span><a href="exportDriverExcel" id="excel" class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
			</div>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow">
							<!-- /.box-header -->
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Id</th>
											<th>Name</th>
											<th>Contact No:</th>
											<th>Reference</th>
											<th>Licence No:</th>
											<th>Id Type</th>
											<th>Current Address</th>
												<th>Actions</th>
											



										</tr>
									</thead>
									<tbody>
										<s:iterator value="driversList">
										<s:if test="%{RecordStatus.equals('ACTIVE')}">
											<tr>
											
											
												

												<td><s:property value="DriverId" /></td>
 											 <td style="position: relative;"><img
													onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{imageStr}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
												
											 
											 <div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
													</div> <s:property value="FirstName" /> <s:property value="LastName" /></td>
												<td><s:property value="ContactNumber" /></td>
												<td><s:property value="RelationTitle" /> <s:property value="RelativeName" /></td>
												
											<td style="position: relative;" id="img"><s:property value="LicenceNumber" /> <img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{licenceStr}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
													</div></td>
													
												

												<td style="position: relative;"><s:property value="IdType" /> <img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{idStr}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
													</div></td>
												
										

                                               <td><s:property value="CurrentAddress" /></td>
                                               
                                               <td>
                                             <div class="col-md-4 no-padding">
                                               <s:form action="viewdriver" title="View Driver"><s:hidden name="driverid" value="%{DriverId}"></s:hidden><button type="submit"  id="fabutton"> <i class="fa fa-eye margin-r-5"></i></button></s:form>
                                               </div>
                                              <div class="col-md-4 no-padding">
                                               <s:form action="editdriver" title="Edit Driver"><s:hidden name="driverid" value="%{DriverId}"></s:hidden><button type="submit"  id="fabutton">  <i class="fa fa-pencil text-blue margin-r-5"></i></button></s:form>
                                             </div>
                                             <s:if test="%{!usedStatus.equals('ACTIVE')}">
                                             <div class="col-md-4 no-padding">
                                               <s:form action="deletedriver" title="Delete Driver"><s:hidden name="driverid" value="%{DriverId}"></s:hidden><s:submit type="button" cssClass="del" id="fabutton"> <i class="fa fa-trash text-red margin-r-5"></i></s:submit></s:form>
                                               </div>
                                                </s:if>  
                                                
                                                
                                                 </td>

											</tr></s:if>
										</s:iterator>
									</tbody>
									
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>

	</div>
 	<script>
		$(function() {
			$('#example1').DataTable();
 		});
		  
	</script>
</body>
</html>
