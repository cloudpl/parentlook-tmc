<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h4></h4>
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getBusesList">Buses</s:a></li>
						<li class="active">View Bus Details</li>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-border no-shadow">

					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
								<div class="box-body">
								<dl class="dl-horizontal">
							    <dt>Bus Id:</dt><dd><s:property value="%{busbean.BusId}" /></dd>
								<dt>Chasis Number:</dt><dd><s:property value="%{busbean.ChasisNumber}" /></dd>
							    <dt>Driver Id:</dt><dd><s:property value="%{busbean.DriverId}" /></dd>
							    <dt>Engine Number:</dt><dd><s:property value="%{busbean.EngineNumber}" /></dd>
							    <dt>Reg Number:</dt><dd><s:property value="%{busbean.RegNumber}" /></dd>											
							      <dt>Return Start Time:</dt><dd><s:property value="%{busbean.ReturnStartTime}" /></dd>	
							          <dt>Route Id:</dt><dd><s:property value="%{busbean.RouteId}" /></dd>	
							              <dt>Transport Start Time:</dt><dd><s:property value="%{busbean.TransportStartTime}" /></dd>	
							                  <dt>StartingPoint:</dt><dd><s:property value="%{busbean.StartPointLocation}" /></dd>	
							      
							   </dl>
							</div>
							</div>
						</div>
					</div>
					<div class="box-footer">
										
						<s:form action="editbus">
			<input type="hidden" name="busid" value="<s:property value='%{busbean.BusId}'/>"/>
				   <input type="submit"  value="Edit" />
				  
							   </s:form>
							   </div>
				</div>

			</section>
		</div>


	</div>

	
	<script type="text/javascript">
	
	
	$(document).ready(function(){
	  	  $(".active").removeClass();
	  	  $("#logistics").addClass("active");
	  	  $("#buses").addClass("active");
	    });
	</script>
</body>
</html>
