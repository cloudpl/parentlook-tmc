<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ParentLook | Log in</title>
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
   
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/AdminLTE.min.css">
   
      <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="js/bootstrap.min.js"></script>
    <!-- iCheck -->
<script type="text/javascript">
history.pushState(null,null,'login');
window.addEventListener('popstate',function(evnet){
	history.pushState(null,null,'login');
});

</script>
    
  </head>
  <body class="hold-transition login-page" style='background:orange url("img/login-bg-map.png") no-repeat scroll center center'>
  <div class="col-md-6 pull-right" style="background:white;height:100vh;">
    <div class="login-box">
      <div class="login-logo">
       <div align="center"><img src="img/logo.png"/></div>
        <a href="#"><b>Parent</b>Look</a>
      </div><!-- /.login-logo -->
     
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <s:form action="adminLogin" theme="simple">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="adminName" placeholder="Username" required>
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="adminPassword" placeholder="Password" required>
            
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
             <span class="text-red"><s:property value="errMsg"/></span>
          </div>
          <div class="row no-margin">
             <div class="pull-right">
              <button type="submit" class="btn bg-blue flat">Sign In</button>
                       </div><!-- /.col --> 
             <a href="forgotpassword">Forgot Password?</a>
          </div>
          
          <br/><br/>
        </s:form>


   
        

      </div> 
    </div> 
</div>
  
     
    
  </body>
</html>
