<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 300px;
        width:660px;
        float:left;
        
      
      }
      #routemap{
      float:right;
      height:300px;
      width:300px;
      }
    </style>
  
    

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmDYBawBkUUGIqUoHs72EdlK3UugfhGM4"></script> 
    
        <script>
    $(document).ready(function()
    		{
    	
    	
     	var directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true});
	var myLatlng = new google.maps.LatLng(17.456929, 78.398779);
	var directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true});

	
    var mapOptions = {
      zoom: 14,
      styles: [{
	  	       featureType: 'all',
	  	        elementType: 'labels',
	  	        stylers: [{
	  	            visibility: 'on'
	  	        }]
	  	    }],
      center: myLatlng,
      disableDefaultUI: true,
      mapTypeControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_CENTER
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	  var map = new google.maps.Map(document.getElementById("map"),
	          mapOptions);
	  
	  var image = {
		        url: 'https://cdn2.iconfinder.com/data/icons/school-set-2/512/15-512.png', // image is 512 x 512
		        scaledSize : new google.maps.Size(22, 32)
		    };



        
	  var marker=new google.maps.Marker({position:myLatlng,map:map,title:"BusLocation",label:"Hyd",icon:image});

    reload();
    function reload() {
		var url = "relodAction";
		$.ajax({
			url : url,
			type : 'POST',
			data : $('form').serializeArray(),
			cache : false,
			success : function(data) {

				var list = data.list;
				var st = "";
				for (var i = 0; i < list.length; i++) {
					var inBean = list[i];	 
					 
					var myLatlng = new google.maps.LatLng(inBean.geoLat, inBean.geoLang);			
					
					 var image = {
						        url: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcR2Xq8ZMmFHy3h_b_cNwYfBMAEW10Xozf7s372ubrkJChOgLNBn', // image is 512 x 512
						        scaledSize : new google.maps.Size(22, 32)
						    };


					
				    var marker=new google.maps.Marker({position:myLatlng,map:map,title:"BusLocation",label:"Hyd",icon:image});
	

				}

				 

			},
			error : function(data) {

			}
		});
	}
    
    
    	
    });
    
    
    
    
    	
    	var getUrlParameter = function getUrlParameter(sParam) {
    	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    	        sURLVariables = sPageURL.split('&'),
    	        sParameterName,
    	        i;

    	    for (i = 0; i < sURLVariables.length; i++) {
    	        sParameterName = sURLVariables[i].split('=');

    	        if (sParameterName[0] === sParam) {
    	            return sParameterName[1] === undefined ? true : sParameterName[1];
    	        }
    	    }
    	};
   
    </script>



	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>




		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<h4>Schools</h4>
				<h5>
					<s:a action="addSchool">+ Add New School</s:a>
				</h5>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Schools</a></li>
					<!--  <li class="active">Add New School</li> -->
				</ol>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Data Table With Full Features</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<div id="map"></div>
							<input type="hidden" name="lat"  id="lat">
							<input type="hidden" name="lon"   id="lon">	
							<table  border="1" id="routemap">
							<thead>
							<tr>
							<th>BusId</th>
							<th>RouteId</th>
							<th>SchoolId</th>
							<th>Pickup</th>
							<th>DropStatus</th>
							</tr>
							</thead>
							<tbody>
							<s:iterator value="busList" >
							<tr>
												<td><s:property value="BusId" /></td>
												<td><s:property value="RouteId" /></td>
												<td><s:property value="SchoolId" /></td>
												<td><s:property value="Pickup" /></td>
												<td><s:property value="DropStatus" /></td>
												</tr>
							
							</s:iterator>
							
							</tbody>
							
							
							</table>						
							
								 
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>



		<%@ include file="footer.jsp" %>


		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>

				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<!-- Home tab content -->
				<div class="tab-pane" id="control-sidebar-home-tab">
					<h3 class="control-sidebar-heading">Recent Activity</h3>

				</div>
				<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab
					Content</div>
				<div class="tab-pane" id="control-sidebar-settings-tab">
					<form method="post">
						<h3 class="control-sidebar-heading">General Settings</h3>
						<div class="form-group">
							<label class="control-sidebar-subheading"> Report panel
								usage <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Some information about this general settings option</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Allow mail
								redirect <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Other sets of options are available</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Expose author
								name in posts <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Allow the user to show his name in blog posts</p>
						</div>

						<h3 class="control-sidebar-heading">Chat Settings</h3>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Show me as
								online <input type="checkbox" class="pull-right" checked>
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Turn off
								notifications <input type="checkbox" class="pull-right">
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Delete chat
								history <a href="javascript::;" class="text-red pull-right"><i
									class="fa fa-trash-o"></i></a>
							</label>
						</div>
					</form>
				</div>
			</div>
		</aside>
		
		<div class="control-sidebar-bg"></div>
	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<!-- page script -->
	<script>
      $(function () {
    	  $('#example1').DataTable();
        
      });
    </script>
</body>
</html>
