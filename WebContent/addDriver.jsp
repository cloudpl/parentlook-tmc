<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
	<style type="text/css">
		.img-responsive {
     
     position: absolute;
    left: 70%;
    top: 70%;
    background-color: white;
    z-index: 150;

    height: 100px;
    margin-top: -200px;

    width: 200px;
    margin-left: -180px;
 }</style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>

		

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
			<s:if test="%{driversbean.DriverId>0}">
				<h4></h4>
				</s:if>
				<s:else>
				<h4></h4>
				</s:else>
				<ol class="breadcrumb">
						<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getDriversList">Drivers</s:a></li>
					<s:if test="%{driversbean.DriverId>0}">
						<li class="active">Edit Driver</li>
						</s:if>
						<s:else>
						<li class="active">Add New Driver</li>
						</s:else>
				
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-shadow no-border flat">
<div class="pad">All Fields Marked<span class="text-red">* </span>Mandatory</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								
								
								<s:form id="form"  enctype="multipart/form-data">
									<div class="box-body">
									 
										<div class="form-group">
											<label for="RegNumber">First Name<span class="text-red">*</span></label> <input type="text"  
												class="form-control" id="exampleInputEmail" pattern="^[a-zA-Z ]+$" title="Allows Only Characters"
												name="FirstName" value='<s:property value="%{driversbean.FirstName}"/>' required>
										</div>
									 
											<div class="form-group">
											<label for="RegNumber">Last Name<span class="text-red">*</span></label> <input type="text" 
												class="form-control" id="exampleInputEmail1" pattern="^[a-zA-Z]+$" title="Allows Only Characters"
												name="LastName" value='<s:property value="%{driversbean.LastName}"/>' required>
										</div>
											<div class="form-group">
											<label for="ContactNumber">Contact Number<span class="text-red">*</span> </label> <input type="text"
												pattern="[789][0-9]{9}" title="Phone Number start with 7 or 8 or 9" class="form-control" id="contactnumber" name="ContactNumber"
										value='<s:property value="%{driversbean.ContactNumber}"/>' maxlength="10" required>
										<s:hidden value="%{driversbean.ContactNumber}" id="contact"></s:hidden>
										</div>
										
										<div class="row">
										<div class="col-md-6">
										<div class="form-group">
										<label>Relation<span class="text-red">*</span></label>
											  <div class="form-inline">
											<select name="RelationTitle"  required>
                                           <s:if test="%{driversbean.DriverId>0}">
                                          <s:iterator value="relation"  var="s">
												<s:if test="#s.equals({driversbean.RelativeName)">
													<option value='<s:property value="#s"/>' selected><s:property value="#s" /></option>
												</s:if>
												<s:else>
													<option value='<s:property value="#s"/>'><s:property value="#s"/></option>
												</s:else>
												
										
												
											</s:iterator>
                                                </s:if>
                                                <s:else>
                                              <option value="S/o">S/o</option>
                                               <option value="D/o">D/o</option>
                                               <option value="W/o">W/o</option>
                                                <option value="C/o">C/o</option>
                                                </s:else>
                                             </select>
                                   
                                             <input type="text" id="RelativeName" name="RelativeName" value='<s:property value="%{driversbean.RelativeName}"/>'required/>
                                             </div>
										</div>
										</div>
										<div class="col-md-6">
										<div class="form-group">
											<label for="DriverAge">Age<span class="text-red">*</span> </label> 
											<div class="form-inline">
											<input id="exampleInputEmail1" name="Age" maxlength="2" value='<s:property value="%{driversbean.Age}"/>'
												required>	</div>							
										</div>
										</div>
										</div>
										
										
										
										<div class="form-group" id="divimg">
											<label for="exampleInputFile">Picture<span class="text-red">*</span></label>
								<s:if test="%{driversbean.DriverId>0}">		
								 <s:if test="%{!imageStr.equals('')}">	
				          <img onerror="imgError1(this);" class="maxyId"
						src="data:image/png;base64,<s:property value='%{imageStr}'/>"
						style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" id="chdriimg"/>
						<div id="imMax" style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;"></div>
					<img id="myImage" src="" />
										<div class="form-inline">  <s:file name="PhotoFile" id="img" accept="image/*" onchange="handleFileSelect(event,img)"/></div>
										</s:if><s:else>
										<input type="file" name="PhotoFile" id="img_new" accept="image/*" onchange="handleFileSelect(event,img_new)" required/>
										
										</s:else>
										</s:if>
										<s:else>
 										  <input type="file" name="PhotoFile" id="img1" accept="image/*" required />
										
										</s:else>
										</div>
										

										<div class="form-group">
										<label for="EngineNumber">Licence Number<span class="text-red">*</span></label>
									   <div class="form-inline">
											
											 <input	type="text" name="LicenceNumber" class="form-control" maxlength="15"
												id="licencenumber" value='<s:property value="%{driversbean.LicenceNumber}"/>' required>
					     <s:if test="%{driversbean.DriverId>0}">
		 		
								 <s:if test="%{!licenceStr.equals('')}">	
				          <img onerror="imgError1(this);" class="maxyId"
						src="data:image/png;base64,<s:property value='%{licenceStr}'/>"
						style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" id="licenceimg"/>
						<div id="imMax" style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;"></div>
					<img id="myImage" src="" />
								  <!-- <file name="Photo" id="schimg"/>  -->
										<div class="form-inline">  <s:file name="licenceattachmentfile" id="licencefile" accept="image/*" onchange="handleFileSelect1(event,licencefile)"/></div>
										
										</s:if>
										<s:else>
										<input type="file" name="licenceattachmentfile" id="licencefile1" accept="image/*" onchange="handleFileSelect1(event,licencefile1 )" required/>
										
										</s:else>
										</s:if>
					     
					  
	                 <s:else>
                  	  <input type="file" name="licenceattachmentfile" accept="image/*" id="licencefilecreate" required />
                       </s:else>
	               </div>
                    </div>
										

								       <div class="form-group">
										<div class="form-inline">
										<label>IdType<span class="text-red">*</span></label>
											<select name="IdType">
											<s:if test="%{driversbean.DriverId>0}">
											<s:iterator value="idtypes"  var="s">
												<s:if test="#s.equals(driversbean.IdType)">
													<option value='<s:property value="#s"/>' selected><s:property value="#s" /></option>
												</s:if>
												<s:else>
													<option value='<s:property value="#s"/>'><s:property value="#s"/></option>
												</s:else>
												
										
												
											</s:iterator>
											
											 
                                                </s:if>
                                                <s:else>
                                              <option value="aadharcard">AadharCard</option>
                                               <option value="pancard">Pan Card</option>
                                               <option value="voterid">Voter Id</option>
                                                <option value="passport">Passport</option>
                                                
                                                
                                                </s:else>
                                             </select>
							 
									     <s:if test="%{driversbean.DriverId>0}">
		 		
								 <s:if test="%{!idStr.equals('')}">	
				          <img onerror="imgError1(this);" class="maxyId"
						src="data:image/png;base64,<s:property value='%{idStr}'/>"
						style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" id="idimg"/>
						<div id="imMax" style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;"></div>
					<img id="myImage" src="" />
								  <!-- <file name="Photo" id="schimg"/>  -->
								   <div class="form-inline">  <input type="file" name="idattachmentfile" id="idfile" accept="image/*" onchange="handleFileSelect2(event,idfile)"/></div>
							
												</s:if>
									 <s:else>
									 
									 <div class="form-inline">  <input type="file" name="idattachmentfile" id="idfile1" accept="image/*" onchange="handleFileSelect2(event,idfile1)" required="required"/></div>
								
									 </s:else>
										</s:if>
										<s:else>
										 <div class="form-inline">  <input type="file" name="idattachmentfile" id="idfilecreate" accept="image/*" required="required"/></div>
								
										</s:else>
									
									
									
									
										</div>
										</div>
										
										<div class="form-group">
											<label for="currentaddress">Current Address<span class="text-red">*</span> </label> 
								             <textarea rows="3" cols="50" id="CurrentAddress"  name="CurrentAddress" required="required"><s:property value="%{driversbean.CurrentAddress}" /></textarea>
										</div>
												
			<s:hidden name="SchoolId" value="%{#session.schoolId}"></s:hidden>
			<s:hidden value="%{driversbean.LicenceNumber}" id="lichid"></s:hidden>
		<input type="hidden" id="driverid" name="DriverId" value='<s:property value="%{driversbean.DriverId}"/>'/>
									</div>


									<div class="box-footer">
									<s:if test="%{driversbean.DriverId>0}">
									<s:submit   id="update"  value="UPDATE"></s:submit>
										
	                                      </s:if>
	                                     <s:else> 	
	                                     <s:submit  value="SUBMIT"></s:submit>
 									
 </s:else>
							
									</div>
								<div id="loading">
    <img src="img/pro.png"  class="img-responsive img-center"/>
</div>
		
							</s:form>
								
							</div>
						</div>
						
					</div>
	
								
				</div>

			</section>
		</div>


			</div>

	
	<script type="text/javascript">
	
	$(document).ready(function()
	{ $("#loading").hide();
		$(".active").removeClass();
	  	  $("#logistics").addClass("active");
	  	  $("#drivers").addClass("active");
	 
	
		});

	
	</script>
	<script>

		
		$(document).ready(function(){  
			
			 $("#loading").hide();
		
			
		 	$("#img").hide();  
            $("#licencefile").hide();
		 	$("#idfile").hide();
			$("#chdriimg").click(function(){ 
				$("#img").click();
			});
			$("#licenceimg").click(function(){ 
				$("#licencefile").click();
			});

			$("#idimg").click(function(){ 
				$("#idfile").click();
			});
			
			$('#form').on('submit',function(e) {
				 $("#loading").hide();
			
				 $(this).attr("disabled", "disabled");
		 	        doWork();
			
				e.preventDefault();
				 var licencenumber=$("#licencenumber").val();
				var driverId=$("#driverid").val();
				
				 
					 
						
						  $.ajax({
						    	 url:"checkdriverlicence?licence="+licencenumber,
						        type: 'POST',
						       
						        context: this
						    }).done( function(data) {
						        
						            if (data.msg== 'Exists') {
						            	 $("#loading").hide();
						            var bean=data.driverbean;
						            if(bean.driverId==driverId){
						            	$("#licencenumber").parent().next(".validation").remove();
						            	
						            
						            	var imagestring="<s:property value='%{imageStr}'/>"
	  					          			if(imagestring==""){ 
	  					          				 var fileName1 = $("#img_new").val();
	  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
	  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
	  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
	  					                        	 $("#img_new").parent().next(".validation").remove();
	  					                          
	  					                          }else{
	  					                             $("#img_new").parent().next(".validation").remove();
							                $("#img_new").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  					                              return false;
	  					                          } 
	  					          			}else{ 
	  					          				var fileName = $("#img").val();
	  					          				var idxDot = fileName.lastIndexOf(".") + 1;
	  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
	  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#img").parent().next(".validation").remove();
	  					                          }else{
	  					                        	  
	  					                        	 if(fileName==""){ $("#img").parent().next(".validation").remove();}else{ 
	  					                             $("#img").parent().next(".validation").remove();
							                $("#img").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  					                              return false;
	  					                          } }
	  					            	 
	  					          			}
						            
	  					          		var imagestring="<s:property value='%{licenceStr}'/>"
	  					          			if(imagestring==""){ 
	  					          				 var fileName1 = $("#licencefile1").val();
	  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
	  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
	  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#licencefile1").parent().next(".validation").remove();
	  					                          }else{
	  					                              $("#licencefile1").parent().next(".validation").remove();
							                $("#licencefile1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  					                              return false;
	  					                          } 
	  					          			}else{ 
	  					          				var fileName = $("#licencefile").val();
	  					          				var idxDot = fileName.lastIndexOf(".") + 1;
	  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
	  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#licencefile").parent().next(".validation").remove();
	  					                          }else{
	  					                        	  
	  					                        	 if(fileName==""){$("#licencefile").parent().next(".validation").remove();}else{ 
	  					                             $("#licencefile").parent().next(".validation").remove();
							                $("#licencefile").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  					                              return false;
	  					                          } }
	  					            	 
	  					          			}
	  					          			
	  					          		var imagestring="<s:property value='%{idStr}'/>"
	  					          			if(imagestring==""){ 
	  					          				 var fileName1 = $("#idfile1").val();
	  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
	  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
	  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#idfile1").parent().next(".validation").remove();
	  					                          }else{
	  					                             $("#idfile1").parent().next(".validation").remove();
							                $("#idfile1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  					                              return false;
	  					                          } 
	  					          			}else{ 
	  					          				var fileName = $("#idfile").val();
	  					          				var idxDot = fileName.lastIndexOf(".") + 1;
	  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
	  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#idfile").parent().next(".validation").remove();
	  					                          }else{
	  					                        	  
	  					                        	 if(fileName==""){$("#idfile").parent().next(".validation").remove();}else{ 
	  					                              $("#idfile").parent().next(".validation").remove();
							                $("#idfile").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  					                              return false;
	  					                          } }
	  					            	 
	  					          			}
	  					          			
						            	$("#licencenumber").parent().next(".validation").remove();
						            	$("#form").attr("action","updateDriver");
										this.submit();
						          
						            }else{
						            	
						            	$("#licencenumber").parent().next(".validation").remove();
							                $("#licencenumber").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Licence Number Exists</div>");
						               
						            }

						            	
						            }
						            else {
						            	if(driverId>0){
						            		var imagestring="<s:property value='%{imageStr}'/>"
		  					          			if(imagestring==""){ 
		  					          				 var fileName1 = $("#img_new").val();
		  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
		  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
		  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#img_new").parent().next(".validation").remove();
		  					                          }else{
		  					                             $("#img_new").parent().next(".validation").remove();
							                $("#img_new").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		  					                              return false;
		  					                          } 
		  					          			}else{ 
		  					          				var fileName = $("#img").val();
		  					          				
		  					          				var idxDot = fileName.lastIndexOf(".") + 1;
		  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
		  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#img").parent().next(".validation").remove();
		  					                          }else{
		  					                        	  if(fileName==""){$("#img").parent().next(".validation").remove();}else{ 
		  					                              $("#img").parent().next(".validation").remove();
							                $("#img").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		  					                              return false;
		  					                          } }
		  					            	
		  					            	}
						            		
						            		var imagestring="<s:property value='%{licenceStr}'/>"
		  					          			if(imagestring==""){ 
		  					          				 var fileName1 = $("#licencefile1").val();
		  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
		  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
		  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#licencefile1").parent().next(".validation").remove();
		  					                          }else{
		  					                              $("#licencefile1").parent().next(".validation").remove();
							                $("#licencefile1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		  					                              return false;
		  					                          } 
		  					          			}else{ 
		  					          				var fileName = $("#licencefile").val();
		  					          				var idxDot = fileName.lastIndexOf(".") + 1;
		  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
		  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#licencefile").parent().next(".validation").remove();
		  					                          }else{
		  					                        	  
		  					                        	 if(fileName==""){$("#licencefile").parent().next(".validation").remove();}else{ 
		  					                             $("#licencefile").parent().next(".validation").remove();
							                $("#licencefile").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		  					                              return false;
		  					                          } }
		  					            	 
		  					          			}
		  					          		var imagestring="<s:property value='%{idStr}'/>"
		  					          			if(imagestring==""){ 
		  					          				 var fileName1 = $("#idfile1").val();
		  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
		  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
		  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#idfile1").parent().next(".validation").remove();
		  					                          }else{
		  					                             $("#idfile1").parent().next(".validation").remove();
							                $("#idfile1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		  					                              return false;
		  					                          } 
		  					          			}else{ 
		  					          				var fileName = $("#idfile").val();
		  					          				var idxDot = fileName.lastIndexOf(".") + 1;
		  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
		  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#idfile").parent().next(".validation").remove();
		  					                          }else{
		  					                        	  
		  					                        	 if(fileName==""){ $("#idfile").parent().next(".validation").remove();}else{ 
		  					                             $("#idfile").parent().next(".validation").remove();
							                $("#idfile").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		  					                              return false;
		  					                          } }
		  					            	 
		  					          			}
		  					          		$("#licencenumber").parent().next(".validation").remove();
		  					            	$("#form").attr("action","updateDriver");
		  									this.submit();
						            	 
						            	}else{
						            		 var fileName = document.getElementById("img1").value;
	  						                  var idxDot = fileName.lastIndexOf(".") + 1;
	  						                  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
	  						                  if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#img1").parent().next(".validation").remove();
	  						                  }else{
	  						                     $("#img1").parent().next(".validation").remove();
							                $("#img1").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  						                      return false;
	  						                  } 
	  						                var fileName = document.getElementById("licencefilecreate").value;
	  						                  var idxDot = fileName.lastIndexOf(".") + 1;
	  						                  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
	  						                  if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#licencefilecreate").parent().next(".validation").remove();
	  						                  }else{
	  						                     $("#licencefilecreate").parent().next(".validation").remove();
							                $("#licencefilecreate").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  						                      return false;
	  						                  } 
	  						                var fileName = document.getElementById("idfilecreate").value;
	  						                  var idxDot = fileName.lastIndexOf(".") + 1;
	  						                  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
	  						                  if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
	  						                	  $("#idfilecreate").parent().next(".validation").remove();
	  						                  
	  						                  }else{
	  						                   $("#idfilecreate").parent().next(".validation").remove();
							                $("#idfilecreate").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
	  						                      return false;
	  						                  } 
						            		
										$("#form").attr("action","saveDriverDetailsAction");
						                this.submit();
						            	}
						            } 
						        });
					
				
				 
		 
			

		});
		});
		 function doWork() {
			 $("#loading").show();
			    setTimeout('$("#btn").removeAttr("disabled")', 1500);
			}
	</script>
	 
	<script type="text/javascript">
function handleFileSelect(evt,id) {
      
  var files = evt.target.files;
  for ( var i = 0, f; f = files[i]; i++) {

   var reader = new FileReader();
   reader.onload = (function(theFile) {
    return function(e) {
  
     $("#chdriimg").attr("src", e.target.result);
    };
   })(f);
   reader.readAsDataURL(f);

  }

 }

</script><script type="text/javascript">
function handleFileSelect1(evt,id) {
   
  var files = evt.target.files;
  for ( var i = 0, f; f = files[i]; i++) {

   var reader = new FileReader();
   reader.onload = (function(theFile) {
    return function(e) {
  
     $("#licenceimg").attr("src", e.target.result);
    };
   })(f);
   reader.readAsDataURL(f);

  }

 }

</script>
	 
<script type="text/javascript">
function handleFileSelect2(evt,id) {
      
   
  var files = evt.target.files;
  for ( var i = 0, f; f = files[i]; i++) {

   var reader = new FileReader();
   reader.onload = (function(theFile) {
    return function(e) {
  
     $("#idimg").attr("src", e.target.result);
    };
   })(f);
   reader.readAsDataURL(f);

  }

 }

</script>
</body>
</html>
