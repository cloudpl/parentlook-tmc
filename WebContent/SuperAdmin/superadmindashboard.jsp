<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/struts-tags"  prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
	
	
  
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
 
  
 <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
 <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/app.min.js"></script>
<script src="js/bootstrap.min.js"></script>


	<script type="text/javascript">
	$(document).ready(function(){

		$("#popup").hide();
		 
		
		$(".cc").click(function(){
			
		   $(".model-title").html(" ");
		   
			$(".modal-body").html(" ");
			var id=$(this).attr("id");
			 $.ajax({
		    	 url:"schoolTrackingVideo?schoolId="+id,
		        type: 'POST',
		     
		        context: this
		    }).done( function(data) {
		    	var school=data.schoolname;
		    	var alist=data.gpsList;
		    	
		    	$(".model-title").append(school);
            	if(alist.length>0){
            	var fData="<div class='table-responsive' style='overflow-y:auto;'><table class='table table-striped no-margin'><tr> <th>BusName</th><th>DriverName</th><th>ContactNumber</th><th>TripStatus</th><th>Tracking</th><th>Video</th></tr>";
            	for(var i=0;i<alist.length;i++){
            		var busbean=alist[i];
            		var bName=busbean.busName;
            		var trip=busbean.tripStatus; 
            		var video=busbean.video;
            		var tracking=busbean.tracking;
            		var dirName=busbean.driverName;
            		var dirContct=busbean.dirContactNum;
            		
            		fData=fData+'<tr><td>'+bName+'</td><td>'+dirName+'</td><td>'+dirContct+'</td><td>'+trip+'</td><td>'+tracking+'</td><td>'+video+'</td></tr>';
            	}
            	$(".modal-body").append(fData+"</table></div>");
            	}else{
            		$(".modal-body").append("No Buses");
            	}
            	
            	
		    }); 
			$("#popup").click();
		});
		
	});
	</script>
	<style type="text/css">
	
	.cc {
  background: none;
  padding: 0px;
  border: none;
}
.modal-content{
   width:640px;
}

	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
		<%@ include file="superadmin-header.jsp" %>
			<%@include file="superadmin-sidebar.jsp"%>
			<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            
          </h1>
        
        </section>

        <!-- Main content -->
        <section class="content">
		<div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-building"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Schools</span>
                  <span class="info-box-number"><s:property  value= "%{#session.schoolslist}"/> </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-bus"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Buses</span>
                  <span class="info-box-number"><s:property  value= "%{#session.buseslist}"/> </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-people"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Parents</span>
                  <span class="info-box-number"><s:property  value= "%{#session.parentslist}"/> </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Students</span>
                  <span class="info-box-number"><s:property  value= "%{#session.studentslist}"/> </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
		  <div class="row">
		  <div class="col-md-12">
		  <div class="box no-border no-shadow no-border flat">
		  <div class="box-header with-border">
          <h3 class="box-title text-blue">Route Preferences and Metrics</h3>

          <div class="box-tools pull-right">
            <button type="button" onclick="window.location.href='adminlogin'" class="btn btn-warning"><i class="fa fa-refresh"></i></button>
            </div>
        </div>
		  <div class="box-body">
		  
		  <table id="example1" class="table table-striped table-bordered" >
		 <thead>
		  <tr>
		  <th>School Name</th>
		  <th>ContactNumber</th>
		   <th>Tracking Inactive</th>
		  <th>Video Inactive</th></tr></thead>
		   <tbody>
		  <s:iterator value="gpsweblist">
		  <tr>
	  
	 <td> <button title="click here to konw the buses info"  class="cc" id='<s:property value="%{schoolId}"/>'> <s:property value='schoolName'/></button>
 
</td> 
	  <td> <s:property value='contacnum'/></td>
		<td>  <s:property value='tcount'/></td>
		<td><s:property value='vcount'/></td></tr>
		  </s:iterator>
                <%-- <div class="box-header text-center">
                  <h3 class="box-title ">Recently Added Schools</h3><s:property value="%{schoolslist.length}"/>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                  <s:iterator value="schoolslist">
                    <li class="item col-md-6 inline">
                      <div class="product-img">
                       <img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{imageStr}'/>"
													style="height: 50px; width: 50px; margin-top: 0px; border-radius: 100%;" />
                      </div>
                      <div class="product-info">
                        <s:a action="schoolAdmins"><s:param name="SchoolId" value="%{SchoolId}"></s:param><s:property value="SchoolName"/></s:a>
                        <span class="product-description">
                         <s:property value="Address"/>
                        </span>
                      </div>
                    </li><!-- /.item -->
                     </s:iterator> </ul>
                </div><!-- /.box-body -->
                <div class="box-footer text-center  bg-gray">
                  <a href="getAdminSchoolList" class="uppercase">View All Schools</a>
				  
                </div> --%><!-- /.box-footer -->
          </tbody>  </table>
          </div>
          						 <button type="button" id="popup" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"></button>
    				<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
   
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">�</span></button>
          <h4 class="model-title"> </h4>
      </div>
      
      <div class="modal-body">
      
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
								
							</div>
						</div>
					</div>
          
            </div>
		  </div>
		  
		  </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
			<%@ include file="superadmin-footer.jsp" %>
			</div>
			
		<script type="text/javascript">

$(function () {
  $("#example1").DataTable();
});



</script>	
</body>

</html>