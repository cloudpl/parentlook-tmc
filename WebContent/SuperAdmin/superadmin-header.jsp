

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="adminlogin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>o</b>o</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Parentlook</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
            <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-warning">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 0 notifications</li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                <ul class="menu">
                  <li><!-- start notification -->
                    <a href="#">
                      <i class="fa fa-bell text-aqua"></i> Recent Notifications 0
                    </a>
                  </li>
                  <!-- end notification -->
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <!-- <li class="dropdown tasks-menu">
            Menu Toggle Button
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-handshake-o"></i>
              <span class="label label-danger">9</span>
            </a>
            
          </li> -->
		  
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" style="padding:5px 10px;" data-toggle="dropdown">
              <!-- The user image in the navbar-->
			  
			       <span class="hidden-xs">Administrator</br></span>
				   <img src="img/user.png" style="float:right;"class="user-image"  alt="User Image">
             
            </a>
            <ul class="dropdown-menu">
             
			 <!-- <li>
             <ul class="menu">
                  <li>
                    <a href="#">
                      Change Password
                       </a>.
					  </li>
				  <li>
					Signout
                       </a>.
					 
                  </li>
                  
                </ul>
              
			  </li> -->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
