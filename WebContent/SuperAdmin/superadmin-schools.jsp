<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
<style type="text/css">
	
#fabutton {
  background: none;
  padding: 0px;
  border: none;
}
	</style>
 </head>
<%@ taglib uri="/struts-tags" prefix="s" %>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
 
<%@ include file="superadmin-header.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="superadmin-sidebar.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Manage Schools
          </h1>
          <ol class="breadcrumb">
            <li><a href="adminLogin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Schools</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           <%@ include file="subsuperadmin-sidebar.jsp" %>
            <div class="col-md-9">
              <div class="box no-border no-shadow">
                <%-- <div class="box-header with-border">
                <h1></h1>
                  <div class="box-tools pull-right">
                    <div class="has-feedback">
                      <input type="text" class="form-control input-sm" placeholder="Search">
                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                  </div><!-- /.box-tools -->
                </div> --%>
                <div class="box-body no-padding">
                
                <div class="table-resposive">
                <table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>School Id</th>
											<th>School Name</th>
											<th>Email</th>
											<th>ContactNumber</th>
											<th>Address</th>
                                            <th>Actions</th>
										</tr>
									</thead>
									<tbody>
									
										<s:iterator value="schoolsList">
										<s:if test="%{Status==true}">
											<tr>

												<td><s:property value="SchoolId" /></td>
												<td><s:a action="schoolAdmins"><s:param name="SchoolId" value="%{SchoolId}"></s:param><s:property value="SchoolName" /></s:a></td>
												<td><s:property value="Email" /></td>
												<td><s:property value="ContactNumber" /></td>
												<td><s:property value="Address" /></td>
												
												
												 <td>
												  <div class="col-md-6 no-padding"> <s:form action="editSchoolBySuperAdmin" title="Edit School"><s:hidden name="idd" value="%{SchoolId}"></s:hidden><button type="submit" id="fabutton"><i class="fa fa-pencil text-blue margin-r-5"></i></button>
             
			 </s:form></div>
												 
												
												 <s:if test="%{usedStatus.equals('INACTIVE')}">
												 <div class="col-md-6 no-padding">  <s:form action="deleteSchoolBySuperAdmin" title="Delete School"><s:hidden name="id1" value="%{SchoolId}"></s:hidden><s:submit type="button" cssClass="del" id="fabutton"><i class="fa fa-trash text-red"></i></s:submit>
             
			 </s:form></div>  </s:if> 
												 </td>
                                              
                                               
											</tr></s:if>
										</s:iterator>
									</tbody>
									 
								</table>
                  </div>
                </div><!-- /.box-body -->
                <div class="box-footer no-padding">
                  
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- Main Footer -->
   
    <%@ include file="superadmin-footer.jsp" %>
  

	</div>
 

<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<!-- Bootstrap 3.3.6 -->
<script src="js/bootstrap.min.js"></script>

<script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
</body>
</html>
