<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<style type="text/css">
	
	#pac-input{
    height:25px;
    width:300px;
}
	
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="superadmin-header.jsp" %>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@ include file="superadmin-sidebar.jsp" %>
		</aside>
	<!-- =============================================== -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
			  <%@ include file="subsuperadmin-sidebar.jsp" %>
				<s:if test="%{schoolsDetailsBean.SchoolId>0}"> 
				 <h4></h4>
				<ol class="breadcrumb">
				 
					<li><s:a action="adminLogin"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getAdminSchoolList">Schools</s:a></li>
						<li class="active"> Edit School</li>
				
					</ol></s:if>
					<s:else>
					
					<h4></h4>
				<ol class="breadcrumb">
					<li><s:a action="adminLogin"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getAdminSchoolList">Schools</s:a></li>
						<li class="active">Add New School</li>
				
					</ol>
					</s:else>
			</section>
			<section class="content">
				<div class="box box-default">

					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
						
								<s:form  id="form" role="form" enctype="multipart/form-data">
									<div class="box-body">
									<h4>All Fields Marked<span class="text-red">*</span>  Mandatory</h4>
									
									
									 
										<div class="form-group">
											<label for="SchoolName">School Name <span class="text-red">*</span></label>
											<s:if test="%{schoolsDetailsBean.SchoolId>0}">
											 <input
												type="text" class="form-control" id="SchoolName" value="<s:property value='schoolsDetailsBean.SchoolName'/>"
												name="SchoolName" placeholder="Enter SchoolName" readonly="readonly"></s:if>
												<s:else>
												<input
												type="text" class="form-control" id="SchoolName" 
												name="SchoolName" placeholder="Enter SchoolName" required>
												
												</s:else>
										</div>

										<div class="form-group">
											<label for="Email">Email address <span class="text-red">*</span></label> <input type="email"
												class="form-control" id="Email" name="Email"
												placeholder="Enter email" value="<s:property value='schoolsDetailsBean.Email'/>" required>
										</div>
										
										<div class="form-group">
											<!-- <label for="ProfilePhoto">Photo</label> <input type="file"
												 id="photo" name="SchoolPhoto"> -->
												 <s:if test="%{schoolsDetailsBean.SchoolId>0}"> </s:if><s:else>
												 <label>Photo <span class="text-red">*</span></label>
												 <input type="file" name="PhotoFile" required/></s:else>
										</div>

									 	<div class="form-group">
											<label for="ContactNumber">Contact Number<span class="text-red">*</span></label> <input
												type="text" pattern="[789][0-9]{9}" title="Phone Number start with 7 or 8 or 9" class="form-control" id="ContactNumber"
												placeholder="Enter Contact Number"  value="<s:property value='schoolsDetailsBean.ContactNumber'/>"  name="ContactNumber" maxlength="10" required>
										</div> 

  

										 <!-- textarea -->
										<div class="form-group">
											<label>Address <span class="text-red">*</span></label>
											 <input type="text" name="Address"
												class="form-control" id="Address"
											value="<s:property value='%{schoolsDetailsBean.Address}'/>"/> 
											<%-- <s:textarea name="Address" 
												class="form-control" id="Address" value="%{schoolsDetailsBean.Address}"></s:textarea> --%>
										</div> 
										
										<div class="form-group">
											<label>MapLocation <span class="text-red">*</span></label>
											 
												
												 <input
												type="hidden" class="form-control" value='<s:property value='%{schoolsDetailsBean.MapLocation}'/>' name="MapLocation" id="MapLocation">
										</div>
										<div id="mapdetails">
								 <input id="pac-input" class="controls" type="text"
        placeholder="Enter a location">
                  <div class='img-responsive' id="map" style="height:300px;width:100%;"  ></div>
    <div class="description pad">
    
    
    				 <i class="fa fa-map-marker margin-r-5 text-orange" id="latlang"></i>
                  <i class="fa fa-map-marker margin-r-5 text-blue" id="waypoint"></i>
                  
                 
							</div>
							
							</div>



									</div>


<input 	type="hidden"  value='<s:property value='%{schoolsDetailsBean.MapLocationLatLang}'/>'  name="MapLocationLatLang" id="MapLocationLatLang" />



									<!-- /.box-body -->

									<div class="box-footer">
										<s:if test="%{schoolsDetailsBean.SchoolId>0}">
									<!-- <input type="submit" id="update" value="save" /> -->
									 
 					 <input id="update" type="submit" value="Update"/>
										</s:if>
										<s:else>
										<input type="submit" id="save" value="save" />
										</s:else>
									</div>
								</s:form>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.box-body -->

				</div>
				<!-- /.box -->

			</section>
			<!-- /.content-->
		</div>
		<!-- /.content-wrapper -->


		<%-- <footer class="main-footer">
    <%@ include file="superadmin-footer.jsp" %>
  </footer> --%>

<%@ include file="superadmin-footer.jsp" %>



		 
		
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="js/bootstrap.min.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

 
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY&libraries=places"
        async defer></script>
         <script>


	$(document)
			.ready(
					function() {

						initMap();

					});

	</script>
	
	<script>
      var map;
      var palcename;
      var marker;
      var options = {
    		  enableHighAccuracy: true,
    		  timeout: 10000,
    		  maximumAge: 0
    		};
      function initMap() {
    	//alert("hii");
    	  var input = document.getElementById('pac-input');
    	  if (navigator.geolocation) {
    	        navigator.geolocation.getCurrentPosition(showPosition,showError,options);
    	    }

          
    	  


function getPlaceDetailsByLatLang(myLatLang){




    var geocoder;
	  geocoder = new google.maps.Geocoder();
	   

	  geocoder.geocode(
	      {'latLng': myLatLang}, 
	      function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	                  if (results[0]) {
	                      var add= results[0].formatted_address ;
	                      var  value=add.split(",");

	                   var	   count=value.length;
	                  var    country=value[count-1];
	                    var  state=value[count-2];
	                    var  palcename=value[count-3];
							//alert(palcename);
					$("#latlang").html("&nbsp;&nbsp;Latitude,Longitude:"+myLatLang);
					 $("#llt").attr("value",myLatLang);		
					$("#waypoint").html("&nbsp;&nbsp;"+value);
					$("#MapLocation").attr("value",value);
					$("#MapLocationLatLang").attr("value",myLatLang);
					document.getElementById('pac-input').value = value;
					 
					//$("#way").attr("value",value)	;
				//$("#pid").attr("value",value[count-5]+" "+value[count-4])	;
				$("#MapLocationLatLang").attr("value",myLatLang)	;
					
	                      //alert(state);
	                     
	                      
	                  }
	                  else  {
	                     alert("address not found");
	                  }
	          }
	           else {
	             alert("Geocoder failed due to: " + status);
	          }
	      }
	  );

	
}

function showPosition(position) {

	 var  myLatlng;
	 var schoolid='<s:property value="%{schoolsDetailsBean.SchoolId}"/>'
	    var gpslist='<s:property value="gpslist.size"/>'
	    	

	    if(schoolid>0){
	  	  var startid=$("#MapLocationLatLang").val();
	        startid = startid.slice(1,-1);
	        
	        var  value=startid.split(",");
	        myLatlng=new google.maps.LatLng(value[0],value[1]);;
	        }else{
	        	
	       	   myLatlng= new google.maps.LatLng(position.coords.latitude,position.coords.longitude);

	        }
		 
		 
	
	
    var mapOptions = {
      zoom: 14,
      center: myLatlng,
      disableDefaultUI: true,//disable default controls
      mapTypeControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_RIGHT
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	    map = new google.maps.Map(document.getElementById("map"),
	          mapOptions);

	    
	    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
if(gpslist>0){
      marker=new google.maps.Marker({position:myLatlng,map:map,title:"Set Way Point",draggable:false,});
      }else{
    	  marker=new google.maps.Marker({position:myLatlng,map:map,title:"Set Way Point",draggable:true,});
      }

    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
       


        getPlaceDetailsByLatLang(event.latLng);


    });
    
    
    
    
    var autocomplete = new google.maps.places.Autocomplete(input);
    // If the place has a geometry, then present it on a map.
   autocomplete.addListener('place_changed', function() {
    
    var place = autocomplete.getPlace();
   //alert(JSON.stringify(place));
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return; 
    }

    // If the place has a geometry, then present it on a map.
   var  myLatlng =  place.geometry.location;
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
       } else {  map.setCenter(place.geometry.location);
    }
    marker.setPosition(myLatlng);

    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
         getPlaceDetailsByLatLang(event.latLng);


    });
    
   });

	
}

          
      }
      
      
      
      function showError(error) {
      	
	   	  if(error.code == 0){
	        // unknown error
	       
	       
	        alert(" Your Browser not supported for GeoLocation Access.")
	    //  location.reload(true);
	        }   else if(error.code == 1) {
	        // permission denied
	      alert("The acquisition of the geolocation information failed because the page didn't have the permission to do it.Please allow location information for this website.")
	 //  location.reload(true); 
	        } else if(error.code == 2) {
	        // position unavailable
	        alert("The acquisition of the geolocation failed because at least one internal source of position returned an internal error.Please try again.");
	      location.reload(true);
	        }else if(error.code == 3){
	        	
	        	alert("The time allowed to acquire the geolocation, defined by PositionOptions.timeout information was reached before the information was obtained.Please try again.");
	        	 location.reload(true);
	        }
	        
	 
	}

    </script>
   <script type="text/javascript">
   
   $("form").submit(function(){
		 
	   var schoolid='<s:property value="%{schoolsDetailsBean.SchoolId}"/>'
		   var inputval=$("#pac-input").val();
	   if(schoolid>0){
			  if(inputval==""){
				  initMap();
				 
				  return false;
			  }
	    
	   $("#form").attr("action","updateSchoolsDetailsBySuperAdmin?schoolid="+schoolid);
    this.submit();
	   }else{
		   if(inputval==""){
				  initMap();
				 
				  return false;
			  }
		   $("#form").attr("action","saveSchoolsDetails");
	       this.submit(); 
		   
	   }
    });
   </script>
  
</body>
</html>
