<!DOCTYPE html>
<%@ taglib uri="/struts-tags"  prefix="s"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../css/skins/_all-skins.min.css">

 </head>

<body class="hold-transition skin-blue sidebar-mini">

  

  
            <div class="col-md-3">
              <!-- <a href="compose.html" class="btn btn-primary btn-block margin-bottom">+ Add New Student</a> -->
			  
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Profiles</h3>
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href=""><i class="fa fa-users"></i> Admins <span class="label label-primary pull-right"><s:property value="stucount"/></span></a></li>
                    <li><a href="#"><i class="fa fa-inbox"></i> Students <span class="label label-primary pull-right"><s:property value="stucount"/></span></a></li>
                    <li><a href="#"><i class="fa fa-envelope-o"></i> Parents</a></li>
                    <li><a href="#"><i class="fa fa-file-text-o"></i>  Classes </a></li>
					<li><a href="#"><i class="fa fa-circle-o text-red"></i> Pick-up Points</a></li>
                    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Drop-Off Points</a></li>
                    </ul>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Logistics</h3>
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#"><i class="fa fa-circle-o text-red"></i> Routes</a></li>
                    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Buses</a></li>
                    <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Drivers</a></li>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
           
      

  
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="js/app.min.js"></script>

</body>
</html>
