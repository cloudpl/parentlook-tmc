<aside class="control-sidebar control-sidebar-dark"
			style="margin-top: 50px; padding: 0; height: 90vh; overfolw: hidden;">
			<div class="tab-content">

				<div class="box no-border no-shadow  flat"
					style="background: #282d32 !important">
					<div class="box-header no-border no-padding">
						<h3 class="box-title" style="color: white">
							<i class="fa fa-info-circle text-aqua margin-r-5"></i>Help
						</h3>


					</div>
					<div class="box-body no-padding"
						style="height: 80vh; overflow-y: auto; overflow-x: hidden; overflow-wrap: break-word;">


						<div class="form-group has-success">
							<label class="control-label" for="inputSuccess"><i
								class="fa fa-check"></i> Input with success</label>
							<div>
								Pace loading works automatically on page. You can still
								implement it with ajax requests by adding this js: <br>
								<div class="row pad">
									<div class="col-xs-12 text-center">
										<button type="button" class="btn btn-default btn-lrg ajax"
											title="Ajax Request">
											<i class="fa fa-spin fa-refresh"></i>&nbsp; Get External
											Content
										</button>
									</div>
								</div>
								<div class="ajax-content"></div>
							</div>
							<strong><i class="fa fa-file-text-o margin-r-5"></i>
								Notes</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Etiam fermentum enim neque.</p>
						</div>

					</div>
				</div>
			</div>
		</aside>