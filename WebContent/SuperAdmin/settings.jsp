<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<style type="text/css">
	
	#pac-input{
    height:25px;
    width:300px;
}
	
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="superadmin-header.jsp" %>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@ include file="superadmin-sidebar.jsp" %>
		</aside>
	<!-- =============================================== -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
		<div class="col-md-3">
		<div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Content Management</h3>
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="info"><i class="fa fa-inbox"></i> Info <span class="label label-primary pull-right"><s:property  value="schActive"/></span></a></li>
                    <li><a href="#"><i class="fa fa-envelope-o"></i> Help <span class="label label-primary pull-right"><s:property  value="schInActive"/></span></a></li>
                   
					</ul>
                </div><!-- /.box-body -->
              </div></div>
			 
			<section class="content">
				 

			</section> 
			<!-- /.content-->
		</div>
		<!-- /.content-wrapper -->


		<%-- <footer class="main-footer">
    <%@ include file="superadmin-footer.jsp" %>
  </footer> --%>

<%@ include file="superadmin-footer.jsp" %>



		 
		
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="js/bootstrap.min.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

 <script type="text/javascript">
 $(".active").removeClass();
 $("#setting").addClass("active");
 </script>
    
    
  
</body>
</html>
