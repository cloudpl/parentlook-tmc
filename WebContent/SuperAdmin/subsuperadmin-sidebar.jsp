<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../css/skins/_all-skins.min.css">
</head>
<body>
    
            <div class="col-md-3">
              <a href="addSchool" class="btn btn-primary btn-block margin-bottom">+ Add New School</a>
			  
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title"> School Profiles</h3>
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#"><i class="fa fa-inbox"></i> Active <span class="label label-primary pull-right"><s:property  value="schActive"/></span></a></li>
                    <li><a href="#"><i class="fa fa-envelope-o"></i> Inactive <span class="label label-primary pull-right"><s:property  value="schInActive"/></span></a></li>
                    <li><a href="#"><i class="fa fa-file-text-o"></i>  Recently Added </a></li>
					</ul>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">School Admins</h3>
                  <div class="box-tools">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
				<div class="box-body no-padding">
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="#"><i class="fa fa-circle-o text-red"></i> Active <span class="label label-primary pull-right"><s:property  value="schAdminActive"/></span></a></li>
                    <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> Inactive <span class="label label-primary pull-right"><s:property  value="schAdminInActive"/></span></a></li>
                    <li><a href="#"><i class="fa fa-circle-o text-light-blue"></i> Recently Added</a></li>
                  </ul>
                </div><!-- /.box-body -->
                              </div><!-- /.box -->
            </div><!-- /.col -->
          
     
</body>
</html>