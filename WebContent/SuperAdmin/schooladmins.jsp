<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->

  
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
 </head>
<%@ taglib uri="/struts-tags" prefix="s" %>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
 
<%@ include file="superadmin-header.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="superadmin-sidebar.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          <s:property  value= "%{#session.schoolName}"/>  
          </h1>
          
          <ol class="breadcrumb">
          
            <li><a href="adminLogin"><i class="fa fa-dashboard"></i> Home</a></li>
             <li><a href="getAdminSchoolList"><i  class="active"></i> Schools</a></li>
           <li class="active">Admin</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
           <%@ include file="subsuperadminschool-sidebar.jsp" %>
            <div class="col-md-9">
            
                  
                  <a href="addSchoolAdmin" style="width:140px;" class="btn btn-primary btn-block">+ Add New Admin</a>
                  
              <div class="box no-border no-shadow">
               
               <%--  <div class="box-header with-border">
                
                  <div class="box-tools pull-right">
                  
                    <div class="has-feedback">
                      <span><input type="text" class="form-control input-sm" placeholder="Search"></span>
                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                  </div><!-- /.box-tools -->
                </div> --%><!-- /.box-header -->
                
                <div class="box-body no-padding">
                
                
                
                <table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
										<th>AdminId</th>
											<!-- <th>SchoolId</th> -->
											<th>AdminName</th>
											<th>Email</th>
											 
											<th>PhoneNumber</th>
											 

										</tr>
									</thead>
									<tbody>
										<s:iterator value="adminImgList">
											<tr>
                                                <td><s:property value="AdminId" /></td>
												<%-- <td><s:property value="SchoolId" /></td> --%>
												 
												<td style="position: relative;"><img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{imageStr}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
													</div>
													<s:property value="AdminName"/>
													</td>
												<td><s:property value="Email" /></td>
												 <td><s:property value="PhoneNumbe" /></td>
											</tr>
										</s:iterator>
									</tbody>
									 
								</table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer no-padding">
                  
                </div>
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  <!-- Main Footer -->
   
    <%@ include file="superadmin-footer.jsp" %>
  
 	</div>
 

<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

 
<!-- Bootstrap 3.3.6 -->
 <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
	 <script src="js/bootstrap.min.js"></script>

<script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
</body>
</html>
