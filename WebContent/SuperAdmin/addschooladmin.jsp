<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="css/skins/_all-skins.min.css">


	 <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="superadmin-header.jsp" %>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@ include file="superadmin-sidebar.jsp" %>
		</aside>
	<!-- =============================================== -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h4>New School Admin for <s:property  value= "%{#session.schoolName}"/>    </h4>
				<ol class="breadcrumb">
					<li><a href="adminLogin"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="getAdminSchoolList"><i class="active"></i> School</a></li>
					<li><a href="schoolAdmins"><i class="active"></i> Admins</a></li>
					 
					<li class="active">Add New Admin</li>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default">

					<div class="box-body">
						<div class="row">
						 <%@ include file="subsuperadminschool-sidebar.jsp" %>
							<div class="col-md-6">
								<!-- form start -->
								<s:form action="savePhoto" role="form" enctype="multipart/form-data">
									<div class="box-body">
									<h4>All fields Marked <span class="text-red">*</span> Mandatory</h4>
										<div class="form-group">
											<label for="AdminName">Username <span class="text-red">*</span></label> <input
												type="text" class="form-control" id="AdminName"
												name="AdminName" placeholder="Enter Username" required>
										</div>
										
										<div class="form-group">
											<label for="AdminPassword">Password <span class="text-red">*</span></label> <input
												type="text" class="form-control" id="AdminPassword"
												name="AdminPassword" placeholder="Enter Password" required>
										</div>

										<div class="form-group">
											<label for="Email">Email <span class="text-red">*</span></label> <input type="email"
												class="form-control" id="Email" name="Email"
												placeholder="Enter email" required>
										</div>
										
										<div class="form-group">
											<label for="PhoneNumbe">Mobile Number <span class="text-red">*</span></label> <input
												type="text" pattern="[789][0-9]{9}" title="Phone Number start with 7 or 8 or 9" class="form-control" id="PhoneNumbe" maxlength="10"
												placeholder="Enter Phone Number" name="PhoneNumbe" required>
										</div>
										
										
										<div class="form-group">
											<!-- <label for="ProfilePhoto">Photo</label> <input type="file"
												 id="photo" name="SchoolPhoto"> -->
												 <label>Photo <span class="text-red">*</span></label>
												 <input type="file" name="PhotoAdmin" required/>
										</div>

										
										 
										
										 



									</div>


 



									<!-- /.box-body -->

									<div class="box-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</s:form>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.box-body -->

				</div>
				<!-- /.box -->

			</section>
			<!-- /.content-->
		</div>
		<!-- /.content-wrapper -->


		<%-- <footer class="main-footer">
    <%@ include file="superadmin-footer.jsp" %>
  </footer> --%>

<%@ include file="superadmin-footer.jsp" %>



		 
		
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="js/bootstrap.min.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

 
    
    
</body>
</html>
