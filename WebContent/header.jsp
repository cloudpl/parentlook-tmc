<%@taglib uri="/struts-tags" prefix="s" %>
<script>

$(document).ready(function()
		{ 
	
	$("#hero").hide();
	notify();
	function notify(){
		 
	var url ="schoolNotifications";
	$.ajax({
		url : url,
		type : 'POST',
		cache : false,
		success : function(data) 
		{
		//alert(JSON.stringify(data));
		var bean = data.count;
		//alert(bean);
		 $("#count").html(bean);
		 $("#nCount").html(bean);
		 var listbean=data.list;
		 var not="";
		  for(var i=0;i< listbean.length;i++){
	       		 
	       		 var inBean=listbean[i];
	       		not=not+'<div style="float:left;"><a href="#"> <i class="fa fa-bell text-aqua"></i>'+inBean.description.substring(0,25)+'</a></div>';
		 
		  }
		  $("#dynamicdata").html(not);
		},
		error : function(data) {
			alert("error");
		}
	});
	
	}setInterval(notify,5000);
	
});


</script>
<header class="main-header">
	<a href="getbuseslistbyId" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><img src="img/logo.png" id="landing" style="width:50px;"/></span> <!-- logo for regular state and mobile devices -->
		<span class="logo-lg text-black"><img src="img/logo.png" id="landing" style="width:50px;"/><b>Parent</b>Look</span>
	</a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button" > <span class="sr-only margin-r-5">Toggle navigation</span></a>
		
		<span class="navbar-left text-white" style=" font-size:18px;padding:10px;"><span class="hidden-xs"><s:property  value="%{#session.schoolName}"/></span></span>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
					

				<li class="dropdown notifications-menu">
					<!-- Menu toggle button --> <a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <span
						class="label label-warning"><span id="nCount"></span></span>
				</a>
					<ul class="dropdown-menu">
						<li class="header">You have <span id="count"></span> notifications</li>
						<li>
							<ul class="menu">
								<li>
								<div id="dynamicdata"></div>
									 
								</li>
							</ul>
						</li>
						<li class="footer"><a href="schoolNotificationList">View all</a></li>
					</ul>
				</li>
				

				<li class="dropdown user user-menu">
					<!-- Menu Toggle Button --> <a href="#" class="dropdown-toggle"
					style="padding: 7px 10px;" data-toggle="dropdown"> 
                 <s:property value="%{#session.adminName}"/>
						
						
						
						<img
													onerror="imgError1(this);" class="maxyId"
													src="data:image/png;base64,<s:property value='%{#session.image}'/>"
													style="height: 36px; width: 36px; padding:2px; border-radius: 100%;" />
													
													
												

				</a>
					<ul class="dropdown-menu">


						<li class="user-footer">
							<div class="pull-left">
								<a href="schoolProfileAction" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="logout" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>

			</ul>
		</div>
	</nav>
</header>