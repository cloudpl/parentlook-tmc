<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="js/bootstrap.min.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	 <script type="text/javascript">
	 $(document).ready(function(){
   	  $(".active").removeClass();
   	  $("#profiles").addClass("active");
   	  $("#classes").addClass("active");
   	var classList = "<s:property value='classeslist.size'/>";
   if(classList>0){
	   $("#excel").show();
	 //  $("#pdf").show();
	   
   }else{
	   $("#excel").hide();
	 //  $("#pdf").hide();
   }
   $(".del").click(function(){
   var x = confirm("Are you sure you want to delete ?");
	  
	  if(x==true){
		  
		  return true;
	  }else{
		  
		  return false;
	  }
   });
 
     });
	 </script>
	 <style type="text/css">
	 
	#fabutton {
  background: none;
  padding: 0px;
  border: none;
}
	 </style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>

<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<!-- =============================================== -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="text-right" >
				<span><s:a action="addClass" cssClass="bg-blue pad margin-r-5">+AddNewClass</s:a></span>
				<span><a href="exportClassExcel" id="excel" class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
				<%-- <span><a href="exportClassPdf" id="pdf"  class="btn no-padding"><i class="fa fa-file-pdf-o fa-2x text-red"></i></a></span> --%>
			</div>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow">
							
							<!-- /.box-header -->
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
										<th>Id</th>
											<!-- <th>School Id</th> -->
											<th>Class</th>
											<th>Section</th>
											<th>Description</th>
											<th>Actions</th>

										</tr>
									</thead>
									<tbody>
								<s:hidden id="classlist" value="classeslist.size"/>
										<s:iterator value="classeslist">
											<s:if test="%{RecordStatus.equals('ACTIVE')}">
											<tr>

												<td><s:property value="ClassId" /></td>
												<%-- <td><s:property value="SchoolId" /></td> --%>
												<td><s:property value="StudentClass" /></td>
												<td><s:property value="Section" /></td>
												<td><s:property value="Description" /></td>
											
												
												<td>
            
           <div class="col-md-2 no-padding"> <s:form action="viewclass" title="View Class"><s:hidden name="classid" value="%{ClassId}"></s:hidden><button type="submit" id="fabutton"><i class="fa fa-eye margin-r-5"></i></button>
             
			 </s:form></div>
           <div class="col-md-2 no-padding">  <s:form action="editclass" title="Edit Class"><s:hidden name="classid" value="%{ClassId}"></s:hidden> <button type="submit" id="fabutton"> <i class="fa fa-pencil text-blue margin-r-5"></i></button>
            
			 </s:form>
            </div>
            
            <s:if test="%{!usedStatus.equals('ACTIVE')}">
           
           <div class="col-md-2 no-padding">  <s:form action="deleteclass" title="Delete Class"><s:hidden name="classid" value="%{ClassId}"></s:hidden><s:submit type="button" cssClass="del" id="fabutton"><i class="fa fa-trash text-red"></i></s:submit>
             
			 </s:form></div>
            
                                                </s:if> </td>
											</tr></s:if>
										</s:iterator>
										
									</tbody>
									
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->

			</section>
			<!-- /.content-->
		</div>
		<!-- /.content-wrapper -->





		 
	</div>
	<!-- ./wrapper -->

	 

	<!-- page script -->
	<script>
      $(function () {
    	  $('#example1').DataTable();
        /*$('#example2').DataTable({
            
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });*/
      });
     
    </script>
</body>
</html>
