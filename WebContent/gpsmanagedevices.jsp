<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="js/bootstrap.min.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<script type="text/javascript">
	 $(document).ready(function(){
		 $("#popup").hide();
   	  $(".active").removeClass();
   	  $("#logistics").addClass("active");
   	  $("#buses").addClass("active");
   	  var buslist= "<s:property value='gpsTrackingWebBeans.size'/>";
   	 
   	 if(buslist>0){
  	   $("#excel").show();
  	   //$("#pdf").show();
  	   
     }else{
  	   $("#excel").hide();
  	//   $("#pdf").hide();
     }
   $(".del").click(function(){
	   var x = confirm("Are you sure you want to delete ?");
		  
		  if(x==true){
			  
			  return true;
		  }else{
			  
			  return false;
		  }
   });
   
  
  
	 });  
	 
	</script>
	 	<script type="text/javascript">
	 	
	 	 
		 
		 $(document).ready(function(){
			var busId=""; 
			$(".cc").click(function(){
				var busid=$(this).attr("id");
				busId=$(this).attr("id");
				$("#reset").hide();
				 $("#reset").click();
			    $("#errMsg").parent().next(".validation").remove();
				 $("#popup").click();
				
			}) ;
			
			
			
			$("#save").click(function(e){
				e.preventDefault();
				
				   var pwd=$("#pwd").val();
				    var confirmpwd=$("#cpwd").val();
				    if(pwd==confirmpwd){
				    	$.ajax({
					    	 url:"gpsBusPwdUpdate?busId="+busId+"&password="+pwd+"&confirmpwd="+confirmpwd, 
							   catche:false,
							   
					            success: function(data)
						           {
					            	 
						        	   location.reload(); 
						           }
				 }); 
				     }
				    else{
				    	$("#errMsg").parent().next(".validation").remove();
				    	$("#errMsg").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Password doesn't match</div>");
				    	return false;
				    } 
				    
					 
			   });
		 });
	 	</script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="text-right">
				<span><s:a action="getBusesList" cssClass="bg-blue pad margin-r-5" >ViewBuses</s:a></span>
				<span><s:a action="addbus" cssClass="bg-orange pad margin-r-5">+AddNewBus</s:a></span>
				 
				<span><a href="exportBusesExcelForGps" id="excel" class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
				<%-- <span><a href="exportBusesPdf" id="pdf" class="btn no-padding"><i class="fa fa-file-pdf-o fa-2x text-red"></i></a></span> --%>
			</div>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow">
							<!-- /.box-header -->
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>BusId</th>
											<th>BusName</th>
											<th>UserName</th>
											<th>Password</th>
											<th>LoginStatus</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
									
										<s:iterator value="gpsTrackingWebBeans">
										 
											<tr>

												<td><s:property value="BusId" /></td>
												<td><s:property value="busName" /></td> 
												<td><s:property value="userName" /></td> 
												<td><s:property value="password" /></td>
												<td><s:property value="loginstatus" /></td>
												 
													<td>
													 <div class="col-md-4 no-padding">
													<div class="col-md-4 no-padding"> <input type="submit" value="Reset Password" class="cc" id='<s:property value="%{BusId}"/>'></div>
    													</div>	 				
									         
  
                                                     </td>
										

											</tr> 
										</s:iterator>
									</tbody>
									
								</table>
								 <button type="button" id="popup" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"></button>
    				<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         
      </div>
      <div class="modal-body">
      
         <s:form>
          
       
       <s:property value="%{BusId}"/>
         
         <div class="form-group">
		<label  for="password">New Password<span class="text-red">*</span></label> <input
		type="password" name="password" class="form-control" style="width:300px;" placeholder="Enter New Password"
						id="pwd" required>
													</div>
													
         <div class="form-group">
		<label  for="confirmpwd">Confirm Password<span class="text-red">*</span></label> <input
		type="password" name="confirmpwd" class="form-control" style="width:300px;" placeholder="Enter Confirm Password"
						id="cpwd" required>  <div id="errMsg"></div>
													</div>
        <div class="box-footer">
										<input type="button" class="btn btn-primary" id="save" value="Save" />
										<input  id="reset"  class="btn btn-primary" type="reset" value="Reset">
									</div>
         </s:form>
         
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
								
							</div>
						</div>
					</div>
				</div>
</div></div></div>
			</section>
		</div>

	</div>

	

	<!-- page script -->
	<script>
      $(function () {
    	  $('#example1').DataTable();
    
      });
     
    </script>
</body>
</html>
