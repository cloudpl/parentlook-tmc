<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	


</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp"%>


		 


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			
			<section class="content">
				<div class="box box-default no-border no-shadow">
					<div class="box-header with-border">
						<h4 class="text-orange text-center text-bold">Manage Profile</h4>
						</div>
					<div class="box-body">
						<div class="row">
						<div class="col-md-3"></div>
							<div class="col-md-6">
								<s:form action="updateProfileAction" role="form"  theme="simple" enctype="multipart/form-data" method="post">
									<div class="box-body">
										<div class="form-group">
											<label>SchoolName: </label>
											<div class="form-inline">
											<s:textfield cssClass="form-control" cssStyle="width:400px;" name="SchoolName"  value="%{schoolBean.SchoolName}"> </s:textfield>
										</div></div>
											<div class="form-group">
											<label>Image </label>
											<img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{#session.image}'/>"
													style="height: 100px; width: 100px; margin-top: 0px; border-radius: 100%;" title="Choose Image To Update" id="chschimg"/>
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
										 	<s:file name="Photo2" id="schimg" accept="image/*" onchange="handleFileSelect(event,schimg)"></s:file> 
											</div>
										
										
									</div>

									<div class="box-footer">
									
									<s:submit value="Update"></s:submit>
								<s:submit action="getbuseslistbyId" class="btn btn-primary" value="Cancel"></s:submit>
					
							
										
									</div>
									
									
								</s:form>
								
						</div>
					</div>
				</div>
		</div>

		</section>
	</div>






	 
	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>

	<script>
		$(function() {
			$('#example1').DataTable();
			
		});
		
		
		$(document).ready(function(){  
			
		 	$("#schimg").hide();  
			$("#chschimg").click(function(){ 
				$("#schimg").click();
				
			});
		});
	</script>
	<script type="text/javascript">
function handleFileSelect(evt,id) {
      
         
       
         
  var files = evt.target.files;
  for ( var i = 0, f; f = files[i]; i++) {

   var reader = new FileReader();
   //while reading call function with file name that function gives stream and create a spam and set result to our main image
   reader.onload = (function(theFile) {
    return function(e) {

     $("#chschimg").attr("src", e.target.result);
    };
   })(f);
   reader.readAsDataURL(f);

  }

 }

</script>
</body>
</html>
