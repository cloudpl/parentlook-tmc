<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@taglib prefix="sx"  uri="/struts-dojo-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"></link>
 <link rel="stylesheet" href="/resources/demos/style.css"></link>
 
	<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	 
	  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header"></section>
			<section class="content">
			<div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#attendance" id="attendance1" data-toggle="tab">Attendance</a></li>
                  
                  <li><a href="#absenteesonboard" id="absenteesonboard1" data-toggle="tab">Absentees On-Board</a></li>
                  <li><a href="#absenteesdeboard" id="absenteesdeboard1" data-toggle="tab">Absentees De-Board</a></li>
                </ul>
                <div class="pull-right text-orange  margin-r-5" style="margin-top:-35px;">
                <i  class="fa fa-calendar margin-r-5"></i><input type="text" class="form-inline" style="border: 1px solid #e5e5e5;padding:2px 5px;" id="regdate" name="RegDate" value="<s:property  value="%{#session.todaydate}"/>">                
			     <span>
			   
			     <s:a href="exportAttendenceExcel" id="excel" class="btn no-padding" >
			     <span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i>
			     
			     </s:a>
			      </span>
 					<span>
 				<%-- 	<s:a href="exportAttendencePdf" id="pdf" class="btn no-padding" >
 					<i class="fa fa-file-pdf-o fa-2x text-red"></i><!-- </a> -->
 					</s:a> --%>
 					</span>
			 
                </div>
                <div class="tab-content">
                  <div class="active tab-pane" id="attendance">
                  <div class="box no-border no-shadow">
							
							<!-- /.box-header -->
							<div class="box-body">
								<table id="total-table" class="table table-bordered table-striped">
									<thead>
										<tr>
										<th>Bus</th>
											<th>Route</th>
										    <th>TotalStudents</th>
											<th>On-Boarded</th>
											<th>Absentees</th>
											<th>De-Boarded</th>
											<th>Absentees</th>
										</tr>
									</thead>
									<tbody id="total-data">
									
											<s:iterator value="busesattendencelist">
									<tr>
										<td><s:property value="busName"></s:property></td>
							              <td><s:property value="routename"/></td>
								         <td><s:property value="TotalStudents"/></td>
										<td><s:property value="onboard"/></td>
										<td><s:property value="onboardAbsent"/></td>
                                          <td><s:property value="deBoard"/></td>   
                                          <td><s:property value="deBoardAbsent"/>     								
									</tr>
			
								</s:iterator>
									</tbody>
									<tfoot>
										
									</tfoot>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
                  
                  </div>
                  <div class="tab-pane" id="absenteesonboard">
                  <div class="box no-border no-shadow">
							
							<!-- /.box-header -->
							<div class="box-body">
								<table id="onboard-table" class="table table-bordered table-striped">
									<thead>
										<tr>
										<th>Student Name</th>
										    <th>Class</th>
											<th>Roll No</th>
											<th>Bus Name </th>
											<th>Route Name </th>
										</tr>
									</thead>
									<tbody id="onboard-data">
										<s:iterator value="absentstudentlist">
									<tr>
										<td><s:property value="name"></s:property></td>
							              <td><s:property value="studclass"/></td>
								         <td><s:property value="rollno"/></td>
								         <td><s:property value="busName"/></td>
										<td><s:property value="routename"/></td>
										
									</tr>
			
								</s:iterator>
									</tbody>
									<tfoot>
										
									</tfoot>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
                  </div>
                  <div class="tab-pane" id="absenteesdeboard">
                  <div class="box no-border no-shadow">
							
							<!-- /.box-header -->
							<div class="box-body">
							
								<table id="deboard-table" class="table table-bordered table-striped">
									<thead>
										<tr>
										<th>Student Name</th>
										    <th>Class</th>
											<th>Roll No</th>
											<th>Bus Name </th>
											<th>Route Name </th>
										</tr>
									</thead>
									<tbody id="deboard-data">
										<s:iterator value="deboardedlist">
									<tr>
										<td><s:property value="name"></s:property></td>
							              <td><s:property value="studclass"/></td>
								         <td><s:property value="rollno"/></td>
								           <td><s:property value="busName"/></td>
										<td><s:property value="routename"/></td>
										
									</tr>
			
								</s:iterator>
									</tbody>
									<tfoot>
										
									</tfoot>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						 <input type="hidden" id="tabinfo"/>
						<!-- /.box -->
                  </div>
			</div>
			</div>
			</div>
			
				</section>
				
			
	
			</div>
	</div>
	
	
	
	

    	
	 
	 
	 
	<script type="text/javascript">
	$(document).ready(function(){
		$("#tabinfo").attr("value","1");
		$("body").delegate("#excel","click",function(){
			var date=$("#regdate").val();
			$("#excel").attr("href","exportAttendenceExcel?date="+date);
			$("#tabinfo").attr("value","1");
			
		}); 
		var busesattendencelist = $("#busesattendencelistId").val();
		
		    if(busesattendencelist>0){
			   $("#excel").show();
			 // $("#pdf").show();
			   
		   }else{
			   $("#excel").hide();
		   } 
		   $("#attendance1").click(function(){
				 var busesattendencelist =  $("#busesattendencelistId").val();
					   if(busesattendencelist>0){
						   $("#excel").show();
						  $("body").delegate("#excel","click",function(){
							   var date=$("#regdate").val();
						   $("#excel").attr("href","exportAttendenceExcel?date="+date);
							$("#tabinfo").attr("value","1");
						  });
					   }else{
						   $("#excel").hide();
					   }
			 });
			 
	 $("#absenteesonboard1").click(function(){
	 var absentstudentlist =  $("#absentstudentlistId").val();
	 
	 
	   if(absentstudentlist>0){
		   $("#excel").show();
		   $("body").delegate("#excel","click",function(){
				var date=$("#regdate").val();
				$("#excel").attr("href","exportattendenceonboard?date="+date);
				$("#tabinfo").attr("value","2");
				});
	   }else{
		   $("#excel").hide();
	   } 
	});				
 	$("#absenteesdeboard1").click(function(){ 
	 var deboardedlist =  $("#deboardedlistId").val();
	 $("#tabinfo").attr("value","3");
	
	    if(deboardedlist>0){
		   $("#excel").show();
		   $("body").delegate("#excel","click",function(){
				var date=$("#regdate").val();
				$("#excel").attr("href","exportattendencedeboard?date="+date);
				
				});
	   }else{
		   $("#excel").hide();
	   } 
	});
 
 
	});
	</script>
	
	
<%-- 	<script type="text/javascript">
	 $(document).ready(function(){
		 //var busesattendencelist = "<s:property value='busesattendencelist.size'/>";
	 var busesattendencelist = $("#busesattendencelistId").val();
	 
		 if(busesattendencelist>0){
			   $("#pdf").show();
		   }else{
			   $("#pdf").hide();
		   } 
		
 $("body").delegate("#pdf","click",function(){
	var date=$("#regdate").val();
	//alert("pdf")
	$("#pdf").attr("href","exportAttendencePdf?date="+date);
	//alert("kkk")
	$("#tabinfo").attr("value","1");
});    		

$("#attendance1").click(function(){
	 //var busesattendencelist = "<s:property value='busesattendencelist.size'/>";
	 var busesattendencelist = $("#busesattendencelistId").val();
	 $("#tabinfo").attr("value","1");
	 if(busesattendencelist>0){
		   $("#pdf").show();
		   $("body").delegate("#pdf","click",function(){
			   var date=$("#regdate").val();
		$("#pdf").attr("href","exportAttendencePdf?date="+date);
		
		});
	   }else{
		   $("#pdf").hide();
	   } 

});
	 $("#absenteesdeboard1").click(function(){ 
		 //var deboardedlist = "<s:property value='deboardedlist.size'/>";
		  var deboardedlist =  $("#deboardedlistId").val();
		   if(deboardedlist>0){
			   $("#pdf").show();
			   $("body").delegate("#pdf","click",function(e){
					var date=$("#regdate").val();
					$("#pdf").attr("href","exportattendencedeboardPdf?date="+date);
					$("#tabinfo").attr("value","3");
					});
		   }else{
			   $("#pdf").hide();
		   }  
			});	
	 
	 $("#absenteesonboard1").click(function(){ 
		 $("#tabinfo").attr("value","2");
		 //var absentstudentlist = "<s:property value='absentstudentlist.size'/>";
	 var absentstudentlist =  $("#absentstudentlistId").val();
	 
		   if(absentstudentlist>0){
			//   alert("n jn jhnj nj")
			   $("#pdf").show();
			   $("body").delegate("#pdf","click",function(e){
					var date=$("#regdate").val();
					
				$("#pdf").attr("href","exportattendenceonboardPdf?date="+date);
				
				});
		   }else{
			 
			   $("#pdf").hide();
		   } 
			});		
		
	});
	</script>

 --%> 	
	<script>
      $(function () {
    	  $('#total-table').DataTable();
    	  $('#onboard-table').DataTable();
    	  $('#deboard-table').DataTable();
    	 
       
      });
      $(document).ready(function(){
    	 $(".sidebar-menu").find(".active").removeClass();
    	  $("#time").addClass("active");
    	  $("#attendance").addClass("active");
    	  $("#regdate").datepicker({ dateFormat: 'yy-mm-dd' });
    	 
  });
      </script>
      <script type="text/javascript">
      $(document).ready(function()
    {
    	    	 
     $("#regdate").change(function(){
    	 var date=$("#regdate").val();
    	 
    	 $("#temDate").attr("value",date);
    	
   		 	getAttendencelist(date);
   		
   	}); 
    });
      </script>
      
      <script>
      function getAttendencelist(date)
      {
      		  
      	     $.ajax
      	     ({
      				
      		  url:"getAttendenceListbyDate?changedDate="+date,
      		  type:"post",
      		   catche:true,
                 success: function(data)
      	           {
              		$('#total-data').html("");
              		$('#onboard-data').html("");
              		$('#deboard-data').html("");
              		
              		
      		     var attendencelist=data.busesattendencelist;
      		     var onboardedlist=data.absentstudentlist;
      		     var deboardedlist=data.deboardedlist;
      		   var tabinfo=$("#tabinfo").val();
      		   if(tabinfo==1){
      			 if(attendencelist.length>0){
      			 // $("#pdf").show();
      			 $("#excel").show();
      			 /*   $("body").delegate("#pdf","click",function(e){
      					var date=$("#regdate").val();
      					$("#pdf").attr("href","exportAttendencePdf?date="+date);
      					
      					
      					});  */
      			 $("body").delegate("#excel","click",function(){
       				var date=$("#regdate").val();
       				$("#excel").attr("href","exportAttendenceExcel?date="+date);
       				
       				});
      		   }else{
      			   //$("#pdf").hide();
      			   $("#excel").hide();
      		   }  
      			 
      			 
      			       			 
      			 
      		   }else if(tabinfo==2){
      			 if(onboardedlist.length>0){
      		//	$("#pdf").show();
         			 $("#excel").show();
      			 /*   $("body").delegate("#pdf","click",function(e){
      					var date=$("#regdate").val();
      					$("#pdf").attr("href","exportattendenceonboardPdf?date="+date);
      					
      					});  */
      			 $("body").delegate("#excel","click",function(){
        				var date=$("#regdate").val();
        				$("#excel").attr("href","exportattendenceonboard?date="+date);
        				
        				});
      		   }else{
      			// $("#pdf").hide(); 
      			 $("#excel").hide();
      		   } 
      			   
      		   }else{
      			   
      			 if(deboardedlist.length>0){
      		//	$("#pdf").show();
         			 $("#excel").show();
      			/*  $("body").delegate("#pdf","click",function(e){
      					var date=$("#regdate").val();
      					$("#pdf").attr("href","exportattendencedeboardPdf?date="+date);
      					
      					}); */ 
      			 $("body").delegate("#excel","click",function(){
        				var date=$("#regdate").val();
        				$("#excel").attr("href","exportattendencedeboard?date="+date);
        				
        				});
      		   }else{
      			// $("#pdf").hide();
      			 $("#excel").hide();
      		   } 
      		   }
      		   
      		     
      		     
      		     $("#busesattendencelistId").val(attendencelist.length);
      		   
      		     $("#absentstudentlistId").val(onboardedlist.length);
      		     $("#deboardedlistId").val(deboardedlist.length);
      		   
                      var st;
                      
                     	for(var i=0;i<attendencelist.length;i++)
                		{
                        var attendence=attendencelist[i];
                        st+="<tr><td>"+attendence.busName +"</td>"+
                             "<td>"+attendence.routename+"</td>"+
                            "<td>"+attendence.totalStudents+"</td>"+
                             "<td>"+attendence.onboard+"</td>"+
                             "<td>"+attendence.onboardAbsent+"</td>"+
                             "<td>"+attendence.deBoard+"</td>"+
                             "<td>"+attendence.deBoardAbsent+"</td></tr>";
      	         	  }
	                     	
                     	
                     	var str1="";
                        
                    	for(var i=0;i<onboardedlist.length;i++)
                		{
                        var onboardattendence=onboardedlist[i];
                        str1=str1+"<tr><td>"+onboardattendence.name +"</td>"+
                             "<td>"+onboardattendence.studclass+"</td>"+
                            "<td>"+onboardattendence.rollno+"</td>"+
                             "<td>"+onboardattendence.busName+"</td>"+
                             "<td>"+onboardattendence.routename+"</td></tr>";
                             } 
                    	var st2="";
                    
                    	for(var i=0;i<deboardedlist.length;i++)
                		{
                        var deboardattendence=deboardedlist[i];
                        st2=st2+"<tr><td>"+deboardattendence.name +"</td>"+
                        "<td>"+deboardattendence.studclass+"</td>"+
                       "<td>"+deboardattendence.rollno+"</td>"+
                        "<td>"+deboardattendence.busName+"</td>"+
                        "<td>"+deboardattendence.routename+"</td></tr>";
      	           		}
                	
                    	
                        	
                    	/*$('#total-data').append(st);
                        	 $('#onboard-data').append(str1);
                        	$('#deboard-data').append(st2); */
                        	
                        	if ( $.fn.DataTable.isDataTable( '#total-table' ) ) {
                      			$('#total-table').DataTable().destroy();
                      			$('#total-data').html(st);
                      			$("#total-table").DataTable();
                      			};
                        	
                        	if ( $.fn.DataTable.isDataTable( '#onboard-table' ) ) {
                      			$('#onboard-table').DataTable().destroy();
                      			$('#onboard-data').html(str1);
                      			$("#onboard-table").DataTable();
                      			};
                      			
                      			if ( $.fn.DataTable.isDataTable( '#deboard-table' ) ) {
                          			$('#deboard-table').DataTable().destroy();
                          			$('#deboard-data').html(st2);
                          			$("#deboard-table").DataTable();
                          			};
                          			
                          			
                          			
                      			
      	           },error:function(e){
      	        	   alert(e);
      	           }
      	     });
      }
      
	
      </script>
   <input type="hidden" id="temDate" name="temDate"/>
   
    <s:hidden value="%{busesattendencelist.size}" id="busesattendencelistId"/>
	 <s:hidden value="%{absentstudentlist.size}" id="absentstudentlistId"/>
	 <s:hidden value="%{deboardedlist.size}" id="deboardedlistId"/>
	 
</body>
</html>
