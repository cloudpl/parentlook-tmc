<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	 <script type="text/javascript">
	 $(document).ready(function(){
   	  $(".active").removeClass();
   	  $("#stop").addClass("active");
   	  $("#pending").addClass("active");
  	var pendingApproveList = "<s:property value='viewbean.size'/>";
  	
  	   if(pendingApproveList>0){
  		   $("#excel").show();
  		 //  $("#pdf").show();
  		   
  	   }else{
  		   $("#excel").hide();
  		  // $("#pdf").hide();
  	   }
   	
   	  
     });
	 </script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

				<div class="box no-border no-shadow">
					<div class="box-header with-border">
							<div class=" col-md-8 pull-left">
						<h4 class="text-orange text-right text-bold">Pending Approvals</h4></div>
						<div class="col-md-4 pull-right text-right">
					<span><a href="exportPendingApprovalsExcel" id="excel" class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
				<%-- 	<span><a href="exportPendingApprovalsPdf" id="pdf" class="btn no-padding"><i class="fa fa-file-pdf-o fa-2x text-red"></i></a></span> --%> 
					</div>
					</div>
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Name</th>
											<th>Class</th>
											<th>Address</th>
											<th>PickupLocation</th>
											<th>DropLocation</th>
											



										</tr>
									</thead>
									<tbody>
										<s:iterator value="viewbean">
											<tr>

												<td><s:property value="StudentName" /></td>
												<td><s:property value="StudentClass" /></td>
												
												<td><s:property value="Location" /></td>
												<s:if test="%{PickupPointId==0}">
												<td>
													<s:form action="approveStudent">
													
													<s:hidden name="StudentId" value="%{StudentId}"></s:hidden>
													<s:hidden name="BusId" value="%{BusId}"></s:hidden>
													<s:hidden name="SchoolId"  value="%{#session.schoolId}"></s:hidden>
													<s:submit value="Set PickUp Point"></s:submit>
													
													
													
													</s:form>
													
													
													
													</td>
												
												</s:if>
												<s:else>
												<td><s:property value="PickupPoint" /></td>
												
												</s:else>
												
												<s:if test="%{DropPointId==0}">
												<td> 
													 <s:form action="approveStudentDrop">
													 
													<s:hidden name="StudentId" value="%{StudentId}"></s:hidden>
													<s:hidden name="BusId" value="%{BusId}"></s:hidden>
													<s:hidden name="SchoolId"  value="%{#session.schoolId}"></s:hidden>
													<s:submit value="Set Drop Point"></s:submit>
													</s:form> 
													
													</td>
												
												</s:if>
												<s:else>
												<td><s:property value="DropPoint" /></td>
												
												</s:else>

												



											</tr>
										</s:iterator>
									</tbody>
									
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>

	</div>

	 
		<script>
      $(function () {
    	  $('#example1').DataTable();
    	  
       
      });
     
    </script>

	
	
</body>
</html>
