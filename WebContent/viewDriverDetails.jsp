
 <%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
    	
    	$(".active").removeClass();
    	  $("#logistics").addClass("active");
    	  $("#drivers").addClass("active");
      	
    $(".maxyId").hover(function(){
    		
    		$("#myImage").attr("src",this.src);
    		$("#imMax").css("display","block");
    	
    	});
    	
    	$(".maxyId").mouseleave(function(){
    		    	
    		$("#imMax").css("display","none");

    	
    	}); 
    	
  
    	
    });
    
    
	function imgError1(image) {
		
		
		image.onerror = "";
		image.src = "img/default_profile.png";
		return true;
	}
	

</script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>


		

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content-header">
				<h4></h4>
				
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getDriversList">Drivers</s:a></li>
					<li class="active">Edit Driver</li>
				</ol>
			</section>
			<section class="content">
				<div class="row">
					 <div class="col-md-6">

						 
							<div class="box-header">
							
							<div class="box-body">
							 <dl class="dl-horizontal">
							<dt> Driver Id: </dt><dd><s:property value="%{driversbean.DriverId}" /> </dd>
								<dt> First Name:</dt><dd> <s:property value="%{driversbean.FirstName}" /></dd> 
							    <dt>Last Name:</dt><dd> <s:property value="%{driversbean.LastName}" /> </dd>
							   <dt> ContactNumber:</dt><dd> <s:property value="%{driversbean.ContactNumber}" /> </dd>
							  <dt>  Reference:</dt><dd><s:property value="%{driversbean.RelationTitle}" /> <s:property value="%{driversbean.RelativeName}"/></dd>
							  <dt> Age: </dt><dd><s:property value="%{driversbean.Age}" /> 	</dd>										
							  <div  style="position: relative;" id="img">
							 <dt> Image: </dt><dd> <img  class="maxyId"
													src="data:image/png;base64,<s:property value='%{imageStr}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
									 </div></dd>
							 <div  style="position: relative;" id="img">  <dt>Licence Number:</dt><dd><s:property value="%{driversbean.LicenceNumber}" /> <img onerror="imgError1(this);" class="maxyId"
													src="data:image/png;base64,<s:property value='%{licenceStr}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" /></div></dd>
							  <div  style="position: relative;" id="img"><dt>  ID Type:</dt><dd><s:property value="%{driversbean.IdType}" /> <img onerror="imgError1(this);" class="maxyId"
													src="data:image/png;base64,<s:property value='%{idStr}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" /></div></dd>
							  <dt>CurrentAddress:</dt><dd><s:property value="%{driversbean.CurrentAddress}" /></dd>
							   
							   
							   
							   
							  
							   
							 
						</div>
							<!-- /.box-body -->
							<div class="box-footer">
										
						<s:form action="editdriver">
			<input type="hidden" name="driverid" value="<s:property value='%{driversbean.DriverId}'/>"/>
				   <input type="submit" value="Edit" />
							   </s:form>
							   </div>
						</div>
					</div>
					</dl>
				</div></div></div></div>

			</section>
		</div>


	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<!-- page script -->
	<script>
      $(function () {
    	  $('#example1').DataTable();
      
      });
      $(document).ready(function(){  
			$(".active").removeClass();
		  	  $("#logistics").addClass("active");
		  	  $("#drivers").addClass("active");
      });
    </script>
</body>
</html>
 