<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script> 
	
	<style type="text/css">
	.img-responsive {
     
     position: absolute;
    left: 70%;
    top: 70%;
    background-color: white;
    z-index: 150;

    height: 100px;
    margin-top: -200px;

    width: 200px;
    margin-left: -180px;
 }
	
	</style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		 
		
		<div class="content-wrapper">
			<section class="content-header margin">
			<s:if test="%{routebean.RouteId>0}">
				
				<ol class="breadcrumb">
				<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getRoutesList">Routes</s:a></li>
						<li class="active">Edit Route</li>
				</ol></s:if>
				<s:else><h4></h4>
				<ol class="breadcrumb">
				<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getRoutesList">Routes</s:a></li>
						<li class="active">Add New Route</li>
				</ol></s:else>
			</section>
			<section class="content">
				<div class="box box-default no-shadow no-border flat">
				 

 <div class="pad">All Fields Marked<span class="text-red">* </span>Mandatory</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
								<s:form  role="form" id="form">
									<div class="box-body">
										 <s:hidden name="schoolId" value="%{#session.schoolId}"></s:hidden>
										 <div class="form-group">
											<label for="RouteName">RouteName<span class="text-red">*</span></label> <input type="text"
												class="form-control" id="RouteName" name="RouteName" value='<s:property value="%{routebean.RouteName}"/>'
												placeholder="RouteName" required>
										</div>
											
										<div class="form-group">
											<label for="Email">Start Point<span class="text-red">*</span></label> <input type="text"
												class="form-control" id="StartPoint" name="StartPoint" value='<s:property value="%{routebean.StartPoint}"/>'
												placeholder="Enter Start Point" required>
										</div>

										<div class="form-group">
											<label for="ContactNumber">End Point<span class="text-red">*</span></label> <input
												type="text" class="form-control" id="EndPoint" value='<s:property value="%{routebean.EndPoint}"/>'
												placeholder="Enter End Point" name="EndPoint" required>
										</div>

										 



									</div>







									
									<div class="box-footer">
									<s:hidden name="RouteId" id="routeid" label="RouteId" value="%{routebean.RouteId}"></s:hidden>
									<s:if test="%{routebean.RouteId>0}">
									
										<s:submit value="Update"  class="btn btn-primary"></s:submit>
										</s:if>
										<s:else>
										<s:submit value="Save"  class="btn btn-primary"></s:submit>
										</s:else>
									</div>
									<div id="loading">
    <img src="img/pro.png"  class="img-responsive img-center"/>
</div>
								</s:form>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>

	</div>

	


<script type="text/javascript">
$(document).ready(function(){
	 $("#loading").hide();
	 $(".active").removeClass();
	  $("#logistics").addClass("active");
	  $("#routes").addClass("active");
	  $('#form').on('submit',function(e) {
		  $("#loading").hide();
		  $(this).attr("disabled", "disabled");
	        doWork();
			e.preventDefault();
			 var RouteName=$("#RouteName").val();
			var routeid=$("#routeid").val();
			
			 
				 
					
					  $.ajax({
					    	 url:"checkroutename?routename="+RouteName,
					        type: 'POST',
					       
					        context: this
					    }).done( function(data) {
					        	
					            if (data.msg== 'Exists') {
					            	 $("#loading").hide();
					            var bean=data.routebean;
					            if(bean.routeId==routeid){
					            	$("#RouteName").parent().next(".validation").remove();
					            	$("#form").attr("action","updateroute");
									this.submit();
					          
					            }else{
					            	
					            	$("#RouteName").parent().next(".validation").remove();
						                $("#RouteName").parent().after("<div class='validation text-right' style='color:red;margin-bottom: 20px;'>RouteName already Exists</div>");
					               
					            }

					            	
					            }
					            else {
					            	if(routeid>0){
					            	$("#RouteName").parent().next(".validation").remove();
					            	$("#form").attr("action","updateroute");
									this.submit();
					            	}else{
									$("#form").attr("action","saveRouteDetails");
					                this.submit();
					            	}
					            } 
					        });
				
			
			 
	 
		

	});
	
});
function doWork() {
	 $("#loading").show();
    setTimeout('$("#btn").removeAttr("disabled")', 1500);}

</script>
	<script>
      $(function () {
    	  $('#example1').DataTable();
        
      });
     
    </script>
</body>
</html>
