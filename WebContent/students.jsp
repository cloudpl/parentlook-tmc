<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	
<script type="text/javascript">
$(document).ready(function(){
	 

	
	
		
		
   	  $(".active").removeClass();
   	  $("#profiles").addClass("active");
   	  $("#students").addClass("active");
   	var studentList= "<s:property value='dummyList.size'/>";
	 
	 if(studentList>0){
	   $("#excel").show();
	 //  $("#pdf").show();
	   
 }else{
	   $("#excel").hide();
	  // $("#pdf").hide();
 }
	 
	  $(".del").click(function(){
		  var x = confirm("Are you sure you want to delete ?");
		  
		  if(x==true){
			  
			  return true;
		  }else{
			  
			  return false;
		  }
	  });
}); 
</script>
<script type="text/javascript">


</script>
	
	<style type="text/css">
	
	#fabutton {
  background: none;
  padding: 0px;
  border: none;
}

.loader
	{
	   background:#000 url(img/pro.png) no-repeat center center;
	   height: 100px;
	   width: 100px;
	   position: fixed;
	   z-index: 1000;
	   left: 50%;
	   top: 50%;
	   margin: -25px 0 0 -25px;
	}
	</style>
	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp"%>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
		<div class="content-wrapper">
			<section class="content-header">
			<div class="text-right">
				<span><s:a action="addStudent" cssClass="bg-blue pad margin-r-5">+AddNewStudent</s:a></span>
				<span><a href="exportStudentExcel" id="excel" class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
				<%-- <span><a href="exportStudentPdf"  id="pdf" class="btn no-padding"><i class="fa fa-file-pdf-o fa-2x text-red"></i></a></span> --%>
			</div>	
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow flat no-padding">

							<!-- /.box-header -->
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Id</th>
											 
											<th>Name</th>
											<!-- <th>School Id</th> -->
											<th>Class</th>
											<th>RollNo:</th>
											<th>Parent</th>
											<th>Bus</th>
											<th>Route</th>
											
											<th>Actions</th>

										</tr>
									</thead>
									<tbody>
										 <s:iterator value="dummyList">
										 <s:if test="%{RecordStatus.equals('ACTIVE')}">
											<tr>

												<td><s:property value="StudentId" /></td>
												
												<td style="position: relative;"><img
													onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{ProfilePicture}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													  <s:property value="StudentFirstName" /> <s:property value="StudentLastName" /></td>
												<td><s:property value="Description" /></td>
												<td><s:property value="RollNo" /></td>
												<td><s:property value="Name" /></td>

												<td><s:property value="BusName" /></td>
												<td><s:property value="RouteName"/></td>
                                                
                                                   <td> 
                                                   <div class="col-md-4 no-padding">
                                                   <s:form action="viewStudent" title="View Student"><s:hidden name="id1" value="%{StudentId}"></s:hidden><button type="submit"  id="fabutton"><i class="fa fa-eye margin-r-5"></i></button></s:form>
                                                   </div><div class="col-md-4 no-padding"> <s:form action="editStudent" title="Edit Student"><s:hidden name="id2" value="%{StudentId}"></s:hidden><button type="submit"  id="fabutton"> <i class="fa fa-pencil text-blue margin-r-5"></i></button>
    											</s:form></div>
												
               									 <s:if test="%{!usedStatus.equals('ACTIVE')}">
                								
                									 <div class="col-md-4 no-padding"><s:form action="deleteStudent" title="Delete Student"><s:hidden name="id3" value="%{StudentId}"></s:hidden>  <s:submit type="button"  cssClass="del" id="fabutton" ><i class="fa fa-trash text-red margin-r-5"></i></s:submit></s:form> 
                									 </div></s:if>
                						
                								 
                
               									
               										
                										</td>  
											</tr></s:if>
										</s:iterator>
									</tbody>

								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>






	</div>

	

	<script>
		$(function() {
			$('#example1').DataTable();
			

		});
		 
	</script>
</body>
</html>
