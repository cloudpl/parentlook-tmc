<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
       #map
      {
      height:350px;
      width:100%;
      }
      .embed-responsive-16by9{
      padding-bottom:75.25%;
      }
       #busesdata {
       display: block;
       }
      #busesdata span{
      font-size: 15px;
      cursor: pointer;
      }
      #busesdata div{
       display: inline-block;
       cursor: pointer;
      }
       
    
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY"></script> 

	

    <script type="text/javascript">
    
    var getUrlParameter = function getUrlParameter(sParam) {
   	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
   	        sURLVariables = sPageURL.split('&'),
   	        sParameterName,
     	        i;

   	    for (i = 0; i <sURLVariables.length; i++) {
   	        sParameterName = sURLVariables[i].split('=');

   	        if (sParameterName[0] === sParam) {
   	            return sParameterName[1] === undefined ? true : sParameterName[1];
   	        }
   	    }
   	    
   	};
   function get_routedetails()
   {
	  $.ajax({
		  
		  url:"getBusdetails",
		  type:"post",
		   catche:true,
           success: function(data)
	           {
		     
        	   
	
		      var buseslist=data.buseslist;
          	var routelist=data.routelist;
                var st;

               	for(var i=0;i<routelist.length;i++)
          		{
          		var route=routelist[i];
          
       		  $('#allroutes').append('<option value = "'+route.routeId+'">'+route.routeName+'</option>');
        		  
        		}
               	
     
          		for(var j=0;j<buseslist.length;j++)
          			{
                  var busdetail=buseslist[j];
                  
                  st+="<tr><td>"+busdetail.routeId +"</td>"+
                       "<td>"+busdetail.busId+"</td>"+
                      "<td>"+busdetail.activities+"</td>"+
                       "<td>"+busdetail.pickup+"</td>"+
                       "<td>"+busdetail.dropStatus+"</td></tr>";
	           }
          	
          	$('#tdata').append(st);
	           }
	  });
	   
   }
    
    
    
    $(document).ready(function()
    		
   {
    	$(".active").removeClass();
    	 $("#track").addClass("active");
    
    	get_routedetails();
    	
    	getMap();
    	
    	getBusesForRoutesonMap();
    	
    	$("#allroutes").change(function(){
    		getMap();
    			getBusesForRoutesonMap();
    			
    	});
    	
    	$("#popup").hide();
   	 $(".knob-label").click(function(){ 
   		 var reqbusstatus=$(this).attr("id");
   		 var rId=$("#allroutes").val();
   		 $(".modal-body").html("");
   		 border='1'
   			 $.ajax({
   		    	 url:"getBusesBasedOnStatus?routeId="+rId+"&busStatus="+reqbusstatus, 
   				   catche:false,
   		            success: function(data)
   			           {
   		            	var alist=data.busDetailBeans;
   		            	
   		            	if(alist.length>0){
    		            	var fData="<table><tr class='col-md-12'> <th class='col-md-3'>BusId</th><th class='col-md-4'>BusName</th><th class='col-md-5'>RouteName</th></tr>";
    		            	//$(".modal-body").append(fData);
    		              
    		            	for(var i=0;i<alist.length;i++){
    		            		
    		            		var busbean=alist[i];
    		            		var bName=busbean.busName;
    		            		var rName=busbean.routeName; 
    		            		var busId=busbean.busId;
    		            		
    		            		fData=fData+'<tr class="col-md-12" style="word-break:break-all;"><td class="col-md-3">'+busId+'</td><td class="col-md-4">'+bName+'</td><td class="col-md-5">'+rName+'</td></tr>';
    		            	}
    		            	$(".modal-body").append(fData+"</table>");
    		            	}else{
   		            		$(".modal-body").append("No Buses");
   		            	}
   		            	$("#popup").click();
   			           }
   	 }); 
   	
   });
    	
    });
    
    </script>
    <script type="text/javascript">
    
    var directionsService = new google.maps.DirectionsService();
    var geocoder = new google.maps.Geocoder();
var directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true});
         var myLatlng;
		 var GOOGLE_MAP_KEY = "AIzaSyBmDYBawBkUUGIqUoHs72EdlK3UugfhGM4";
    		var image="img/busicon.png";
    	var businact="img/busicon.png";
    	var busact="img/busicon.png";
    	var busreach="img/busicon.png";
    	var schoolimage="img/schoolicon.png";
		var ss=localStorage.getItem("apikey");
    	
    	
    	 
    		        var script = document.createElement('script');
  		          script.type = 'text/javascript';
  		     
  		       
  		          
  		     
  		     
  		          
  		    var icou=0;
  		  var  map;
  		var marksarr=[];
    function	getBusesForRoutesonMap(){
    	var rId=$("#allroutes").val();
    	var rText=$("#allroutes option:selected").text();
    	$("#dynaroute").html(rText);
    	
    	 for (var i = 0; i < marksarr.length; i++) {
           }

  		$.ajax({
	    	 url:"getBusesANdGpsForRoute?routeId="+rId, 
			   catche:false,
	            success: function(data)
		           {
	            	var schoolname=data.schoolname;
	            	var latitude=data.latitude;   
	     		     var longitude=data.longitude;
	     		  
	     		   
	     	 
	  				
	  				var coordinatelist=data.busesdetail;
		                 var st;
		                
		                
		                 $("#busInactivePick").html(data.busInactivePick);
		                 $("#activePick").html(data.activePick);
		                 $("#busArrviedPick").html(data.busArrviedPick);
		                 $("#busInactiveDrop").html(data.busInactiveDrop);
		                 $("#activeDrop").html(data.activeDrop);
		                 $("#busArrviedDrop").html(data.busArrviedDrop);
		                 
		                 var active=data.activePick;
			                var dropactive=data.activeDrop;
			              var picarrive=data.busArrviedPick;
			              var droparrive=data.busArrviedDrop;
			                if(active>0 || dropactive>0){
			               getMap();
			                
			                }
		         
		           
		           $("#busesdata").html("");
	  				 
	  				 var busbean=data.busDetailBeans;
	  				
	  				 for(var k=0;k<busbean.length;k++){
	  					 var bBean=busbean[k];
	  					 var str1="";
	  					 var str2="";
	  					 if(bBean.busPick==0){
	  						 str1="<i class='fa fa-circle text-gray' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-bus margin-r-5 text-gray'></i>"+bBean.busName;
	  		                
	  					 }else if(bBean.busPick==1){
	  						 
	  						str1="<i class='fa fa-cog fa-spin text-blue' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-bus margin-r-5 text-blue text-wrap'></i>"+bBean.busName;
	  					 }else{
	  						 
	  						str1="<i class='fa fa-circle text-orange' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-bus margin-r-5 text-orange'></i>"+bBean.busName;
	  					 }
	  					 
	  					if(bBean.busDrop==0){
	  						 
	  						str2="<i class='fa fa-bus margin-r-5 text-gray'></i>"+bBean.busName+"<i class='fa fa-circle text-gray' style='float:right;margin-right:-62px'></i>";
	  					 }else if(bBean.busDrop==1){
	  						str2="<i class='fa fa-bus margin-r-5 text-blue'></i>"+bBean.busName+"<i class='fa fa-cog fa-spin text-blue' style='float:right;margin-right:-62px'></i>"; 
	  						 
	  					 }else{
	  						 
	  						str2="<i class='fa fa-bus margin-r-5 text-green'></i>"+bBean.busName+"<i class='fa fa-circle text-green' style='float:right;margin-right:-62px'></i>";
	  					 }
	  					 $("#busesdata").append("<li style='float:none;' class='row no-margin no-padding'><a href='#' style='height:auto; padding:0 15px;'><span class='pull-left col-md-6'>"+str1+"</span><span class='pull-right col-md-6'>"+str2+"</span></a></li>");
	  					 
	  					 
	  					 
	  					 
	  				 }
	  		       
	  				 
			}
	    });
  	}
  setInterval(getBusesForRoutesonMap,2000);
    
   function getMap(){
	   
		var rId=$("#allroutes").val();
		
		
		
  	 for (var i = 0; i < marksarr.length; i++) {
         }

		$.ajax({
	    	 url:"getBusesANdGpsForRoute?routeId="+rId, 
			   catche:false,
	            success: function(data)
		           { 
	            	 var schoolname=data.schoolname;
	            	var latitude=data.latitude;   
	     		     var longitude=data.longitude;
	     		     
	     		    
	  		   
	  				if(icou==0){
	  					myLatlng = new google.maps.LatLng(latitude,longitude);
	  	  		      var mapOptions = {
	  	  		        zoom: 11,
	  	  		    styles: [{
	  	  	       featureType: 'all',
	  	  	        elementType: 'labels',
	  	  	        stylers: [{
	  	  	            visibility: 'on'
	  	  	        }]
	  	  	    }],
	  	  		        center: myLatlng,
	  	  		    fullscreenControl: true,
	  	  		        mapTypeControlOptions: {
	  	  		            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
	  	  		            position: google.maps.ControlPosition.TOP_CENTER
	  	  		        },
	  	  		        mapTypeId: google.maps.MapTypeId.ROADMAP
	  	  		      };
	  					 map = new google.maps.Map(document.getElementById("map"),
	  		     		          mapOptions);	
	  					 var marker=new google.maps.Marker({position:myLatlng,map:map,title:""+schoolname,icon:schoolimage});
		                 marksarr.push(marker);
	  					
	  				} 
	  				
	            	
		            	var coordinatelist=data.busesdetail;
		                 var st;
		               
		           for(var i=0;i<coordinatelist.length;i++)
		    	      {
		        	   
		              var coordinate=coordinatelist[i];
		             
		               myLatlng = new google.maps.LatLng(coordinate.geoLat, coordinate.geoLang);
		               var marker;
		              
		            
		               if(icou<coordinatelist.length){
		              if(coordinate.status=="0"){
		            	 marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:businact});
		              }else if(coordinate.status=="1" || coordinate.status=="3"){
		            	  marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:busact});
		              }else if(coordinate.status=="2"){
		            	  marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:image});
		              }else{
		            	  
		            	  marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:busreach});
		              }
		              marksarr.push(marker);
		               }else{
		            	   marksarr[i+1].setPosition( myLatlng ); 
		               }
		              
		             icou++;
		    	}
		           
		  				 
			}
	    });
	}
    </script>

	
</head>
<body class="hold-transition skin-yellow sidebar-mini" >
	<div class="wrapper">

		<%@ include file="header.jsp" %>

		

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		
		<div class="content-wrapper">
			
			<section class="content-header">
				 
			</section>
			  <section class="content">
	<div class="row margin">
	<div class="col-md-6">
	<div class="box no-shadow no-border flat">
                    <div class="box-header">
					<div class="form-group col-md-8 no-padding no-margin">
                 <select class="form-control form-control-downarrow select2" id="allroutes" style="width: 100%;">
				  <option value="0">All Routes</option>
                </select>
				
				 </div>
              
					</div>
                    <div class="box-body" style="box-shadow:0 2px 10px rgba(0,0,0,.2);">
                      <div class="embed-responsive embed-responsive-16by9">
                       <div style="float:left;" id="map">
                                    </div>
                      </div>
                    </div>
                    
                  </div>
	</div>
	<div class="col-md-6">
	<div class="box no-shadow no-border flat">
	<div class="box-header">
	<ul>
			   <li class="time-label" style="margin-bottom:0px; list-style:none;">
                        <span class="text-blue">
                         <i class="fa fa-bus margin-r-5 text-orange"></i><span id="dynaroute"> All Routes</span>
                        </span>
                  </li></ul>
				  </div>
				  
				  <div class="box-header text-center">
     <div class="row">
     <div class="col-md-6">
     <div class="row">
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="busInactivePick"  class="text-orange text-bold">0</div>
                      <div class="knob-label" id="inactive_pickup"><i class="fa fa-circle-o text-gray margin-r-5"></i>Inactive</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="activePick" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="active_pickup"><i class="fa fa-gear fa-spin text-blue margin-r-5"></i>Active</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding">
                      <div id="busArrviedPick" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="arrived_pickup"><i class="fa fa-circle text-orange margin-r-5"></i>Arrived</div>
                    </div>
                    
                        </div>
     <div class="row margin text-center bg-aqua">
     Pickup
     </div>
     </div>
     <div class="col-md-6">
     <div class="row">
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="busInactiveDrop" class="text-orange text-bold">0</div>
                     <div class="knob-label" id="inactive_drop" ><i class="fa fa-circle-o text-gray margin-r-5"></i> Inactive</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="activeDrop" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="active_drop"><i class="fa fa-gear fa-spin text-blue margin-r-5"></i>Active</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding">
                      <div id="busArrviedDrop" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="arrived_drop"><i class="fa fa-circle text-green margin-r-5"></i>Arrived</div>
                    </div>
                                
     </div>
     <div class="row margin text-center bg-green-gradient">
     Drop-Off
     </div>
     </div>
      </div>
      <button type="button" id="popup" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         
      </div>
      <div class="modal-body">
         
         
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
     </div>
		 <div class="box-body" style="height:56vh; overflow-y:auto;">
				<ul class="timeline" style="border:3px dotted gray;">
                <li>
				
				<div class="timeline-item" style="z-index:999; margin-left:10px;">
				<ul class="nav nav-pills" id="busesdata">
                  
                 </ul>
				</div>
			  </li>
               </ul>
             
                </div>
          </div>
	</div>
	
		
		
		
		</div>
		
		
		</section>



	</div>


	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<script>
      $(function () {
    	  $('#example1').DataTable();
       
      });
    </script>


    			<s:hidden value="%{Latitude}" id="latitude"></s:hidden>
				<s:hidden value="%{Longitude}" id="longitude"></s:hidden>
				<s:hidden value="%{schoolname}" id="schoolname"></s:hidden>
</body>
</html>
