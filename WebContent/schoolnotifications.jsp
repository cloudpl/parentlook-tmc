<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<style>
     
      #map
      {
      height:350px;
      width:100%;
      }
      .embed-responsive-16by9{
      padding-bottom:75.25%; 
      }
      
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


 <script type="text/javascript">
 $(document).ready(function(){ 
	 
	 $(".active").removeClass();
	  $("#notifications").addClass("active");
	   
	 $("#show").hide();
	 $(".read").click(function(){ 
		 
		 var nId=$(this).attr("id");
		 $.ajax({
				url : "readNotifications?notifyId="+nId,
				type : 'POST',
				cache : false,
				success : function(data) 
				{
				 var bean=data.notification;
				$("#cat").html(bean.category);
				$("#sname").html(data.studentName);
					$("#dynaData").html(bean.description);
					$("#show").click();
				},
				error : function(data) {
					alert("error");
				}
			});
		 
	 });
		 $(".close").click(function(){ 
			 
			 location.reload();
		 });
	 
 });
 
 
 </script>

	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			 <section class="content">
			  
			   
	<div class="box no-shadow no-border no-margin flat">
                    <div class="box-header with-border no-padding">
						<h4 class="text-orange text-center text-bold">Notifications</h4></div>
					
                    <div class="box-body">
                      
                    <div class="table-responsive mailbox-messages">
                    <table class="table table-hover table-striped">
                      <tbody>
                      <s:iterator value="notificationsWebs">
                        <tr> 
                          <td class="mailbox-star"><a href="#">
                          <s:if test="%{ReadStatus.equals('FALSE')}">
                          <i class="fa fa-star"></i>
                          </s:if><s:else>
                          <i class="fa fa-star text-gray"></i>
                          </s:else>
                          </a></td>
                          <td class="mailbox-name"><span class="margin-r-5"><s:property value="Category"/> For <s:property value="StudentName"/>:</span><span><a href="#" id="<s:property value='Id'/>" class="read text-blue"> <s:property value="Description"/></a></span></td>
                          <td class="mailbox-date"><s:property value="Time"/></td>
                        </tr>
                        
                        
                        </s:iterator>
                      </tbody>
                    </table>
                  </div>
                  
	</div>
	 	</div>
		</section>
			


		 <button type="button" id="show" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id="cat"></span> Notification for:<span id="sname" class="text-blue margin-l-5"> </span></h4>
      </div>
      <div class="modal-body" id="dynaData">
        
      </div>
    </div>

  </div>
</div>

	</div>


	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	


    			<s:hidden value="%{Latitude}" id="latitude"></s:hidden>
				<s:hidden value="%{Longitude}" id="longitude"></s:hidden>
				<s:hidden value="%{schoolname}" id="schoolname"></s:hidden>
</body>
</html>
