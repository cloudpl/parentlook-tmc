<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
 
<link rel="stylesheet" href="css/skins/_all-skins.min.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
  
    <script type="text/javascript">
    
    function myChange(){
    		var a =document.getElementById("scId").value;
    	
    }
    
    
    </script>
    <style type="text/css">
		.img-responsive {
     
     position: absolute;
    left: 70%;
    top: 70%;
    background-color: white;
    z-index: 150;

    height: 100px;
    margin-top: -200px;

    width: 200px;
    margin-left: -180px;
 }</style>
 
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>

	 
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

	 	<div class="content-wrapper">
			<section class="content-header">
				<h4></h4>
				<ol class="breadcrumb">
				
				<s:if test="%{classbean.ClassId>0}">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getClassesList">Classes</s:a></li>
					<li class="active">Edit Class</li></s:if>
					<s:else>
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getClassesList">Classes</s:a></li>
					<li class="active">Add New Class</li>
					</s:else>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-border no-shadow">
   <div class="pad">All Fields Marked<span class="text-red">* </span>Mandatory</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<s:form id="form" role="form">
									<div class="box-body">
										<div class="form-group">
											
											<label for="StudentClass">School Name::</label>
											
											<s:property value="%{#session.schoolName}"/>
												
												<s:hidden name="SchoolId" value="%{#session.schoolId}"/>
											 
										</div>
										<div class="form-group">
											<label for="StudentClass">Class Name<span class="text-red">*</span></label> <input
												type="text" name="StudentClass" class="form-control"
												id="classname" value='<s:property value="%{classbean.StudentClass}"/>' required>
										</div>

										<div class="form-group">
											<label for="Section">Section<span class="text-red">*</span></label> <input type="text"
												name="Section" class="form-control"
												id="section" value='<s:property value="%{classbean.Section}"/>' required>
										</div>

										<div class="form-group">
											<label for="Description">Description<span class="text-red">*</span></label> <input
												type="text" name="Description" class="form-control"
												id="exampleInputPassword1" value='<s:property value="%{classbean.Description}"/>' required>
										</div>


									</div>

									<div class="box-footer">
									<input type="hidden" id="classid" name="ClassId" value="<s:property value='%{classbean.ClassId}'/>"/>
									<s:if test="%{classbean.ClassId>0}">
										
										<s:submit value="Update" class="btn btn-primary"></s:submit>
										
										</s:if>
										<s:else>
										<s:submit value="Save" class="btn btn-primary"></s:submit>
										</s:else>
									</div>
									<div id="loading">
    <img src="img/pro.png"  class="img-responsive img-center"/>
</div>
								</s:form>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>


	</div>

	
	 <script type="text/javascript">
	 $(document).ready(function(){
		 $("#loading").hide();
		 $(".active").removeClass();
	   	  $("#profiles").addClass("active");
	   	  $("#classes").addClass("active");
   	  $('#form').on('submit',function(e) {
   		 $("#loading").hide();
   		 $(this).attr("disabled", "disabled");
	        doWork();
				e.preventDefault();
				 var classname=$("#classname").val();
					var section=$("#section").val();
				var classid=$("#classid").val();
				
				 
						
						  $.ajax({
						    	 url:"checkstudentclass?classname="+classname+"&section="+section,
						        type: 'POST',
						       
						        context: this
						    }).done( function(data) {
						        	 
						            if (data.msg== 'Exists') {
						            	 $("#loading").hide();
						            	
						            var bean1=data.classbean;
						          
						            if(bean1.classId==classid){
						            	$("#section").parent().next(".validation").remove();
						            	$("#form").attr("action","updateClassDetails");
										this.submit();
						          
						            }else{
						            	$("#section").parent().next(".validation").remove();
							                $("#section").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Section Exists For this class</div>");
						               
						            }

						            	
						            } 
						            else {
						            	if(classid>0){
						            	$("#section").parent().next(".validation").remove();
						            	$("#form").attr("action","updateClassDetails");
										this.submit();
						            	}else{
										$("#form").attr("action","saveClassDetails");
						                this.submit();
						            	}
						            } 
						        });
					
				
				 
		 
			

		});
   

	 });
	 function doWork() {
		 $("#loading").show();
		    setTimeout('$("#btn").removeAttr("disabled")', 1500);
		}
	 </script>
	
</body>
</html>
