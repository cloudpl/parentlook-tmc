<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ParentLook | Log in</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   
    <link rel="stylesheet" href="css/bootstrap.min.css">
    
    <link rel="stylesheet" href="fonts/font-awesome.min.css">
   
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/AdminLTE.min.css">
   
    

    
  </head>
  <body class="hold-transition login-page" style='background:orange url("img/login-bg-map.png") no-repeat scroll center center'>
  <div class="col-md-6 pull-right" style="background:white;height:100vh;">
    <div class="login-box">
      <div class="login-logo">
       <div align="center"><img src="img/logo.png"/></div>
        <a href="#"><b>Parent</b>Look</a>
      </div> 
     
      <div class="login-box-body">
       
        <s:form action="AuthenticateForgotRequest" theme="simple">
          <div class="form-group has-feedback">
          <label>Token</label>
            <input type="text" class="form-control" name="token" placeholder="Token" required>
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>
          
          <div class="row no-margin">
            
            <div class="pull-right">
              <button type="submit" class="btn bg-blue flat">Submit</button>
         
            </div> 
             
          </div>
          <br/><br/>
        </s:form>


        

      </div> 
    </div> 
</div>
    <script src="jQuery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
     
    
  </body>
</html>
