<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="css/font-awesome.min.css">
<link rel="stylesheet"
	href="css/ionicons.min.css">
<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">


<script src="jQuery-2.1.4.min.js"></script>

 
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		  <div class="content-wrapper">
        <section class="content">
  <div class="box box-solid">
   <div class="box no-border no-shadow">
   <div class="box-header">
   <div class="text-blue"><i class="fa fa-check margin-r-5 text-green"></i>Choose from Near by Pickup Points</div>
              <div class="box-tools pull-right">
      <button class="btn btn-box-tool text-blue" data-widget="remove"><i class="fa fa-close"></i></button>
     </div>  
            </div> 
  <div class="box-body no-padding">
  <div class="col-md-9">
                    <div class="form-group">
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                          Option one is this and that&mdash;be sure to include why it's great
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                          Option two can be something else and selecting it will deselect option one
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" disabled>
                          Option three is disabled
                        </label>
                      </div>
                    </div>
     <div class="box-footer" align="center">
     <button type="submit" class="btn btn-success">Submit</button>
   </div>
   </div>
   </div>
   <div align="center" class="text-green h4">----- or -----</div>
   </div>
   
   <div class="box no-border no-shadow">
   <div class="box-header">
   <div class="text-blue"><i class="fa fa-plus margin-r-5 text-green"></i>Setup a New Pickup Point</div>  
            </div><!-- /.box-header -->  
  <div class="box-body no-padding">
  <div class="col-md-9">
  <img class='img-responsive' src='images/map.jpg' alt='map'> 
    <div class="description pad">
                  <i class="fa fa-map-marker margin-r-5 text-blue"> Malakpet, Telangana</i>
                  <p class="text-muted">2km from your location</p>
     </div>
     <div class="box-footer" align="center">
     <button type="submit" class="btn btn-success">Submit</button>
   </div>
   </div>
   </div>
   </div>
   </div>
   
    </section>
    
       </div>

		<%@ include file="footer.jsp" %>

		<aside class="control-sidebar control-sidebar-dark">
			<!-- Create the tabs -->
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>

				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
			<div class="tab-content">
				<!-- Home tab content -->
				<div class="tab-pane" id="control-sidebar-home-tab">
					<h3 class="control-sidebar-heading">Recent Activity</h3>

				</div>
				<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab
					Content</div>
				<div class="tab-pane" id="control-sidebar-settings-tab">
					<form method="post">
						<h3 class="control-sidebar-heading">General Settings</h3>
						<div class="form-group">
							<label class="control-sidebar-subheading"> Report panel
								usage <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Some information about this general settings option</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Allow mail
								redirect <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Other sets of options are available</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Expose author
								name in posts <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Allow the user to show his name in blog posts</p>
						</div>

						<h3 class="control-sidebar-heading">Chat Settings</h3>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Show me as
								online <input type="checkbox" class="pull-right" checked>
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Turn off
								notifications <input type="checkbox" class="pull-right">
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Delete chat
								history <a href="javascript::;" class="text-red pull-right"><i
									class="fa fa-trash-o"></i></a>
							</label>
						</div>
					</form>
					
					 

      
					
				</div>
			</div>
		</aside>
		
		<div class="control-sidebar-bg"></div>
	</div>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDbpEFRTD2-nsKFed6DPcfbZo-Mpm7B9QI&libraries=places"
        async defer></script>
	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	
	
	
	
	<script src="js/app.min.js"></script>
	
	

	
	
	
	
	
	
	 
	
	
	<script>
		$(function() {
			$('#example1').DataTable();
			
		});
	</script>
</body>
</html>
