<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<h4></h4>
				<ol class="breadcrumb">
					<s:if test="%{classbean.ClassId>0}">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getClassesList">Classes</s:a></li>
					<li class="active">View Class</li></s:if>
					<s:else>
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getClassesList">Classes</s:a></li>
					<li class="active">View Class</li>
					</s:else>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-border no-shadow">

					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<div class="box-body">
								<dl class="dl-horizontal">
							<dt> ClassId:</dt><dd> <s:property value="%{classbean.ClassId}"/> </dd>
								<dt> SchoolId:</dt><dd><s:property value="%{classbean.SchoolId}" /></dd> 
							  <dt>  StudentClass:</dt><dd> <s:property value="%{classbean.StudentClass}" /> </dd>
							<dt>    Section:</dt><dd><s:property value="%{classbean.Section}" /> </dd>
							<dt>    Description:</dt><dd> <s:property value="%{classbean.Description}" /> </dd>										
							  
							   </dl>
							  
							</div>
							</div>
						</div>
					</div>
					 <div class="box-footer">
										
						<s:form action="editclass">
			<input type="hidden" name="classid" value="<s:property value='%{classbean.ClassId}'/>"/>
				   <input type="submit" value="Edit" />
							   </s:form>
							   </div>
				</div>

			</section>
		</div>


		
	</div>

	
	<script type="text/javascript">
	
	
	 $(document).ready(function(){
    	  $(".active").removeClass();
    	  $("#profiles").addClass("active");
    	  $("#classes").addClass("active");
    
      });
	</script>
</body>
</html>
