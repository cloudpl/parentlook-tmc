<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Cache-Control" content="no-store"/>
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Expires" content="-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/ionicons.min.css">

<link rel="stylesheet" href="css/custom.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
	 <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	
	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY&libraries=places"
        async defer></script>
 <style type="text/css">
	
	#pac-input{
    height:25px;
    width:300px;
}
	
	</style>

</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content margin">
			
			<div class="row">
			<div class="col-md-6">
			<h3 class="text-blue">Set Pickup Point</h3>
					<div class="form-group">
					<s:hidden value="%{busi}" id="busidd"></s:hidden>
					 
					<s:if test="%{busi>0}">
					<div><label>Route Name: </label><s:property value="%{routename}"/></div>
					<div><label>Bus Name: </label><s:property value="%{busname}"/></div>
					<div><label>Drop-off Point: </label><s:property value="%{dropaddr}"/></div>
					</s:if><s:else>
									<select id="root">
										<option>Select Route</option>
									
										<s:iterator value="rootsBeans">
											<option value='<s:property value="RouteId" />'><s:property
													value="RouteName" /></option>
									</s:iterator>
									</select></s:else>
									</div>
									
								<div class="form-group">	
									<div id="rootbusses"></div>
									</div>
			</div>
			<div class="col-md-5 margin pad">
			<div class="row">
			<span class="text-blue"><b><s:property value="viewBean2.StudentName" /></b></span>
			<span class="text-blue"> - <s:property value="viewBean2.StudentClass" /></span>
			</div>
			<div class="row"><s:property value="viewBean2.Location" /></div>
			</div>
			</div>
			
			<div class="box box-solid">
   <div class="box no-border no-shadow" id="approve">
   <div class="box-header">
   <div class="text-blue"><i class="fa fa-check margin-r-5 text-green" ></i>Choose from Near by Pickup Points</div>
              <div class="box-tools pull-right">
      <button class="btn btn-box-tool text-blue" data-widget="collapse"><i class="fa fa-minus"></i></button>
     </div>  
            </div> 
  <div class="box-body no-padding">
  <div class="col-md-9">
  <!-- radio -->
                    <div class="form-group">
                    <div id="radioerror"></div>
                    
                    <s:form action="approvePickup" id="approvePickup">
										<s:hidden name="StudentId" value="%{StudentId}"></s:hidden>
										<div id="rootPickups"></div>

								<button type="submit" class="btn btn-success" id="app">Approve Pickup Point</button>
										 						
									</s:form>
                    
                    
                      
                    </div>
     <div class="box-footer" align="center">
     
   </div>
   </div>
   </div>
  <div align="center" class="text-green h3">----- <b>or</b> -----</div>
   </div>
   
   <div class="box no-border no-shadow" id="new">
   <div class="box-header">
   <div class="text-blue"><i class="fa fa-plus margin-r-5 text-green"  id="addPickup"></i>  Setup a New Pickup Point</div>  
            </div><!-- /.box-header -->  
  <div class="box-body no-padding">
  <div class="col-md-9">
  
  		<div id="mapdetails">
								<input id="pac-input" class="controls" type="text"
        placeholder="Enter a location">
                  <div class='img-responsive' id="map" style="height:300px;width:100%;"  ></div>
    <div class="description pad">
    
    				 <i class="fa fa-map-marker margin-r-5 text-orange" id="latlang"></i>
                  <i class="fa fa-map-marker margin-r-5 text-blue" id="waypoint"></i>
                  
                 
							</div>
							
							</div>
							<!-- action="addPickupPoint -->
							
							<s:form id="formId">
										<s:hidden name="StudentId" value="%{StudentId}"></s:hidden>
										 
									<s:hidden name="SchoolId" value="%{SchoolId}"></s:hidden>
									<s:hidden name="RouteId" value="%{RouteId}" id="rid"></s:hidden>
									<s:hidden name="BusId" value="%{busi}" id="bid"></s:hidden>
									<s:hidden name="PickupPoint" value="%{wayPoint}" id="pid"></s:hidden>
									<s:hidden name="PLatLang" value="%{PLatLang}" id="platLang"></s:hidden>
									 	<s:hidden name="direction" value="0"></s:hidden>

									 
											<input type="submit" class="btn btn-success"  value="Set Pickup Point" id="addPickup"/>		
									</s:form>
  
  
  
   </div>
   </div>
   </div>
   </div>
			
			
			<s:hidden name="origin"   id="origin-input"></s:hidden>
    <s:hidden name="destination" value="%{wayPoint}" id="destination-input"></s:hidden>
    <s:hidden name="latLang" value="%{latLang}" id="waylat"></s:hidden>
    
			
			<s:hidden name="BusId" value="%{BusId}" id="mainbusId"></s:hidden>
			
			
			
			
			</section> 
		</div>

	</div>


	

	
	
	<script>


	$(document)
			.ready(
					function() {
						
						$("#approve,#new").hide();
						
						
			
				var pNames=[];
				var pIds=[];
				var pDist=[];
				var pDur=[];

				var bid=$("#busidd").val();
				 
				if(bid!=0){

					 
								$("#approve,#new").hide();
								$("#addPickup").show();
								$("#rootPickups").html("");
								 

								var url = "getPickupPoints?busId="+
								bid+"&direction=0";
								 
								$
										.ajax({
											url : url,
											type : 'get',
											cache : false,
											success : function(data) {

												

												app = "";
												
												for (var i = 0; i < data.pickupPoints.length; i++) {

													
													  
													  
													var directionsService = new google.maps.DirectionsService;
											         
												        var origin_input = data.pickupPoints[i].pickupPoint;
												        pNames.push(origin_input);
												        var picId=data.pickupPoints[i].pickupPointId;
												        pIds.push(picId);
												        var destination_input = document.getElementById('destination-input').value;
												        var modes = document.getElementById('mode-selector');
												 
												 
												       
												          directionsService.route({
												            origin: origin_input,
												            destination: destination_input,
												            travelMode: "DRIVING"
												          }, function(response, status) {
												        	  
												            if (status === google.maps.DirectionsStatus.OK) {
												                
												                 
												            	pDist.push(response.routes[0].legs[0].distance.text);
												            	pDur.push(response.routes[0].legs[0].duration.text);
													
												              
												            } else {
												              window.alert('Directions request failed due to ' + status);
												            }
												          });
												        		 
													 
													 
													 
													 
													 
												}
												
							//buildData();
							$("#rootPickups").html("");
							 setTimeout(function() {
								// buildData();
						    }, 1000);
							
							 
							 $("#bid").attr("value",$("#busId").val());
								$("#rid").attr("value",$("#root").val());
								 
								initMap();	
								  buildData();

								  $("#approve,#new").show();	
											},
											error : function() {
												alert("error");
											}
										});

						 
							


					

					}else{  
						$("#root")
						.change(
								function() {
									$("#rootPickups").html("");
									$("#approve,#new").hide();
									var rootid = $(this).val();
									
									var url = "getBussesForRoot?rootId="
											+ rootid;
									var ap = "";
									$
											.ajax({
												url : url,
												type : 'get',
												cache : true,
												success : function(data) {

													 
													var len = data.bussesBeans.length;
													
													ap = "<select id='busId'><option>Select Bus</option>";
													for (var i = 0; i < len; i++) {

														ap = ap
																+ "<option  value="+data.bussesBeans[i].busId+">"
																+ data.bussesBeans[i].busName
																+ "</option>";
													}
													ap = ap
															+ "</select>";

													$("#rootbusses")
															.html(ap);
												},
												error : function() {
													alert("error");
												}
											});
									
								});


					 

						
						
						
								$("body").delegate(
												"#busId",
												"change",
												function() {
													
													$("#approve,#new").hide();
													$("#addPickup").show();
													var busid = $(this).val();
													$("#rootPickups").append("");
													var url = "getPickupPoints?busId="+
														busid+"&direction=0";
													
			
													$
															.ajax({
																url : url,
																type : 'get',
																cache : true,
																success : function(data) {

																	 


																	app = "";
																	 pNames=[];
																	var cou=data.pickupPoints.length;
																	
																	
																	
																	
																	
																	var jk=1;
																	if(cou>0){
																		
																	
																	for (var i = 0; i < data.pickupPoints.length; i++) {
		 
																		 
																		  
																		var directionsService = new google.maps.DirectionsService;
																		 
																        
																	        var origin_input = data.pickupPoints[i].pickupPoint;
																	        pNames.push(origin_input);
																	        var picId=data.pickupPoints[i].pickupPointId;
																	        pIds.push(picId);
																	        var destination_input = document.getElementById('destination-input').value;
																	        var modes = document.getElementById('mode-selector');
																	         
																	          directionsService.route({
																	            origin: origin_input,
																	            destination: destination_input,
																	            travelMode: "DRIVING"
																	          }, function(response, status) {
																	        	   
																	            if (status === google.maps.DirectionsStatus.OK) {
																	                
																	            	
																	            	pDist.push(response.routes[0].legs[0].distance.text);
																	            	pDur.push(response.routes[0].legs[0].duration.text);
																		
																	              
																	            } else {
																	              window.alert('Directions request failed due to ' + status);
																	            }
																	          });
																	        		 
																		 
																		 if(jk==cou){
																			 setTimeout(function() {
																			    }, 3000);
																				
																			 calu();
																			 break;
																		 }
																		 jk++;
																		 
																		 
																		 
																	}
																	}else{
																		calu();
																		
																	}
																	
													
																},
																error : function() {
																	alert("error");
																}
															});

												});



						}

				
				function calu(){
					
					  
					 
					$("#rootPickups").html("");
					 var a=$("#busId").val();

					 $("#bid").attr("value",$("#busId").val());
						$("#rid").attr("value",$("#root").val());
						 
						initMap();	
						  buildData();

						  $("#approve,#new").show();
				}
				
						
						$("body")
						.delegate(
								".pickupPointId",
								"change",
								function() {
									$('input[type="submit"]').prop('disabled', false);
									
								});
						
						
						function buildData(){ 
							
							
						
							if(pNames.length==0){
								
								$("#approvePickup").hide();
								$("#radioerror").html("No pickUp points Found Near to the location");
								
							}else{
								$("#radioerror").html("");
								$("#approvePickup").show();
								$("#rootPickups").html("");
							}
							for(var k=0;k<pNames.length;k++){ 
								
								//$("#rootPickups").html("");
								var inp="";
								if(k==0){
									
									inp="<input type='radio' class='pickupPointId' name='PickupPointId' value='"+pIds[k]+"' checked></input>";
								}else{
									
									inp="<input type='radio' class='pickupPointId' name='PickupPointId' value='"+pIds[k]+"'></input>";	
								}
								
								
								
								
								$("#rootPickups").append(inp
								+ pNames[k]
								+ "&nbsp;&nbsp;&nbsp; <span>Distance from WayPoint "+pDist[k] +"</span>,<span>Time To Reach "+pDur[k]+"<br/>");
								 

							}
						}
						 
						
						
					});





	
</script>
	
	
	
	<script>
      var map;
      var palcename;
      var marker;
      var options = {
    		  enableHighAccuracy: true,
    		  timeout: 10000,
    		  maximumAge: 0
    		};
      
      function initMap() {
    	 
    	
    	
    	  var input = document.getElementById('pac-input');
    	  
    	  if (navigator.geolocation) {
    		  navigator.geolocation.getCurrentPosition(showPosition,showError,options);
    	    }else{
	        	alert("browser do not support geolocation")
	        }
          
    	  


function getPlaceDetailsByLatLang(myLatLang){




    var geocoder;
	  geocoder = new google.maps.Geocoder();
	   

	  geocoder.geocode(
	      {'latLng': myLatLang}, 
	      function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	                  if (results[0]) {
	                      var add= results[0].formatted_address ;
	                      var  value=add.split(",");

	                   var	   count=value.length;
	                  var    country=value[count-1];
	                    var  state=value[count-2];
	                    var  palcename=value[count-3];
					$("#latlang").html("&nbsp;&nbsp;Latitude,Longitude:"+myLatLang);
					
					$("#platLang").attr("value",myLatLang);
					
					$("#waypoint").html("&nbsp;&nbsp;"+value);
					
					document.getElementById('pac-input').value = value;
						
				
					$("#pid").attr("value",value);
					
	                      
	                     
	                      
	                  }
	                  else  {
	                      alert("address not found");
	                  }
	          }
	           else {
	              alert("Geocoder failed due to: " + status);
	          }
	      }
	  );

	
}

function showPosition(position) {

 
	
	  
  var a=$("#waylat").val();
  var ab=a.substring(1, a.length-1);
  var aa=ab.split(",");
  var  myLatlng = new google.maps.LatLng(aa[0],aa[1]);
    var mapOptions = {
      zoom: 14,
      styles: [{
	  	       // featureType: 'poi.business',
	  	       featureType: 'all',
	  	        elementType: 'labels',
	  	        stylers: [{
	  	            visibility: 'on'
	  	        }]
	  	    }],
      center: myLatlng,
      fullscreenControl: true,
      disableDefaultUI: true,
      mapTypeControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_RIGHT
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	    map = new google.maps.Map(document.getElementById("map"),
	          mapOptions);

	    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	 

	 
      marker=new google.maps.Marker({position:myLatlng,map:map,title:"Set Way Point",draggable:true,});

    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
        


        getPlaceDetailsByLatLang(event.latLng);


    });
	
}




     
     
     

     var autocomplete = new google.maps.places.Autocomplete(input);

		autocomplete.addListener('place_changed', function() {
    
    var place = autocomplete.getPlace();
  
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return; 
    }

    // If the place has a geometry, then present it on a map.
   var  myLatlng =  place.geometry.location;
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
       } else {  map.setCenter(place.geometry.location);
    }
    marker.setPosition(myLatlng);

    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
         getPlaceDetailsByLatLang(event.latLng);


    });
    
  });

          
      }
       function showError(error) {
    	//  alert("User denied the request for Geolocation.");
    	   if(error.code == 0){
     	        // unknown error
     	       
     	       
     	        alert(" Your Browser not supported for GeoLocation Access.")
     	      location.reload(true);
     	        }  else if(error.code == 1) {
     	        // permission denied
     	      alert("The acquisition of the geolocation information failed because the page didn't have the permission to do it.Please allow location information for this website.")
     	   location.reload(true); 
     	        } else if(error.code == 2) {
     	        // position unavailable
     	        alert("The acquisition of the geolocation failed because at least one internal source of position returned an internal error.Please try again.");
     	      location.reload(true);
     	        }  else if(error.code == 3){
     	        	
     	        	alert("The time allowed to acquire the geolocation, defined by PositionOptions.timeout information was reached before the information was obtained.Please try again.");
     	        	 location.reload(true);
     	        }
    	        
    	      
    	 
    	}
       
    
     </script>
	
	
	<script>
		$(function() {
			$('#example1').DataTable();
			/*$('#example2').DataTable({
			    
			  "paging": true,
			  "lengthChange": false,
			  "searching": false,
			  "ordering": true,
			  "info": trueswitch
			  "autoWidth": false
			});*/
		});
		$(document).ready(function(){
	    	  $(".active").removeClass();
	    	  $("#stop").addClass("active");
	    	  $("#pending").addClass("active");
	    	 
	    	  $("form").submit(function(){
	    		  var inputval=$("#platLang").val();
	    		  if(inputval==""){
	    			  initMap();
	    			 
	    			  return false;
	    		  }else{
	    			  $("#formId").attr("action","addPickupPoint");
		                this.submit();
	    		  }
	    		  
	    	  });
	    	  
	      });
	</script>
	 
</body>
</html>
