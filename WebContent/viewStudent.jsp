<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>


		 
		<div class="content-wrapper">
			<section class="content-header">
				<h4 class="text-blue"></h4>
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getStudentsList">Students</s:a></li>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-shadow no-border flat">

					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
							
								<s:form action="editStudent" role="form"  enctype="multipart/form-data">
									<div class="box-body">
									<dl class="dl-horizontal">
										<dt>Student First Name : </dt><dd>
                                         <s:property value="%{studentbean.StudentFirstName}"/></dd>
										
										<dt>Student Last Name : </dt>
                                          <dd> <s:property value="%{studentbean.StudentLastName}"/></dd>
										

											<dt>Class Name : </dt>
                                       <dd> <s:property value="%{classBean.Description}"/></dd>
										
										
									
											<dt>Parent Name :</dt><dd>
											 <s:property value="%{parentsbean.FirstName}"/></dd>
												
												
											
											<dt>Roll No : </dt>
                                     <dd>  <s:property value="%{studentbean.RollNo}"/></dd>
										
										
										
</dl>

									 
 
											<s:hidden name="SchoolId" value="%{#session.schoolId}"></s:hidden>
											
										 
									<div class="box-footer">
									
								 <div class="col-md-7">	<s:a action="editStudent" ><input type="button" value="Edit"/><s:param name="id2" >
                                                          <s:property value="%{studentbean.StudentId}" /></s:param>
                                              </s:a></div>
									
				 </div>
									</div>
									 
								</s:form>
							</div>
							
						</div>
					</div>

				</div>

			</section>
		</div>



 
	</div>

	

	<script>
      $(function () {
    	  $('#example1').DataTable();
       
      });
   
    $(document).ready(function(){
  	  $(".active").removeClass();
  	  $("#profiles").addClass("active");
  	  $("#students").addClass("active");
    });
    </script>
    
    
    
    
    
   
     
    
    </body>
</html>
