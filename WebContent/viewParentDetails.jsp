<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
</head>
<body class="hold-transition skin-yellow sidebar-mini" >
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content-header">
				<h4></h4>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Schools</a></li>
					<li class="active">Add New School</li>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-border no-shadow">

					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<s:form   role="form" theme="simple">
									<div class="box-body" >
									 <dl class="dl-horizontal">
							<dt>	 Name: </dt><dd>
						 <s:property value="%{parentBean.FirstName}"/> <s:property value="%{parentBean.LastName}"/> 	</dd>									
 								 <dt>  Address: </dt><dd>
									 <s:property value="%{parentBean.Address}"/> </dd>
								  <dt> MobileNumber: </dt><dd>
										 <s:property value="%{parentBean.MobileNumber}"/> </dd>
										<dt> Email: </dt><dd>
											 <s:property value="%{parentBean.Email}"/> </dd>
										  </dl>
									</div>
									<div class="box-footer">
												<s:a action="editParent"><input type="button" value="Edit"/><s:param name="id1">
               											 <s:property value="%{parentBean.ParentId}"/></s:param>
                										</s:a>
									
								</s:form>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>


		
	</div></center>

	
	<script type="text/javascript">
	
	
	 $(document).ready(function(){
   	  $(".active").removeClass();
   	  $("#profiles").addClass("active");
   	  $("#parents").addClass("active");
     });
	</script>
</body>
</html>
