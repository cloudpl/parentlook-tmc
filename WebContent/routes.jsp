<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	<script type="text/javascript">
	 $(document).ready(function(){
   	  
   	  $(".active").removeClass();
   	  $("#logistics").addClass("active");
   	  $("#routes").addClass("active");
   	  var routesList= "<s:property value='routesList.size'/>";
    	 
    	 if(routesList>0){
   	   $("#excel").show();
   	  // $("#pdf").show();
   	   
      }else{
   	   $("#excel").hide();
   	 //  $("#pdf").hide();
      }
    $(".del").click(function(){
    	 var x = confirm("Are you sure you want to delete ?");
		  
		  if(x==true){
			  
			  return true;
		  }else{
			  
			  return false;
		  }
    })
   	  
     });
	</script>
	<style type="text/css">
	
	#fabutton {
  background: none;
  padding: 0px;
  border: none;
}
	</style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content-header">
				<div class="text-right">
				<span><s:a action="addRoute" cssClass="bg-blue pad margin-r-5">+AddNewRoute</s:a></span>
				<span><a href="exportRouteExcel" id="excel" class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
				<%-- <span><a href="exportRoutePdf"   id="pdf" class="btn no-padding"><i class="fa fa-file-pdf-o fa-2x text-red"></i></a></span> --%>
			</div>
			</section>

			
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow flat">
							 
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
										<th>Route Id</th>
											<th>RouteName</th>
											<th>Start Point</th>
											<th>End Point</th>
											 
											<th>Actions</th>
										</tr>
																			</thead>
									<tbody>
									<s:iterator value="routesList">
										<s:if test="%{RecordStatus.equals('ACTIVE')}">
											<tr>
													<td><s:property value="RouteId" /></td>
												<td><s:property value="RouteName" /></td>
												<td><s:property value="StartPoint" /></td>
												<td><s:property value="EndPoint" /></td>
												 <td>
												<div class="col-md-4 no-padding"> <s:form action="viewroute" title="View Route"><s:hidden name="routeid" value="%{RouteId}"></s:hidden> <button type="submit" id="fabutton"><i class="fa fa-eye margin-r-5"></i></button></s:form>
           </div>
         <div class="col-md-4 no-padding">    <s:form action="editroute" title="Edit Route"><s:hidden name="routeid" value="%{RouteId}"></s:hidden> <button type="submit" id="fabutton"><i class="fa fa-pencil text-blue margin-r-5"></i></button></s:form>
            </div>
            <s:if test="%{UsedStatus.equals('INACTIVE')}">
           
           
         <div class="col-md-4 no-padding">   <s:form action="deleteroute" title="Delete Route"><s:hidden name="routeid" value="%{RouteId}"></s:hidden> <s:submit type="button" cssClass="del" id="fabutton"><i class="fa fa-trash text-red"></i></s:submit></s:form>
           </div>
           </s:if>
                                                 </td>

											</tr></s:if>
										</s:iterator>
									
																			</tbody>
									 
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
			
			
			
			
			
			
			
			
			
			
			
		</div>
	</div>

	
	<script>
      $(function () {
    	  $('#example1').DataTable();
        
      });
     
    </script>
</body>
</html>
