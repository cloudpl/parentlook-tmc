<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">

<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
	<style type="text/css">
	.img-responsive {
     
     position: absolute;
    left: 70%;
    top: 70%;
    background-color: white;
    z-index: 150;

    height: 100px;
    margin-top: -200px;

    width: 200px;
    margin-left: -180px;
 }
	
	</style>
</head>

<body class="hold-transition skin-yellow sidebar-mini">

<br>
	<div class="wrapper">

		<%@ include file="header.jsp" %>

		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
			<s:if test="%{parentsBean.ParentId>0}">
				<h4></h4>
				
				<ol class="breadcrumb">
				
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getParentsList">Parents</s:a></li>
					<li class="active">Edit Parent</li>
				</ol></s:if>
				<s:else>
				<h4></h4>
				
				<ol class="breadcrumb">
				
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getParentsList">Parents</s:a></li>
					<li class="active">Add New Parent</li>
				</ol>
				</s:else>
			</section>
			<section class="content">
				<div class="box box-default no-border no-shadow">
<div class="pad">All Fields Marked<span class="text-red">* </span>Mandatory</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
								<s:form  role="form" enctype="multipart/form-data" method="post" id="datasubmit" >
									<div class="box-body">
										<div class="form-group">
											<label for="Name">FirstName <span class="text-red">*</span></label> <input
												type="text"  name="FirstName" pattern="^[a-zA-Z ]+$" title="Allows Only Characters" class="form-control"
												id="exampleInputPassword1" value='<s:property value="%{parentsBean.FirstName}"/>' required>
										</div>
                                 
                                 <div class="form-group">
											<label for="Name">LastName<span class="text-red">*</span></label> <input
												type="text" pattern="^[a-zA-Z]+$" title="Allows Only Characters" name="LastName" class="form-control"
												id="exampleInputPassword1" value='<s:property value="%{parentsBean.LastName}"/>' required>
										</div>
										

										<div class="form-group">
											<label for="Address">Address<span class="text-red">*</span></label> <input type="text"
												name="Address" class="form-control"
												id="exampleInputPassword1" value='<s:property value="%{parentsBean.Address}"/>' required>
										</div>

                                       <div class="form-group">
											<label for="Email">Email<span class="text-red">*</span></label> <input
												type="email" pattern="[a-zA-Z--9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,9}$"  name="Email" class="form-control"
												id="exampleInputPassword1" value='<s:property value="%{parentsBean.Email}"/>' required>
										</div>
										
										<div class="form-group">
										
											<label for="MobileNumber">Mobile Number<span class="text-red">*</span></label> 
											<s:if test="%{parentsBean.ParentId>0}">
											<input
												type="text" name="MobileNumber" pattern="[789][0-9]{9}" title="Phone Number start with 7 or 8 or 9" class="form-control"
												id="phonenumber" value='<s:property value="%{parentsBean.MobileNumber}"/>' maxlength="10"  readonly="readonly" required>
												
												</s:if>
												<s:else>
												<input
												type="text" name="MobileNumber" pattern="[789][0-9]{9}" title="Phone Number start with 7 or 8 or 9" class="form-control"
												id="phonenumber"  maxlength="10"   required/>
												</s:else>
												
												
										</div>
										<div class="form-group">
										<s:if test="%{parentsBean.ParentId>0}">
											<label>Image <span class="text-red">*</span></label>
											 <s:if test="%{!parentsBean.ProfilePicture.equals('')}">
											 <img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{parentsBean.ProfilePicture}'/>"
													style="height: 100px; width: 100px; margin-top: 0px; border-radius: 100%;" title="Choose Image To Update" id="chparimg"/>
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" /></div>
										 	<s:file name="PhotoFile1" id="parimg" accept="image/*" onchange="handleFileSelect(event,parimg)"></s:file>
											 </s:if>
									
											 <s:else>
											 <input type="file" name="PhotoFile1" accept="image/*" id="parimg-new"  required="required"/>
											 </s:else>
										 	</s:if>
												<s:else>
												
									 	<label for="Name">Photo<span class="text-red">*</span></label> <input
												type="file" name="PhotoFile" accept="image/*" 
												id="fileName" required>
												</s:else>
										</div>
										
							
									</div>
                               <s:hidden name="ParentId"  id="parentid" label="ParentId" value="%{parentsBean.ParentId}"></s:hidden>
 
									<div class="box-footer">
									<s:if test="%{parentsBean.ParentId>0}">
										 <s:submit value="Update" class="btn btn-primary"></s:submit> 
										</s:if>
										<s:else>
									 <s:submit value="Save" class="btn btn-primary" ></s:submit><s:token />
										</s:else>
									</div>
									<div id="loading">
    <img src="img/pro.png"  class="img-responsive img-center"/>
</div>
								</s:form> 
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>


	</div>

	

	
	
<script type="text/javascript">
$(document).ready(function(){  
 $("#loading").hide();
		 $(".active").removeClass();
	   	  $("#profiles").addClass("active");
	   	  $("#parents").addClass("active");
	   	
	   	  
	 	$("#parimg").hide();  
		$("#chparimg").click(function(){ 
			$("#parimg").click();
			
			
			
		});
		 
		$('#datasubmit').on('submit',function(e) {
			   $("#loading").hide();
			  $(this).attr("disabled", "disabled");
		        doWork(); 
			e.preventDefault();
		var parentid=$("#parentid").val();
	 
		if(parentid>0){
			var imagestring="<s:property value='%{parentsBean.ProfilePicture}'/>"
			if(imagestring==""){
				 var fileName1 = $("#parimg-new").val();
				var idxDot = fileName1.lastIndexOf(".") + 1;
                var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
                if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#parimg-new").parent().next(".validation").remove();
                }else{
                     $("#parimg-new").parent().next(".validation").remove();
		            	  $("#parimg-new").parent().after("<div class='validation text-right' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
                    return false;
                } 
			}else{
				var fileName = $("#parimg").val();
				var idxDot = fileName.lastIndexOf(".") + 1;
                var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
                if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#parimg").parent().next(".validation").remove();
                }else{
                	if(fileName==""){}else{
                     $("#parimg").parent().next(".validation").remove();
		            	  $("#parimg").parent().after("<div class='validation text-right' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
                    return false;
                } 
                }
			}
                  $("#datasubmit").attr("action","updateParentDetails");
              
               
                
				this.submit();
				
			}else{
				
		   var phone=$("#phonenumber").val();
		    $.ajax({
		    	 url:"checkparentphone?phonenumber="+phone,
		        type: 'POST',
		        data: { PhoneNumber: phone },
		        
		        context: this
		    }).done( function(data) {
		        	
		            if (data.msg== 'Exists') {
		            	 $("#loading").hide();
		            	  var fileName = document.getElementById("fileName").value;
		                  var idxDot = fileName.lastIndexOf(".") + 1;
		                  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
		                  if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#fileName").parent().next(".validation").remove();
		                  }else{
		                     $("#fileName").parent().next(".validation").remove();
		            	  $("#fileName").parent().after("<div class='validation text-right' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		                      return false;
		                  } 
		            	$("#phonenumber").parent().next(".validation").remove();
		            	  $("#phonenumber").parent().after("<div class='validation text-right' style='color:red;margin-bottom: 20px;'>Phone Number Exists</div>");
		               
		            } else { $("#phonenumber").parent().next(".validation").remove();
		            	  var fileName = document.getElementById("fileName").value;
		                  var idxDot = fileName.lastIndexOf(".") + 1;
		                  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
		                  if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#fileName").parent().next(".validation").remove();
		                  }else{
		                     $("#fileName").parent().next(".validation").remove();
		            	  $("#fileName").parent().after("<div class='validation text-right' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
		                      return false;
		                  } 
		            	$("#datasubmit").attr("action","saveParentDetailsAction");
		                this.submit();
		            } 
		        });
			}
		function doWork() {
		   $("#loading").show();
		    setTimeout('$("#btn").removeAttr("disabled")', 1500);
		}
	});

});

</script>
<script type="text/javascript">
function handleFileSelect(evt,id) {
    
	  var files = evt.target.files;
	  for ( var i = 0, f; f = files[i]; i++) {

	   var reader = new FileReader();
	   reader.onload = (function(theFile) {
	    return function(e) {

	     $("#chparimg").attr("src", e.target.result);
	    };
	   })(f);
	   reader.readAsDataURL(f);

	  }

	 }
</script>
 
</body>
</html>
