<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
       
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<h4></h4>
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getRoutesList">Routes</s:a></li>
						<li class="active">View Route</li>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-border no-shadow">

					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
								<s:form action="editroute"  role="form" theme="simple">
									<div class="box-body">
									
									<dl class="dl-horizontal">
										<dt>	RouteId:</dt><dd>
											<s:property value="%{routebean.RouteId}"/></dd>
									
										<dt>SchoolId:</dt><dd>
											<s:property value="%{routebean.SchoolId}"/> 
										</dd>

											<dt>RouteName:</dt>

											<dd><s:property value="%{routebean.RouteName}"/></dd>
										<dt>StartPoint:</dt><dd>
											<s:property value="%{routebean.StartPoint}"/>
										</dd>
											<dt>EndPoint:</dt>
											<dd><s:property value="%{routebean.EndPoint}"/></dd>
										
										</dl>
						

										<div class="box-footer">
											
							  
							   
			<input type="hidden" name="routeid" value="<s:property value='%{routebean.RouteId}'/>"/>
				   <input type="submit" value="Edit" />
							   
											</div>
									</div>

									
								</s:form>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>

	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
		<script>
      $(function () {
    	  $('#example1').DataTable();
       
      });
      $(".active").removeClass();
	  $("#logistics").addClass("active");
	  $("#routes").addClass("active");
    </script>
</body>
</html>
