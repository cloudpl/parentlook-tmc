<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				
				<h4>Approve Pickuppoints</h4>
		
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i>Home</s:a></li>
						</ol>
	         </section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow">
							<!-- /.box-header -->
							<div class="box-body">
											
											
											
											
						<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Student Id</th>
											<th>Name</th>
											<th>School Name</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="studentList">
											<tr>
												<td><s:property value="StudentId" /></td>
												<td><s:property value="StudentName" /></td>
												<td><s:property value="schoolName" /></td>
												<td><s:a action="approveStudent">Approve
													<s:param name="StudentId" value="%{StudentId}" />
														<s:param name="SchoolId" value="%{SchoolId}" />
														<s:param name="StudentName" value="%{StudentName}" />
														<s:param name="schoolName" value="%{schoolName}" />
														<s:param name="BusId" value="%{BusId}" />


													</s:a></td>
											</tr>


										</s:iterator>
									</tbody>
									<tfoot>
										<tr>
											<th>Student Id</th>
											<th>Name</th>
											<th>School Name</th>
											<th>Action</th>
										</tr>
									</tfoot>
								</table>
								
											
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>
	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<script>
      $(function () {
    	  $('#example1').DataTable();
     
      });
      $(document).ready(function(){
    	  $(".active").removeClass();
    	  $("#stop").addClass("active");
    	  $("#pick").addClass("active");
      });
    </script>
</body>
</html>
