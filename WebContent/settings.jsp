<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>
	 <script>
$(document).ready(function(){
	
$(".sidebar-menu").find(".active").removeClass();
var ta= $("#tabcou").val();
 if(ta=="2"){
	 $("#password,#lipass").removeClass("active");
	 $("#profile,#lipro").addClass("active");
 }
 $("#settings").addClass("active");
 $("#schimg").hide();  
 
	$("#chschimg").click(function(){ 
		$("#schimg").click();
		
	});
	
	
	$("#newPwd").change(function(){
		
		var uname=$(this).val();
		
		$.ajax({
			url:"checkAdminPwd",
		    type:'POST',
		    data:$('form').serializeArray(),
		    cache:false,
		    success:function(data){
		    	
		    	 var ms=data.msg;
		      	  if(ms=="Exists")
		      	  {
		      		 $("#errId").parent().next(".validation").remove();
		      		$("#errId").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>OLDPASSWORD AND NEWPASWORD ARE SAME</div>");
		      		  
		      	  }else{
		      		  
		      		  
		      		  $("#errId").parent().next(".validation").remove();
		      	  }
		      	  
		         		      	  
		         },
		         
		    error:function(){
		    	alert("error");
		    }
		    
		});
		
	});
	
});
</script> 
<script type="text/javascript">
function handleFileSelect(evt,id) {
      
         
        
         
  var files = evt.target.files;
  for ( var i = 0, f; f = files[i]; i++) {

   var reader = new FileReader();
   reader.onload = (function(theFile) {
    return function(e) {

     $("#chschimg").attr("src", e.target.result);
    };
   })(f);
   reader.readAsDataURL(f);

  }

 }

</script>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			
			
			
			
			
			
			<section class="content">
			<div class="col-md-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active" id="lipass"><a href="#password" data-toggle="tab">Change Password</a></li>
                  <li id="lipro"><a href="#profile" data-toggle="tab">Manage Profile</a></li>
                   
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="password">
                  <div class="box no-border no-shadow">
							
							<div class="box-body">
						
						<div class="row">
						<div class="col-md-3"></div>
							<div class="col-md-6">
									  <s:actionerror/>
                                 <s:form action="savePassword" role="form">
									<div class="box-body">
										
																		
										<div class="form-group">
											<label for="OldPassword">Old Password<span class="text-red">*</span></label> <input type="password"
												name="OldPasswordVal" class="form-control" style="width:400px;" placeholder="Enter Old Password"
												id="pass" required>
												<span class="text-red"><s:property value="msg"/></span>
										</div>
										<div class="form-group">
											<label  for="NewPassword">New Password<span class="text-red">*</span></label> <input
												type="password" name="NewPassword" class="form-control" style="width:400px;" placeholder="Enter New Password"
												id="newPwd" required>
												<div id="errId"></div>
													</div>
										
                                      <div class="form-group">
											<label for="ConfirmPassword">Confirm Password<span class="text-red">*</span></label> <input
												type="password" name="ConfirmPassword" class="form-control"  style="width:400px;" placeholder="Enter Confirm Password"
												id="exampleInputPassword1" required>
										</div>
                                       </div>
										

									<div class="box-footer">
										<input type="submit" class="btn btn-primary" value="Save" />
									</div>
								</s:form>
								<s:hidden name="SchoolId" label="SchoolId" id="SchoolId"></s:hidden>
							</div>
						</div>
					</div>
							 
						</div>
                  
                  </div>
                  <div class="tab-pane" id="profile">
                  <div class="box no-border no-shadow">
							
							<!-- /.box-header -->
							<div class="box-body">
						<div class="row">
						<div class="col-md-3"></div>
							<div class="col-md-6">
								<s:form action="updateProfileAction" role="form"  theme="simple" enctype="multipart/form-data" method="post">
									<div class="box-body">
										<div class="form-group">
											<label>SchoolName: </label>
											<div class="form-inline">
											<s:textfield cssClass="form-control" cssStyle="width:400px;" name="SchoolName"  value="%{schoolBean.SchoolName}"> </s:textfield>
										</div></div>
											<div class="form-group">
											<label>Image </label>
											<img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value='%{#session.image}'/>"
													style="height: 100px; width: 100px; margin-top: 0px; border-radius: 100%;" title="Choose Image To Update" id="chschimg"/>
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
										 	<s:file name="Photo2" id="schimg" accept="image/*" onchange="handleFileSelect(event,schimg)"></s:file> 
											</div>
										
										
									</div>

									<div class="box-footer">
									
									<s:submit value="Update"></s:submit>
								<s:submit action="getbuseslistbyId" class="btn btn-primary" value="Cancel"></s:submit>
					
							
										
									</div>
									
									
								</s:form>
								<s:hidden id="tabcou" name="tab" value="%{tab}"></s:hidden>
								
						</div>
					</div>
				</div>
						</div>
                  </div>
                   
			</div>
			</div>
			</div>
			
				</section>
				
			
			
			
			
			
			
			
			
			
			
			 
		</div>

	</div>

	
	 
	
	
	
	
	
	
</body>
</html>