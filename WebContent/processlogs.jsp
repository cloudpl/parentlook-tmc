<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp"%>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content-header">
				<h4>ProcessLogs</h4>

			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow">

							<div class="box-body">

								<table>

									<s:iterator value="routesandbusesmap">
										<tr>
										<td>
                                        <b> RuteId:<s:property value="route"/></b>
											<b><s:property value="key" /></b></td>
										</tr>

										<td><s:iterator value="%{value}">

												<table border="1">
													<tr width="500px">
													<td width="200px">Bus:<s:property value="BusId" /></td>
                                                      <td width="200px">
                                                       <form action="excelProcess" method="post">
													
                                                   <s:hidden name="busId" value="%{BusId}"></s:hidden>
														<button type="submit"  style="float: right;">Process</button></form></td>
													</tr>
												</table>

											</s:iterator></td>

									</s:iterator>
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>


	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script src="js/app.min.js"></script>

	<script>
		$(function() {
			$('#example1').DataTable();
			
		});
		 $(document).ready(function(){
	    	  $(".active").removeClass();
	    	
	    	  $("#profilelogs").addClass("active");
	      });
	</script>
</body>
</html>
