<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map
      {
      height:350px;
      width:800px;
      }
       #busesdata {
       display: block;
       }
     /*  #busesdata span{
      font-size: 15px;
      cursor: pointer;
      }
      #busesdata div{
       display: inline-block;
       cursor: pointer;
      } */
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY"></script> 
	

    <script type="text/javascript">
    
    var getUrlParameter = function getUrlParameter(sParam) {
   	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
   	        sURLVariables = sPageURL.split('&'),
   	        sParameterName,
     	        i;

   	    for (i = 0; i <sURLVariables.length; i++) {
   	        sParameterName = sURLVariables[i].split('=');

   	        if (sParameterName[0] === sParam) {
   	            return sParameterName[1] === undefined ? true : sParameterName[1];
   	        }
   	    }
   	    
   	};
   function get_routedetails()
   {
	  $.ajax({
		  
		  url:"getBusdetails",
		  type:"post",
		   catche:true,
           success: function(data)
	           {
		     
        	   
	
		      var buseslist=data.buseslist;
          	var routelist=data.routelist;
                var st;

               	for(var i=0;i<routelist.length;i++)
          		{
          		var route=routelist[i];
          
       		  $('#allroutes').append('<option value = "'+route.routeId+'">'+route.routeName+'</option>');
        		  
        		}
       
          		for(var j=0;j<buseslist.length;j++)
          			{
                  var busdetail=buseslist[j];
                  
                  st+="<tr><td>"+busdetail.routeId +"</td>"+
                       "<td>"+busdetail.busId+"</td>"+
                      "<td>"+busdetail.activities+"</td>"+
                       "<td>"+busdetail.pickup+"</td>"+
                       "<td>"+busdetail.dropStatus+"</td></tr>";
	           }
          	
          	$('#tdata').append(st);
	           }
	  });
	   
   }
    
    
    var ivid=0;
    var count=0;
    $(document).ready(function()
    		
   {
    	$(".active").removeClass();
    	 $("#monitor").addClass("active");
   
    	get_routedetails();
    	
    	
    	getBusesForRoutesonMap();
    	$("#allroutes").change(function(){
    		 count=1;
    		
    		 
    
    			getBusesForRoutesonMap();
    			
    	});
    	
    	
    	
    });
    
    </script>
    <script type='text/javascript'>
function reload(busid){
var video = document.getElementById(busid);
var sr=document.getElementById(busid).src;
var rand=Math.random();
		  
 var url="https://www.portal.parentlook.com/livestreaming/Bus"+busid+"/live.mp4?rand="+rand;
			 //video.src = sr+"?rand="+rand;
			 //document.getElementById(busid).src=sr+"?rand="+rand;
			 document.getElementById(busid).src=url;
  // video.play();
}
</script>
    <script type="text/javascript">
    
   
    function	getBusesForRoutesonMap(){
    	var rId=$("#allroutes").val();
    	var rText=$("#allroutes option:selected").text();
    	$("#dynaroute").html(rText);
    	//var count=localStorage.getItem("count");
    	
  		$.ajax({
	    	 url:"getBusesANdGpsForRoute?routeId="+rId, 
			   catche:false,
	            success: function(data)
		           {
	            	var schoolname=data.schoolname;
	            	var latitude=data.latitude;   
	     		     var longitude=data.longitude;
	  				if(ivid==0 || count==1){
	  					
	     		    $("#map").html("");
		            	var coordinatelist=data.busDetailBeans;
		                  for(var i=0;i<coordinatelist.length;i++)
		    	      {
		        	   
		              var coordinate=coordinatelist[i];
		              var bhId=coordinate.busId;
		              var routename=coordinate.routeName;
		             var pick=coordinate.busPick;
		             var bName=coordinate.busName;
	  					 var drop=coordinate.busDrop;
	  					 var surveillance=coordinate.surveillance;
	  					 if(pick==1 || drop==1 ){
	  						 if(surveillance==1){
	  						 
	  				     $("#map").append("<video  id='"+bhId+"' onended='reload(this.id)'   class='video'    controls autoplay  style='width:135px; padding:5px;'title='  Bus-"+bName+"  RouteName-"+routename+"' ><img src='img/logo.png'/><source    src=' https://www.portal.parentlook.com/livestreaming/Bus"+bhId+"/live.mp4'  type='video/mp4'></source></video>");
	  						 }
			        
	  					 }
	  					 
	  					 
	  				 }
		                 
	  		var vidCount=$(".video").length;
	  		if(vidCount==0){
	  			 $("#map").append("No Active Buses To Show.");
	  			
	  		}
		               
		    	
  		
		           
		           
		                 /*  $("#busesdata").html("");
			  				 
			  				 var busbean=data.busDetailBeans;
			  				
			  				 for(var k=0;k<busbean.length;k++){
			  					 var bBean=busbean[k];
			  					 var str="<li><a href='#'>";
			  					 var str1="";
			  					 var str2="";
			  			
			  					
			  					 
			  					 if(bBean.busPick==0){
			  						 str1="<i class='fa fa-circle text-gray' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-video-camera margin-r-5 text-gray'></i>"+bBean.busName;
			  		                
			  					 }else if(bBean.busPick==1){
			  						 
			  						str1="<i class='fa fa-cog fa-spin text-blue' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-video-camera margin-r-5 text-blue text-wrap'></i>"+bBean.busName;
			  					 }else{
			  						 
			  						str1="<i class='fa fa-circle text-orange' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-video-camera margin-r-5 text-orange'></i>"+bBean.busName;
			  					 }
			  					 
			  					if(bBean.busDrop==0){
			  						 
			  						str2="<i class='fa fa-video-camera margin-r-5 text-gray'></i>"+bBean.busName+"<i class='fa fa-circle text-gray' style='float:right;margin-right:-62px'></i>";
			  					 }else if(bBean.busDrop==1){
			  						str2="<i class='fa fa-video-camera margin-r-5 text-blue'></i>"+bBean.busName+"<i class='fa fa-cog fa-spin text-blue' style='float:right;margin-right:-62px'></i>"; 
			  						 
			  					 }else{
			  						 
			  						str2="<i class='fa fa-video-camera margin-r-5 text-green'></i>"+bBean.busName+"<i class='fa fa-circle text-green' style='float:right;margin-right:-62px'></i>";
			  					 }
			  					 $("#busesdata").append("<li style='float:none;' class='row no-margin no-padding'><a href='#' style='height:auto; padding:0 15px;'><span class='pull-left col-md-6'>"+str1+"</span><span class='pull-right col-md-6'>"+str2+"</span></a></li>");
			  					 
			  					 
			  					 
			  					 
			  					 
			  					 
			  				 }
	 */  		       
	  				}ivid++;count++;
	  			  $("#busesdata").html("");
	  				 
	  				 var busbean=data.busDetailBeans;
	  				 var fData="<div class='table-responsive' style='height:60vh;'><table class='table table-striped no-margin'><tr> <th>Bus Name</th><th>Trip Status</th><th>Camera</th></tr>";
	  				 for(var k=0;k<busbean.length;k++){
	  					 var bBean=busbean[k];
	  					 var str1=bBean.tripStatus;
	  					 var str2="";
 
	  					/*  if(bBean.busPick==0){
	  						 str1="<i class='fa fa-circle text-gray' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-video-camera margin-r-5 text-gray'></i>"+bBean.busName;
	  		                
	  					 }else if(bBean.busPick==1){
	  						 
	  						str1="<i class='fa fa-cog fa-spin text-blue' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-video-camera margin-r-5 text-blue text-wrap'></i>"+bBean.busName;
	  					 }else{
	  						 
	  						str1="<i class='fa fa-circle text-orange' style='margin-right:20px;margin-left:-48px'></i><i class='fa fa-video-camera margin-r-5 text-orange'></i>"+bBean.busName;
	  					 }
	  					 
	  					if(bBean.busDrop==0){
	  						 
	  						str2="<i class='fa fa-video-camera margin-r-5 text-gray'></i>"+bBean.busName+"<i class='fa fa-circle text-gray' style='float:right;margin-right:-62px'></i>";
	  					 }else if(bBean.busDrop==1){
	  						str2="<i class='fa fa-video-camera margin-r-5 text-blue'></i>"+bBean.busName+"<i class='fa fa-cog fa-spin text-blue' style='float:right;margin-right:-62px'></i>"; 
	  						 
	  					 }else{
	  						 
	  						str2="<i class='fa fa-video-camera margin-r-5 text-green'></i>"+bBean.busName+"<i class='fa fa-circle text-green' style='float:right;margin-right:-62px'></i>";
	  					 } */
	  					/*  $("#busesdata").append("<li style='float:none;' class='row no-margin no-padding'><a href='#' style='height:auto; padding:0 15px;'><span class='pull-left col-md-6'>"+str1+"</span><span class='pull-right col-md-6'>"+str2+"</span></a></li>"); */
	  					 if(bBean.surveillance==0){
	  						 
	  						 str2="<i class='fa fa-video-camera margin-r-5 text-gray'></i>";
	  					 }else{
	  						str2="<i class='fa fa-video-camera margin-r-5 text-green'></i>";
	  						
	  					 }
	  					 
	  					 /* $("#busesdata").append("<li style='float:none;' class='row no-margin no-padding'><a href='#' style='height:auto; padding:0 15px;'><span class='pull-left col-md-4'>"+bBean.busName+"</span><span class='pull-right col-md-4'>"+str2+"</span><span class='pull-right col-md-4'>"+str1+"</span></a></li>"); */
	  					 
	  					 fData=fData+'<tr><td>'+bBean.busName+'</td><td>'+str1+'</td><td>'+str2+'</td></tr>'; 
	  				 }
  					 $("#busesdata").append(fData+"</table></div>");

			}
	    });
  	}
  setInterval(getBusesForRoutesonMap,2000	);
    
    
    </script>

	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
 		<div class="content-wrapper">
			<section class="content-header">
				 
			</section>
			 
			 
			 
			 <section class="content">
	<div class="row margin">
	<div class="col-md-6">
	<div class="box no-shadow no-border flat">
                    <div class="box-header">
					<div class="form-group col-md-8 no-padding no-margin">
                 <select class="form-control form-control-downarrow select2" id="allroutes" style="width: 100%;">
				 <option value="0">All Routes</option>
                </select>
				 
              </div>
					</div>
                    <div class="box-body text-left" id="map" style="overflow:auto;height:60vh;direction:rtl;">
                       
					  			  
                    </div>
					</div>
	</div>
	<div class="col-md-6">
	<div class="box no-shadow no-border flat">
	<div class="box-header">
	<ul>
			   <li class="time-label" style="margin-bottom:0px; list-style:none;">
                        <span class="text-blue">
                         <i class="fa fa-bus margin-r-5 text-orange"></i> <span id="dynaroute">All Routes</span>
                        </span>
                  </li></ul>
                  <!-- <div class="pad">
      <div class="col-md-4 no-padding"><i class="fa fa-video-camera fa-spin text-blue margin-r-5"></i> Bus Name</div>
      <div class="col-md-4 no-padding"><i class="fa fa-video-camera text-gray margin-r-5"></i> Trip Status</div>
      <div class="col-md-4 no-padding"><i class="fa fa-video-camera text-green margin-r-5"></i>Camera</div>
                      </div> -->
				  </div>
		 <div class="box-body"   id="busesdata">
				<!-- <ul class="timeline" style="border:3px dotted gray;">
                <li>
				
				<div class="timeline-item" style="z-index:999; margin-left:10px;">
				<ul class="nav nav-pills" id="busesdata">
                 
                 </ul>
                 
				</div>
			  </li>
               </ul> -->
                </div>
          </div>
	</div>
	
		
		
		
		</div>
		
		
		</section>
			 
		 


	</div>


	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<script>
      $(function () {
    	  $('#example1').DataTable();
       });
    </script>


    			<s:hidden value="%{Latitude}" id="latitude"></s:hidden>
				<s:hidden value="%{Longitude}" id="longitude"></s:hidden>
				<s:hidden value="%{schoolname}" id="schoolname"></s:hidden>
</body>
</html>
