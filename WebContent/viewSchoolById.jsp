<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<h4>Schools</h4>
				<h5>
					<s:a action="addSchool">+ Add New School</s:a>
					
				</h5>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><a href="#">Schools</a></li>
					<!--  <li class="active">Add New School</li> -->
				</ol>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Data Table With Full Features</h3>
							</div>
							
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
				                 <s:iterator value="detailsBean">
											
											<div class="form-group">
											<label>School ID:</label> 
											 <s:property value="SchoolId" />
										</div>
											
									
									
									
										<div class="form-group">
											<label>School Name:</label> 
											 <s:property value="SchoolName" />
										</div>
											
											
                               <div class="form-group">
											<label>Email:</label> 
											 <s:property value="Email" />
										</div>
                            
                                   			
                               <div class="form-group">
											<label>ContactNumber</label> 
											 <s:property value="ContactNumber" />
										</div>
                                   <div class="form-group">
											<label>School Address:</label> 
											 <s:property value="Address" />
										</div>
                                   
                                   
                                   
                                     
                                   
                                 <div class="form-group">  <s:a action="editSchool"><h4>Edit</h4><s:param name="idd"><s:property value="SchoolId"/></s:param></s:a></div>
                                   
                                 
                                   
                              
												

																			</s:iterator>
									</tbody>
									
								</table>
							</div>
						
						</div>
					
					</div>
				</div>

			</section>
		</div>


		 
		
		<%@ include file="footer.jsp" %>

		<aside class="control-sidebar control-sidebar-dark">
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>

				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane" id="control-sidebar-home-tab">
					<h3 class="control-sidebar-heading">Recent Activity</h3>

				</div>
				<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab
					Content</div>
				<div class="tab-pane" id="control-sidebar-settings-tab">
					<form method="post">
						<h3 class="control-sidebar-heading">General Settings</h3>
						<div class="form-group">
							<label class="control-sidebar-subheading"> Report panel
								usage <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Some information about this general settings option</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Allow mail
								redirect <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Other sets of options are available</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Expose author
								name in posts <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Allow the user to show his name in blog posts</p>
						</div>

						<h3 class="control-sidebar-heading">Chat Settings</h3>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Show me as
								online <input type="checkbox" class="pull-right" checked>
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Turn off
								notifications <input type="checkbox" class="pull-right">
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Delete chat
								history <a href="javascript::;" class="text-red pull-right"><i
									class="fa fa-trash-o"></i></a>
							</label>
						</div>
					</form>
				</div>
			</div>
		</aside>
		
		<div class="control-sidebar-bg"></div>
	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<script>
      $(function () {
    	  $('#example1').DataTable();
        
      });
    </script>
</body>
</html>
