<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">

<link rel="stylesheet" href="css/font-awesome.min.css">

<link rel="stylesheet" href="css/ionicons.min.css">

<link rel="stylesheet" href="css/custom.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<link rel="stylesheet" href="css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	
	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY&libraries=places"
        async defer></script>
 <style type="text/css">
	
	#pac-input{
    height:25px;
    width:300px;
}
	
	</style>

</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content margin">
			<div class="row">
			<div class="col-md-6">
			<h3 class="text-blue">Set Drop-Off Point</h3>
					<div class="form-group">
					<s:hidden value="%{busi}" id="busidd"></s:hidden>
					<s:if test="%{busi>0}">
					<div><label>Route Name : </label><s:property value="%{routename}"/></div>
					<div><label>Bus Name : </label><s:property value="%{busname}"/></div>
					<div><label>Pickup Point : </label><s:property value="%{pickaddr}"/></div>
					</s:if><s:else>
									<select id="root">
										<option>Select Route</option>
										<s:iterator value="rootsBeans"> 
											<option value='<s:property value="RouteId" />'><s:property
													value="RouteName" /></option>



										</s:iterator>
									</select></s:else>
									</div>
									
								<div class="form-group">	
									<div id="rootbusses"></div>
									</div>
			</div>
			<div class="col-md-5 margin pad">
			<div class="row">
			<span class="text-blue"><b><s:property value="viewBean2.StudentName" /></b></span>
			<span class="text-blue"> - <s:property value="viewBean2.StudentClass" /></span>
			</div>
			<div class="row"><s:property value="viewBean2.Location" /></div>
			</div>
			</div>
			
			<div class="box box-solid">
   <div class="box no-border no-shadow" id="approve">
   <div class="box-header">
   <div class="text-blue"><i class="fa fa-check margin-r-5 text-green" ></i>Choose from Near by Drop-Off Points</div>
              <div class="box-tools pull-right">
      <button class="btn btn-box-tool text-blue" data-widget="collapse"><i class="fa fa-minus"></i></button>
     </div>  
            </div><!-- /.box-header -->  
  <div class="box-body no-padding">
  <div class="col-md-9">
                    <div class="form-group">
                    
                    <div id="errDrop"></div>
                    <s:form action="approveDrop" id="approveDrop">
										<s:hidden name="StudentId" id="stuid" value="%{StudentId}"></s:hidden>
										<div id="rootPickups"></div>

<button type="submit" class="btn btn-success" id="app">Approve Drop-Off Point</button>
										 						
									</s:form>
                    
                    
                      
                    </div>
     <div class="box-footer" align="center">
     
   </div>
   </div>
   </div>
   <div align="center" class="text-green h3">----- <b>or</b> -----</div>
   </div>
   
   <div class="box no-border no-shadow" id="new">
   <div class="box-header">
   <div class="text-blue"><i class="fa fa-plus margin-r-5 text-green"  id="addPickup"></i>Setup a New Drop-Off Point</div>  
            </div><!-- /.box-header -->  
  <div class="box-body no-padding">
  <div class="col-md-9">
  
  		<div id="mapdetails">
								<input id="pac-input" class="controls" type="text"
        placeholder="Enter a location">
                  <div class='img-responsive' id="map" style="height:300px;width:100%;"  ></div>
    <div class="description pad">
    
    				 <i class="fa fa-map-marker margin-r-5 text-orange" id="latlang"></i>
                  <i class="fa fa-map-marker margin-r-5 text-blue" id="waypoint"></i>
                  
                 
							</div>
							
							</div>
							<!--action="addDropPoint"  -->
							<s:form id="formId" >
										<s:hidden name="StudentId" value="%{StudentId}"></s:hidden>
										 
									<s:hidden name="SchoolId" value="%{SchoolId}"></s:hidden>
									<s:hidden name="RouteId" value="%{RouteId}" id="rid"></s:hidden>
									<s:hidden name="BusId" value="%{busi}" id="bid" ></s:hidden>
									<s:hidden name="PickupPoint" value="%{wayPoint}" id="pid"></s:hidden>
									<s:hidden name="PLatLang" value="%{PLatLang}" id="platLang"></s:hidden>
										<s:hidden name="direction" value="1"></s:hidden>
									 

									 
											<input type="submit" class="btn btn-success" value="Set Drop-Off Point" id="addPickup"/>				
									</s:form>
  
  
  
   </div>
   </div>
   </div>
   </div>
			
			
			<s:hidden name="origin"   id="origin-input"></s:hidden>
    <s:hidden name="destination" value="%{wayPoint}" id="destination-input"></s:hidden>
    <s:hidden name="latLang" value="%{latLang}" id="waylat"></s:hidden>
    
			
			
			
			
			
			 </section>
		</div>


		

		<aside class="control-sidebar control-sidebar-dark">
			<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
				<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i
						class="fa fa-home"></i></a></li>

				<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i
						class="fa fa-gears"></i></a></li>
			</ul>
			<div class="tab-content">
				<!-- Home tab content -->
				<div class="tab-pane" id="control-sidebar-home-tab">
					<h3 class="control-sidebar-heading">Recent Activity</h3>

				</div>
				<div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab
					Content</div>
				<div class="tab-pane" id="control-sidebar-settings-tab">
					<form method="post">
						<h3 class="control-sidebar-heading">General Settings</h3>
						<div class="form-group">
							<label class="control-sidebar-subheading"> Report panel
								usage <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Some information about this general settings option</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Allow mail
								redirect <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Other sets of options are available</p>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Expose author
								name in posts <input type="checkbox" class="pull-right" checked>
							</label>
							<p>Allow the user to show his name in blog posts</p>
						</div>

						<h3 class="control-sidebar-heading">Chat Settings</h3>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Show me as
								online <input type="checkbox" class="pull-right" checked>
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Turn off
								notifications <input type="checkbox" class="pull-right">
							</label>
						</div>

						<div class="form-group">
							<label class="control-sidebar-subheading"> Delete chat
								history <a href="javascript::;" class="text-red pull-right"><i
									class="fa fa-trash-o"></i></a>
							</label>
						</div>
					</form>
					
					 

      
					
				</div>
			</div>
		</aside>
		
		<div class="control-sidebar-bg"></div>
	</div>

	

	
	<script>
      var map;
      var palcename;
      var marker;
      var options = {
    		  enableHighAccuracy: true,
    		  timeout: 10000,
    		  maximumAge: 0
    		};
      
      function initMap() {
    	
  var input = document.getElementById('pac-input');
    	  
    	  if (navigator.geolocation) {
    		  navigator.geolocation.getCurrentPosition(showPosition,showError,options);
    	    } 
          

          
    	  


function getPlaceDetailsByLatLang(myLatLang){




    var geocoder;
	  geocoder = new google.maps.Geocoder();
	   

	  geocoder.geocode(
	      {'latLng': myLatLang}, 
	      function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	                  if (results[0]) {
	                      var add= results[0].formatted_address ;
	                      var  value=add.split(",");

	                   var	   count=value.length;
	                  var    country=value[count-1];
	                    var  state=value[count-2];
	                    var  palcename=value[count-3];
							
					$("#latlang").html("&nbsp;&nbsp;Latitude,Longitude:"+myLatLang);
					$("#waypoint").html("&nbsp;&nbsp;"+value);
					$("#platLang").attr("value",myLatLang)	;
					document.getElementById('pac-input').value = value;
					 
					$("#pid").attr("value",value);
					
					 
	                      
	                  }
	                  else  {
	                      alert("address not found");
	                  }
	          }
	           else {
	              alert("Geocoder failed due to: " + status);
	          }
	      }
	  );

	
}

function showPosition(position) {

 
	
	  
  var a=$("#waylat").val();
  var ab=a.substring(1, a.length-1);
  var aa=ab.split(",");
  var  myLatlng = new google.maps.LatLng(aa[0],aa[1]);
	  
    var mapOptions = {
      zoom: 14,
      styles: [{
	  	       // featureType: 'poi.business',
	  	       featureType: 'all',
	  	        elementType: 'labels',
	  	        stylers: [{
	  	            visibility: 'on'
	  	        }]
	  	    }],
      center: myLatlng,
      fullscreenControl: true,
      disableDefaultUI: true,
      mapTypeControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_RIGHT
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	    map = new google.maps.Map(document.getElementById("map"),
	          mapOptions);

	    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	 

	
      marker=new google.maps.Marker({position:myLatlng,map:map,title:"Set Way Point",draggable:true,});

    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
      


        getPlaceDetailsByLatLang(event.latLng);


    });
	
}




     
     
     

     var autocomplete = new google.maps.places.Autocomplete(input);


		autocomplete.addListener('place_changed', function() {
    
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return; 
    }

   var  myLatlng =  place.geometry.location;
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
       } else {  map.setCenter(place.geometry.location);
    }
    marker.setPosition(myLatlng);

    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
         getPlaceDetailsByLatLang(event.latLng);


    });
    
  });

          
      }
      
      function showError(error) {
    	 
    	 
    	  if(error.code == 0){
    	        // unknown error
    	       
    	       
    	        alert(" Your Browser not supported for GeoLocation Access.")
    	      location.reload(true);
    	        }  else if(error.code == 1) {
    	        // permission denied
    	      alert("The acquisition of the geolocation information failed because the page didn't have the permission to do it.Please allow location information for this website.")
    	   location.reload(true); 
    	        } else if(error.code == 2) {
    	        // position unavailable
    	        alert("The acquisition of the geolocation failed because at least one internal source of position returned an internal error.Please try again.");
    	      location.reload(true);
    	        }  else if(error.code == 3){
    	        	
    	        	alert("The time allowed to acquire the geolocation, defined by PositionOptions.timeout information was reached before the information was obtained.Please try again.");
    	        	 location.reload(true);
    	        }
    	}
    </script>
	
	
	<script>


	$(document)
			.ready(
					function() {
						 
						//initMap();
						$("#approve,#new").hide();
						
						
				

				var pNames=[];
				var pIds=[];
				var pDist=[];
				var pDur=[];

				var bid=$("#busidd").val();
				if(bid!=0){
					 
								$("#approve,#new").hide();
								$("#addPickup").show();
								 
								 $("#root").hide();

								var url = "getPickupPoints?busId="+bid+"&direction=1";
								 
								$.ajax({
											url : url,
											type : 'get',
											cache : true,
											success : function(data) {

												 

												app = "";
												var directionsService = new google.maps.DirectionsService;
												
												for (var i = 0; i < data.pickupPoints.length; i++) {

													 
													  
													  
													 
											         
												        var origin_input = data.pickupPoints[i].pickupPoint;
												        pNames.push(origin_input);
												        var picId=data.pickupPoints[i].pickupPointId;
												        pIds.push(picId);
												        var destination_input = document.getElementById('destination-input').value;
												        var modes = document.getElementById('mode-selector');
												 
												 
												       
												          directionsService.route({
												            origin: origin_input,
												            destination: destination_input,
												            travelMode: "DRIVING"
												          }, function(response, status) {
												            if (status === google.maps.DirectionsStatus.OK) {
												                
												                 
												            	pDist.push(response.routes[0].legs[0].distance.text);
												            	pDur.push(response.routes[0].legs[0].duration.text);
													
												              
												            } else {
												              window.alert('Directions request failed due to ' + status);
												            }
												          });
												        		 
													 
													 
													 
													 
													 
												}
												
							//buildData();
							$("#rootPickups").html("");
							 setTimeout(function() {
						    }, 1000);
							
							
								initMap();	
								  buildData();

								  $("#approve,#new").show();	
											},
											error : function() {
												alert("error");
											}
										});

						 
							


					

					}else{  

						$("#root")
						.change(
								function() {
									 
									$("#approve,#new").hide();
									var rootid = $(this).val();
									var url = "getBussesForRoot?rootId="
											+ rootid;
									var ap = "";
									$
											.ajax({
												url : url,
												type : 'get',
												cache : true,
												success : function(data) {

													  
													var len = data.bussesBeans.length;
													ap = "<select id='busId'><option>Select Bus</option>";
													for (var i = 0; i < len; i++) {

														ap = ap
																+ "<option value="+data.bussesBeans[i].busId+">"
																+ data.bussesBeans[i].busName
																+ "</option>";
													}
													ap = ap
															+ "</select>";

													$("#rootbusses")
															.html(ap);
												
												},
												error : function() {
													alert("error");
												}
											});

								});




						
						
						
								$("body").delegate(
												"#busId",
												"change",
												function() {
													$("#approve,#new").hide();
													$("#addPickup").show();
													var busid = $(this).val();

													var url = "getPickupPoints?busId="+
														busid+"&direction=1";
													 
													$
															.ajax({
																url : url,
																type : 'get',
																cache : true,
																success : function(data) {

																	 

																	app = "";
																	 pNames=[];
																	var directionsService = new google.maps.DirectionsService;
																	for (var i = 0; i < data.pickupPoints.length; i++) {
		 
																	        var origin_input = data.pickupPoints[i].pickupPoint;
																	        pNames.push(origin_input);
																	        var picId=data.pickupPoints[i].pickupPointId;
																	        pIds.push(picId);
																	        var destination_input = document.getElementById('destination-input').value;
																	        var modes = document.getElementById('mode-selector');
																	 
																	 
																	       
																	          directionsService.route({
																	            origin: origin_input,
																	            destination: destination_input,
																	            travelMode: "DRIVING"
																	          }, function(response, status) {
																	            if (status === google.maps.DirectionsStatus.OK) {
																	                
																	                 
																	            	pDist.push(response.routes[0].legs[0].distance.text);
																	            	pDur.push(response.routes[0].legs[0].duration.text);
																		
																	              
																	            } else {
																	              window.alert('Directions request failed due to ' + status);
																	            }
																	          });
																	        		 
																		 
																		 
																		 
																		 
																		 
																	}
																	
												$("#rootPickups").html("");
												 setTimeout(function() {
											    }, 1000);
												

												 $("#bid").attr("value",$("#busId").val());
													$("#rid").attr("value",$("#root").val());
													initMap();	
													  buildData();

													  $("#approve,#new").show();	
																},
																error : function() {
																	alert("error");
																}
															});

												});



						}

				
				
				
						
						$("body")
						.delegate(
								".pickupPointId",
								"change",
								function() {
									$('input[type="submit"]').prop('disabled', false);
									
								});
						
						
						function buildData(){ 
							
                                if(pNames.length==0){
								
								$("#approveDrop").hide();
								$("#errDrop").html("No dropOff points Found Near to the location");
								
							}else{
								$("#errDrop").html("");
								$("#approveDrop").show();
								$("#rootPickups").html("");
							}
							
							for(var k=0;k<pNames.length;k++){ 
							 
								var inp="";
								if(k==0){
									
									inp="<input type='radio' class='pickupPointId' name='PickupPointId' value='"+pIds[k]+"' checked></input>";
								}else{
									
									inp="<input type='radio' class='pickupPointId' name='PickupPointId' value='"+pIds[k]+"'></input>";	
								}
								
								
								
								
								$("#rootPickups").append(inp
								+ pNames[k]
								+ "&nbsp;&nbsp;&nbsp; <span>Distance from WayPoint "+pDist[k] +"</span>,<span>Time To Reach "+pDur[k]+"<br/>");
								
							}
						}
						 
						
						
					});





	
</script>
	
	
	
		
	<script>
		$(function() {
			$('#example1').DataTable();
			
		});
		$(document).ready(function(){
	    	  $(".active").removeClass();
	    	  $("#stop").addClass("active");
	    	  $("#pending").addClass("active");
	    	  $("form").submit(function(){
	    		  var inputval=$("#platLang").val();
	    		  if(inputval==""){
	    			  initMap();
	    			  return false;
	    		  }else{
	    			  $("#formId").attr("action","addDropPoint");
		                this.submit();
	    		  }
	    		  
	    	  });
	      });
	</script>
</body>
</html>
