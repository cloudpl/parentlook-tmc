<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">

<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	 <style type="text/css">
	.img-responsive {
     
     position: absolute;
    left: 70%;
    top: 70%;
    background-color: white;
    z-index: 150;

    height: 100px;
    margin-top: -200px;

    width: 200px;
    margin-left: -180px;
 }
	
	</style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>


		 
		<!-- =============================================== -->
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h4 class="text-blue"></h4>
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getStudentsList">Students</s:a></li>
					<li class="active">Add New Student</li>
				</ol>
			</section>
			<section class="content">
				<div class="box box-default no-shadow no-border flat">
<div class="pad">All Fields Marked<span class="text-red">* </span>Mandatory</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
								<s:form  role="form" id="formId"  enctype="multipart/form-data" >
									 
										<div class="form-group">
											<label for="StudentFirstName">Student First  Name <span class="text-red">*</span></label> <input
												type="text" pattern="^[a-zA-Z ]+$" title="Allows Only Characters"  name="StudentFirstName" value='<s:property value='%{studentbean.StudentFirstName}'/>' class="form-control"
												id="exampleInputPassword1" required>
										</div>
											 
                                        
										<div class="form-group">
											<label for="StudentLastName">Student Last  Name <span class="text-red">*</span></label> <input
												type="text"  pattern="^[a-zA-Z]+$" title="Allows Only Characters"  name="StudentLastName" value='<s:property value='%{studentbean.StudentLastName}'/>' class="form-control"
												id="exampleInputPassword1" required>
										
											 
											<div class="form-group">
											<label for="ClassId">Class <span class="text-red">*</span></label></label> 
											
											
											<select  id="scId"  
												name="ClassId"  class="form-control select2" required="required">
												 <s:if test="%{studentbean.StudentId>0}">
												 <option value='<s:property value="%{studentbean.ClassId}"/>' selected="selected"><s:property value='%{classBean.StudentClass}'/>-<s:property value='%{classBean.Section}'/> </option>
												 </s:if>
												 <s:else>
												 </s:else>
												<s:iterator value="classeslist">
												<option value='<s:property value="ClassId"/>'><s:property value="StudentClass"/>-<s:property value="Section"/></option>												
												</s:iterator>
												
											</select> 
											 
										</div>			
											
											 
										<div class="form-group">
											<label for="RollNo">Roll No <span class="text-red">*</span></label> <input
												type="text" name="RollNo" class="form-control " value='<s:property value='%{studentbean.RollNo}'/>'
												id="rollno" required>
												
										</div>
											 
										
										<div class="form-group">
											 
											<s:hidden name="SchoolId" value="%{#session.schoolId}"></s:hidden>
											 
										 
										</div>

</div>

										<div class="form-group">
											<label for="ParentName">Parent Name <span class="text-red">*</span></label> 
												<s:if test="%{studentbean.StudentId>0}">
												 
												<s:property value="%{parentsBean2.FirstName}"/>&nbsp;<s:property value="%{parentsBean2.LastName}"/>
												<s:hidden name="ParentId" value="%{studentbean.ParentId}"/>
												
												 </s:if>
												 <s:else>
												  <select id="ParentId"  
												name="FirstName" class="form-control select2" required> 
												 
												 <s:iterator value="parentsList">
												<option value='<s:property value="ParentId"/>'><s:property value="FirstName"/> <s:property value="LastName"/> - <s:property value="MobileNumber"/> </option>												
												</s:iterator> </select></s:else>
												
												
												
										</div>
										<div id="error">
							
										</div>


										 <div class="form-group">
										 <s:if test="%{studentbean.StudentId>0}">
										 <label for="PhotoFile">Student Image<span class="text-red">*</span></label> 
										 <s:if test="%{!studentbean.ProfilePicture.equals('')}">
										 	<img   class="maxyId" id="imgchange"
													src="data:image/png;base64, <s:property value='%{studentbean.ProfilePicture}'/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													
												<s:file name="PhotoFile" id="filechange" accept="image/*" onchange="handleFileSelect(event,filechange)"/>	
										 </s:if>
										 <s:else>
										 <input type="file" name="PhotoFile" id="filechange_new" accept="image/*" onchange="handleFileSelect(event,filechange)" required="required"/>
										 </s:else>
										
							</s:if>
							<s:else>
							 <label for="PhotoFile">Student Image<span class="text-red">*</span></label>
							<input type="file" id="stuimage" name="PhotoFile" accept="image/*" required/>
							</s:else>
												</div>
												 
									
										
										
										

									
									<input type="hidden" id="studentid"  name="StudentId" value="<s:property value='%{studentbean.StudentId}'/>" >
                                    <s:if test="%{studentbean.StudentId>0}">
                                   	
									<div class="box-footer">
									<s:submit  value="Update" enctype="multipart/form-data"/>
										
										
									
									</div>
								</s:if>
								<s:else>
								<div class="box-footer">
								
								<s:submit  id="validation" value="Save"/>
									</div>
								</s:else>
							<div id="loading">
    <img src="img/pro.png"  class="img-responsive img-center"/>
</div>
		
								<s:hidden id="schoId" name="schoolId" value="%{#session.schoolId}"></s:hidden>
								</s:form>
							</div>
							
						</div>
					</div>

				</div>

			</section>
		</div>



 
	</div>



	<script>
      $(function () {
    	  $('#example1').DataTable();
        
      });
    </script>
    
    <script>
    $(document).ready(function(){
    	  $("#loading").hide();
    	 $(".active").removeClass();
     	  $("#profiles").addClass("active");
     	  $("#students").addClass("active");
     	  
    	$("#filechange").hide();
    	$("#imgchange").click(function(){
    		
    		$("#filechange").click();
    	});
    
  
  	  
    	  $('#formId').on('submit',function(e) {
    		  $("#loading").hide();
    		  $(this).attr("disabled", "disabled");
	 	        doWork();
  			e.preventDefault();
  			
  			
  			 var classid=$("#scId").val()
  	  		  var rollno=$("#rollno").val();
  	  		var school=$("#schoId").val()
  				var studentid=$("#studentid").val();
  					  $.ajax({
  						  url:"checkrollno?classId="+classid+"&rollno="+rollno+"&schoolId="+school,
  					        type: 'POST',
  					       
  					        context: this
  					    }).done( function(data) {
  					            if (data.actMsg== 'Exists') {
  					              $("#loading").hide();
  					            var bean=data.studentbean;
  					            if(bean.studentId==studentid){$("#rollno").parent().next(".validation").remove();
  					            	var imagestring="<s:property value='%{studentbean.ProfilePicture}'/>"
  					            
  					          			if(imagestring==""){ 
  					          				 var fileName1 = $("#filechange_new").val();
  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#filechange_new").parent().next(".validation").remove();
  					                          }else{
  					                             $("#filechange_new").parent().next(".validation").remove();
  						            $("#filechange_new").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
  					                              return false;
  					                          } 
  					          			}else {
  					          				var fileName = $("#filechange").val();
  					          				var idxDot = fileName.lastIndexOf(".") + 1;
  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#filechange").parent().next(".validation").remove();
  					                          }else{
  					                        	 if(fileName==""){}else{ 
  					                              $("#filechange").parent().next(".validation").remove();
  						            $("#filechange").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
  					                              return false;
  					                          } }
  					            	 
  					          			}
  					          		$("#rollno").parent().next(".validation").remove();
  					          		$("#formId").attr("action","updateStudent");
  					          	this.submit();
  					            }else{
  					            	 
  					            	$("#rollno").parent().next(".validation").remove();
  						            $("#rollno").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Rollno already used for this class</div>");
  					               
  					            }

  					            	
  					            } 
  					            else {$("#rollno").parent().next(".validation").remove();
  					            	if(studentid>0){
  					            		 $("#rollno").parent().next(".validation").remove();
  					            		 
  					            		var imagestring="<s:property value='%{studentbean.ProfilePicture}'/>"
  					          			if(imagestring==""){ 
  					          				 var fileName1 = $("#filechange_new").val();
  					          				var idxDot = fileName1.lastIndexOf(".") + 1;
  					                          var extFile = fileName1.substr(idxDot, fileName1.length).toLowerCase();
  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){$("#filechange_new").parent().next(".validation").remove();
  					                          }else{
  					                             $("#filechange_new").parent().next(".validation").remove();
  						            $("#filechange_new").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
  					                              return false;
  					                          } 
  					          			}else{
  					          				var fileName = $("#filechange").val();
  					          				
  					          				var idxDot = fileName.lastIndexOf(".") + 1;
  					                          var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  					                          if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#filechange").parent().next(".validation").remove();
  					                          }else{
  					                        	  if(fileName==""){}else{ 
  					                               $("#filechange").parent().next(".validation").remove();
  						            $("#filechange").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
  					                              return false;
  					                          } }
  					            	$("#rollno").parent().next(".validation").remove();
  					            	$("#formId").attr("action","updateStudent");
  									this.submit();
  					            	}
  					            		}
  					            	else{
  					            		 var fileName = document.getElementById("stuimage").value;
  						                  var idxDot = fileName.lastIndexOf(".") + 1;
  						                  var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
  						                  if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ $("#stuimage").parent().next(".validation").remove();
  						                  }else{
  						                      $("#stuimage").parent().next(".validation").remove();
  						            $("#stuimage").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>Allowes Only png/jpg/jpeg Extensions</div>");
  						                      return false;
  						                  } 
  						                $("#stuimage").parent().next(".validation").remove();
  									$("#formId").attr("action","saveStudentDetailsAction");
  					                this.submit();
  					            	}
  					            } 
  					        });
  				
  			
  			 
  	 
  		

  	});

  	  
    });
    function doWork() {
    	  $("#loading").show();
	    setTimeout('$("#btn").removeAttr("disabled")', 1500);
	}
    </script>
  
    <script type="text/javascript">
function handleFileSelect(evt,id) {
      
        
         
  var files = evt.target.files;
  for ( var i = 0, f; f = files[i]; i++) {

   var reader = new FileReader();
   reader.onload = (function(theFile) {
    return function(e) {

     $("#imgchange").attr("src", e.target.result);
    };
   })(f);
   reader.readAsDataURL(f);

  }

 }

</script>
       
</body>
</html>
