<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@taglib prefix="sx"  uri="/struts-dojo-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY&libraries=places"
        async defer></script> 
        

	<link rel="stylesheet" href="css/wickedpicker.css">
        <script type="text/javascript" src="js/wickedpicker.js"></script>
	 
<script>

	$(document)
			.ready(
					function() {
						 
						 $("#loading").hide();

						 $(".active").removeClass();
				     	  $("#logistics").addClass("active");
				     	  $("#buses").addClass("active");
				     	 $( "#regdate" ).datepicker();
				     	 $( "#insval" ).datepicker();
				     	 $( "#regval" ).datepicker();
				     	
				     	var idd=$("#idd").val();
				     	var TransportStartTimeId=$("#TransportStartTimeId").val();
				     	var SchoolArraivalTimeId=$("#SchoolArraivalTimeId").val();
				     	var ReturnStartTimeId=$("#ReturnStartTimeId").val();
				     	var ReturnArrivalTimeId=$("#ReturnArrivalTimeId").val();
				     	
				     	if(idd>0){
				     		   
				     		 $('#transport').wickedpicker({now: TransportStartTimeId, twentyFour: true, title:
				                    'My Timepicker', showSeconds: true});
				     		 $('#schoolarr').wickedpicker({now: SchoolArraivalTimeId, twentyFour: true, title:
				                    'My Timepicker', showSeconds: true});
				     		 $('#retstart').wickedpicker({now: ReturnStartTimeId, twentyFour: true, title:
				                    'My Timepicker', showSeconds: true});
				     		 $('#retarrv').wickedpicker({now: ReturnArrivalTimeId, twentyFour: true, title:
				                    'My Timepicker', showSeconds: true});
				     		
				     	}else{
				     		 
				     		 $('.timepicker').wickedpicker({now: '00:00', twentyFour: true, title:
				                    'My Timepicker', showSeconds: true});	
				     	}
				     	
			           
				     	$('#form').on('submit',function(e) {
							 $("#loading").hide();
							 var inputval=$("#pac-input").val();
							 
				     		 $(this).attr("disabled", "disabled");
							e.preventDefault();
							 var bName=$("#busname").val();
							 
							var busid=$("#busid").val();
							
							 
									
									  $.ajax({
									    	 url:"checkregnoforbus?busName="+bName,
									        type: 'POST',
									     
									        context: this
									    }).done( function(data) {
									        	 
									            if (data.busmsg== 'Exists') {
													 $("#loading").hide();

									            var bean=data.busbean;
									            if(bean.busId==busid){
									            	$("#busname").parent().next(".validation").remove();
									            	var regdate=$("#regdate").val();
									       		 var regval=$("#regval").val();
									       		  if(new Date(regval)< new Date(regdate)){
									       			 alert("Validity date can not be before registration date");
									       			 $("#loading").hide();
									       			  return false;
									       		  } 
									       		  
									       		 var transport=$("#transport").val();
									       		 var schoolarr=$("#schoolarr").val();
									       		 if(transport>schoolarr || transport==schoolarr){
									       			 alert("school arrival time always greater than schooltransport time");
									       			 $("#loading").hide();
									       			 return false;
									       		 }
									       		 
									       		  var retstart=$("#retstart").val();
									       		 var retarrv=$("#retarrv").val();
									       		 if(schoolarr>retstart ||schoolarr==retstart){
								           			 
								           			 alert("  return start time always greater than school arrival time");
								           			 $("#loading").hide();
								           			 return false;
								           		 }
									       		 
									       		 
									       		if(retstart>retarrv ||retstart==retarrv){
									       			
									       			alert("return arrival time always greater than return start time");
									       		 $("#loading").hide();
									       			 return false;
									       		 }
									       		if(inputval==""){
									           		/*  alert("User denied the request for Geolocation."); */
									           		
									           		 $("#loading").hide();
									           		 initMap();
									           		 return false;
									           		}
									            	$("#form").attr("action","updatebusdetail");
													this.submit();
									          
									            }else{
									            	
									            	$("#busname").parent().next(".validation").remove();
										                $("#busname").parent().after("<div class='validation text-right' style='color:red;margin-bottom: 20px;'>BusName Exists</div>");
									               
									            }

									            	
									            } 
									            else {
									            	if(busid>0){
									            	$("#busname").parent().next(".validation").remove();
									            	var regdate=$("#regdate").val();
									       		 var regval=$("#regval").val();
									       		  if(new Date(regval)< new Date(regdate)){
									       			 alert("Validity date can not be before registration date");
									       			 $("#loading").hide();
									       			  return false;
									       		  } 
									       		  
									       		 var transport=$("#transport").val();
									       		 var schoolarr=$("#schoolarr").val();
									       		 if(schoolarr>retstart ||schoolarr==retstart){
								           			 
								           			 
								           			 alert("  return start time always greater than school arrival time");
								           			 $("#loading").hide();
								           			 return false;
								           		 }
									       		 if(transport>schoolarr || transport==schoolarr){
									       			 alert("school arrival time always greater than schooltransport time");
									       			 $("#loading").hide();
									       			 return false;
									       		 }
									       		 
									       		  var retstart=$("#retstart").val();
									       		 var retarrv=$("#retarrv").val();
									       		if(retstart>retarrv ||retstart==retarrv){
									       			alert("return arrival time always greater than return start time");
									       		 $("#loading").hide();
									       			 return false;
									       		 }if(inputval==""){
									           		/*  alert("User denied the request for Geolocation."); */
									           		
									           		 $("#loading").hide();
									           		 initMap();
									           		 return false;
									           		}
									            	$("#form").attr("action","updatebusdetail");
													this.submit();
									            	}else{
									            		var regdate=$("#regdate").val();
									           		 var regval=$("#regval").val();
									           		  if(new Date(regval)< new Date(regdate)){
									           			 alert("Validity date can not be before registration date");
									           			 $("#loading").hide();
									           			  return false;
									           		  } 
									           		  
									           		 var transport=$("#transport").val();
									           		 var schoolarr=$("#schoolarr").val();
									           		 if(transport>schoolarr || transport==schoolarr){
									           			 alert("school arrival time always greater than schooltransport time");
									           			 $("#loading").hide();
									           			 return false;
									           		 }
									           		 
									           		var retstart=$("#retstart").val();
									           		 var retarrv=$("#retarrv").val();
									           		 if(schoolarr>retstart ||schoolarr==retstart){
									           			 
									           			 
									           			 alert("return start time always greater than school arrival time");
									           			 $("#loading").hide();
									           			 return false;
									           		 }
									           		  
									           		if(retstart>retarrv ||retstart==retarrv ){
									           			alert("return arrival time always greater than return start time");
									           		 $("#loading").hide();
									           			 return false;
									           		 }
									           		if(inputval==""){
									           		/*  alert("User denied the request for Geolocation."); */
									           		
									           		 $("#loading").hide();
									           		 initMap();
									           		 return false;
									           		}
													$("#form").attr("action","saveBusesDetailsAction");
									                this.submit();
									            	}
									            } 
									        }); 
								
									
							    
							 doWork();
					 
						

					}); 

			          			        
						initMap();
						 
					});
	 
	 function doWork() {
		 $("#loading").show();

		    setTimeout('$("#btn").removeAttr("disabled")', 1500);
		}

	</script>
 
    	    	
	
	
	<style type="text/css">
	
	#pac-input{
    height:25px;
    width:300px;
    top:10px;
    left:10px;
    position:absolute;
}
.img-responsive1 {
     
     position: absolute;
    left: 70%;
    top: 70%;
    background-color: white;
    z-index: 150;

    height: 100px;
    margin-top: -200px;

    width: 200px;
    margin-left: -180px;
 }
	
	</style>
	 
	 
	
	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>

 		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
 		<div class="content-wrapper">
			<section class="content-header">
			<s:if test="%{busbean.BusId>0}">
				<h4></h4>
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getBusesList">Buses</s:a></li>
						<li class="active"> Edit Bus</li>
				
					</ol></s:if>
					<s:else>
					
					<h4></h4>
				<ol class="breadcrumb">
					<li><s:a action="getbuseslistbyId"><i class="fa fa-dashboard"></i> Home</s:a></li>
					<li><s:a action="getBusesList">Buses</s:a></li>
						<li class="active">Add New Bus</li>
				
					</ol>
					</s:else>
			</section>
			<section class="content">
				<div class="box box-default no-shadow no-border flat">
			 <div class="pad">All Fields Marked<span class="text-red">* </span>Mandatory</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-6">
								<!-- form start -->
								<s:form  role="form" theme="simple" id="form">
			
									<div class="box-body">
								 
				<div class="form-group">
					<label for="RouteId">Route<span class="text-red">*</span></label> 
					  <div class="col-sm-8 pull-right">
						<s:if test="%{busbean.BusId>0}">
						<select class="form-control" name="RouteId" required> 
						 <s:iterator value="routesList">
						 <s:if test="%{busbean.RouteId==RouteId}">
						 
						 <option value='<s:property value="RouteId"/>' selected><s:property value="RouteName"/></option>
						 </s:if>
						 <s:else>
						 
						<option value='<s:property value="RouteId"/>'><s:property value="RouteName"/></option>
						</s:else>
						</s:iterator> 
						</select> 
						</s:if>
						<s:else>
						<select class="form-control" name="RouteId" required>
						<s:iterator value="routesList">
						<option value='<s:property value="RouteId"/>' selected><s:property value="RouteName"/></option>
						</s:iterator>
						</select></s:else>
					</div>	
				
				</div>
				
				
				<div class="form-group">
				 
					<label for="RegNumber">Bus No/Name<span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
					  <input type="text"
						class="form-control" id="busname" pattern="^[a-zA-Z0-9-]*$" title="Allows only AlphaNumeric and hyphen" name="BusName" value="<s:property value='busbean.BusName'/>"
						required/>
						
						 
						
						</div>
				</div>
				
				<div class="form-group">
					<label for="RegNumber">Description<span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
					 <input type="text"
						class="form-control" id="exampleInputEmail2" name="Description" value="<s:property value='busbean.Description'/>"
						required/>
				</div></div>
				<div class="form-group">
					<label for="RegNumber">Reg Number<span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
					 <input type="text"
						class="form-control" id="regnum" name="RegNumber"  value="<s:property value='busbean.RegNumber'/>"
						required/>
				</div></div>
				 
				
				<div class="form-group">
					<label for="Occupency">Occupancy<span class="text-red">*</span></label> 
					<div class="col-sm-8 pull-right">
					<input type="number"
						name="Occupency" min="10" max="60" title="Allows Only two digit number" class="form-control" id="exampleInputPassword4" value="<s:property value='busbean.Occupency'/>"
						required>
				</div></div>

				<div class="form-group">
					<label for="EngineNumber">Engine Number<span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
					 <input type="text"
						name="EngineNumber" class="form-control" value="<s:property value='busbean.EngineNumber'/>"
						id="exampleInputPassword1" required>
				</div></div>

				<div class="form-group">
					<label for="ChasisNumber">Chasis Number<span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
					 <input type="text"
						name="ChasisNumber" class="form-control" value="<s:property value='busbean.ChasisNumber'/>"
						id="exampleInputPassword1" required>
				</div></div>

				<div class="form-group">
				
					<label for="DriverId">Driver<span class="text-red">*</span></label> 
						<div class="col-sm-8 pull-right">
						<s:if test="%{busbean.BusId>0}">
						<select class="form-control" name="DriverId" required> 
						 <s:iterator value="driversBeans">
						 <s:if test="%{busbean.DriverId==DriverId}">
						 <option value='<s:property value="DriverId"/>' selected><s:property value="FirstName"/> <s:property value="LastName"/></option>
						 </s:if>
						 <s:else>
						<option value='<s:property value="DriverId"/>'><s:property value="FirstName"/></option>
						</s:else>
						</s:iterator> 
						</select> 
						</s:if>
						<s:else>
						<select class="form-control" name="DriverId" required>
						<s:iterator value="driversBeans">
						<option value='<s:property value="DriverId"/>' selected><s:property value="FirstName"/> <s:property value="LastName"/></option>
						</s:iterator>
						</select></s:else>
					</div>	
				</div>

				 <div class="form-group">
					<label  for="RegDate">Reg Date<span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
					<input class="form-control" type="text" id="regdate" name="RegDate"  value='<s:property value='busbean.RegDate'/>' required >
					</div>
					</div>

 

				<div class="form-group">
					<label for="RegValidity">Reg Validity<span class="text-red">*</span></label> 
					<div class="col-sm-8 pull-right">
						<input class="form-control" type="text" id="regval" name="RegValidity" value='<s:property value='busbean.RegValidity'/>' required >
						
				</div>
				</div>
				<div class="form-group">
					<label for="InsurenceValidity">Insurance Validity<span class="text-red">*</span></label>
				<div class="col-sm-8 pull-right">
				<input class="form-control" type="text" id="insval" name="InsurenceValidity" value='<s:property value='busbean.InsurenceValidity'/>' required >
				</div>
				</div> 
				<div class="form-group">
					<label for="TransportStartTime">Transport Start Time<span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
			 <input   type="text" id="transport"  name="TransportStartTime" class="timepicker form-control"  value='<s:property value='busbean.TransportStartTime'/>' required>
		
				</div>
				</div>
				<div class="form-group">
					<label for="SchoolArraivalTime">School Arrival Time<span class="text-red">*</span></label> 
					<div class="col-sm-8 pull-right">
						<input  type="text" id="schoolarr"  name="SchoolArraivalTime" class="form-control timepicker"  value='<s:property value='busbean.SchoolArraivalTime'/>' required>
				 <s:hidden id="idd" value="%{busbean.BusId}"></s:hidden>
				 <s:hidden id="TransportStartTimeId" value="%{busbean.TransportStartTime}"></s:hidden>
				 <s:hidden id="SchoolArraivalTimeId" value="%{busbean.SchoolArraivalTime}"></s:hidden>
				 <s:hidden id="ReturnStartTimeId" value="%{busbean.ReturnStartTime}"></s:hidden>
				 <s:hidden id="ReturnArrivalTimeId" value="%{busbean.ReturnArrivalTime}"></s:hidden>
				</div></div>
				
				<div class="form-group">
					<label for="ReturnStartTime">Return Start Time<span class="text-red">*</span></label> 
					<div class="col-sm-8 pull-right">
						<input  type="text" id="retstart"  name="ReturnStartTime" class="form-control timepicker" value='<s:property value='busbean.ReturnStartTime'/>' required>
					</div>	
				</div>

				<div class="form-group">
					<label for="ReturnArrivalTime">Return Arrival Time <span class="text-red">*</span></label>
					<div class="col-sm-8 pull-right">
					<input   type="text" id="retarrv"  name="ReturnArrivalTime" class="form-control timepicker"  value='<s:property value='busbean.ReturnArrivalTime'/>' required >
				</div></div>
 			 <div class="form-group">
											<label>Bus Start Location<span class="text-red">*</span></label> 
											 
												
												 <input
												type="hidden" class="form-control" name="StartPointLocation" id="StartPointLocation"
												value='<s:property value='busbean.StartPointLocation'/>' required >
												
												 </div> 
										
										<div id="mapdetails">
								 <input id="pac-input" class="controls" type="text"
        placeholder="Enter a location">
                  <div class='img-responsive' id="map" style="height:300px;width:100%;"  ></div>
    <div class="description pad">
    
    
    				 <i class="fa fa-map-marker margin-r-5 text-orange" id="latlang"></i>
                  <i class="fa fa-map-marker margin-r-5 text-blue" id="waypoint"></i>
                  
                 
							</div>
							
							
							 </div> 




								
<input 	type="hidden"  value="<s:property value="%{busbean.StartPointLatLang}"/>"  name="StartPointLatLang" id="StartPointLatLang" />

			 
				
				 
				

			
			<s:hidden name="SchoolId" value="%{#session.schoolId}"></s:hidden>
			<div class="box-footer">
			<s:hidden name="BusId" label="BusId" id="busid" value="%{busbean.BusId}"></s:hidden>
				<s:if test="%{busbean.BusId>0}">
									<!-- <input type="submit" id="update" value="save" /> -->
									<s:submit value="Update" class="btn btn-primary"></s:submit> 
										</s:if>
										<s:else>
										<input type="submit" id="save" value="save" />
										</s:else>
			</div>
							
							</div>
							<div id="loading">
    <img src="img/pro.png"  class="img-responsive1 img-center"/>
</div>
							
							</s:form>
						</div>
					</div></div>

				</div>

			</section>
		</div>
	</div>
          <script>


	$(document)
			.ready(
					function() {

						initMap();

					});

	</script>
	
	<script>
      var map;
      var palcename;
      var marker;
      var options = {
    		  enableHighAccuracy: true,
    		  timeout: 10000,
    		  maximumAge: 0
    		};
      
      function initMap() {
    	  var input = document.getElementById('pac-input');
    	  var inputval=$("#pac-input").val();
    	  if (navigator.geolocation) {
    		  
    		 
    		  navigator.geolocation.getCurrentPosition(showPosition,showError,options);
    		  
    	    } 

          
    	  


function getPlaceDetailsByLatLang(myLatLang){




    var geocoder;
	  geocoder = new google.maps.Geocoder();
	   

	  geocoder.geocode(
	      {'latLng': myLatLang}, 
	      function(results, status) {
	          if (status == google.maps.GeocoderStatus.OK) {
	                  if (results[0]) {
	                      var add= results[0].formatted_address ;
	                      var  value=add.split(",");

	                   var	   count=value.length;
	                  var    country=value[count-1];
	                    var  state=value[count-2];
	                    var  palcename=value[count-3];
					$("#latlang").html("&nbsp;&nbsp;Latitude,Longitude:"+myLatLang);
					 $("#llt").attr("value",myLatLang);		
					$("#waypoint").html("&nbsp;&nbsp;"+value);
					$("#StartPointLocation").attr("value",value);
					$("#StartPointLatLang").attr("value",myLatLang);
					document.getElementById('pac-input').value = value;
			 
				$("#StartPointLatLang").attr("value",myLatLang)	;
					
	                     
	                      
	                  }
	                  else  {
	                     alert("address not found");
	                  }
	          }
	           else {
	             alert("Geocoder failed due to: " + status);
	          }
	      }
	  );

	
}

function showPosition(position) {
 
	  var  myLatlng;
    var idbus=$("#busid").val();
    if(idbus>0){
  	  var startid=$("#StartPointLatLang").val();
        startid = startid.slice(1,-1);
        
        var  value=startid.split(",");
        myLatlng=new google.maps.LatLng(value[0],value[1]);;
        }else{
        	
        	 myLatlng= new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
        }
	 
    var mapOptions = {
      zoom: 14,
      styles: [{
	  	       // featureType: 'poi.business',
	  	       featureType: 'all',
	  	        elementType: 'labels',
	  	        stylers: [{
	  	            visibility: 'on'
	  	        }]
	  	    }],
      center: myLatlng,
      disableDefaultUI: true, 
      mapTypeControl: true,
      mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
          position: google.maps.ControlPosition.TOP_RIGHT
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
	    map = new google.maps.Map(document.getElementById("map"),
	          mapOptions);

	    
	    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
 
      marker=new google.maps.Marker({position:myLatlng,map:map,title:"Set Way Point",draggable:true,});
     
    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
       
        getPlaceDetailsByLatLang(event.latLng);


    });
    
    
    
    
    var autocomplete = new google.maps.places.Autocomplete(input);
   autocomplete.addListener('place_changed', function() {
    
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return; 
    }

   var  myLatlng =  place.geometry.location;
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
       } else {  map.setCenter(place.geometry.location);
    }
    marker.setPosition(myLatlng);

    getPlaceDetailsByLatLang(myLatlng); 
    marker.addListener('dragend',function(event) {
         getPlaceDetailsByLatLang(event.latLng);


    });
    
   });

	
}

          
      }
      
      function showError(error) {
    	
    	   	  if(error.code == 0){
  	        // unknown error
  	       
  	       
  	        alert(" Your Browser not supported for GeoLocation Access.")
  	      location.reload(true);
  	        }   else if(error.code == 1) {
  	        // permission denied
  	      alert("The acquisition of the geolocation information failed because the page didn't have the permission to do it.Please allow location information for this website.")
  	   location.reload(true); 
  	        } else if(error.code == 2) {
  	        // position unavailable
  	        alert("The acquisition of the geolocation failed because at least one internal source of position returned an internal error.Please try again.");
  	      location.reload(true);
  	        }else if(error.code == 3){
  	        	
  	        	alert("The time allowed to acquire the geolocation, defined by PositionOptions.timeout information was reached before the information was obtained.Please try again.");
  	        	 location.reload(true);
  	        }
  	        
    	 
    	}
  

    </script>

	
</body>
</html>
