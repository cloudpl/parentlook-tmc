<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="js/bootstrap.min.js"></script>

	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<script type="text/javascript">
	 $(document).ready(function(){
   	  $(".active").removeClass();
   	  $("#logistics").addClass("active");
   	  $("#buses").addClass("active");
   	  var buslist= "<s:property value='busDetailBeans.size'/>";
   	 
   	 if(buslist>0){
  	   $("#excel").show();
  	   //$("#pdf").show();
  	   
     }else{
  	   $("#excel").hide();
  	//   $("#pdf").hide();
     }
   $(".del").click(function(){
	   var x = confirm("Are you sure you want to delete ?");
		  
		  if(x==true){
			  
			  return true;
		  }else{
			  
			  return false;
		  }
   })
     });
	</script>
	<style type="text/css">
	
#fabutton {
  background: none;
  padding: 0px;
  border: none;
}
	</style>	
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="text-right">
				<span><s:a action="managedevices" cssClass="bg-blue pad margin-r-5">ManageDevices</s:a></span>
				<span><s:a action="addbus" cssClass="bg-orange pad margin-r-5">+AddNewBus</s:a></span>
				<span><a href="exportBusesExcel" id="excel" class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
				<%-- <span><a href="exportBusesPdf" id="pdf" class="btn no-padding"><i class="fa fa-file-pdf-o fa-2x text-red"></i></a></span> --%>
			</div>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow">
							<!-- /.box-header -->
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Id</th>
											<th>Name</th>
											<th>Route</th>
											<th>Driver</th>
											<th>RegNo:</th>
											<th>OTP</th>
											<th>StartTime</th>
											<th>ReturnTime</th>
											<th>StartingPoint</th>
										    <th>Actions</th>
										</tr>
									</thead>
									<tbody>
									
										<s:iterator value="busDetailBeans">
										<s:if test="%{RecordStatus.equals('ACTIVE')}">
											<tr>

												<td><s:property value="BusId" /></td>
												<td><s:property value="BusName" /></td> 
												<td><s:property value="routeName" /></td> 
												<td><s:property value="driverName" /></td>
												<td><s:property value="RegNumber" /></td>
												<td><s:property value="otp"/></td>
												<td><s:property value="TransportStartTime" /></td>
												  <td><s:property value="ReturnStartTime" /></td>
												<td><s:property value="StartPointLocation" /></td>
												
													<td>
           <div class="col-md-4 no-padding"> <s:form action="viewbusdetailbyschool" title="View Bus"><s:hidden name="busid" value="%{BusId}"></s:hidden> <button type="submit"  id="fabutton"> <i class="fa fa-eye margin-r-5"></i></button></s:form>
    			</div>								
												
            <div class="col-md-4 no-padding"> <s:form action="editbus" title="Edit Bus"><s:hidden name="busid" value="%{BusId}"></s:hidden><button type="submit"  id="fabutton">  <i class="fa fa-pencil text-blue margin-r-5"></i></button></s:form></div>
            <s:if test="%{usedStatus.equals('INACTIVE')}">
        
      <div class="col-md-4 no-padding">    <s:form action="deletebus" title="Delete Bus"><s:hidden name="busid" value="%{BusId}"></s:hidden> <s:submit type="button"  cssClass="del" id="fabutton" > <i class="fa fa-trash text-red margin-r-5"></i></s:submit>
    											
												</s:form></div>
            </s:if>
                                                 </td>
										

											</tr></s:if>
										</s:iterator>
									</tbody>
									
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>

	</div>

	

	<!-- page script -->
	<script>
      $(function () {
    	  $('#example1').DataTable();
    
      });
     
    </script>
</body>
</html>
