<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title>
<link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
 <link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	<style>
     
      #map
      {
      height:350px;
      width:100%;
      }
      .embed-responsive-16by9{
      padding-bottom:75.25%; 
      }
    
      
    </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY"></script> 
	
<script type='text/javascript'>
function reload(busid){
 	var video = document.getElementById(busid);
var sr=document.getElementById(busid).src;
var rand=Math.random();
 //var url="http://36.255.3.149:8080/livestreaming/Bus"+busid+"/live.mp4?rand="+rand;
  var url="https://www.portal.parentlook.com/livestreaming/Bus"+busid+"/live.mp4?rand="+rand; 
			 
			 document.getElementById(busid).src=url;
			
   video.play();
 
	    }
	    

</script>
    <script type="text/javascript">
    
    var getUrlParameter = function getUrlParameter(sParam) {
   	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
   	        sURLVariables = sPageURL.split('&'),
   	        sParameterName,
     	        i;

   	    for (i = 0; i <sURLVariables.length; i++) {
   	        sParameterName = sURLVariables[i].split('=');

   	        if (sParameterName[0] === sParam) {
   	            return sParameterName[1] === undefined ? true : sParameterName[1];
   	        }
   	    }
   	    
   	};
   function get_routedetails()
   {
	  $.ajax({
		  
		  url:"getBusdetails",
		  type:"post",
		   catche:true,
           success: function(data)
	           {
		     
        	   
	
		      var buseslist=data.buseslist;
          	var routelist=data.routelist;
                var st;

               	for(var i=0;i<routelist.length;i++)
          		{
          		var route=routelist[i];
          
       		  $('#allroutes').append('<option value = "'+route.routeId+'">'+route.routeName+'</option>');
        		  
        		}
       
          		for(var j=0;j<buseslist.length;j++)
          			{
                  var busdetail=buseslist[j];
                  
                  st+="<tr><td>"+busdetail.routeId +"</td>"+
                       "<td>"+busdetail.busId+"</td>"+
                      "<td>"+busdetail.activities+"</td>"+
                       "<td>"+busdetail.pickup+"</td>"+
                       "<td>"+busdetail.dropStatus+"</td></tr>";
	           }
          	
          	$('#tdata').append(st);
	           }
	  });
	   
   }
    
    
   var ivid=0;
   var count=0;
    $(document).ready(function()
    		
   {
    	
    	 $(".active").removeClass();
      	  $("#dashboard").addClass("active");
    	get_routedetails();
    	
    	
    	
    	getBusesForRoutesonMap();
    	getMap();
    	$("#allroutes").change(function(){
    		 count=1;
    			getBusesForRoutesonMap();
    			getMap();
    			
    	});
    	$("#popup").hide();
    	 $(".knob-label").click(function(){ 
    		 
    var reqbusstatus=$(this).attr("id");
    		 var rId=$("#allroutes").val();
    		
    		 $(".modal-body").html("");
    			 $.ajax({
    		    	 url:"getBusesBasedOnStatus?routeId="+rId+"&busStatus="+reqbusstatus, 
    				   catche:false,
    		            success: function(data)
    			           {
    		            	var alist=data.busDetailBeans;
    		            	 
    		            	if(alist.length>0){
    		            	var fData="<table><tr class='col-md-12'> <th class='col-md-3'>BusId</th><th class='col-md-4'>BusName</th><th class='col-md-5'>RouteName</th></tr>";
    		              
    		            	for(var i=0;i<alist.length;i++){
    		            		
    		            		var busbean=alist[i];
    		            		var bName=busbean.busName;
    		            		var rName=busbean.routeName; 
    		            		var busId=busbean.busId;
    		            		
    		            		fData=fData+'<tr class="col-md-12" style="word-break:break-all;"><td class="col-md-3">'+busId+'</td><td class="col-md-4">'+bName+'</td><td class="col-md-5">'+rName+'</td></tr>';
    		            	}
    		            	$(".modal-body").append(fData+"</table>");
    		            	}else{
    		            		$(".modal-body").append("No Buses");
    		            	}
    		            	$("#popup").click();
    			           }
    	 }); 
    	
    });
   });
    
    </script>
    <script type="text/javascript">
    function vidError1(vid){
    	$("#"+vid).attr("src","novideo.gif");
    	
    	
    }
    </script>
    <script type="text/javascript">
    
    var directionsService = new google.maps.DirectionsService();
    var geocoder = new google.maps.Geocoder();
var directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true});
         var myLatlng;
		 var GOOGLE_MAP_KEY = "AIzaSyADxbpNayyUUqtdpQ71fdkcW0d1ebZsqPY";
    	var image="img/busicon.png";
    	var businact="img/busicon.png";
    	var busact="img/busicon.png";
    	var busreach="img/busicon.png";
    	var schoolimage="img/schoolicon.png";
		var ss=localStorage.getItem("apikey");
    	
    	
    	
    	
    	
    	
    	var bus1="M281.6 819.2c-42.347 0-76.8-34.451-76.8-76.8s34.453-76.8 76.8-76.8 76.8 34.451 76.8 76.8-34.453 76.8-76.8 76.8zM281.6 716.8c-14.115 0-25.6 11.485-25.6 25.6s11.485 25.6 25.6 25.6 25.6-11.485 25.6-25.6-11.485-25.6-25.6-25.6z";
          
          var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";

      



          var icon = {
				path:car,
				scale: .7,
				strokeColor: '#403b37',
				strokeWeight: 1,  
				fillOpacity: 1,
				fillColor: 'yellow',
				offset: '5%',
				anchor: new google.maps.Point(10, 25) 
				};
    	
    	
    	
    	
    	
    	
    		        var script = document.createElement('script');
  		          script.type = 'text/javascript';
  		     
  		       
  		          
  		     
  		     
  		          
  		    var icou=0;
  		  
  		  var  map;
  		var marksarr=[];
    function	getBusesForRoutesonMap(){
    	var rId=$("#allroutes").val();
    	 for (var i = 0; i < marksarr.length; i++) {
           }

  		$.ajax({
	    	 url:"getBusesANdGpsForRoute?routeId="+rId, 
			   catche:false,
	            success: function(data)
		           { 
	            	 var schoolname=data.schoolname;
	            	var latitude=data.latitude;   
	     		     var longitude=data.longitude;
	     		     
	     		    
	  		   
	  				if(icou==0){
	  					myLatlng = new google.maps.LatLng(latitude,longitude);
	  	  		      var mapOptions = {
	  	  		        zoom: 11,
	  	  		    styles: [{
	  	  	       featureType: 'all',
	  	  	        elementType: 'labels',
	  	  	        stylers: [{
	  	  	            visibility: 'on'
	  	  	        }]
	  	  	    }],
	  	  		        center: myLatlng,
	  	  		    fullscreenControl: true,
	  	  		        mapTypeControlOptions: {
	  	  		            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
	  	  		            position: google.maps.ControlPosition.TOP_CENTER
	  	  		        },
	  	  		        mapTypeId: google.maps.MapTypeId.ROADMAP
	  	  		      };
	  					 map = new google.maps.Map(document.getElementById("map"),
	  		     		          mapOptions);	
	  					 var marker=new google.maps.Marker({position:myLatlng,map:map,title:""+schoolname,icon:schoolimage});
		                 marksarr.push(marker);
	  					
	  				} 
	  				
	            	
		            	var coordinatelist=data.busesdetail;
		                 var st;
		                
 		           for(var i=0;i<coordinatelist.length;i++)
		    	      {
		        	   
		              var coordinate=coordinatelist[i];
		             
		               myLatlng = new google.maps.LatLng(coordinate.geoLat, coordinate.geoLang);
		               var marker;
		               if(icou<coordinatelist.length){
		              if(coordinate.status=="0"){
		            	 marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:businact});
		              }else if(coordinate.status=="1" || coordinate.status=="3"){
		            	  marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:busact});
		              }else if(coordinate.status=="2"){
		            	  marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:image});
		              }else{
		            	  
		            	  marker=new google.maps.Marker({position:myLatlng,map:map,title:""+coordinate.busName,icon:busreach});
		              }
		              marksarr.push(marker);
		               }else{
		            	   marksarr[i+1].setPosition( myLatlng ); 
		               }
		              
		             icou++;
		    	}
		           
		  				 
			}
	    });
  	}
    function getMap(){
    	var rId=$("#allroutes").val();
    
   	 for (var i = 0; i < marksarr.length; i++) {
          }

 		$.ajax({
	    	 url:"getBusesANdGpsForRoute?routeId="+rId, 
			   catche:false,
	            success: function(data)
		           { 
	            	var schoolname=data.schoolname;
	            	var latitude=data.latitude;   
	     		     var longitude=data.longitude;
	     		     
	     
	  		   
	     		   var coordinatelist=data.busesdetail;
	                 var st;
	                
	                
	                 
	                 $("#busInactivePick").html(data.busInactivePick);
	                 $("#activePick").html(data.activePick);
	                 $("#busArrviedPick").html(data.busArrviedPick);
	                 $("#busInactiveDrop").html(data.busInactiveDrop);
	                 $("#activeDrop").html(data.activeDrop);
	                 $("#busArrviedDrop").html(data.busArrviedDrop);
	                  var active=data.activePick;
		                var dropactive=data.activeDrop;
		              var picarrive=data.busArrviedPick;
		              var droparrive=data.busArrviedDrop;
		                if(active>0 || dropactive>0){
		                getBusesForRoutesonMap();
		                
		                }
		              
	                 if(ivid==0 || count==1){
			        	   $("#busesdata").html("");
			  				 
			  				 var busbean=data.busDetailBeans;
			  				 for(var k=0;k<busbean.length;k++){
			  					 var bBean=busbean[k];
			  					 var bhId=bBean.busId;
			  					 var bName=bBean.busName;
			  					 var pick=bBean.busPick;
			  					 var drop=bBean.busDrop;
			  				     var surveillance=bBean.surveillance;
			  				 
			  					 if(pick==1 || drop==1){
			  						 
			  						
			  						 if(surveillance==1){
			  					
					             $("#busesdata").append("<video  id='"+bhId+"' onended='reload(this.id)'   class='video'    controls autoplay style='width:135px; padding:5px;' title='  Bus-"+bName+"  RouteName-"+bBean.routeName+"'><img src='img/logo.png'/><source    src=' https://www.portal.parentlook.com/livestreaming/Bus"+bhId+"/live.mp4'  type='video/mp4'></source></video>");
			  						 }
			  					 } 
			  					 
			  					
			  				 }
			  				 
			  				 
			  				 
			  				 
			  				 
			  		var vidCount=$(".video").length; 
			  		
			
			  localStorage.setItem("vidCount",vidCount);
			  
			  		if(vidCount==0 ){
			  			 $("#busesdata").append("No Active Buses To Show.");
			  			
			  		}
			  		
			            
		           }  
	                ivid++;
	                count++;
		           }
			});
 		
    }
    
setInterval(getMap,2000);
    

    </script>
    </head>
<body class="hold-transition skin-yellow sidebar-mini">
	<!-- Site wrapper -->
	
	<div class="wrapper">

		<%@ include file="header.jsp" %>
 
		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
 
		<div class="content-wrapper">
			<section class="content-header">
				
				
				
				
			</section>
			 
			 
			 
			 
			  <section class="content">
			  
			   
	<div class="row margin">
	<div class="col-md-6">
	<div class="box no-shadow no-border flat">
                    <div class="box-header">
					<div class="form-group col-md-8 no-padding no-margin">
                 <select class="form-control form-control-downarrow select2 " id="allroutes" style="width: 100%;">
				  <option value="0">All Routes</option>
                </select>
                   </div>
 					</div>
                    <div class="box-body" style="box-shadow:0 2px 10px rgba(0,0,0,.2);">
                      <div class="embed-responsive embed-responsive-16by9">
                       <div style="float:left;" id="map">
                                    </div>
                      </div>
                    </div>
                    
                  </div>
                  

	</div>
	<div class="col-md-6 ">
	<div class="box no-shadow no-border flat">
	<div class="box-header text-center">
     <div class="row">
     <div class="col-md-6">
     <div class="row">
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="busInactivePick"  class="text-orange text-bold">0</div>
                      <div class="knob-label" id="inactive_pickup"><i class="fa fa-circle-o text-gray margin-r-5"></i>Inactive</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="activePick" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="active_pickup"><i class="fa fa-gear fa-spin text-blue margin-r-5"></i>Active</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding">
                      <div id="busArrviedPick" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="arrived_pickup"><i class="fa fa-circle text-orange margin-r-5"></i>Arrived</div>
                      
                    </div>
                    
                        </div>
     <div class="row margin text-center bg-aqua">
     Pickup
     </div>
     </div>
   
      
     <div class="col-md-6">
     <div class="row">
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="busInactiveDrop" class="text-orange text-bold">0</div>
                     <div class="knob-label" id="inactive_drop" ><i class="fa fa-circle-o text-gray margin-r-5"></i> Inactive</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding" style="border-right: 1px solid #f4f4f4">
                      <div id="activeDrop" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="active_drop"><i class="fa fa-gear fa-spin text-blue margin-r-5"></i>Active</div>
                    </div>
                    <div class="col-xs-4 text-center no-padding">
                    <div id="busArrviedDrop" class="text-orange text-bold">0</div>
                      <div class="knob-label" id="arrived_drop"><i class="fa fa-circle text-green margin-r-5"></i>Arrived</div>
                    </div>
                             
     </div>
     <div class="row margin text-center bg-green-gradient">
     Drop-Off
     </div>
     </div>
      </div> 
     </div>
                    
                    <div class="box-body text-center" id="busesdata" style="overflow:auto;height:60vh;direction:rtl;">
                       
					  			  
                    </div>
                   
					</div>
	</div>
	
		<button type="button" id="popup" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"></button>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
         
      </div>
      <div class="modal-body">
         
         
         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
		
		
		</div>
		
		
		</section>
  	</div>
 	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
 	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
     			<s:hidden value="%{Latitude}" id="latitude"></s:hidden>
				<s:hidden value="%{Longitude}" id="longitude"></s:hidden>
				<s:hidden value="%{schoolname}" id="schoolname"></s:hidden>
</body>
</html>
