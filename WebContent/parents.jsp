<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
 <link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
	<script type="text/javascript">
	 $(document).ready(function(){
   	  $(".active").removeClass();
   	  $("#profiles").addClass("active");
   	  $("#parents").addClass("active");
    	var parentList= "<s:property value='parentsImageList.size'/>";
   	
   	 if(parentList>0){
   	   $("#excel").show();
   	 //  $("#pdf").show();
   	   
    }else{
   	   $("#excel").hide();
   	  // $("#pdf").hide();
    }
   	 $(".del").click(function(){
   		 var x = confirm("Are you sure you want to delete ?");
		  
		  if(x==true){
			  
			  return true;
		  }else{
			  
			  return false;
		  }
   	 })
     });
	</script>
	<style type="text/css">
	
	#fabutton {
  background: none;
  padding: 0px;
  border: none;
}
	</style>
</head>
<body class="hold-transition skin-yellow sidebar-mini">
	<div class="wrapper">

		<%@ include file="header.jsp" %>
 		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
 		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<div class="text-right">
				<span><s:a action="addParent" cssClass="bg-blue pad margin-r-5">+AddNewParent</s:a></span>
				<span><a href="exportParentExcel" id="excel"  class="btn no-padding" ><span class="margin-r-5"><i class="fa fa-download"></i>Download</span><i class="fa fa-file-excel-o fa-2x text-green"></i></a></span>
 	</div>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow flat">
							 
							<!-- /.box-header -->
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Id</th>
											<th>Name</th>
											<th>Address</th>
											<th>Mobile No:</th>
											<th>Email</th>
											<th>Registered</th>
											<th>OTP</th>
											<th>Actions</th>

										</tr>
									</thead>
									<tbody>
										<s:iterator value="parentsImageList">
										<s:if test="%{RecordStatus.equals('ACTIVE')}">
											<tr>

												<td><s:property value="ParentId" /></td>
												
												<td style="position: relative;"><img onerror="this.src='img/user.png';" class="maxyId"
													src="data:image/png;base64,<s:property value="ProfilePicture"/>"
													style="height: 35px; width: 35px; margin-top: 0px; border-radius: 100%;" />
													<div id="imMax"
														style="width:200px; none; position:absolute; z-index: 100;transition: all 3s ease-in-out;">
														<img id="myImage" src="" />
													</div> <s:property value="FirstName" /> <s:property value="LastName" />
													</td>
												<td><s:property value="Address" /></td>
												<td><s:property value="MobileNumber" /></td>
												<td><s:property value="Email" /></td>
												<td><s:property value="loginStatus" /></td>
												<td><s:property value="otp"/></td>
												
												<td> 
											<div class="col-md-4 no-padding">	<s:form action="viewParent" title="View Parent" method="post">
												
												<s:hidden name="id3" value="%{ParentId}"></s:hidden>
												
												 <button type="submit"  id="fabutton"><i class="fa fa-eye margin-r-5"></i></button>
    											 
												
												
												</s:form></div>
                											
               										<div class="col-md-4 no-padding"> <s:form action="editParent" title="Edit Parent" method="post">
												
												<s:hidden name="id1" value="%{ParentId}"></s:hidden>
												
												 <button type="submit"  id="fabutton"> <i class="fa fa-pencil text-blue margin-r-5"></i></button>
    											
												</s:form></div>
                 								<s:if test="%{!usedStatus.equals('ACTIVE')}">
                								
                							<div class="col-md-4 no-padding">	 <s:form action="deleteParent" title="Delete Parent" method="post">
												
												<s:hidden name="id2" value="%{ParentId}"></s:hidden>
												
												 <s:submit type="button" cssClass="del"  id="fabutton"><i class="fa fa-trash text-red margin-r-5"></i>
    											 </s:submit>
											
												</s:form></div>
                           </s:if>
                
               									
               										
                										</td>
        

												</tr></s:if>
										</s:iterator>
									</tbody>
									 
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>





		 
	</div>
 	<script>
      $(function () {
    	  $('#example1').DataTable();
       });
     
    </script>
</body>
</html>
