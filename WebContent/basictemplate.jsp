<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

</head>
<body class="hold-transition skin-yellow sidebar-mini" >
	<div class="wrapper">

		<!-- Main Header -->
		<%@ include file="header.jsp" %>
		
		
		<!-- Left side column. contains the logo and sidebar -->
		
		<%@ include file="sidebar.jsp" %>
		

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>
				<i class="fa fa-lock margin-r-5 text-blue"></i>Change Password
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
				<li class="active">Here</li>
			</ol>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="row margin">


				<div class="col-xs-12">
					<div class="box no-border no-shadow">
						<div class="box-header with-border"></div>
						<!-- /.box-header -->


						<div class="box-body">
							<form class="form">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Password</label> <input type="password"
												class="form-control" placeholder="Enter Password" required>
										</div>
										<div class="form-group">
											<label>New Password</label> <input type="password"
												class="form-control" placeholder="Enter New Password"
												required>
										</div>
										<div class="form-group">
											<label>Confirm Password</label> <input type="password"
												class="form-control" placeholder="Re Enter New Password"
												required>
										</div>
									</div>

								</div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>

						</div>
						<!-- /.box-body -->

					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->

			</div>
			<!-- /.row --> </section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<!-- Main Footer -->
		
		<%@ include file="footer.jsp" %>
		


	</div>
	<!-- ./wrapper -->

	<!-- REQUIRED JS SCRIPTS -->

	<!-- jQuery 2.2.3 -->
	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="js/app.min.js"></script>

</body>
</html>
