<%@taglib uri="/struts-tags" prefix="s" %>
<aside class="main-sidebar">
	<section class="sidebar">


		<ul class="sidebar-menu">
			
			<li id="dashboard">
		<s:if test="%{#session.adminRole.equals('SITEADMIN')}">
			<a href="getSchoolsList"><i class="fa fa-dashboard"></i>
					<span>Dashboard</span></a>
					</s:if>
					<s:else>
					<a href="getbuseslistbyId"><i class="fa fa-dashboard"></i>
					<span>Dashboard</span></a>
					
					</s:else>
					
					
					
					
					</li>
					
					
			<li id="track"><a href="getBusDetails"><i class="fa fa-map-marker"></i><span>Track</span></a></li>
			<li id="monitor"><a href="getbusesVideos"><i class="fa fa-video-camera"></i><span>Monitor</span></a></li>
			<li id="time"><a href="getBusesListbySchoolId"><i class="fa fa-calendar"></i><span>Time & Attendance</span></a></li>
			
			
		
			<li class="treeview" id="profiles"><a href="#"><i
					class="fa fa-fort-awesome"></i> <span>Profiles</span> <span
					class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span> </a>
				<ul class="treeview-menu">
					
					<li id="parents"><a href="getParentsList"><i class="fa fa-group"></i>Parents</a></li>
					<li id="classes"><a href="getClassesList"><i class="fa fa-share-alt"></i>Classes</a></li>
					<li id="students"><a href="getStudentsList"><i class="fa fa-address-book-o"></i>Students</a></li>
				</ul></li>
			<li class="treeview" id="logistics"><a href="getRoutesList"><i class="fa fa-road"></i>
					<span>Logistics</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span> </a>
				<ul class="treeview-menu">
					<li id="routes"><a href="getRoutesList"><i class="fa fa-list"></i>Routes</a></li>
					<li id="drivers"><a href="getDriversList"><i class="fa fa-users"></i>Drivers</a></li>
					<li id="buses"><a href="getBusesList"><i class="fa fa-bus"></i>Buses</a></li>
					
				</ul></li>
				
				
				
				<li class="treeview" id="stop"><a href="#"><i class="fa fa-road"></i>
					<span>Stop Points</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span> </a>
				<ul class="treeview-menu">
					<li id="pending"><a href="pendingApprove"><i class="fa fa-arrow-circle-o-right"></i>Pending Approvals</a></li>
					  <li id="stoppoint"><a href="stopPointApprove"><i class="fa fa-arrow-circle-o-right"></i>Stop Points Approved</a></li>
					   <li id="location"><a href="locationUnsubmitted"><i class="fa fa-arrow-circle-o-right"></i>Locations Unsubmitted</a></li>
				</ul></li>
				
				
		
			
			<li id="notifications"><a href="schoolNotificationList"><i class="fa fa-envelope-o"></i> <span>Notifications</span></a></li>
			<li id="settings"><a href="adminChangePwd"><i class="fa fa-gear"></i> <span>Settings</span></a></li>
			<li><a href="#"><i class="fa fa-exclamation-circle"></i> <span>Help</span></a></li>
			<li><a href="logout"><i class="fa fa-sign-out"></i> <span>Signout</span></a></li>
		</ul>
	</section>
</aside>