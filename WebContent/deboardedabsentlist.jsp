<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
 
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
 </head>
<body class="hold-transition skin-yellow sidebar-mini" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">

	<div class="wrapper">

		<%@ include file="header.jsp" %>
 		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>
 		<div class="content-wrapper">
			<section class="content-header">
				<h4>DeboardedAbsentList > <s:a action="getBusesListbySchoolId">Times and Attendence</s:a> ><s:a action="getonboardabsentlist">OnBoardedAbsentList</s:a></h4>
 				<ol class="breadcrumb">
 				</ol>
			</section>


	<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow">
							
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
										<th>StudentName</th>
										    <th>StudentClass</th>
											<th>RollNo</th>
											<th>BusRoute </th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="deboardedlist">
									<tr>
										<td><s:property value="name"></s:property></td>
							              <td><s:property value="studclass"/></td>
								         <td><s:property value="rollno"/></td>
										<td><s:property value="routename"/></td>
										
									</tr>
			
								</s:iterator>
									</tbody>
									<tfoot>
										
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>
	</div>
 	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
 	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>
 	<script>
      $(function () {
    	  $('#example1').DataTable();
       });
    </script>
   
</body>
</html>
