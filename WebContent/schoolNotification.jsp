 
<%@taglib uri="/struts-tags" prefix="s"%>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Akshara Information Technologies::ParentLook</title><link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons.min.css">
<link rel="stylesheet" href="css/AdminLTE.min.css">
<link rel="stylesheet" href="css/skins/_all-skins.min.css">

<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
	
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<!-- Site wrapper -->
	<div class="wrapper">

		<%@ include file="header.jsp" %>


		<aside class="main-sidebar">
			<%@include file="sidebar.jsp"%>
		</aside>

		<div class="content-wrapper">
			<section class="content-header">
				<h4>School Notifications</h4>
				  <s:form action="Export">
               <s:hidden name="content" id="content"></s:hidden>
				<s:submit value="Export" id="export"></s:submit>
				</s:form>
				<ol class="breadcrumb">
				
					<li>
					
					</li>
					 
				</ol>
			</section>
			<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box no-border no-shadow flat">
							 
							<div class="box-body">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
										 
											<th>Description</th>
											<th>School Id</th>
											<th>Type</th>
											<th>Read Status</th>
											<th>Time</th>
											<th>Category</th>
											

										</tr>
									</thead>
									<tbody>
										<s:iterator value="list">
											<tr>

											 
												<td><s:property value="description"/></td>
												<td><s:property value="schoolId"/></td>
												<td><s:property value="type"/></td>
												<td><s:property value="readStatus"/></td>
												<td><s:property value="time"/></td>
												<td><s:property value="category"/></td>
												
												
        

												</tr>
										</s:iterator>
									</tbody>
									 
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>





		 
	</div>

	<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	 <script src="js/app.min.js"></script>

	<script>
      $(function () {
    	  $('#example1').DataTable();
       
      });
    </script>
    
    
    <script type="text/javascript">
$(document).ready(function()
		{
	 
	 $("#export").click(function()
			  {
		 
		 var k = $(".content").html();
		 alert(k);
		 $("#content").attr("value",k);
		   });
		 
	 
}); 
</script>


    
</body>
</html>
